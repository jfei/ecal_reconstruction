cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)
project(ECAL_Reconstruction)

include_directories(
    ${CMAKE_SOURCE_DIR}/include/spdlog
    ${CMAKE_SOURCE_DIR}/include/spdlog/cfg
    ${CMAKE_SOURCE_DIR}/include/spdlog/details
    ${CMAKE_SOURCE_DIR}/include/spdlog/fmt
    ${CMAKE_SOURCE_DIR}/include/spdlog/sinks
    ${CMAKE_SOURCE_DIR}/include
    ${CMAKE_SOURCE_DIR}/EventInfo
    ${CMAKE_SOURCE_DIR}/Candidate
)

file(GLOB SRC_FILES "${CMAKE_SOURCE_DIR}/src/*.cc" "${CMAKE_SOURCE_DIR}/src/*.C" "${CMAKE_SOURCE_DIR}/Candidate/*.cc" "${CMAKE_SOURCE_DIR}/EventInfo/*.cc")

add_executable(app ${CMAKE_SOURCE_DIR}/Clustering.cc ${SRC_FILES})

find_package(ROOT REQUIRED COMPONENTS MathCore GenVector)
include(${ROOT_USE_FILE})

target_link_libraries(app ${ROOT_LIBRARIES} ROOT::MathCore ROOT::GenVector)