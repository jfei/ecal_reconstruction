#include "FCluster2D.hh"
#include "FLayer.hh"
#include "FCell.hh"
#include "FRegion.hh"
#include "FCluster3D.hh"
#include "Mixture_Calo.hh"
#include "TMath.h"
#include "FModule.hh"
#include "FCell3D.hh"
uint64 FCluster2D::CalculateGlobalId(float CellSize, int layer_id, float layer_thick, float ax, float ay, float az)
{
    return ((uint64)((int)round(CellSize))) << 56 | (uint64)((int)round(layer_thick)) << 32 | (uint64)((int)layer_id) << 24 | (uint64)((int)(ax + 180)) << 16 | (uint64)((int)(ay + 180)) << 8 | (uint64)((int)(az + 180)); //|Cell size(8)|LayerThickness(32)|
}

void FCluster2D::RecRawInfoByCells()
{
    RawGlobalPos.SetXYZ(0, 0, 0); // 清空坐标；
    LocalPos.SetXYZ(0, 0, 0);

    RawE = 0;
    E3 = 0;
    E5 = 0;
    E7 = 0;
    E9 = 0;
    float tmpE = 0;
    float E9Range4Clu2D;
    float E7Range4Clu2D;
    float E5Range4Clu2D;
    float E3Range4Clu2D;
    if (Seed2D != NULL)
    {
        E9Range4Clu2D = 4.2 * Seed2D->Dx();
        E7Range4Clu2D = 3.2 * Seed2D->Dx();
        E5Range4Clu2D = 2.2 * Seed2D->Dx();
        E3Range4Clu2D = 1.2 * Seed2D->Dx();

        RawTime = Seed2D->GetTime(event);
    }
    for (auto v_CellSeq : Cluster_Cell)
    {
        int CellType = v_CellSeq.first;
        auto v_Cell = v_CellSeq.second;
        for (auto Cell : v_Cell)
        {
            float CellE = Cell->GetEnergy(this);
            float CellX = Cell->X();
            float CellY = Cell->Y();
            float CellZ = Cell->Z();
            float CellLocalX = Cell->LocalX();
            float CellLocalY = Cell->LocalY();
            float CellLocalZ = Cell->LocalZ();
            if (Seed2D != NULL)
            {
                if (Cell != Seed2D && CellType == 0)
                {
                    if (fabs(CellX - Seed2D->X()) < E9Range4Clu2D && fabs(CellY - Seed2D->Y()) < E9Range4Clu2D)
                    {
                        E9 += CellE;
                        if (fabs(CellX - Seed2D->X()) < E7Range4Clu2D && fabs(CellY - Seed2D->Y()) < E7Range4Clu2D)
                        {
                            E7 += CellE;
                            if (fabs(CellX - Seed2D->X()) < E5Range4Clu2D && fabs(CellY - Seed2D->Y()) < E5Range4Clu2D)
                            {
                                E5 += CellE;
                                if (fabs(CellX - Seed2D->X()) < E3Range4Clu2D && fabs(CellY - Seed2D->Y()) < E3Range4Clu2D)
                                {
                                    E3 += CellE;
                                }
                            }
                        }
                    }
                }
            }

            if (CellType <= 2)
            {
                float Old_Clu2dX = RawX();
                float Old_Clu2dY = RawY();
                float Old_Clu2dZ = RawZ();
                float New_Clu2dX = Old_Clu2dX + CellX * CellE;
                float New_Clu2dY = Old_Clu2dY + CellY * CellE;
                float New_Clu2dZ = Old_Clu2dZ + CellZ * CellE;
                RawGlobalPos.SetXYZ(New_Clu2dX, New_Clu2dY, New_Clu2dZ);

                float Old_Clu2dLocalX = LocalX();
                float Old_Clu2dLocalY = LocalY();
                float Old_Clu2dLocalZ = LocalZ();
                float New_Clu2dLocalX = Old_Clu2dLocalX + CellLocalX * CellE;
                float New_Clu2dLocalY = Old_Clu2dLocalY + CellLocalY * CellE;
                float New_Clu2dLocalZ = Old_Clu2dLocalZ + CellLocalZ * CellE;
                LocalPos.SetXYZ(New_Clu2dLocalX, New_Clu2dLocalY, New_Clu2dLocalZ);
                tmpE += CellE;
            }
            // std::cout << "Cell " << Cell->GetID() << " E " << CellE << std::endl;
            RawE += CellE;
        }
    }
    float Old_Clu2dX = RawX();
    float Old_Clu2dY = RawY();
    float Old_Clu2dZ = RawZ();
    if (tmpE <= 0)
    {
        if (Seed2D != NULL)
            RawGlobalPos.SetXYZ(Seed2D->X(), Seed2D->Y(), Seed2D->Z());
        else
            RawGlobalPos.SetXYZ(f_Clu3D->Seed3D->X(), f_Clu3D->Seed3D->Y(), f_Clu3D->Seed3D->v_Cell.at(layer_id).at(0)->Z());
    }
    else
        RawGlobalPos.SetXYZ(Old_Clu2dX / tmpE, Old_Clu2dY / tmpE, Old_Clu2dZ / tmpE);
    // std::cout << "Clu2D Raw x : " << RawX() << " y " << RawY() << " z " << RawZ() << " e " << RawEnergy() << std::endl;
    float Old_Clu2dLocalX = LocalX();
    float Old_Clu2dLocalY = LocalY();
    float Old_Clu2dLocalZ = LocalZ();
    if (tmpE <= 0)
    {
        if (Seed2D != NULL)
            LocalPos.SetXYZ(Seed2D->LocalX(), Seed2D->LocalY(), Seed2D->LocalZ());
        else
            LocalPos.SetXYZ(f_Clu3D->Seed3D->LocalX(), f_Clu3D->Seed3D->LocalY(), f_Clu3D->Seed3D->v_Cell.at(layer_id).at(0)->LocalZ());
    }
    else
        LocalPos.SetXYZ(Old_Clu2dLocalX / tmpE, Old_Clu2dLocalY / tmpE, Old_Clu2dLocalZ / tmpE);
}

// float FCluster2D::CalculateTime()
// {
//     int layerType = f_layer->layerType;
//     if (Seed2D == NULL)
//     {
//         std::cout << "ERROR ! Cluster2D seed not initial yet " << std::endl;
//         time = -7;
//         return -7;
//     }
//     float seed2d_x = Seed2D->X();
//     float seed2d_y = Seed2D->Y();
//     if (layerType == 0)
//     {
//         time = Seed2D->GetTime(event);
//         return time;
//     }
//     else if (layerType == 1)
//     {
//         double SiEnergyAccum = 0;
//         double SiSeedTime = 0;
//         for (auto v_CellSeq : Cluster_Cell)
//         {
//             int CurCellType = v_CellSeq.first;
//             auto v_Cell = v_CellSeq.second;
//             for (auto CurCell : v_Cell)
//             {
//                 float tmp_x = CurCell->X();
//                 float tmp_y = CurCell->Y();

//                 // std::cout << "seed2d_x : " << seed2d_x << std::endl;

//                 float diffLength = sqrt(pow(tmp_x - seed2d_x, 2) + pow(tmp_y - seed2d_y, 2));
//                 // std::cout << "tmp_x : " << tmp_x << " seed2d_x : " << seed2d_x << std::endl;
//                 float CurrentCellEnergy = CurCell->GetEnergy(event);

//                 // std::cout << "CurrentCell->f_Cluster3D.count(CurrentEvent) : " << CurrentCell->f_Cluster3D.count(CurrentEvent) << std::endl;
//                 if (CurCell->GetClu3DWeight_V(event)->size() >= 1) // if this cell belong to more than 1 cluster, recaculate the energy
//                 {
//                     std::cout << "belong to more than 1 clusters" << std::endl;
//                     int CluSeq = 0;

//                     for (auto CellClu : *CurCell->GetMotherClu3D_V(event))
//                     {
//                         if (CellClu == f_Clu3D)
//                             break;
//                         CluSeq++;
//                     }

//                     CurrentCellEnergy *= CurCell->GetClu3DWeight_V(event)->at(CluSeq);
//                 }

//                 SiSeedTime += (CurCell->GetTime(event) - diffLength / 283) * CurrentCellEnergy;
//                 // std::cout << "CurrentCell->time.at(CurrentEvent) : " << CurrentCell->time.at(CurrentEvent) << std::endl;
//                 // std::cout << "(CurrentCell->time.at(CurrentEvent) - diffLength / 283) : " << (CurrentCell->time.at(CurrentEvent) - diffLength / 283) << std::endl;
//                 SiEnergyAccum += CurrentCellEnergy;
//             }
//         }
//         SiSeedTime /= SiEnergyAccum;
//         time = SiSeedTime;
//         return time;
//     }
//     else
//         return 0;
// }

int FCluster2D::IsMissingXCell()
{
    float Seed2DLocalX = Seed2D->LocalX();
    float Seed2DLocalY = Seed2D->LocalY();
    float Seed2D_Dx = Seed2D->Dx();
    float Seed2D_Dy = Seed2D->Dy();
    float ModuleDx = f_module->Dx();
    float ModuleDy = f_module->Dy();

    float ModuleX = f_module->X();
    float ModuleY = f_module->Y();
    float ModuleZ = f_module->Z();
    FRegion *ThisRegion = f_region;
    float Seed3DDx = ThisRegion->Cell3DSize;
    float Seed3DDy = ThisRegion->Cell3DSize;

    float ModuleXBinWidth = Mixture_Calo::get_instance()->module_map.GetXaxis()->GetBinWidth(1);
    float ModuleYBinWidth = Mixture_Calo::get_instance()->module_map.GetYaxis()->GetBinWidth(1);
    int NextXType_p = f_region->type;
    int NextXType_n = f_region->type;
    if (Seed2DLocalX > ModuleDx / 2 - Seed3DDx)
    {
        NextXType_p = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX + ModuleXBinWidth, ModuleY);
    }
    if (NextXType_p != f_region->type)
    {
        if (NextXType_p == -77)
            return 77;
        else
            return (NextXType_p);
    }
    if (Seed2DLocalX < -ModuleDx / 2 + Seed3DDx)
    {
        NextXType_n = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX - ModuleXBinWidth, ModuleY);
    }

    if (NextXType_n != f_region->type)
    {
        if (NextXType_n == -77)
            return (-77);
        else
            return (-1 * NextXType_n);
    }

    // float p_E = 0, m_E = 0;

    // for (auto CellSeq : Cluster_Cell)
    // {
    //     int CellType = CellSeq.first;
    //     auto v_Cell = CellSeq.second;
    //     for (auto Cell : v_Cell)
    //     {

    //             if (Cell->X() > Seed2D->X())
    //             {
    //                 p_E += Cell->GetEnergy(event);
    //             }
    //             else if (Cell->X() < Seed2D->X())
    //             {
    //                 m_E += Cell->GetEnergy(event);
    //             }
    //     }
    // }
    // if (p_E / m_E >= 10.)
    // {
    //     return 111;
    // }
    // if (m_E / p_E >= 10.)
    // {
    //     return -111;
    // }

    return (0);
}

int FCluster2D::IsMissingYCell() // 首先是否为最边界cell 边界外是否是另一个类型的region
{
    float Seed2DLocalX = Seed2D->LocalX();
    float Seed2DLocalY = Seed2D->LocalY();
    float Seed2D_Dx = Seed2D->Dx();
    float Seed2D_Dy = Seed2D->Dy();
    float ModuleDx = f_module->Dx();
    float ModuleDy = f_module->Dy();

    float ModuleX = f_module->X();
    float ModuleY = f_module->Y();
    float ModuleZ = f_module->Z();
    FRegion *ThisRegion = f_region;
    float Seed3DDx = ThisRegion->Cell3DSize;
    float Seed3DDy = ThisRegion->Cell3DSize;
    float ModuleXBinWidth = Mixture_Calo::get_instance()->module_map.GetXaxis()->GetBinWidth(1);
    float ModuleYBinWidth = Mixture_Calo::get_instance()->module_map.GetYaxis()->GetBinWidth(1);
    int NextYType_p = f_region->type;
    int NextYType_n = f_region->type;
    // std::cout << "Seed2DLocalY " << Seed2DLocalY << " -ModuleDy / 2 " << -ModuleDy / 2 << " Seed2D_Dy " << Seed2D_Dy << std::endl;
    if (Seed2DLocalY > ModuleDy / 2 - Seed3DDy)
    {
        NextYType_p = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX, ModuleY + ModuleYBinWidth);
    }
    if (NextYType_p != f_region->type)
    {
        if (NextYType_p == -77)
            return (77);
        else
            return (NextYType_p);
    }
    if (Seed2DLocalY < -ModuleDy / 2 + Seed3DDy)
    {
        NextYType_n = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX, ModuleY - ModuleYBinWidth);
        // std::cout << "n module " << Mixture_Calo::get_instance()->GetModuleIDByPos(ModuleX, ModuleY - ModuleYBinWidth) << std::endl;
    }

    // std::cout << "NextYType_n " << NextYType_n << std::endl;

    if (NextYType_n != f_region->type)
    {
        if (NextYType_n == -77)
            return (-77);
        else
            return (-1 * NextYType_n);
    }

    // float p_E = 0, m_E = 0;

    // for (auto CellSeq : Cluster_Cell)
    // {
    //     int CellType = CellSeq.first;
    //     auto v_Cell = CellSeq.second;
    //     for (auto Cell : v_Cell)
    //     {

    //             if (Cell->Y() > Seed2D->Y())
    //             {
    //                 p_E += Cell->GetEnergy(event);
    //             }
    //             else if (Cell->Y() < Seed2D->Y())
    //             {
    //                 m_E += Cell->GetEnergy(event);
    //             }
    //     }
    // }
    // if (p_E / m_E >= 10.)
    // {
    //     return 111;
    // }
    // if (m_E / p_E >= 10.)
    // {
    //     return -111;
    // }

    return (0);
}

void FCluster2D::SCorrection()
{
    S0Correction();
    if (f_region->DoS1Cor)
        S1Correction();
}

bool FCluster2D::S0Correction()
{
    
    if (Seed2D == NULL)
    {
        spdlog::get(ERROR_NAME)->error("ERROR ! Cluster2D seed not initial yet");

        SetS0CorX(0);
        SetS0CorY(0);
        return 0;
    }
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "UnCorrect X : {}", RawX());
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "UnCorrect Y : {}", RawY());
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "XType : {}", XType);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "YType : {}", YType);

    // std::cout << "test" << std::endl;
    float Clu2DLayerX = RawX();
    float Clu2DLayerY = RawY();
    // float Clu2DLayerZ = RawZ();
    float cellXsize = Seed2D->Dx();
    float cellYsize = Seed2D->Dy();
    if (abs(Clu2DLayerX - Seed2D->X()) < 1e-3)
    {
        SetS0CorX(Seed2D->X());
        SetX(Seed2D->X());
    }
    else
    {
        float InputX = (Clu2DLayerX - Seed2D->X()) / cellXsize;
        TF1 *XSCorFun;
        if (XType == 0)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluType0SCorX;
        }
        else if (XType == 1)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeP1SCorX;
        }
        else if (XType == -1)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeM1SCorX;
        }
        else if (XType == 2)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeP2SCorX;
        }
        else if (XType == -2)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeM2SCorX;
        }
        else if (XType == 3)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeP3SCorX;
        }
        else if (XType == -3)
        {
            XSCorFun = f_region->Layers.at(layer_id)->CluTypeM3SCorX;
        }
        else
        {
            spdlog::get(LOG_NAME)->error("Error XType {}", XType);
            exit(1);
        }
        float OutputX = XSCorFun->Eval(InputX);
        SetS0CorX(OutputX * cellXsize + Seed2D->X());
        SetX(OutputX * cellXsize + Seed2D->X());
    }

    if (abs(Clu2DLayerY - Seed2D->Y()) < 1e-3)
    {
        SetS0CorY(Seed2D->Y());
        SetY(Seed2D->Y());
    }
    else
    {
        float InputY = (Clu2DLayerY - Seed2D->Y()) / cellYsize;

        TF1 *YSCorFun;

        if (YType == 0)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluType0SCorY;
        }
        else if (YType == 1)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeP1SCorY;
        }
        else if (YType == 2)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeP2SCorY;
        }
        else if (YType == 3)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeP3SCorY;
        }
        else if (YType == -1)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeM1SCorY;
        }
        else if (YType == -2)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeM2SCorY;
        }
        else if (YType == -3)
        {
            YSCorFun = f_region->Layers.at(layer_id)->CluTypeM3SCorY;
        }
        else
        {
            spdlog::get(LOG_NAME)->error("Error YType {}", YType);
            exit(1);
        }

        float OutputY = YSCorFun->Eval(InputY);

        SetS0CorY(OutputY * cellYsize + Seed2D->Y());
        SetY(OutputY * cellYsize + Seed2D->Y());
    }
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct X : {}", X());
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct Y : {}", Y());
    AfterSCor = 1;
    return 1;
}

void FCluster2D::S1Correction()
{
    if (Seed2D == NULL)
    {
        spdlog::get(ERROR_NAME)->error("ERROR ! Cluster2D seed not initial yet");

        SetX(0);
        SetY(0);
    }
    float Clu2DLayerX = S0CorX();
    float Clu2DLayerY = S0CorY();
    float cellXsize = Seed2D->Dx();
    float cellYsize = Seed2D->Dy();
    float InputX = (Clu2DLayerX - Seed2D->X()) / cellXsize;
    float InputY = (Clu2DLayerY - Seed2D->Y()) / cellYsize;
    TF1 *S1xCorFun;
    if (Clu2DLayerX > 0)
    {
        S1xCorFun = f_region->Layers.at(layer_id)->S1xCorPFun;
    }
    else
    {
        S1xCorFun = f_region->Layers.at(layer_id)->S1xCorNFun;
    }
    TF1 *S1yCorFun;
    if (Clu2DLayerY > 0)
    {
        S1yCorFun = f_region->Layers.at(layer_id)->S1yCorPFun;
    }
    else
    {
        S1yCorFun = f_region->Layers.at(layer_id)->S1yCorNFun;
    }
    float OutputX = S1xCorFun->Eval(InputX);
    float OutputY = S1yCorFun->Eval(InputY);
    SetX(Clu2DLayerX - OutputX * cellXsize);
    SetY(Clu2DLayerY - OutputY * cellYsize);
    AfterSCor = 2;
}

float FCluster2D::LCorrection(int nPV)
{

    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct Z Pos at Clu2D (layer) : {}", layer_id);
    if (Seed2D == NULL)
    {
        spdlog::get(ERROR_NAME)->warn("WARNNING ! Cluster2D seed not initial yet");
        SetZ(0);
        return Z();
    }
    TF1 *LCorrFun = f_region->Layers.at(layer_id)->LCorrFun;
    // std::cout << "LCorrFun " << LCorrFun << std::endl;
    // std::cout << "f_Clu3D->energy " << f_Clu3D->energy << std::endl;
    float e = f_Clu3D->energy / 1000;
    // std::cout << "e " << e << std::endl;
    float ShowerDepth;
    ShowerDepth = LCorrFun->Eval(e);
    // std::cout << "ShowerDepth " << ShowerDepth << std::endl;
    float FrontFace = f_region->FrontFace;
    float PositionAtVertexX;
    float PositionAtVertexY;
    float PositionAtVertexZ;
    if (nPV == -1)
    {
        PositionAtVertexX = 0;
        PositionAtVertexY = 0;
        PositionAtVertexZ = -12836;
    }
    else
    {
        PositionAtVertexZ = Mixture_Calo::get_instance()->GetPrimaryZ(event, nPV);
        PositionAtVertexX = Mixture_Calo::get_instance()->GetPrimaryX(event, nPV);
        PositionAtVertexY = Mixture_Calo::get_instance()->GetPrimaryY(event, nPV);
    }
    // std::cout << "PositionAtVertexZ " << PositionAtVertexZ << " PositionAtVertexX " << PositionAtVertexX << " PositionAtVertexY " << PositionAtVertexY << std::endl;
    float ThetaX = atan((X() - PositionAtVertexX) / (RawZ() - PositionAtVertexZ));
    float ThetaY = atan((Y() - PositionAtVertexY) / (RawZ() - PositionAtVertexZ));
    float NewAx = fabs(ThetaX) + fabs(Ax()) / 180. * TMath::Pi();
    float NewAy = fabs(ThetaY) + fabs(Ay()) / 180. * TMath::Pi();
    // std::cout << "NewAx " << NewAx << " ThetaX " << ThetaX << " Ax() " << Ax() << std::endl;
    float TmpDeltaZ = ShowerDepth / sqrt(1 + pow(tan(NewAx), 2) + pow(tan(NewAy), 2));
    // std::cout << "TmpDeltaZ " << TmpDeltaZ << std::endl;
    // std::cout << "FrontFace " << FrontFace << std::endl;
    float TmpLocalZ = TmpDeltaZ + FrontFace;
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "LocalX : {} , LocalY : {}", LocalX(), LocalY());
    FPoint GlobalPoint = f_region->Local2Global(LocalX(), LocalY(), TmpLocalZ);
    // std::cout << "GlobalPoint.Z() " << GlobalPoint.Z() << std::endl;
    // std::cout << "f_module->Z() " << f_module->Z() << std::endl;
    SetZ(GlobalPoint.Z() + f_module->Z());
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct Z : {}", Z());
    AfterLCor = 1;
    return Z();
}

float FCluster2D::TCorrection()
{
    spdlog::get(LOG_NAME)->info("Correct time at Clu2D (layer) : {}", layer_id);

    if (Seed2D == NULL)
    {
        spdlog::get(ERROR_NAME)->warn("WARNNING ! Cluster2D seed not initial yet");

        time = -7;
        return time;
    }
    FModule *CurModule = f_module;
    FRegion *CurRegion = f_region;
    float EGeV = f_Clu3D->energy / 1000;
    // std::cout << "test1" << std::endl;
    TF1 *TShiftFun = f_region->Layers.at(layer_id)->TShiftFun;
    // std::cout << "test2" << std::endl;
    float tmpTShift = TShiftFun->Eval(EGeV);
    // std::cout << "CurRegion->Layers.at(layer_id)->OnlySeedT " << CurRegion->Layers.at(layer_id)->OnlySeedT << std::endl;
    float LayerAbsPos = f_module->Z() + CurRegion->Layers.at(layer_id)->layerPos;
    if (CurRegion->Layers.at(layer_id)->OnlySeedT)
    {
        // std::cout << "test" << std::endl;
        time = Seed2D->GetTime(event) - (Seed2D->Z() - LayerAbsPos) / 299.79 - tmpTShift;
    }
    else
    {
        float Par0 = 1;
        float Par1 = 1;
        float seed2d_x = Seed2D->X();
        float seed2d_y = Seed2D->Y();

        double EnergyAccum = 0;
        double SeedTime = 0;
        for (auto v_CellSeq : Cluster_Cell)
        {
            int CurCellType = v_CellSeq.first;
            auto v_Cell = v_CellSeq.second;
            for (auto CurCell : v_Cell)
            {
                float tmp_x = CurCell->X();
                float tmp_y = CurCell->Y();

                // std::cout << "seed2d_x : " << seed2d_x << std::endl;

                float diffLength = sqrt(pow(tmp_x - seed2d_x, 2) + pow(tmp_y - seed2d_y, 2));
                // std::cout << "tmp_x : " << tmp_x << " seed2d_x : " << seed2d_x << std::endl;
                float CurrentCellEnergy = CurCell->GetEnergy(this);

                SeedTime += (CurCell->GetTime(event) - (Par0 + Par1 * log(diffLength + 1))) * CurrentCellEnergy;
                // std::cout << "CurrentCell->time.at(CurrentEvent) : " << CurrentCell->time.at(CurrentEvent) << std::endl;
                // std::cout << "(CurrentCell->time.at(CurrentEvent) - diffLength / 283) : " << (CurrentCell->time.at(CurrentEvent) - diffLength / 283) << std::endl;
                EnergyAccum += CurrentCellEnergy;
            }
        }
        SeedTime /= EnergyAccum;
        time = SeedTime - (Seed2D->Z() - LayerAbsPos) / 299.79 - tmpTShift;
    }
    AfterTCor = 1;
    return time;
}

float FCluster2D::ECorrection()
{
    spdlog::get(LOG_NAME)->info("\tClu2D RawE : {}", RawE);
    TF1 *Clu2DECaliFun = f_region->Layers.at(layer_id)->ECaliFun;

    float EGeV = RawE / 1000;
    float DeltaE_E = Clu2DECaliFun->Eval(EGeV);
    energy = RawE / (1 + DeltaE_E);
    spdlog::get(LOG_NAME)->info("\tClu2D Correct E : {}", energy);
    AfterECor = 1;
    return energy;
}

void FCluster2D::ReAllocateCell()
{
    f_region->Layers.at(layer_id)->ReAlloCell_CallBack(this);
}

void FCluster2D::AddCell2D(FCell *Cell2D)
{
    int Cell2DType = 0;
    auto Cell2DRegion = Cell2D->MotherModule()->f_region;
    if (f_module->GetID() != Cell2D->MotherModule()->GetID())
    {
        if (Cell2DRegion->type == f_region->type)
        {
            if (Cell2DRegion->GlobalId == f_region->GlobalId)
            {
                Cell2DType = 1;
            }
            else
            {
                Cell2DType = 2;
            }
        }
        else
        {
            Cell2DType = 3;
        }
    }
    if (std::find(Cluster_Cell[Cell2DType].begin(), Cluster_Cell[Cell2DType].end(), Cell2D) == Cluster_Cell[Cell2DType].end())
    {
        Cluster_Cell[Cell2DType].emplace_back(Cell2D);
        Cell2D->AddMotherClu2D(this);
        CellNumber++;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Already add this Cell2D in this Clu3D");
    }
}

void FCluster2D::AddCell2D(FCell *Cell2D, int Cell2DType)
{
    if (std::find(Cluster_Cell[Cell2DType].begin(), Cluster_Cell[Cell2DType].end(), Cell2D) == Cluster_Cell[Cell2DType].end())
    {
        Cluster_Cell[Cell2DType].emplace_back(Cell2D);
        Cell2D->AddMotherClu2D(this);
        CellNumber++;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Already add this Cell2D in this Clu3D");
    }
}

void FCluster2D::RemoveCell2D(FCell *Cell2D, int Cell2DType)
{
    auto FindPoint = std::find(Cluster_Cell[Cell2DType].begin(), Cluster_Cell[Cell2DType].end(), Cell2D);
    if (FindPoint != Cluster_Cell[Cell2DType].end())
    {
        Cluster_Cell[Cell2DType].erase(FindPoint);
        Cell2D->RemoveMotherClu2D(this);
        CellNumber--;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Not have this Cell2D (type {}) in this Clu2D", Cell2DType);
    }
}

void FCluster2D::RemoveCell2D(FCell *Cell2D)
{
    int Cell2DType = 0;
    auto Cell2DRegion = Cell2D->MotherModule()->f_region;
    if (f_module->GetID() != Cell2D->MotherModule()->GetID())
    {
        if (Cell2DRegion->type == f_region->type)
        {
            if (Cell2DRegion->GlobalId == f_region->GlobalId)
            {
                Cell2DType = 1;
            }
            else
            {
                Cell2DType = 2;
            }
        }
        else
        {
            Cell2DType = 3;
        }
    }
    auto FindPoint = std::find(Cluster_Cell[Cell2DType].begin(), Cluster_Cell[Cell2DType].end(), Cell2D);
    if (FindPoint != Cluster_Cell[Cell2DType].end())
    {
        Cluster_Cell[Cell2DType].erase(FindPoint);
        Cell2D->RemoveMotherClu2D(this);
        CellNumber--;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Not have this Cell2D (type {}) in this Clu2D", Cell2DType);
    }
}

FCluster2D::FCluster2D(FCell *i_Seed2D, int i_event)
{
    LocalPos.SetXYZ(0, 0, 0);
    GlobalPos.SetXYZ(0, 0, 0);
    RawGlobalPos.SetXYZ(0, 0, 0);
    S0CorGlobalPos.SetXYZ(0, 0, 0);
    time = 0;
    CellNumber = 1;
    Seed2D = i_Seed2D;
    f_module = i_Seed2D->MotherModule();
    f_region = i_Seed2D->MotherModule()->f_region;
    ax = f_region->ax;
    ay = f_region->ay;
    az = f_region->az;
    f_layer = i_Seed2D->MotherLayer();
    layer_id = i_Seed2D->LayerId();
    event = i_event;

    Cluster_Cell[0].emplace_back(i_Seed2D);
    i_Seed2D->AddMotherClu2D(this);
}

void FCluster2D::InitCluType()
{
    int XPNeiWithSameType = 0;
    int XNNeiWithSameType = 0;
    int YPNeiWithSameType = 0;
    int YNNeiWithSameType = 0;
    int CellCount = 0;
    int XOutOfModule = 0, XOutOfRegionId = 0, YOutOfModule = 0, YOutOfRegionId = 0, XOutOfRegionType = 0, YOutOfRegionType = 0;
    float Mx = f_module->X();
    float My = f_module->Y();
    float Mdx = f_module->Dx();
    float Mdy = f_module->Dy();
    float Seed2DLocalX = Seed2D->LocalX();
    float Seed2DLocalY = Seed2D->LocalY();
    for (auto v_Cell3DSeq : Cluster_Cell)
    {
        int Cell3DType = v_Cell3DSeq.first;
        auto v_Cell3D = v_Cell3DSeq.second;
        for (auto Cell3D : v_Cell3D)
        {
            auto Cell3DMod = Cell3D->MotherModule();
            float Cell3DModX = Cell3DMod->X();
            float Cell3DModY = Cell3DMod->Y();

            if (Cell3DType != 3)
            {
                if (Cell3D->LocalX() + Cell3DModX - Mx - Seed2DLocalX > 1e-3)
                {
                    XPNeiWithSameType += 1;
                }
                else if (Cell3D->LocalX() + Cell3DModX - Mx - Seed2DLocalX < -1e-3)
                {
                    XNNeiWithSameType += 1;
                }

                if (Cell3D->LocalY() + Cell3DModY - My - Seed2DLocalY > 1e-3)
                {
                    YPNeiWithSameType += 1;
                }
                else if (Cell3D->LocalY() + Cell3DModY - My - Seed2DLocalY < -1e-3)
                {
                    YNNeiWithSameType += 1;
                }
            }

            if (Cell3DType == 1)
            {
                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfModule = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfModule = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfModule = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfModule = -1;
                }
            }
            else if (Cell3DType == 2)
            {

                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfRegionId = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfRegionId = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfRegionId = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfRegionId = -1;
                }
            }
            else if (Cell3DType == 3)
            {
                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfRegionType = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfRegionType = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfRegionType = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfRegionType = -1;
                }
            }
            CellCount++;
        }
    }

    Type = 0;
    XType = 0;
    YType = 0;
    // //std::cout << "XOutOfRegionId " << XOutOfRegionId << std::endl;
    if (XOutOfModule != 0 || YOutOfModule != 0)
    {
        Type = 1;

        if (XOutOfModule == 1)
        {
            XType = 1;
        }
        else if (XOutOfModule == -1)
        {
            XType = -1;
        }
        else
            XType = 0;

        if (YOutOfModule == 1)
        {
            YType = 1;
        }
        else if (YOutOfModule == -1)
        {
            YType = -1;
        }
        else
            YType = 0;
    }
    if (XOutOfRegionId != 0 || YOutOfRegionId != 0)
    {
        Type = 2;

        if (XOutOfRegionId == 1)
        {
            XType = 2;
        }
        else if (XOutOfRegionId == -1)
        {
            XType = -2;
        }

        if (YOutOfRegionId == 1)
        {
            YType = 2;
        }
        else if (YOutOfRegionId == -1)
        {
            YType = -2;
        }
    }

    if (XOutOfRegionType != 0 || YOutOfRegionType != 0 || CellCount < 9)
    {
        Type = 3;
        if (XOutOfRegionType || CellCount < 9)
        {
            if (XPNeiWithSameType != XNNeiWithSameType)
            {
                if (XPNeiWithSameType <= 1)
                {
                    XType = 3;
                }
                else if (XNNeiWithSameType <= 1)
                {
                    XType = -3;
                }
            }
        }
        if (YOutOfRegionType || CellCount < 9)
        {
            if (YPNeiWithSameType != YNNeiWithSameType)
            {
                if (YPNeiWithSameType <= 1)
                {
                    YType = 3;
                }
                else if (YNNeiWithSameType <= 1)
                {
                    YType = -3;
                }
            }
        }
    }
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu2D type {}", Type);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu2D x type {}", XType);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu2D y type {}", YType);
}

void FCluster2D::ClearCell2D()
{
    for (auto v_CellSeq : Cluster_Cell)
    {
        int CellType = v_CellSeq.first;
        auto v_Cell = v_CellSeq.second;
        for (auto Cell : v_Cell)
        {
            Cell->RemoveMotherClu2D(this);
            // Cell->RemoveMotherClu3D(f_Clu3D);
        }
    }
    CellNumber = 0;
    Cluster_Cell.clear(); // 清空当前存储的cell
}

float FCluster2D::GetTRes()
{
    return f_region->Layers.at(layer_id)->TResFun->Eval(Energy() / 1000.);
}

float FCluster2D::GetSeedE()
{
    return Seed2D->GetEnergy(this);
}

void FCluster2D::PrintCellInfo()
{
    for (auto CellSeq : Cluster_Cell)
    {
        int CellType = CellSeq.first;
        auto v_Cell = CellSeq.second;
        spdlog::get(LOG_NAME)->info("Has {} Cells with Cell Type {}", v_Cell.size(), CellType);
        for (auto Cell : v_Cell)
        {
            spdlog::get(LOG_NAME)->info("\t X {}, Y {}, Z {}, E {}, T {}", Cell->X(), Cell->Y(), Cell->Z(), Cell->GetEnergy(this), Cell->GetTime(event));
        }
    }
}