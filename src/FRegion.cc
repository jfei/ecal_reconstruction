#include "FRegion.hh"
#include "iostream"
#include "FCalorimeter.hh"
#include "FCell.hh"
#include "FCluster3D.hh"
#include "math.h"
#include "FModule.hh"
#include "RecAlg.h"
#include "FPoint.hh"
#include "FLayer.hh"
void FRegion::InitEventInfo()
{
    
    // std::cout << "max_Eventseq : " << f_calo->max_Eventseq << std::endl;
    for (int loop = 0; loop < f_calo->max_Eventseq; loop++)
    {
        // std::cout << "test" << std::endl;
        RegionEventInfo tmpEventInfo;
        // std::cout << "TotLayerNum" << TotLayerNum << std::endl;
        tmpEventInfo.InitTotLayerNum(TotLayerNum);
        // std::cout << "Inital Region Event Infomation1" << std::endl;
        tmpEventInfo.ClearEnDep();
        // std::cout << "Inital Region Event Infomation2" << std::endl;
        tmpEventInfo.ClearSig();
        // std::cout << "Inital Region Event Infomation3" << std::endl;
        tmpEventInfo.ClearShowerZ();
        // std::cout << "Inital Region Event Infomation4" << std::endl;
        tmpEventInfo.SetEventID(loop);
        EventInfo.emplace_back(tmpEventInfo);
        // std::cout << "EventInfo " << EventInfo.size() << std::endl;
    }
}

int FRegion::GetCellLocalIdByRelaPos(float x_pos, float y_pos, float z_pos)
{
    int layer_id = Get_layerid(z_pos);

    if (layer_id == -1) // if current region doesn't have this layer
    {
        // std::cout << "no this cell" << std::endl;
        return -1;
    }
    auto TmpCellMap = Layers[layer_id]->cell_map;
    float cellSize = Layers[layer_id]->CellSize;
    int tmpId = Layers[layer_id]->cell_map.GetBinContent(TmpCellMap.FindBin(x_pos, y_pos));

    // if (tmpId == 0)
    // {
    //     float tmpX = TmpCellMap.GetXaxis()->GetBinLowEdge(1);
    //     float tmpY = TmpCellMap.GetYaxis()->GetBinLowEdge(1);
    //     if (abs(x_pos - tmpX) + abs(y_pos - tmpY) > 2 * cellSize)
    //         return -1;
    //     else
    //         return 0;
    // }
    // else
    return tmpId;
}

int FRegion::GetCellLocalIdByRelaPos(double x_pos, double y_pos, double z_pos)
{
    int layer_id = Get_layerid(z_pos);

    if (layer_id == -1) // if current region doesn't have this layer
    {
        // std::cout << "no this cell" << std::endl;
        return -1;
    }
    auto TmpCellMap = Layers[layer_id]->cell_map;
    float cellSize = Layers[layer_id]->CellSize;
    int tmpId = Layers[layer_id]->cell_map.GetBinContent(TmpCellMap.FindBin(x_pos, y_pos));

    // if (tmpId == 0)
    // {
    //     float tmpX = TmpCellMap.GetXaxis()->GetBinLowEdge(1);
    //     float tmpY = TmpCellMap.GetYaxis()->GetBinLowEdge(1);
    //     if (abs(x_pos - tmpX) + abs(y_pos - tmpY) > 2 * cellSize)
    //         return -1;
    //     else
    //         return 0;
    // }
    // else
    return tmpId;
}

int FRegion::GetCellLocalIdByRelaPos(float x_pos, float y_pos, int layer_id)
{

    if (layer_id < 0 || layer_id >= Layers.size()) // if current region doesn't have this layer
    {
        // std::cout << "no this cell" << std::endl;
        return -1;
    }
    auto TmpCellMap = Layers[layer_id]->cell_map;
    float cellSize = Layers[layer_id]->CellSize;
    int tmpId = Layers[layer_id]->cell_map.GetBinContent(TmpCellMap.FindBin(x_pos, y_pos));

    // if (tmpId == 0)
    // {
    //     float tmpX = TmpCellMap.GetXaxis()->GetBinLowEdge(1);
    //     float tmpY = TmpCellMap.GetYaxis()->GetBinLowEdge(1);
    //     if (abs(x_pos - tmpX) + abs(y_pos - tmpY) > 2 * cellSize)
    //         return -1;
    //     else
    //         return 0;
    // }
    // else
    return tmpId;
}

int FRegion::GetCell3DLocalIdByRelaPos(float x_pos, float y_pos)
{
    return cell3D_map.GetBinContent(cell3D_map.FindBin(x_pos, y_pos));
}

FPoint FRegion::Global2Local(FPoint GlobalPos)
{
    return FPoint((Ro.Inverse() * GlobalPos));
}

FPoint FRegion::Local2Global(FPoint LocalPos)
{
    return FPoint((Ro * LocalPos));
}

FPoint FRegion::Global2Local(float GlobalX, float GlobalY, float GlobalZ)
{
    auto GlobalPos = FPoint(GlobalX, GlobalY, GlobalZ);
    return Global2Local(GlobalPos);
}

FPoint FRegion::Local2Global(float LocalX, float LocalY, float LocalZ)
{
    auto LocalPos = FPoint(LocalX, LocalY, LocalZ);
    return Local2Global(LocalPos);
}

void FRegion::CalculateCellGlobalPos()
{

    std::vector<std::vector<float>> tmpGlobalX(TotLayerNum);
    std::vector<std::vector<float>> tmpGlobalY(TotLayerNum);
    std::vector<std::vector<float>> tmpGlobalZ(TotLayerNum);
    for (int layer_id = 0; layer_id < TotLayerNum; layer_id++)
    {

        TH2I tmp_cell_map = Layers.at(layer_id)->cell_map;
        int tmp_xbin = tmp_cell_map.GetNbinsX();
        int tmp_ybin = tmp_cell_map.GetNbinsY();

        for (int x_loop = 1; x_loop <= tmp_xbin; x_loop++) // loop cell map
            for (int y_loop = 1; y_loop <= tmp_ybin; y_loop++)
            {
                // 以module位置为原点 向module旋转方向旋转相同角度
                float tmp_cell_x = tmp_cell_map.GetXaxis()->GetBinCenter(x_loop);
                float tmp_cell_y = tmp_cell_map.GetYaxis()->GetBinCenter(y_loop);
                float tmp_cell_z = Layers[layer_id]->layerPos;
                FPoint tmpPos = Local2Global(tmp_cell_x, tmp_cell_y, tmp_cell_z);
                tmpGlobalX.at(layer_id).emplace_back(tmpPos.X());
                tmpGlobalY.at(layer_id).emplace_back(tmpPos.Y());
                tmpGlobalZ.at(layer_id).emplace_back(tmpPos.Z());
            }
    }
    for (auto ModuleSeq : module_container)
    {
        float ModuleX = ModuleSeq->X();
        float ModuleY = ModuleSeq->Y();
        float ModuleZ = ModuleSeq->Z();
        for (auto layer : ModuleSeq->v_layer)
        {
            int CellCount = 0;
            for (auto cell : layer->v_cell)
            {

                cell->SetGlobalPosition(tmpGlobalX.at(layer->id).at(CellCount) + ModuleX, tmpGlobalY.at(layer->id).at(CellCount) + ModuleY, tmpGlobalZ.at(layer->id).at(CellCount) + ModuleZ);
                CellCount++;
            }
        }
    }
}

uint64 FRegion::CalculateFullId(int type, float ax, float ay, float az, int Nx, int Ny)
{

    uint64 result = 0;

    result |= static_cast<uint64>(type & 0b1111) << 0;                 // 保留4位
    result |= static_cast<uint64>((int)(ax + 180) & 0b11111111) << 4;  // 保留8位
    result |= static_cast<uint64>((int)(ay + 180) & 0b11111111) << 12; // 保留8位
    result |= static_cast<uint64>((int)(az + 180) & 0b11111111) << 20; // 保留8位
    result |= static_cast<uint64>(Nx & 0b1111111111) << 28;            // 保留10位
    result |= static_cast<uint64>(Ny & 0b1111111111) << 38;            // 保留10位
    result |= static_cast<uint64>(FiberNumX & 0b11111111) << 48;       // 保留8位
    result |= static_cast<uint64>(FiberNumY & 0b11111111) << 56;       // 保留8位

    return result;
}

uint64 FRegion::CalculateTypeId(int type, float CellSize, float ax, float ay, float az)
{

    uint64 result = 0;

    result |= static_cast<uint64>(type & 0b1111) << 0;                           // 保留4位
    result |= static_cast<uint64>((int)(ax + 180) & 0b11111111) << 4;            // 保留8位
    result |= static_cast<uint64>((int)(ay + 180) & 0b11111111) << 12;           // 保留8位
    result |= static_cast<uint64>((int)(az + 180) & 0b11111111) << 20;           // 保留8位
    result |= static_cast<uint64>((int)CellSize & 0b11111111111111111111) << 28; // 保留10位
    result |= static_cast<uint64>(FiberNumX & 0b11111111) << 48;                 // 保留8位
    result |= static_cast<uint64>(FiberNumY & 0b11111111) << 56;                 // 保留8位

    return result;
}