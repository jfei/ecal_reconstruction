#include "FDataType.hh"
#include "Parameters.hh"

#include "ConfigFile.hh"

#include <algorithm>
#include <string>
#include <sstream>

Parameters *Parameters::fInstance = NULL;

Parameters::Parameters()
{
  if (fInstance)
  {
    return;
  }
  this->fInstance = this;
}

Parameters::~Parameters() {}

void Parameters::ReadMainConfig(std::string fileName)
{
  // this routines triggers the read of the main config file
  ConfigFile config(fileName.c_str());
  config.showValues();
  ecal_position = 0;

  /////////////////////
  // Input
  ///////////////////////
  InputPath = config.read<std::string>("InputPath", "");

  GeoFilesType = config.read<int>("GeoFilesType", 0);    // 1 : SimFile 2: RecGeoFile
  TruthFileType = config.read<int>("TruthFileType", 0);  // 1 : SimFile (both primary and hit info) 2 : Flux file (only primary) 3 :Rec File
  DataFileType = config.read<int>("DataFileType", 0);    // 1: TrigFileName 2 : RecFile
  InitTruthInfo = config.read<bool>("InitTruthInfo", 0); //
  tagCharge = config.read<bool>("tagCharge", 0);         //

  SimFileName = config.read<std::string>("SimFileName", "");
  TrigFileName = config.read<std::string>("TrigFileName", "");
  FluxFileName = config.read<std::string>("FluxFileName", "");
  RecFileName = config.read<std::string>("RecFileName", "");
  RecGeoFileName = config.read<std::string>("RecGeoFileName", "");
  ChargeSampleFilePrefix = config.read<std::string>("ChargeSampleFilePrefix", "");
  ////////////////////////////////

  OutputPath = config.read<std::string>("OutputPath", "");
  OutputPrefix = config.read<std::string>("OutputPrefix", "");
  Module_Id_Map = config.read<std::string>("Module_Id_Map", "");
  config.readIntoVect(ecal_type, "ecal_type");
  config.readIntoVect(module_config_list, "modules_config");
  config.readIntoVect(CalibrationFileName, "CalibrationFileName");
  ProgramType = config.read<int>("ProgramType", 0);
  CluFlow = config.read<int>("CluFlow", 2);
  RatioECor = config.read<bool>("RatioECor", 0);

  PGun = config.read<bool>("PGun", 0);
  SaveAll = config.read<bool>("SaveAll", 0);
  SaveCellInfo = config.read<bool>("SaveCellInfo", 0);
  SaveRawInfo = config.read<bool>("SaveRawInfo", 0);
  SaveCluster2D = config.read<bool>("SaveCluster2D", 0);
  SaveCluster3D = config.read<bool>("SaveCluster3D", 0);
  SaveGeometry = config.read<bool>("SaveGeometry", 0);
  SavePrimaryInfo = config.read<bool>("SavePrimaryInfo", 0);
  SaveMore = config.read<int>("SaveMore", 0);
  SaveHitMap = config.read<bool>("SaveHitMap", 0);

  ConstructSiliconCell = config.read<bool>("ConstructSiliconCell", 0);

  ////////////////////////
  // Particle
  ///////////////////////
  ConstructGamma = config.read<bool>("ConstructGamma", 0);
  ConstructElectron = config.read<bool>("ConstructElectron", 0);
  ConstructPi0 = config.read<bool>("ConstructPi0", 0);
  //////////////////////
  ////////////////////////////
  // Physical Channel Candiadate
  /////////////////////////////
  B2KstGamma_HardronFile = config.read<std::string>("B2KstGamma_HardronFile", "");
  B2KstGamma = config.read<bool>("B2KstGamma", 0);
  B2D0GammaPi_HardronFile = config.read<std::string>("B2D0GammaPi_HardronFile", "");
  B2D0GammaPi = config.read<bool>("B2D0GammaPi", 0);
  Bs02JPsiPi0_HardronFile = config.read<std::string>("Bs02JPsiPi0_HardronFile", "");
  Bs02JPsiPi0 = config.read<bool>("Bs02JPsiPi0", 0);
  Bd02PipPimPi0 = config.read<bool>("Bd02PipPimPi0", 0);

  /////////////////
  main_config = FillStructure(fileName);
}

void Parameters::ReadModuleConfig(std::string fileName, int ecal_type)
{
  // this routines triggers the read a module config file

  // invoke the routine FillStructure to actually read the file
  Data_t data = FillStructure(fileName);
  data.ecal_position = ecal_type;

  // add structure to list of module config files
  module_config.emplace(ecal_type, data);
}

struct Data_t Parameters::FillStructure(std::string fileName)
{
  // prepare a Data_t structure
  Data_t data;
  // open input config file
  ConfigFile config(fileName.c_str());

  data.SiTimingNoise = config.read<float>("SiTimingNoise", 0);
  data.SiTimingCellSize = config.read<float>("SiTimingCellSize", 0);
  data.EnPerMIP = config.read<float>("EnPerMIP", 0);

  config.readIntoVect(data.WindowShape4Layers, "WindowShape4Layers");
  data.WindowShape = config.read<int>("WindowShape", 0);
  data.Seed3DMethod = config.read<int>("Seed3DMethod", 0);
  config.readIntoVect(data.Seed2DCut, "Seed2DCut");
  data.Seed3DCut = config.read<float>("Seed3DCut", 50);
  data.CombineSiEnWhenCali = config.read<bool>("CombineSiEnWhenCali", 0);
  data.CellECaliFunType = config.read<int>("CellECaliFunType", 1);
  // data.UsingPar = config.read<bool>("UsingPar", 1);
  data.DoSCor = config.read<bool>("DoSCor", 0);
  data.CombineEnergyPoint2R = config.read<bool>("CombineEnergyPoint2R", 0);
  data.CombineEnergyPoint2T = config.read<bool>("CombineEnergyPoint2T", 0);
  data.OnlySeedT = config.read<bool>("OnlySeedT", 1);
  data.DoS1Cor = config.read<bool>("DoS1Cor", 0);

  data.EnableLayerInfo = config.read<bool>("EnableLayerInfo", 1);
  data.SplitLayer = config.read<int>("SplitLayer", 0);
  return data;
}

void Parameters::PrintConfig(Data_t data)
{
}

void Parameters::WriteParameters(TFile *outfile)
{
  CreateParamTree();
  // write main config parameters (always)
  // TDirectory *parametersDir = outfile->mkdir("Parameters");
  outfile->cd();
  // TDirectory *configDir = gDirectory->mkdir("ConfigFile");
  // configDir->cd();
  // doWriteParameters(outfile, main_config);
  // configDir->cd("../..");
  // write all the module config parameters, if present
  for (unsigned int i = 0; i < module_config.size(); i++)
  {
    doWriteParameters(outfile, module_config[i]);
  }
  paramTTree->Write();
}

void Parameters::doWriteParameters(TFile *outfile, Data_t parameters)
{
  // easy way, use a TTree to write the struc in the output file
  // do a copy
  paramStruct = parameters;
  // fill TTree
  paramTTree->Fill();
  // write to file
  // paramTTree->Write();
}

void Parameters::CreateParamTree()
{
  // create the ttree
  paramTTree = new TTree("parameters", "parameters");
}
