#include "AnalysisAlg.hh"
#include "TTree.h"
#include "TFile.h"
#include "FModule.hh"
#include "FCluster3D.hh"
#include "FRegion.hh"
#include "FCell3D.hh"
#include "Parameters.hh"
#include "TLorentzVector.h"
#include "FCluster2D.hh"
#include "FParticle.hh"
#include "FPi0.hh"
#include "FGamma.hh"
#include "Mixture_Calo.hh"
#include "FPip.hh"
#include "FPim.hh"
#include "Bd_PipPimPi0.hh"
#include "FBd0.hh"
#include "ModuleEventInfo.hh"

void ConstructBd02PipPimPi0(Mixture_Calo *my_ECAL, TFile *OutFile)
{

    TFile *CandidateFile = OutFile;
    TTree *CandidateTree = new TTree("tree", "tree");
    CandidateTree->Branch("event", &my_ECAL->IntputNum, "event/I");

    float B0_M;
    CandidateTree->Branch("B0_M", &B0_M, "B0_M/F");
    float B0_M_truePi0;
    CandidateTree->Branch("B0_M_truePi0", &B0_M_truePi0, "B0_M_truePi0/F");

    // double TRes;
    // CandidateTree->Branch("TRes", &TRes, "TRes/F");

    float B0_E;
    CandidateTree->Branch("B0_E", &B0_E, "B0_E/F");
    float B0_px;
    CandidateTree->Branch("B0_px", &B0_px, "B0_px/F");
    float B0_py;
    CandidateTree->Branch("B0_py", &B0_py, "B0_py/F");
    float B0_pz;
    CandidateTree->Branch("B0_pz", &B0_pz, "B0_pz/F");
    float B0_pt;
    CandidateTree->Branch("B0_pt", &B0_pt, "B0_pt/F");

    float Pi0_px;
    CandidateTree->Branch("Pi0_px", &Pi0_px, "Pi0_px/F");
    float Pi0_py;
    CandidateTree->Branch("Pi0_py", &Pi0_py, "Pi0_py/F");
    float Pi0_pz;
    CandidateTree->Branch("Pi0_pz", &Pi0_pz, "Pi0_pz/F");
    float Pi0_pt;
    CandidateTree->Branch("Pi0_pt", &Pi0_pt, "Pi0_pt/F");
    float Pi0_E;
    CandidateTree->Branch("Pi0_E", &Pi0_E, "Pi0_E/F");
    float Pi0_M;
    CandidateTree->Branch("Pi0_M", &Pi0_M, "Pi0_M/F");
    int Pi0_Type;
    CandidateTree->Branch("Pi0_Type", &Pi0_Type, "Pi0_Type/I");

    int Pi0_Type_true;
    CandidateTree->Branch("Pi0_Type_true", &Pi0_Type_true, "Pi0_Type_true/I");
    float Pi0_M_true;
    CandidateTree->Branch("Pi0_M_true", &Pi0_M_true, "Pi0_M_true/F");
    float Pi0_px_true;
    CandidateTree->Branch("Pi0_px_true", &Pi0_px_true, "Pi0_px_true/F");
    float Pi0_py_true;
    CandidateTree->Branch("Pi0_py_true", &Pi0_py_true, "Pi0_py_true/F");
    float Pi0_pz_true;
    CandidateTree->Branch("Pi0_pz_true", &Pi0_pz_true, "Pi0_pz_true/F");
    float Pi0_pt_true;
    CandidateTree->Branch("Pi0_pt_true", &Pi0_pt_true, "Pi0_pt_true/F");
    float Pi0_E_true;
    CandidateTree->Branch("Pi0_E_true", &Pi0_E_true, "Pi0_E_true/F");

    float Pi0_Gamma1_px;
    CandidateTree->Branch("Pi0_Gamma1_px", &Pi0_Gamma1_px, "Pi0_Gamma1_px/F");
    float Pi0_Gamma1_py;
    CandidateTree->Branch("Pi0_Gamma1_py", &Pi0_Gamma1_py, "Pi0_Gamma1_py/F");
    float Pi0_Gamma1_pz;
    CandidateTree->Branch("Pi0_Gamma1_pz", &Pi0_Gamma1_pz, "Pi0_Gamma1_pz/F");
    float Pi0_Gamma1_pt;
    CandidateTree->Branch("Pi0_Gamma1_pt", &Pi0_Gamma1_pt, "Pi0_Gamma1_pt/F");
    float Pi0_Gamma1_E;
    CandidateTree->Branch("Pi0_Gamma1_E", &Pi0_Gamma1_E, "Pi0_Gamma1_E/F");
    float Pi0_Gamma1_Seed3DE;
    CandidateTree->Branch("Pi0_Gamma1_Seed3DE", &Pi0_Gamma1_Seed3DE, "Pi0_Gamma1_Seed3DE/F");
    float Pi0_Gamma1_Seed3DDx;
    CandidateTree->Branch("Pi0_Gamma1_Seed3DDx", &Pi0_Gamma1_Seed3DDx, "Pi0_Gamma1_Seed3DDx/F");
    float Pi0_Gamma1_Seed3DE_E;
    CandidateTree->Branch("Pi0_Gamma1_Seed3DE_E", &Pi0_Gamma1_Seed3DE_E, "Pi0_Gamma1_Seed3DE_E/F");
    float Pi0_Gamma1_FrontE;
    CandidateTree->Branch("Pi0_Gamma1_FrontE", &Pi0_Gamma1_FrontE, "Pi0_Gamma1_FrontE/F");
    float Pi0_Gamma1_FrontSeedE;
    CandidateTree->Branch("Pi0_Gamma1_FrontSeedE", &Pi0_Gamma1_FrontSeedE, "Pi0_Gamma1_FrontSeedE/F");
    float Pi0_Gamma1_FrontSeedE_E;
    CandidateTree->Branch("Pi0_Gamma1_FrontSeedE_E", &Pi0_Gamma1_FrontSeedE_E, "Pi0_Gamma1_FrontSeedE_E/F");
    float Pi0_Gamma1_BackE;
    CandidateTree->Branch("Pi0_Gamma1_BackE", &Pi0_Gamma1_BackE, "Pi0_Gamma1_BackE/F");
    float Pi0_Gamma1_BackSeedE;
    CandidateTree->Branch("Pi0_Gamma1_BackSeedE", &Pi0_Gamma1_BackSeedE, "Pi0_Gamma1_BackSeedE/F");
    float Pi0_Gamma1_BackSeedE_E;
    CandidateTree->Branch("Pi0_Gamma1_BackSeedE_E", &Pi0_Gamma1_BackSeedE_E, "Pi0_Gamma1_BackSeedE_E/F");

    float Pi0_Gamma1_px_true;
    CandidateTree->Branch("Pi0_Gamma1_px_true", &Pi0_Gamma1_px_true, "Pi0_Gamma1_px_true/F");
    float Pi0_Gamma1_py_true;
    CandidateTree->Branch("Pi0_Gamma1_py_true", &Pi0_Gamma1_py_true, "Pi0_Gamma1_py_true/F");
    float Pi0_Gamma1_pz_true;
    CandidateTree->Branch("Pi0_Gamma1_pz_true", &Pi0_Gamma1_pz_true, "Pi0_Gamma1_pz_true/F");
    float Pi0_Gamma1_pt_true;
    CandidateTree->Branch("Pi0_Gamma1_pt_true", &Pi0_Gamma1_pt_true, "Pi0_Gamma1_pt_true/F");
    float Pi0_Gamma1_E_true;
    CandidateTree->Branch("Pi0_Gamma1_E_true", &Pi0_Gamma1_E_true, "Pi0_Gamma1_E_true/F");

    float Pi0_Gamma2_px;
    CandidateTree->Branch("Pi0_Gamma2_px", &Pi0_Gamma2_px, "Pi0_Gamma2_px/F");
    float Pi0_Gamma2_py;
    CandidateTree->Branch("Pi0_Gamma2_py", &Pi0_Gamma2_py, "Pi0_Gamma2_py/F");
    float Pi0_Gamma2_pz;
    CandidateTree->Branch("Pi0_Gamma2_pz", &Pi0_Gamma2_pz, "Pi0_Gamma2_pz/F");
    float Pi0_Gamma2_pt;
    CandidateTree->Branch("Pi0_Gamma2_pt", &Pi0_Gamma2_pt, "Pi0_Gamma2_pt/F");
    float Pi0_Gamma2_E;
    CandidateTree->Branch("Pi0_Gamma2_E", &Pi0_Gamma2_E, "Pi0_Gamma2_E/F");
    float Pi0_Gamma2_Seed3DDx;
    CandidateTree->Branch("Pi0_Gamma2_Seed3DDx", &Pi0_Gamma2_Seed3DDx, "Pi0_Gamma2_Seed3DDx/F");
    float Pi0_Gamma2_Seed3DE;
    CandidateTree->Branch("Pi0_Gamma2_Seed3DE", &Pi0_Gamma2_Seed3DE, "Pi0_Gamma2_Seed3DE/F");
    float Pi0_Gamma2_Seed3DE_E;
    CandidateTree->Branch("Pi0_Gamma2_Seed3DE_E", &Pi0_Gamma2_Seed3DE_E, "Pi0_Gamma2_Seed3DE_E/F");
    float Pi0_Gamma2_FrontE;
    CandidateTree->Branch("Pi0_Gamma2_FrontE", &Pi0_Gamma2_FrontE, "Pi0_Gamma2_FrontE/F");
    float Pi0_Gamma2_FrontSeedE;
    CandidateTree->Branch("Pi0_Gamma2_FrontSeedE", &Pi0_Gamma2_FrontSeedE, "Pi0_Gamma2_FrontSeedE/F");
    float Pi0_Gamma2_FrontSeedE_E;
    CandidateTree->Branch("Pi0_Gamma2_FrontSeedE_E", &Pi0_Gamma2_FrontSeedE_E, "Pi0_Gamma2_FrontSeedE_E/F");
    float Pi0_Gamma2_BackE;
    CandidateTree->Branch("Pi0_Gamma2_BackE", &Pi0_Gamma2_BackE, "Pi0_Gamma2_BackE/F");
    float Pi0_Gamma2_BackSeedE;
    CandidateTree->Branch("Pi0_Gamma2_BackSeedE", &Pi0_Gamma2_BackSeedE, "Pi0_Gamma2_BackSeedE/F");
    float Pi0_Gamma2_BackSeedE_E;
    CandidateTree->Branch("Pi0_Gamma2_BackSeedE_E", &Pi0_Gamma2_BackSeedE_E, "Pi0_Gamma2_BackSeedE_E/F");

    float Pi0_Gamma2_px_true;
    CandidateTree->Branch("Pi0_Gamma2_px_true", &Pi0_Gamma2_px_true, "Pi0_Gamma2_px_true/F");
    float Pi0_Gamma2_py_true;
    CandidateTree->Branch("Pi0_Gamma2_py_true", &Pi0_Gamma2_py_true, "Pi0_Gamma2_py_true/F");
    float Pi0_Gamma2_pz_true;
    CandidateTree->Branch("Pi0_Gamma2_pz_true", &Pi0_Gamma2_pz_true, "Pi0_Gamma2_pz_true/F");
    float Pi0_Gamma2_pt_true;
    CandidateTree->Branch("Pi0_Gamma2_pt_true", &Pi0_Gamma2_pt_true, "Pi0_Gamma2_pt_true/F");
    float Pi0_Gamma2_E_true;
    CandidateTree->Branch("Pi0_Gamma2_E_true", &Pi0_Gamma2_E_true, "Pi0_Gamma2_E_true/F");

    int Pi0_Gamma1_TruModule;

    int Pi0_Gamma1_TruRegionId;
    int Pi0_Gamma1_TruRegionType;
    int Pi0_Gamma2_TruModule;
    int Pi0_Gamma2_TruRegionId;
    int Pi0_Gamma2_TruRegionType;
    // CandidateTree->Branch("Pi0_Gamma1_TruModule", &Pi0_Gamma1_TruModule, "Pi0_Gamma1_TruModule/I");
    // CandidateTree->Branch("Pi0_Gamma1_TruRegionId", &Pi0_Gamma1_TruRegionId, "Pi0_Gamma1_TruRegionId/I");
    // CandidateTree->Branch("Pi0_Gamma2_TruModule", &Pi0_Gamma2_TruModule, "Pi0_Gamma2_TruModule/I");
    // CandidateTree->Branch("Pi0_Gamma2_TruRegionId", &Pi0_Gamma2_TruRegionId, "Pi0_Gamma2_TruRegionId/I");
    // CandidateTree->Branch("Pi0_Gamma1_TruRegionType", &Pi0_Gamma1_TruRegionType, "Pi0_Gamma1_TruRegionType/I");
    // CandidateTree->Branch("Pi0_Gamma2_TruRegionType", &Pi0_Gamma2_TruRegionType, "Pi0_Gamma2_TruRegionType/I");

    int Pi0_Gamma1_Module;
    CandidateTree->Branch("Pi0_Gamma1_Module", &Pi0_Gamma1_Module, "Pi0_Gamma1_Module/I");
    int Pi0_Gamma1_RegionId;
    CandidateTree->Branch("Pi0_Gamma1_RegionId", &Pi0_Gamma1_RegionId, "Pi0_Gamma1_RegionId/I");
    int Pi0_Gamma1_RegionType;
    CandidateTree->Branch("Pi0_Gamma1_RegionType", &Pi0_Gamma1_RegionType, "Pi0_Gamma1_RegionType/I");

    int Pi0_Gamma2_Module;
    CandidateTree->Branch("Pi0_Gamma2_Module", &Pi0_Gamma2_Module, "Pi0_Gamma2_Module/I");
    int Pi0_Gamma2_RegionId;
    CandidateTree->Branch("Pi0_Gamma2_RegionId", &Pi0_Gamma2_RegionId, "Pi0_Gamma2_RegionId/I");
    int Pi0_Gamma2_RegionType;
    CandidateTree->Branch("Pi0_Gamma2_RegionType", &Pi0_Gamma2_RegionType, "Pi0_Gamma2_RegionType/I");

    float PipPim_M;
    CandidateTree->Branch("PipPim_M", &PipPim_M, "PipPim_M/F");

    float Pip_px;
    CandidateTree->Branch("Pip_px", &Pip_px, "Pip_px/F");
    float Pip_py;
    CandidateTree->Branch("Pip_py", &Pip_py, "Pip_py/F");
    float Pip_pz;
    CandidateTree->Branch("Pip_pz", &Pip_pz, "Pip_pz/F");
    float Pip_E;
    CandidateTree->Branch("Pip_E", &Pip_E, "Pip_E/F");
    float Pip_pt;
    CandidateTree->Branch("Pip_pt", &Pip_pt, "Pip_pt/F");

    float Pim_px;
    CandidateTree->Branch("Pim_px", &Pim_px, "Pim_px/F");
    float Pim_py;
    CandidateTree->Branch("Pim_py", &Pim_py, "Pim_py/F");
    float Pim_pz;
    CandidateTree->Branch("Pim_pz", &Pim_pz, "Pim_pz/F");
    float Pim_E;
    CandidateTree->Branch("Pim_E", &Pim_E, "Pim_E/F");
    float Pim_pt;
    CandidateTree->Branch("Pim_pt", &Pim_pt, "Pim_pt/F");

    int Pi0_Match;
    CandidateTree->Branch("Pi0_Match", &Pi0_Match, "Pi0_Match/I");

    int Pi0_Gamma1_Match;
    CandidateTree->Branch("Pi0_Gamma1_Match", &Pi0_Gamma1_Match, "Pi0_Gamma1_Match/I");

    int Pi0_Gamma2_Match;
    CandidateTree->Branch("Pi0_Gamma2_Match", &Pi0_Gamma2_Match, "Pi0_Gamma2_Match/I");

    float Pi0_Gamma1_MinDist2Charge;
    CandidateTree->Branch("Pi0_Gamma1_MinDist2Charge", &Pi0_Gamma1_MinDist2Charge, "Pi0_Gamma1_MinDist2Charge/F");

    float Pi0_Gamma2_MinDist2Charge;
    CandidateTree->Branch("Pi0_Gamma2_MinDist2Charge", &Pi0_Gamma2_MinDist2Charge, "Pi0_Gamma2_MinDist2Charge/F");

    float Pi0_Gamma1_T_InPV;
    CandidateTree->Branch("Pi0_Gamma1_T_InPV", &Pi0_Gamma1_T_InPV, "Pi0_Gamma1_T_InPV/F");

    float Pi0_Gamma2_T_InPV;
    CandidateTree->Branch("Pi0_Gamma2_T_InPV", &Pi0_Gamma2_T_InPV, "Pi0_Gamma2_T_InPV/F");

    float Pi0_Gamma_DeltaT;
    CandidateTree->Branch("Pi0_Gamma_DeltaT", &Pi0_Gamma_DeltaT, "Pi0_Gamma_DeltaT/F");
    float Pi0_Gamma1_DeltaT;
    CandidateTree->Branch("Pi0_Gamma1_DeltaT", &Pi0_Gamma1_DeltaT, "Pi0_Gamma1_DeltaT/F");
    float Pi0_Gamma2_DeltaT;
    CandidateTree->Branch("Pi0_Gamma2_DeltaT", &Pi0_Gamma2_DeltaT, "Pi0_Gamma2_DeltaT/F");

    float Pi0_Gamma1_TRes;
    CandidateTree->Branch("Pi0_Gamma1_TRes", &Pi0_Gamma1_TRes, "Pi0_Gamma1_TRes/F");
    float Pi0_Gamma2_TRes;
    CandidateTree->Branch("Pi0_Gamma2_TRes", &Pi0_Gamma2_TRes, "Pi0_Gamma2_TRes/F");

    float Dist2PiPi1;
    CandidateTree->Branch("Dist2PiPi1", &Dist2PiPi1, "Dist2PiPi1/F");
    float Dist2PiPi2;
    CandidateTree->Branch("Dist2PiPi2", &Dist2PiPi2, "Dist2PiPi2/F");

    FGamma *TruGamma1 = new FGamma();
    TruGamma1->SetPxPyPzE(my_ECAL->GetPrimaryVx(0, 0), my_ECAL->GetPrimaryVy(0, 0), my_ECAL->GetPrimaryVz(0, 0), my_ECAL->GetPrimaryE(0, 0));
    TruGamma1->time = my_ECAL->GetHitT(0, 0);
    TruGamma1->HitX = my_ECAL->GetHitX(0, 0);
    TruGamma1->HitY = my_ECAL->GetHitY(0, 0);
    TruGamma1->HitZ = my_ECAL->GetHitZ(0, 0);
    FGamma *TruGamma2 = new FGamma();
    TruGamma2->SetPxPyPzE(my_ECAL->GetPrimaryVx(0, 1), my_ECAL->GetPrimaryVy(0, 1), my_ECAL->GetPrimaryVz(0, 1), my_ECAL->GetPrimaryE(0, 1));
    TruGamma2->time = my_ECAL->GetHitT(0, 1);
    TruGamma2->HitX = my_ECAL->GetHitX(0, 1);
    TruGamma2->HitY = my_ECAL->GetHitY(0, 1);
    TruGamma2->HitZ = my_ECAL->GetHitZ(0, 1);
    FPi0 *TruPi0 = new FPi0({TruGamma1, TruGamma2});
    for (auto tmpPi0 : *my_ECAL->Get_vPi0(0))
    {
        for (auto tmpPip : *my_ECAL->Get_vPip(0))
        {
            for (auto tmpPim : *my_ECAL->Get_vPim(0))
            {
                auto tmpBd_PipPimPi0 = new Bd_PipPimPi0(tmpPip, tmpPim, tmpPi0);
                auto tmpBd_PipPim_TruPi0 = new Bd_PipPimPi0(tmpPip, tmpPim, TruPi0, 1);
                B0_M = tmpBd_PipPimPi0->Bd0->M();
                B0_px = tmpBd_PipPimPi0->Bd0->Px();
                B0_py = tmpBd_PipPimPi0->Bd0->Py();
                B0_pz = tmpBd_PipPimPi0->Bd0->Pz();
                B0_pt = tmpBd_PipPimPi0->Bd0->Pt();
                B0_E = tmpBd_PipPimPi0->Bd0->E();
                B0_M_truePi0 = tmpBd_PipPim_TruPi0->Bd0->M();
                auto tmpPipPim = (*tmpPip) + (*tmpPip);
                PipPim_M = tmpPipPim.M();

                Pi0_Type_true = TruPi0->Type;
                Pi0_M_true = TruPi0->M();
                Pi0_px_true = TruPi0->Px();
                Pi0_py_true = TruPi0->Py();
                Pi0_pz_true = TruPi0->Pz();
                Pi0_pt_true = TruPi0->Pt();
                Pi0_E_true = TruPi0->E();

                Pi0_Type = tmpPi0->Type;
                Pi0_M = tmpPi0->M();
                Pi0_px = tmpPi0->Px();
                Pi0_py = tmpPi0->Py();
                Pi0_pz = tmpPi0->Pz();
                Pi0_pt = tmpPi0->Pt();
                Pi0_E = tmpPi0->E();

                auto tmpGamma1 = tmpPi0->Sub_Gamma.first;
                auto tmpGamma2 = tmpPi0->Sub_Gamma.second;
                Pi0_Gamma1_px = tmpGamma1->Px();
                Pi0_Gamma1_py = tmpGamma1->Py();
                Pi0_Gamma1_pz = tmpGamma1->Pz();
                Pi0_Gamma1_pt = tmpGamma1->Pt();
                Pi0_Gamma1_E = tmpGamma1->E();
                Pi0_Gamma1_Module = tmpGamma1->f_module->GetID();
                Pi0_Gamma1_RegionId = tmpGamma1->f_region->GetID();
                Pi0_Gamma1_RegionType = tmpGamma1->f_region->GetType();

                Pi0_Gamma2_px = tmpGamma2->Px();
                Pi0_Gamma2_py = tmpGamma2->Py();
                Pi0_Gamma2_pz = tmpGamma2->Pz();
                Pi0_Gamma2_pt = tmpGamma2->Pt();
                Pi0_Gamma2_E = tmpGamma2->E();
                Pi0_Gamma2_Module = tmpGamma2->f_module->GetID();
                Pi0_Gamma2_RegionId = tmpGamma2->f_region->GetID();
                Pi0_Gamma2_RegionType = tmpGamma2->f_region->GetType();

                Pi0_Gamma1_px_true = TruGamma1->Px();
                Pi0_Gamma1_py_true = TruGamma1->Py();
                Pi0_Gamma1_pz_true = TruGamma1->Pz();
                Pi0_Gamma1_pt_true = TruGamma1->Pt();
                Pi0_Gamma1_E_true = TruGamma1->E();
                Pi0_Gamma1_TruModule = my_ECAL->GetHitModule(0, 0);
                Pi0_Gamma1_TruRegionId = my_ECAL->GetHitRegion(0, 0);
                Pi0_Gamma1_TruRegionType = my_ECAL->GetHitRegionType(0, 0);

                Pi0_Gamma2_px_true = TruGamma2->Px();
                Pi0_Gamma2_py_true = TruGamma2->Py();
                Pi0_Gamma2_pz_true = TruGamma2->Pz();
                Pi0_Gamma2_pt_true = TruGamma2->Pt();
                Pi0_Gamma2_E_true = TruGamma2->E();
                Pi0_Gamma2_TruModule = my_ECAL->GetHitModule(0, 1);
                Pi0_Gamma2_TruRegionId = my_ECAL->GetHitRegion(0, 1);
                Pi0_Gamma2_TruRegionType = my_ECAL->GetHitRegionType(0, 1);

                Pip_px = tmpPip->Px();
                Pip_py = tmpPip->Py();
                Pip_pz = tmpPip->Pz();
                Pip_pt = tmpPip->Pt();
                Pip_E = tmpPip->E();

                Pim_px = tmpPim->Px();
                Pim_py = tmpPim->Py();
                Pim_pz = tmpPim->Pz();
                Pim_pt = tmpPim->Pt();
                Pim_E = tmpPim->E();

                Pi0_Gamma1_Match = tmpGamma1->MatchedNum;

                Pi0_Gamma2_Match = tmpGamma2->MatchedNum;

                Pi0_Gamma1_T_InPV = tmpBd_PipPimPi0->TGamma1_InPV;
                Pi0_Gamma2_T_InPV = tmpBd_PipPimPi0->TGamma2_InPV;

                Pi0_Gamma_DeltaT = tmpBd_PipPimPi0->DeltaT_Gamma;

                float GenT = tmpPip->GenT;
                float GenX = tmpPip->GenX;
                float GenY = tmpPip->GenY;
                float GenZ = tmpPip->GenZ;

                double newVz_Gamma1 = tmpGamma1->Pz() / tmpGamma1->E();
                double GenT_Gamma1 = tmpGamma1->time + (GenZ - tmpGamma1->HitZ) / 299.79 / newVz_Gamma1;
                // std::cout << "tmpGamma1->time " << tmpGamma1->time << std::endl;
                // std::cout << "GenZ - tmpGamma1->HitZ " << GenZ - tmpGamma1->HitZ << std::endl;
                // std::cout << "newVz_Gamma1 " << newVz_Gamma1 << std::endl;
                // std::cout << "GenZ " << GenZ << " GenT " << GenT << std::endl;
                Pi0_Gamma1_DeltaT = GenT_Gamma1 - GenT;

                double newVz_Gamma2 = tmpGamma2->Pz() / tmpGamma2->E();
                double GenT_Gamma2 = tmpGamma2->time + (GenZ - tmpGamma2->HitZ) / 299.79 / newVz_Gamma2;
                Pi0_Gamma2_DeltaT = GenT_Gamma2 - GenT;

                Pi0_Gamma1_TRes = tmpGamma1->f_region->TResFun->Eval(tmpGamma1->E() / 1000.);

                Pi0_Gamma2_TRes = tmpGamma2->f_region->TResFun->Eval(tmpGamma2->E() / 1000.);

                Pi0_Gamma1_Seed3DE = tmpGamma1->GetSeedE();
                if (Pi0_Gamma1_E == 0)
                    Pi0_Gamma1_Seed3DE_E = 0;
                else
                    Pi0_Gamma1_Seed3DE_E = Pi0_Gamma1_Seed3DE / Pi0_Gamma1_E;

                Pi0_Gamma2_Seed3DE = tmpGamma2->GetSeedE();
                if (Pi0_Gamma2_E == 0)
                    Pi0_Gamma2_Seed3DE_E = 0;
                else
                    Pi0_Gamma2_Seed3DE_E = Pi0_Gamma2_Seed3DE / Pi0_Gamma2_E;

                Pi0_Gamma1_FrontE = tmpGamma1->GetE(0);
                Pi0_Gamma1_FrontSeedE = tmpGamma1->GetSeedE(0);
                if (Pi0_Gamma1_FrontE == 0)
                    Pi0_Gamma1_FrontSeedE_E = 0;
                else
                    Pi0_Gamma1_FrontSeedE_E = Pi0_Gamma1_FrontSeedE / Pi0_Gamma1_FrontE;
                Pi0_Gamma1_BackE = tmpGamma1->GetE(1);
                Pi0_Gamma1_BackSeedE = tmpGamma1->GetSeedE(1);
                if (Pi0_Gamma1_BackE == 0)
                    Pi0_Gamma1_BackSeedE_E = 0;
                else
                    Pi0_Gamma1_BackSeedE_E = Pi0_Gamma1_BackSeedE / Pi0_Gamma1_BackE;

                Pi0_Gamma2_FrontE = tmpGamma2->GetE(0);
                Pi0_Gamma2_FrontSeedE = tmpGamma2->GetSeedE(0);
                if (Pi0_Gamma2_FrontE == 0)
                    Pi0_Gamma2_FrontSeedE_E = 0;
                else
                    Pi0_Gamma2_FrontSeedE_E = Pi0_Gamma2_FrontSeedE / Pi0_Gamma2_FrontE;
                Pi0_Gamma2_BackE = tmpGamma2->GetE(1);
                Pi0_Gamma2_BackSeedE = tmpGamma2->GetSeedE(1);
                if (Pi0_Gamma2_BackE == 0)
                    Pi0_Gamma2_BackSeedE_E = 0;
                else
                    Pi0_Gamma2_BackSeedE_E = Pi0_Gamma2_BackSeedE / Pi0_Gamma2_BackE;

                Pi0_Gamma1_Seed3DDx = tmpGamma1->GetSeedDx();
                Pi0_Gamma2_Seed3DDx = tmpGamma2->GetSeedDx();
                Pi0_Gamma1_MinDist2Charge = tmpGamma1->GetMinDist2Charge();
                Pi0_Gamma2_MinDist2Charge = tmpGamma2->GetMinDist2Charge();
                Dist2PiPi1 = tmpBd_PipPimPi0->Dist2PiPi1;
                Dist2PiPi2 = tmpBd_PipPimPi0->Dist2PiPi2;

                Pi0_Match = tmpPi0->MatchedNum;
                 if (B0_M > 4600 && B0_M < 6700)
                CandidateTree->Fill();
            }
        }
    }

    CandidateFile->cd();
    CandidateTree->Write();
}

void ConstructB2KstGamma(Mixture_Calo *my_ECAL, TFile *OutFile)
{
    TTree *TruthTree;

    std::cout << "my_ECAL->IntputNum " << my_ECAL->IntputNum << std::endl;

    TruthTree = ((TTree *)my_ECAL->TruthFile->Get("tree"))->CopyTree(Form("evtIndex==%d", my_ECAL->IntputNum));

    int runNumber;
    TruthTree->SetBranchAddress("runNumber", &runNumber);
    int evtNumber;
    TruthTree->SetBranchAddress("evtNumber", &evtNumber);
    int evtIndex;
    TruthTree->SetBranchAddress("evtIndex", &evtIndex);
    double prod_vertex_x;
    TruthTree->SetBranchAddress("prod_vertex_x", &prod_vertex_x);
    double prod_vertex_y;
    TruthTree->SetBranchAddress("prod_vertex_y", &prod_vertex_y);
    double prod_vertex_z;
    TruthTree->SetBranchAddress("prod_vertex_z", &prod_vertex_z);
    double entry_x;
    TruthTree->SetBranchAddress("entry_x", &entry_x);
    double entry_y;
    TruthTree->SetBranchAddress("entry_y", &entry_y);
    double entry_z;
    TruthTree->SetBranchAddress("entry_z", &entry_z);

    double px_ture;
    TruthTree->SetBranchAddress("px", &px_ture);
    double py_ture;
    TruthTree->SetBranchAddress("py", &py_ture);
    double pz_ture;
    TruthTree->SetBranchAddress("pz", &pz_ture);
    int G4index;
    TruthTree->SetBranchAddress("G4index", &G4index);
    int pdgID;
    TruthTree->SetBranchAddress("pdgID", &pdgID);
    int mother_index;
    TruthTree->SetBranchAddress("mother_index", &mother_index);
    double charge;
    TruthTree->SetBranchAddress("charge", &charge);
    double timing;
    TruthTree->SetBranchAddress("timing", &timing);
    double eKinetic;
    TruthTree->SetBranchAddress("eKinetic", &eKinetic);
    double eTot;
    TruthTree->SetBranchAddress("eTot", &eTot);
    double Kstpx;
    TruthTree->SetBranchAddress("Kstpx", &Kstpx);
    double Kstpy;
    TruthTree->SetBranchAddress("Kstpy", &Kstpy);
    double Kstpz;
    TruthTree->SetBranchAddress("Kstpz", &Kstpz);
    double Kstpe;
    TruthTree->SetBranchAddress("Kstpe", &Kstpe);
    double Kpx_true;
    TruthTree->SetBranchAddress("Kpx_true", &Kpx_true);
    double Kpy_true;
    TruthTree->SetBranchAddress("Kpy_true", &Kpy_true);
    double Kpz_true;
    TruthTree->SetBranchAddress("Kpz_true", &Kpz_true);
    double Kpe_true;
    TruthTree->SetBranchAddress("Kpe_true", &Kpe_true);
    double Kp_true;
    TruthTree->SetBranchAddress("Kp_true", &Kp_true);
    double Kpx;
    TruthTree->SetBranchAddress("Kpx", &Kpx);
    double Kpy;
    TruthTree->SetBranchAddress("Kpy", &Kpy);
    double Kpz;
    TruthTree->SetBranchAddress("Kpz", &Kpz);
    double Kpe;
    TruthTree->SetBranchAddress("Kpe", &Kpe);
    double Kp;
    TruthTree->SetBranchAddress("Kp", &Kp);
    double pipx_true;
    TruthTree->SetBranchAddress("pipx_true", &pipx_true);
    double pipy_true;
    TruthTree->SetBranchAddress("pipy_true", &pipy_true);
    double pipz_true;
    TruthTree->SetBranchAddress("pipz_true", &pipz_true);
    double pipe_true;
    TruthTree->SetBranchAddress("pipe_true", &pipe_true);
    double pip_true;
    TruthTree->SetBranchAddress("pip_true", &pip_true);
    double pipx;
    TruthTree->SetBranchAddress("pipx", &pipx);
    double pipy;
    TruthTree->SetBranchAddress("pipy", &pipy);
    double pipz;
    TruthTree->SetBranchAddress("pipz", &pipz);
    double pipe;
    TruthTree->SetBranchAddress("pipe", &pipe);
    double pip;
    TruthTree->SetBranchAddress("pip", &pip);

    TFile *CandidateFile = OutFile;
    TTree *CandidateTree = new TTree("tree", "tree");
    CandidateTree->Branch("evtNumber", &evtIndex, "evtNumber/I");

    // std::vector<double> LayerTRes;
    // std::vector<double> LayerT;
    // std::vector<double> LayerE;
    // std::vector<double> LayerTShift;
    // std::vector<double> LayerPos;
    // std::vector<int> LayerId;
    // std::vector<int> LayerRegion;
    // if (Parameters::Instance()->SaveCluster2D == 1)
    // {

    //     CandidateTree->Branch("LayerTWeight", &LayerTRes);

    //     CandidateTree->Branch("LayerT", &LayerT);

    //     CandidateTree->Branch("LayerE", &LayerE);

    //     CandidateTree->Branch("LayerTShift", &LayerTShift);

    //     CandidateTree->Branch("LayerPos", &LayerPos);

    //     CandidateTree->Branch("LayerId", &LayerId);

    //     CandidateTree->Branch("LayerRegion", &LayerRegion);
    // }
    double B_M;
    CandidateTree->Branch("B_M", &B_M, "B_M/D");
    double B_M_trueGamma;
    CandidateTree->Branch("B_M_trueGamma", &B_M_trueGamma, "B_M_trueGamma/D");
    double Kpi_M;
    CandidateTree->Branch("Kpi_M", &Kpi_M, "Kpi_M/D");

    double TRes;
    CandidateTree->Branch("TRes", &TRes, "TRes/D");
    double MeanRes;
    CandidateTree->Branch("MeanRes", &MeanRes, "MeanRes/D");
    double Bpe;
    CandidateTree->Branch("Bpe", &Bpe, "Bpe/D");
    double Bpx;
    CandidateTree->Branch("Bpx", &Bpx, "Bpx/D");
    double Bpy;
    CandidateTree->Branch("Bpy", &Bpy, "Bpy/D");
    double Bpz;
    CandidateTree->Branch("Bpz", &Bpz, "Bpz/D");
    double cx;
    CandidateTree->Branch("x", &cx, "x/D");
    double cy;
    CandidateTree->Branch("y", &cy, "y/D");
    double cz;
    CandidateTree->Branch("z", &cz, "z/D");
    double rec_entry_x;
    CandidateTree->Branch("rec_entry_x", &rec_entry_x, "rec_entry_x/D");
    double rec_entry_y;
    CandidateTree->Branch("rec_entry_y", &rec_entry_y, "rec_entry_y/D");
    double rec_entry_z;
    CandidateTree->Branch("rec_entry_z", &rec_entry_z, "rec_entry_z/D");

    double px;
    CandidateTree->Branch("px", &px, "px/D");
    double py;
    CandidateTree->Branch("py", &py, "py/D");
    double pz;
    CandidateTree->Branch("pz", &pz, "pz/D");
    double pt;
    CandidateTree->Branch("pt", &pt, "pt/D");
    double ct;
    CandidateTree->Branch("t", &ct, "t/D");
    double ce;
    CandidateTree->Branch("e", &ce, "e/D");
    int regionId;
    CandidateTree->Branch("regionId", &regionId, "regionId/I");
    int regionType;
    CandidateTree->Branch("regionType", &regionType, "regionType/I");
    int LayerNums;
    CandidateTree->Branch("LayerNums", &LayerNums, "LayerNums/I");
    std::vector<float> Clu2DTime;
    CandidateTree->Branch("Clu2DTime", &Clu2DTime);
    std::vector<float> Clu2DEnergy;
    CandidateTree->Branch("Clu2DEnergy", &Clu2DEnergy);
    std::vector<float> Clu2DPos;
    CandidateTree->Branch("Clu2DPos", &Clu2DPos);
    std::vector<float> Clu2DThickness;
    CandidateTree->Branch("Clu2DThickness", &Clu2DThickness);
    std::vector<float> Clu2DX;
    CandidateTree->Branch("Clu2DX", &Clu2DX);
    std::vector<float> Clu2DY;
    CandidateTree->Branch("Clu2DY", &Clu2DY);
    std::vector<float> Clu2DZ;
    CandidateTree->Branch("Clu2DZ", &Clu2DZ);

    CandidateTree->Branch("px_ture", &px_ture, "px_ture/D");
    CandidateTree->Branch("py_ture", &py_ture, "py_ture/D");
    CandidateTree->Branch("pz_ture", &pz_ture, "pz_ture/D");
    CandidateTree->Branch("true_eTot", &eTot, "true_eTot/D");

    double pt_ture;
    CandidateTree->Branch("pt_ture", &pt_ture, "pt_ture/D");
    double xSeed;
    CandidateTree->Branch("xSeed", &xSeed, "xSeed/D");
    double ySeed;
    CandidateTree->Branch("ySeed", &ySeed, "ySeed/D");
    double cellSize;
    CandidateTree->Branch("cellSize", &cellSize, "cellSize/D");
    double SiTimingCellSize;
    CandidateTree->Branch("SiTimingCellSize", &SiTimingCellSize, "SiTimingCellSize/D");
    CandidateTree->Branch("true_prod_vertex_x", &prod_vertex_x, "true_prod_vertex_x/D");
    CandidateTree->Branch("true_prod_vertex_y", &prod_vertex_y, "true_prod_vertex_y/D");
    CandidateTree->Branch("true_prod_vertex_z", &prod_vertex_z, "true_prod_vertex_z/D");
    CandidateTree->Branch("true_entry_x", &entry_x, "true_entry_x/D");
    CandidateTree->Branch("true_entry_y", &entry_y, "true_entry_y/D");
    CandidateTree->Branch("true_entry_z", &entry_z, "true_entry_z/D");
    CandidateTree->Branch("true_timing", &timing, "true_timing/D");
    // CandidateTree->Branch("mother_index", &mother_index, "mother_index/I");
    // CandidateTree->Branch("pdgID", &pdgID, "pdgID/I");

    CandidateTree->Branch("Kstpx", &Kstpx, "Kstpx/D");
    CandidateTree->Branch("Kstpy", &Kstpy, "Kstpy/D");
    CandidateTree->Branch("Kstpz", &Kstpz, "Kstpz/D");
    CandidateTree->Branch("Kstpe", &Kstpe, "Kstpe/D");

    CandidateTree->Branch("Kpx_true", &Kpx_true, "Kpx_true/D");
    CandidateTree->Branch("Kpy_true", &Kpy_true, "Kpy_true/D");
    CandidateTree->Branch("Kpz_true", &Kpz_true, "Kpz_true/D");
    CandidateTree->Branch("Kpe_true", &Kpe_true, "Kpe_true/D");
    CandidateTree->Branch("Kp_true", &Kp_true, "Kp_true/D");
    CandidateTree->Branch("Kpx", &Kpx, "Kpx/D");
    CandidateTree->Branch("Kpy", &Kpy, "Kpy/D");
    CandidateTree->Branch("Kpz", &Kpz, "Kpz/D");
    CandidateTree->Branch("Kpe", &Kpe, "Kpe/D");
    CandidateTree->Branch("Kp", &Kp, "Kp/D");
    CandidateTree->Branch("pipx_true", &pipx_true, "pipx_true/D");
    CandidateTree->Branch("pipy_true", &pipy_true, "pipy_true/D");
    CandidateTree->Branch("pipz_true", &pipz_true, "pipz_true/D");
    CandidateTree->Branch("pipe_true", &pipe_true, "pipe_true/D");
    CandidateTree->Branch("pip_true", &pip_true, "pip_true/D");
    CandidateTree->Branch("pipx", &pipx, "pipx/D");
    CandidateTree->Branch("pipy", &pipy, "pipy/D");
    CandidateTree->Branch("pipz", &pipz, "pipz/D");
    CandidateTree->Branch("pipe", &pipe, "pipe/D");
    CandidateTree->Branch("pip", &pip, "pip/D");
    double dist;
    CandidateTree->Branch("dist", &dist, "dist/D");

    TLorentzVector B, BtrueGamma, Dst0, gamma, k, pi, trueGamma;

    for (int i = 0; i < TruthTree->GetEntries(); i++)
    {
        TruthTree->GetEntry(i);
        for (auto module : my_ECAL->module_container)
        {
            FModule *CurrentModule = module.second;
            FRegion *CurrentRegion = CurrentModule->f_region;
            cellSize = CurrentRegion->Cell3DSize;
            SiTimingCellSize = CurrentRegion->SiTimingCellSize;
            for (int CurrentEvent = 0; CurrentEvent < CurrentRegion->f_calo->max_Eventseq; CurrentEvent++)
            {

                for (auto Cluster3D : CurrentModule->GetEventInfo(CurrentEvent)->Cluster3D)
                {
                    Clu2DTime.clear();
                    Clu2DEnergy.clear();
                    Clu2DPos.clear();
                    Clu2DThickness.clear();
                    Clu2DX.clear();
                    Clu2DY.clear();
                    Clu2DZ.clear();
                    int layerCount = 0;
                    for (auto Layer : CurrentRegion->Layers)
                    {

                        Clu2DPos.emplace_back(Layer->layerPos);
                        Clu2DThickness.emplace_back(Layer->layerThickness);

                        FCluster2D *CurrentClu2D = Cluster3D->v_Cluster2D.at(layerCount);
                        FRegion *Clu2DRegion = CurrentClu2D->f_region;
                        FModule *Clu2DModule = CurrentClu2D->f_module;
                        Clu2DTime.emplace_back(CurrentClu2D->time);
                        Clu2DEnergy.emplace_back(CurrentClu2D->energy);
                        Clu2DX.emplace_back(CurrentClu2D->X());
                        Clu2DY.emplace_back(CurrentClu2D->Y());
                        Clu2DZ.emplace_back(CurrentClu2D->Z());

                        layerCount++;
                    }
                    cx = Cluster3D->X();
                    cy = Cluster3D->Y();

                    cz = Cluster3D->Z() - my_ECAL->GetPrimaryZ(i);
                    ce = Cluster3D->energy;
                    rec_entry_x = cx * entry_z / cz;
                    rec_entry_y = cy * entry_z / cz;
                    ct = Cluster3D->time;
                    regionType = Cluster3D->f_region->type;
                    regionId = Cluster3D->f_region->GlobalId;

                    TRes = Cluster3D->f_region->TResFun->Eval(ce / 1000);

                    px = cx / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;
                    py = cy / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;
                    pz = (cz) / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;

                    xSeed = Cluster3D->Seed3D->X();
                    ySeed = Cluster3D->Seed3D->Y();
                    float pre_x = (px_ture * ((cz - entry_z) / pz_ture) + entry_x);
                    float pre_y = (py_ture * ((cz - entry_z) / pz_ture) + entry_y);
                    dist = sqrt(pow(cx - pre_x, 2) + pow(cy - pre_y, 2));
                    k.SetPxPyPzE(Kpx * 1000, Kpy * 1000, Kpz * 1000, Kpe * 1000);
                    pi.SetPxPyPzE(pipx * 1000, pipy * 1000, pipz * 1000, pipe * 1000);
                    gamma.SetPxPyPzE(px, py, pz, ce);
                    trueGamma.SetPxPyPzE(px_ture * 1000, py_ture * 1000, pz_ture * 1000, eTot * 1000);
                    pt = gamma.Pt();
                    pt_ture = trueGamma.Pt();
                    BtrueGamma = k + pi + trueGamma;
                    B = k + pi + gamma;
                    B_M_trueGamma = BtrueGamma.M();
                    B_M = B.M();
                    Kpi_M = (k + pi).M();
                    if (B_M < 2500 && B_M > 9000)
                        continue;
                    Bpe = B.E();
                    Bpx = B.Px();
                    Bpy = B.Py();
                    Bpz = B.Pz();

                    CandidateTree->Fill();
                }
            }
        }
    }
    CandidateFile->cd();
    CandidateTree->Write();
    CandidateFile->Close();
}

void ConstructB2D0Gammapi(Mixture_Calo *my_ECAL, TFile *OutFile)
{
    TTree *TruthTree;

    std::cout << "my_ECAL->IntputNum " << my_ECAL->IntputNum << std::endl;

    TruthTree = ((TTree *)my_ECAL->TruthFile->Get("tree"))->CopyTree(Form("evtIndex==%d", my_ECAL->IntputNum));

    int runNumber;
    TruthTree->SetBranchAddress("runNumber", &runNumber);
    int evtNumber;
    TruthTree->SetBranchAddress("evtNumber", &evtNumber);
    int evtIndex;
    TruthTree->SetBranchAddress("evtIndex", &evtIndex);
    double prod_vertex_x;
    TruthTree->SetBranchAddress("prod_vertex_x", &prod_vertex_x);
    double prod_vertex_y;
    TruthTree->SetBranchAddress("prod_vertex_y", &prod_vertex_y);
    double prod_vertex_z;
    TruthTree->SetBranchAddress("prod_vertex_z", &prod_vertex_z);
    double entry_x;
    TruthTree->SetBranchAddress("entry_x", &entry_x);
    double entry_y;
    TruthTree->SetBranchAddress("entry_y", &entry_y);
    double entry_z;
    TruthTree->SetBranchAddress("entry_z", &entry_z);

    double px_ture;
    TruthTree->SetBranchAddress("px", &px_ture);
    double py_ture;
    TruthTree->SetBranchAddress("py", &py_ture);
    double pz_ture;
    TruthTree->SetBranchAddress("pz", &pz_ture);
    int G4index;
    TruthTree->SetBranchAddress("G4index", &G4index);
    int pdgID;
    TruthTree->SetBranchAddress("pdgID", &pdgID);
    int mother_index;
    TruthTree->SetBranchAddress("mother_index", &mother_index);
    double charge;
    TruthTree->SetBranchAddress("charge", &charge);
    double timing;
    TruthTree->SetBranchAddress("timing", &timing);
    double eKinetic;
    TruthTree->SetBranchAddress("eKinetic", &eKinetic);
    double eTot;
    TruthTree->SetBranchAddress("eTot", &eTot);

    double Dst0px;
    TruthTree->SetBranchAddress("Dst0px", &Dst0px);
    double Dst0py;
    TruthTree->SetBranchAddress("Dst0py", &Dst0py);
    double Dst0pz;
    TruthTree->SetBranchAddress("Dst0pz", &Dst0pz);
    double Dst0pe;
    TruthTree->SetBranchAddress("Dst0pe", &Dst0pe);

    double D0px;
    TruthTree->SetBranchAddress("D0px", &D0px);
    double D0py;
    TruthTree->SetBranchAddress("D0py", &D0py);
    double D0pz;
    TruthTree->SetBranchAddress("D0pz", &D0pz);
    double D0pe;
    TruthTree->SetBranchAddress("D0pe", &D0pe);

    double Kpx_true;
    TruthTree->SetBranchAddress("Kpx_true", &Kpx_true);
    double Kpy_true;
    TruthTree->SetBranchAddress("Kpy_true", &Kpy_true);
    double Kpz_true;
    TruthTree->SetBranchAddress("Kpz_true", &Kpz_true);
    double Kpe_true;
    TruthTree->SetBranchAddress("Kpe_true", &Kpe_true);
    double Kp_true;
    TruthTree->SetBranchAddress("Kp_true", &Kp_true);
    double Kpx;
    TruthTree->SetBranchAddress("Kpx", &Kpx);
    double Kpy;
    TruthTree->SetBranchAddress("Kpy", &Kpy);
    double Kpz;
    TruthTree->SetBranchAddress("Kpz", &Kpz);
    double Kpe;
    TruthTree->SetBranchAddress("Kpe", &Kpe);
    double Kp;
    TruthTree->SetBranchAddress("Kp", &Kp);
    double pi1px_true;
    TruthTree->SetBranchAddress("pi1px_true", &pi1px_true);
    double pi1py_true;
    TruthTree->SetBranchAddress("pi1py_true", &pi1py_true);
    double pi1pz_true;
    TruthTree->SetBranchAddress("pi1pz_true", &pi1pz_true);
    double pi1pe_true;
    TruthTree->SetBranchAddress("pi1pe_true", &pi1pe_true);
    double pi1p_true;
    TruthTree->SetBranchAddress("pi1p_true", &pi1p_true);
    double pi1px;
    TruthTree->SetBranchAddress("pi1px", &pi1px);
    double pi1py;
    TruthTree->SetBranchAddress("pi1py", &pi1py);
    double pi1pz;
    TruthTree->SetBranchAddress("pi1pz", &pi1pz);
    double pi1pe;
    TruthTree->SetBranchAddress("pi1pe", &pi1pe);
    double pi1p;
    TruthTree->SetBranchAddress("pi1p", &pi1p);

    double pi2px_true;
    TruthTree->SetBranchAddress("pi2px_true", &pi2px_true);
    double pi2py_true;
    TruthTree->SetBranchAddress("pi2py_true", &pi2py_true);
    double pi2pz_true;
    TruthTree->SetBranchAddress("pi2pz_true", &pi2pz_true);
    double pi2pe_true;
    TruthTree->SetBranchAddress("pi2pe_true", &pi2pe_true);
    double pi2p_true;
    TruthTree->SetBranchAddress("pi2p_true", &pi2p_true);
    double pi2px;
    TruthTree->SetBranchAddress("pi2px", &pi2px);
    double pi2py;
    TruthTree->SetBranchAddress("pi2py", &pi2py);
    double pi2pz;
    TruthTree->SetBranchAddress("pi2pz", &pi2pz);
    double pi2pe;
    TruthTree->SetBranchAddress("pi2pe", &pi2pe);
    double pi2p;
    TruthTree->SetBranchAddress("pi2p", &pi2p);

    TFile *CandidateFile = OutFile;
    TTree *CandidateTree = new TTree("tree", "tree");
    CandidateTree->Branch("evtNumber", &evtIndex, "evtNumber/I");

    // std::vector<double> LayerTRes;
    // std::vector<double> LayerT;
    // std::vector<double> LayerE;
    // std::vector<double> LayerTShift;
    // std::vector<double> LayerPos;
    // std::vector<int> LayerId;
    // std::vector<int> LayerRegion;
    // if (Parameters::Instance()->SaveCluster2D == 1)
    // {

    //     CandidateTree->Branch("LayerTWeight", &LayerTRes);

    //     CandidateTree->Branch("LayerT", &LayerT);

    //     CandidateTree->Branch("LayerE", &LayerE);

    //     CandidateTree->Branch("LayerTShift", &LayerTShift);

    //     CandidateTree->Branch("LayerPos", &LayerPos);

    //     CandidateTree->Branch("LayerId", &LayerId);

    //     CandidateTree->Branch("LayerRegion", &LayerRegion);
    // }
    double B_M;
    CandidateTree->Branch("B_M", &B_M, "B_M/D");
    double B_M_trueGamma;
    CandidateTree->Branch("B_M_trueGamma", &B_M_trueGamma, "B_M_trueGamma/D");
    double Dst0_M;
    CandidateTree->Branch("Dst0_M", &Dst0_M, "Dst0_M/D");
    double Dst0_M_trueGamma;
    CandidateTree->Branch("Dst0_M_trueGamma", &Dst0_M_trueGamma, "Dst0_M_trueGamma/D");

    double TRes;
    CandidateTree->Branch("TRes", &TRes, "TRes/D");
    double MeanRes;
    CandidateTree->Branch("MeanRes", &MeanRes, "MeanRes/D");
    double Bpe;
    CandidateTree->Branch("Bpe", &Bpe, "Bpe/D");
    double Bpx;
    CandidateTree->Branch("Bpx", &Bpx, "Bpx/D");
    double Bpy;
    CandidateTree->Branch("Bpy", &Bpy, "Bpy/D");
    double Bpz;
    CandidateTree->Branch("Bpz", &Bpz, "Bpz/D");
    double cx;
    CandidateTree->Branch("x", &cx, "x/D");
    double cy;
    CandidateTree->Branch("y", &cy, "y/D");
    double cz;
    CandidateTree->Branch("z", &cz, "z/D");
    double rec_entry_x;
    CandidateTree->Branch("rec_entry_x", &rec_entry_x, "rec_entry_x/D");
    double rec_entry_y;
    CandidateTree->Branch("rec_entry_y", &rec_entry_y, "rec_entry_y/D");
    double rec_entry_z;
    CandidateTree->Branch("rec_entry_z", &rec_entry_z, "rec_entry_z/D");

    double px;
    CandidateTree->Branch("px", &px, "px/D");
    double py;
    CandidateTree->Branch("py", &py, "py/D");
    double pz;
    CandidateTree->Branch("pz", &pz, "pz/D");
    double pt;
    CandidateTree->Branch("pt", &pt, "pt/D");
    double ct;
    CandidateTree->Branch("t", &ct, "t/D");
    double ce;
    CandidateTree->Branch("e", &ce, "e/D");
    int regionId;
    CandidateTree->Branch("regionId", &regionId, "regionId/I");
    int regionType;
    CandidateTree->Branch("regionType", &regionType, "regionType/I");
    int LayerNums;
    CandidateTree->Branch("LayerNums", &LayerNums, "LayerNums/I");
    std::vector<float> Clu2DTime;
    CandidateTree->Branch("Clu2DTime", &Clu2DTime);
    std::vector<float> Clu2DEnergy;
    CandidateTree->Branch("Clu2DEnergy", &Clu2DEnergy);
    std::vector<float> Clu2DPos;
    CandidateTree->Branch("Clu2DPos", &Clu2DPos);
    std::vector<float> Clu2DThickness;
    CandidateTree->Branch("Clu2DThickness", &Clu2DThickness);
    std::vector<float> Clu2DX;
    CandidateTree->Branch("Clu2DX", &Clu2DX);
    std::vector<float> Clu2DY;
    CandidateTree->Branch("Clu2DY", &Clu2DY);
    std::vector<float> Clu2DZ;
    CandidateTree->Branch("Clu2DZ", &Clu2DZ);

    CandidateTree->Branch("px_ture", &px_ture, "px_ture/D");
    CandidateTree->Branch("py_ture", &py_ture, "py_ture/D");
    CandidateTree->Branch("pz_ture", &pz_ture, "pz_ture/D");
    CandidateTree->Branch("true_eTot", &eTot, "true_eTot/D");

    double pt_ture;
    CandidateTree->Branch("pt_ture", &pt_ture, "pt_ture/D");
    double xSeed;
    CandidateTree->Branch("xSeed", &xSeed, "xSeed/D");
    double ySeed;
    CandidateTree->Branch("ySeed", &ySeed, "ySeed/D");
    double cellSize;
    CandidateTree->Branch("cellSize", &cellSize, "cellSize/D");
    double SiTimingCellSize;
    CandidateTree->Branch("SiTimingCellSize", &SiTimingCellSize, "SiTimingCellSize/D");
    CandidateTree->Branch("true_prod_vertex_x", &prod_vertex_x, "true_prod_vertex_x/D");
    CandidateTree->Branch("true_prod_vertex_y", &prod_vertex_y, "true_prod_vertex_y/D");
    CandidateTree->Branch("true_prod_vertex_z", &prod_vertex_z, "true_prod_vertex_z/D");
    CandidateTree->Branch("true_entry_x", &entry_x, "true_entry_x/D");
    CandidateTree->Branch("true_entry_y", &entry_y, "true_entry_y/D");
    CandidateTree->Branch("true_entry_z", &entry_z, "true_entry_z/D");
    CandidateTree->Branch("true_timing", &timing, "true_timing/D");
    // CandidateTree->Branch("mother_index", &mother_index, "mother_index/I");
    // CandidateTree->Branch("pdgID", &pdgID, "pdgID/I");

    CandidateTree->Branch("Dst0px", &Dst0px, "Dst0px/D");
    CandidateTree->Branch("Dst0py", &Dst0py, "Dst0py/D");
    CandidateTree->Branch("Dst0pz", &Dst0pz, "Dst0pz/D");
    CandidateTree->Branch("Dst0pe", &Dst0pe, "Dst0pe/D");

    CandidateTree->Branch("D0px", &D0px, "D0px/D");
    CandidateTree->Branch("D0py", &D0py, "D0py/D");
    CandidateTree->Branch("D0pz", &D0pz, "D0pz/D");
    CandidateTree->Branch("D0pe", &D0pe, "D0pe/D");

    CandidateTree->Branch("Kpx_true", &Kpx_true, "Kpx_true/D");
    CandidateTree->Branch("Kpy_true", &Kpy_true, "Kpy_true/D");
    CandidateTree->Branch("Kpz_true", &Kpz_true, "Kpz_true/D");
    CandidateTree->Branch("Kpe_true", &Kpe_true, "Kpe_true/D");
    CandidateTree->Branch("Kp_true", &Kp_true, "Kp_true/D");
    CandidateTree->Branch("Kpx", &Kpx, "Kpx/D");
    CandidateTree->Branch("Kpy", &Kpy, "Kpy/D");
    CandidateTree->Branch("Kpz", &Kpz, "Kpz/D");
    CandidateTree->Branch("Kpe", &Kpe, "Kpe/D");
    CandidateTree->Branch("Kp", &Kp, "Kp/D");
    CandidateTree->Branch("pi1px_true", &pi1px_true, "pi1px_true/D");
    CandidateTree->Branch("pi1py_true", &pi1py_true, "pi1py_true/D");
    CandidateTree->Branch("pi1pz_true", &pi1pz_true, "pi1pz_true/D");
    CandidateTree->Branch("pi1pe_true", &pi1pe_true, "pi1pe_true/D");
    CandidateTree->Branch("pi1p_true", &pi1p_true, "pi1p_true/D");
    CandidateTree->Branch("pi1px", &pi1px, "pi1px/D");
    CandidateTree->Branch("pi1py", &pi1py, "pi1py/D");
    CandidateTree->Branch("pi1pz", &pi1pz, "pi1pz/D");
    CandidateTree->Branch("pi1pe", &pi1pe, "pi1pe/D");
    CandidateTree->Branch("pi1p", &pi1p, "pi1p/D");

    CandidateTree->Branch("pi2px_true", &pi2px_true, "pi2px_true/D");
    CandidateTree->Branch("pi2py_true", &pi2py_true, "pi2py_true/D");
    CandidateTree->Branch("pi2pz_true", &pi2pz_true, "pi2pz_true/D");
    CandidateTree->Branch("pi2pe_true", &pi2pe_true, "pi2pe_true/D");
    CandidateTree->Branch("pi2p_true", &pi2p_true, "pi2p_true/D");
    CandidateTree->Branch("pi2px", &pi2px, "pi2px/D");
    CandidateTree->Branch("pi2py", &pi2py, "pi2py/D");
    CandidateTree->Branch("pi2pz", &pi2pz, "pi2pz/D");
    CandidateTree->Branch("pi2pe", &pi2pe, "pi2pe/D");
    CandidateTree->Branch("pi2p", &pi2p, "pi2p/D");

    double dist;
    CandidateTree->Branch("dist", &dist, "dist/D");

    TLorentzVector B, BtrueGamma, Dst0, Dst0_trueGamma, gamma, k, pi1, pi2, trueGamma, D0;

    for (int i = 0; i < TruthTree->GetEntries(); i++)
    {
        TruthTree->GetEntry(i);
        for (auto module : my_ECAL->module_container)
        {
            FModule *CurrentModule = module.second;
            FRegion *CurrentRegion = CurrentModule->f_region;
            cellSize = CurrentRegion->Cell3DSize;
            SiTimingCellSize = CurrentRegion->SiTimingCellSize;
            for (int CurrentEvent = 0; CurrentEvent < CurrentRegion->f_calo->max_Eventseq; CurrentEvent++)
            {

                for (auto Cluster3D : CurrentModule->GetEventInfo(CurrentEvent)->Cluster3D)
                {
                    Clu2DTime.clear();
                    Clu2DEnergy.clear();
                    Clu2DPos.clear();
                    Clu2DThickness.clear();
                    Clu2DX.clear();
                    Clu2DY.clear();
                    Clu2DZ.clear();
                    int layerCount = 0;
                    for (auto Layer : CurrentRegion->Layers)
                    {

                        Clu2DPos.emplace_back(Layer->layerPos);
                        Clu2DThickness.emplace_back(Layer->layerThickness);

                        FCluster2D *CurrentClu2D = Cluster3D->v_Cluster2D.at(layerCount);
                        FRegion *Clu2DRegion = CurrentClu2D->f_region;
                        FModule *Clu2DModule = CurrentClu2D->f_module;
                        Clu2DTime.emplace_back(CurrentClu2D->time);
                        Clu2DEnergy.emplace_back(CurrentClu2D->energy);
                        Clu2DX.emplace_back(CurrentClu2D->X());
                        Clu2DY.emplace_back(CurrentClu2D->Y());
                        Clu2DZ.emplace_back(CurrentClu2D->Z());

                            layerCount++;
                    }
                    cx = Cluster3D->X();
                    cy = Cluster3D->Y();

                    cz = Cluster3D->Z() - my_ECAL->GetPrimaryZ(i);
                    ce = Cluster3D->energy;
                    rec_entry_x = cx * entry_z / cz;
                    rec_entry_y = cy * entry_z / cz;
                    ct = Cluster3D->time;
                    regionType = Cluster3D->f_region->type;
                    regionId = Cluster3D->f_region->GlobalId;

                    TRes = Cluster3D->f_region->TResFun->Eval(ce / 1000);

                    px = cx / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;
                    py = cy / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;
                    pz = (cz) / sqrt((cz) * (cz) + cx * cx + cy * cy) * ce;

                    xSeed = Cluster3D->Seed3D->X();
                    ySeed = Cluster3D->Seed3D->Y();
                    float pre_x = (px_ture * ((cz - entry_z) / pz_ture) + entry_x);
                    float pre_y = (py_ture * ((cz - entry_z) / pz_ture) + entry_y);
                    dist = sqrt(pow(cx - pre_x, 2) + pow(cy - pre_y, 2));
                    k.SetPxPyPzE(Kpx * 1000, Kpy * 1000, Kpz * 1000, Kpe * 1000);
                    pi1.SetPxPyPzE(pi1px * 1000, pi1py * 1000, pi1pz * 1000, pi1pe * 1000);
                    pi2.SetPxPyPzE(pi2px * 1000, pi2py * 1000, pi2pz * 1000, pi2pe * 1000);
                    gamma.SetPxPyPzE(px, py, pz, ce);
                    trueGamma.SetPxPyPzE(px_ture * 1000, py_ture * 1000, pz_ture * 1000, eTot * 1000);
                    pt = gamma.Pt();
                    pt_ture = trueGamma.Pt();

                    D0 = k + pi1;
                    Dst0 = k + pi1 + gamma;
                    Dst0_trueGamma = k + pi1 + trueGamma;
                    B = Dst0 + pi2;
                    BtrueGamma = Dst0_trueGamma + pi2;
                    B_M_trueGamma = BtrueGamma.M();
                    B_M = B.M();
                    Dst0_M_trueGamma = Dst0_trueGamma.M();
                    Dst0_M = Dst0.M();
                    if (B_M < 2500 && B_M > 9000)
                        continue;
                    Bpe = B.E();
                    Bpx = B.Px();
                    Bpy = B.Py();
                    Bpz = B.Pz();

                    CandidateTree->Fill();
                }
            }
        }
    }
    CandidateFile->cd();
    CandidateTree->Write();
    CandidateFile->Close();
}
