
#include "Mixture_Calo.hh"
#include "FDataType.hh"

#include <chrono>

#include "TFile.h"
#include <sstream>
#include "iostream"
#include "TCanvas.h"
#include <iomanip>
#include "algorithm"
#include "Parameters.hh"
#include "RecAlg.h"
#include "CreateTree.hh"
#include "TF1.h"
#include "TCut.h"
#include "TLeaf.h"
#include "TProfile.h"
#include "TMath.h"
#include "CalculateGeo.hh"
#include "TRotation.h"
#include "TROOT.h"
#include "TMath.h"
#include "Math/EulerAngles.h"
#include "TVector3.h"

#include "FPi0.hh"
#include "FGamma.hh"
#include "FLayer.hh"
#include "FModule.hh"
#include "FCluster2D.hh"
#include "FCluster3D.hh"
#include "FCell.hh"
#include "FCell3D.hh"
#include "FRegion.hh"
#include "FPip.hh"
#include "FPim.hh"
#include "ModuleEventInfo.hh"

Mixture_Calo *Mixture_Calo::__instance = 0;

bool CmpLayerPos(RegionLayerInfo *a, RegionLayerInfo *b)
{
    return a->layerPos < b->layerPos;
}

void Mixture_Calo::InitCalo()
{

    spdlog::get(LOG_NAME)->info("Geometry file type = {}", Parameters::Instance()->GeoFilesType);
    if (Parameters::Instance()->GeoFilesType == 1)
    {
        spdlog::get(LOG_NAME)->info("Construct geometry using file from simulation");
        ConstructFromRawFile();
    }
    else if (Parameters::Instance()->GeoFilesType == 2)
    {
        spdlog::get(LOG_NAME)->info("Construct geometry using Rec geometry file");
        Construct();
    }

    spdlog::get(LOG_NAME)->info("Initial parameters for calorimeter");
    InitPara();

    spdlog::get(LOG_NAME)->info("Reconstruct region");
    RecRegion();

    spdlog::get(LOG_NAME)->info("Initial Cell Global position");
    InitCellGlobalPos();

    spdlog::get(LOG_NAME)->info("Init Event Structure");

    ////////////////////////////////////////////////////
    InitEventStructure();
    if (Parameters::Instance()->InitTruthInfo)
    {
        InitPVAndHitInfo();
    }

    ///////////////////////////////////////////////////
    if (Parameters::Instance()->ConstructSiliconCell)
    {
        spdlog::get(LOG_NAME)->info("Reconstruction silicon readout cell");
        RecSiliconCell();
    }
    if (Parameters::Instance()->DataFileType == 1)
    {

        spdlog::get(LOG_NAME)->info("Initial scintillator readout data by the file from simulation");
        WriteScintillatorData();
    }
    else if (Parameters::Instance()->DataFileType == 2)
    {
        spdlog::get(LOG_NAME)->info("Initial readout data and hit information by the file from simulation");
        WriteData();
    }
}

void Mixture_Calo::SaveReadOutMap(int event)
{
    for (auto Module : module_container)
    {
        for (auto Cell3D : Module.second->v_Cell3D)
        {

            GetHitMap(event)->Fill(Cell3D->X(), Cell3D->Y(), Cell3D->GetEnergy(event));
        }
    }
}

void Mixture_Calo::InitPVAndHitInfo()
{
    if (Parameters::Instance()->TruthFileType == 1)
    {
        spdlog::get(LOG_NAME)->info("Initial primary information by the file from simulation");
        WritePVInfo_Geant4();
        spdlog::get(LOG_NAME)->info("Initial hit information by the file from simulation");
        WriteHitInfo_Geant4();
    }
    if (Parameters::Instance()->TruthFileType == 2) // only truth hit information
    {
        spdlog::get(LOG_NAME)->info("Initial primary information by flux file");
        WritePVInfo_Flux();
    }
    if (Parameters::Instance()->TruthFileType == 3) // only truth hit information
    {
        spdlog::get(LOG_NAME)->info("Initial primary and hit information by rec file");
        WritePVAndHitInfo_Rec();
    }
}

void Mixture_Calo::InitEventStructure()
{
    if (Parameters::Instance()->InitTruthInfo)
    {
        spdlog::get(LOG_NAME)->info("Init Max Event Number from Truth File");
        if (Parameters::Instance()->TruthFileType == 1)
        {
            TTree *StepTree = ((TTree *)Sim_file->Get("shower"))->CopyTree(Form("trackID<=2&&processName==\"none\"&&totalEnDep==0&&primaryID==trackID"));
            max_Eventseq = StepTree->GetMaximum("event") + 1;
        }
        else if (Parameters::Instance()->TruthFileType == 2) // only truth hit information
        {
            max_Eventseq = 1;
        }
        else if (Parameters::Instance()->TruthFileType == 3)
        {
            TTree *ReadoutInfo = (TTree *)ReadoutFile->Get("ReadoutInfo");
            max_Eventseq = ReadoutInfo->GetMaximum("event") + 1;
        }
    }
    else
    {
        spdlog::get(LOG_NAME)->info("Init Max Event Number from Data File");

        if (Parameters::Instance()->DataFileType == 1)
        {
            spdlog::get(LOG_NAME)->info("\tFrom OutTrigd file");

            TTree *ScintillatorTree = (TTree *)Trig_file->Get("tree");
            if (ScintillatorTree->GetBranch("event") != NULL)
            {

                spdlog::get(LOG_NAME)->info("\thave event branch in trigger file");

                max_Eventseq = ScintillatorTree->GetMaximum("event") + 1;
            }
            else
            {
                max_Eventseq = ScintillatorTree->GetEntries();
            }
        }
        else if (Parameters::Instance()->DataFileType == 2)
        {
            spdlog::get(LOG_NAME)->info("\tFrom Rec file");
            TTree *ReadoutInfo = (TTree *)ReadoutFile->Get("ReadoutInfo");
            max_Eventseq = ReadoutInfo->GetMaximum("event") + 1;
        }
    }
    spdlog::get(LOG_NAME)->info("Initial data structure for event information of calorimeter");
    InitEventInfo();
    for (auto RegionSeq : region_container)
    {
        auto tmpRegion = RegionSeq.second;
        tmpRegion->InitEventInfo();
    }
    for (auto ModuleSeq : module_container)
    {
        auto tmpModule = ModuleSeq.second;
        tmpModule->InitEventInfo();
        for (auto layer : tmpModule->v_layer)
        {
            layer->InitEventInfo();
            for (auto cell : layer->v_cell)
            {
                cell->InitEventInfo();
            }
        }
    }
}

void Mixture_Calo::ConstructFromRawFile()
{

    ((TH2I *)Trig_file->Get("module_ID_map"))->Copy(module_map);

    TTree *paraTree = (TTree *)Sim_file->Get("parameters");

    double Zshift;
    paraTree->SetBranchAddress("Zshift", &Zshift);
    paraTree->GetEntry(0);

    PVZShift = Zshift;

    spdlog::get(LOG_NAME)->info("Construct calorimeter");
    ConstructCalo();
    spdlog::get(LOG_NAME)->info("Construct Regions");
    ConstructRegion();
    spdlog::get(LOG_NAME)->info("Construct modules and cells");
    ConstructModule();

    paraTree->Delete();
}

void Mixture_Calo::ConstructCalo()
{
    TTree *CaloTree = (TTree *)Sim_file->Get("calorimeter");
    float calo_x;
    CaloTree->SetBranchAddress("x", &calo_x);
    float calo_y;
    CaloTree->SetBranchAddress("y", &calo_y);
    float calo_z;
    CaloTree->SetBranchAddress("z", &calo_z);
    float calo_dx;
    CaloTree->SetBranchAddress("dx", &calo_dx);
    float calo_dy;
    CaloTree->SetBranchAddress("dy", &calo_dy);
    float calo_dz;
    CaloTree->SetBranchAddress("dz", &calo_dz);
    CaloTree->GetEntry(0);

    X = calo_x;
    Y = calo_y;
    Z = calo_z;
    Dx = calo_dx;
    Dy = calo_dy;
    Dz = calo_dz;
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Geometry of calorimeter");
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t X = {}", X);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t Y = {}", Y);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t Z = {}", Z);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t Dx = {}", Dx);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t Dy = {}", Dy);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t Dz = {}", Dz);

    ProgramType = Parameters::Instance()->ProgramType;
    CluFlow = Parameters::Instance()->CluFlow;

    int seed = time(NULL) + IntputNum;
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Random seed is = {}", seed);
    CalRand = new TRandom(seed);
}

void Mixture_Calo::ConstructRegion()
{
    std::stringstream tmp_name;

    TTree *paraTree = (TTree *)Sim_file->Get("parameters");
    int ecal_position;
    paraTree->SetBranchAddress("ecal_position", &ecal_position);
    std::vector<int> *SiTiming_materials = 0;
    std::vector<double> *SiTiming_layers = 0;
    double SiTiming_thickness = 0;
    bool ConsiderSiTiming = 0;
    if (paraTree->GetBranch("SiTiming_materials") != NULL)
    {
        paraTree->SetBranchAddress("SiTiming_materials", &SiTiming_materials);
        paraTree->SetBranchAddress("SiTiming_layers", &SiTiming_layers);
        paraTree->SetBranchAddress("SiTiming_thickness", &SiTiming_thickness);
        ConsiderSiTiming = 1;
    }

    double cell_separator_position;
    paraTree->SetBranchAddress("cell_separator_position", &cell_separator_position);
    double separation_thickness;
    paraTree->SetBranchAddress("separation_thickness", &separation_thickness);
    std::vector<double> *CellPositionZ = 0;
    paraTree->SetBranchAddress("CellPositionZ", &CellPositionZ);
    int technology;
    paraTree->SetBranchAddress("technology", &technology);
    double AbsSizeX;
    paraTree->SetBranchAddress("AbsSizeX", &AbsSizeX);
    double AbsSizeY;
    paraTree->SetBranchAddress("AbsSizeY", &AbsSizeY);
    int shashlik_tile_holes_number_x;
    paraTree->SetBranchAddress("shashlik_tile_holes_number_x", &shashlik_tile_holes_number_x);
    int shashlik_tile_holes_number_y;
    paraTree->SetBranchAddress("shashlik_tile_holes_number_y", &shashlik_tile_holes_number_y);

    TTree *PrimaryTree = (TTree *)Sim_file->Get("primaries");
    int event;
    PrimaryTree->SetBranchAddress("event", &event);

    TTree *AbsorberTree = (TTree *)Sim_file->Get("absorbers");

    TTree *ModuleTree = (TTree *)Sim_file->Get("modules");

    TTree *CellTree = (TTree *)Sim_file->Get("cells");

    if (paraTree->GetEntries() == 1) // if only have one region : must be module study
    {
        spdlog::get(LOG_NAME)->info("Only have 1 Region");
        TotalRawRegion = 1;

        ModuleStudy = 1; // set moduleStudy = 1;
        paraTree->GetEntry(0);
        TTree *RegionCellTree = CellTree->CopyTree(Form("type==%d", ecal_position));
        float cell_Z;
        RegionCellTree->SetBranchAddress("z", &cell_Z);
        float cellDz;
        RegionCellTree->SetBranchAddress("dz", &cellDz);
        region_container.emplace(ecal_position, new FRegion(ecal_position, ecal_position));

        region_container.at(ecal_position)->SiTimingThickness = SiTiming_thickness;
        region_container.at(ecal_position)->SiTimingCellSize = Parameters::Instance()->main_config.SiTimingCellSize;

        auto tmpCell3DMap = CalCulateMapByTreeFrom_Simulation(CellTree, ecal_position);
        region_container.at(ecal_position)->cell3D_map = *tmpCell3DMap;
        region_container.at(ecal_position)->CellNx = tmpCell3DMap->GetXaxis()->GetNbins();
        region_container.at(ecal_position)->CellNy = tmpCell3DMap->GetYaxis()->GetNbins();
        delete tmpCell3DMap;
        tmp_name.str("");
        tmp_name << "Cell3DMap_region_" << ecal_position;
        region_container.at(ecal_position)->cell3D_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
        region_container.at(ecal_position)->Cell3DSize = region_container.at(ecal_position)->cell3D_map.GetXaxis()->GetBinWidth(2);

        if (technology == 0) // if SPACAL
        {
            spdlog::get(LOG_NAME)->info("\tregion {} is SPACAL region", ecal_position);
            region_container.at(ecal_position)->Separator_position = cell_separator_position;
            ////////////////////
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Separator_position = {}", region_container.at(ecal_position)->Separator_position);
            ///////////////////
            region_container.at(ecal_position)->detector_type = 0;
            float tmp_sint_pos = -7777;

            for (int cellLoop = 0; cellLoop < RegionCellTree->GetEntries(); cellLoop++)
            {
                RegionCellTree->GetEntry(cellLoop);
                if (fabs(tmp_sint_pos - cell_Z) > 1e-6) // a new layer pos
                {
                    tmp_sint_pos = cell_Z;
                    RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                    tempLayerInfo->layerPos = round(cell_Z * 1000.0) / 1000.0;
                    tempLayerInfo->layerType = 0;
                    tempLayerInfo->layerThickness = round(cellDz * 1000.0) / 1000.0;
                    region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                }
            }
            if (ConsiderSiTiming && SiTiming_materials->size() > 0) // if exist sitiming layers
            {
                spdlog::get(LOG_NAME)->info("\t\tregion {} has silicon layers !", ecal_position);
                region_container.at(ecal_position)->Have_SiTiming = 1;
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "separation_thickness = {}", separation_thickness);
                float tmp_thickness = -separation_thickness / 2 + cell_separator_position;
                for (int l = 0; l < SiTiming_materials->size(); l++)
                {

                    if (SiTiming_materials->at(l) == 3) // material == si
                    {
                        tmp_thickness += SiTiming_layers->at(l) / 2;

                        RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                        tempLayerInfo->layerPos = round(tmp_thickness * 1000.0) / 1000.0;
                        tempLayerInfo->layerType = 1;
                        tempLayerInfo->layerThickness = round(SiTiming_layers->at(l) * 1000.0) / 1000.0;
                        region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);

                        region_container.at(ecal_position)->SiTimingLayerNum++;
                        tmp_thickness += SiTiming_layers->at(l) / 2;
                    }
                    else
                    {
                        tmp_thickness += SiTiming_layers->at(l);
                    }
                }
            }
        }
        else if (technology == 1) // if shashlik
        {
            spdlog::get(LOG_NAME)->info("\tregion {} is Shashlik region", ecal_position);

            region_container.at(ecal_position)->detector_type = 1;
            region_container.at(ecal_position)->FiberNumX = shashlik_tile_holes_number_x;
            region_container.at(ecal_position)->FiberNumY = shashlik_tile_holes_number_y;
            TTree *tmpTree = AbsorberTree->CopyTree(Form("type==%d", ecal_position), "", 1);
            region_container.at(ecal_position)->Separator_position = tmpTree->GetLeaf("separation_z")->GetValue();
            ////////////////////
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Separator_position = {}", region_container.at(ecal_position)->Separator_position);
            ///////////////////

            if (ConsiderSiTiming && SiTiming_materials->size() > 0) // if exist sitiming layers
            {
                spdlog::get(LOG_NAME)->info("\t\tregion {} has silicon layers !", ecal_position);

                region_container.at(ecal_position)->Have_SiTiming = 1;

                TTree *tmpTree = ModuleTree->CopyTree(Form("type==%d", ecal_position), "", 1);
                float module_dz = tmpTree->GetLeaf("dz")->GetValue();

                float tmp_thickness = -(module_dz) / 2; // 由于SiTiming 层放在shashlik之前
                for (int l = 0; l < SiTiming_materials->size(); l++)
                {

                    if (SiTiming_materials->at(l) == 3) // material == si
                    {

                        tmp_thickness += SiTiming_layers->at(l) / 2;

                        region_container.at(ecal_position)->SiTimingLayerNum++;

                        RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                        tempLayerInfo->layerPos = round(tmp_thickness * 1000.0) / 1000.0;
                        tempLayerInfo->layerType = 1;
                        tempLayerInfo->layerThickness = round(SiTiming_layers->at(l) * 1000.0) / 1000.0;
                        region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                        tmp_thickness += SiTiming_layers->at(l) / 2;
                    }
                    else
                    {
                        tmp_thickness += SiTiming_layers->at(l);
                    }
                }
            }

            float tmp_sint_pos = -7777;

            for (int cellLoop = 0; cellLoop < RegionCellTree->GetEntries(); cellLoop++)
            {
                RegionCellTree->GetEntry(cellLoop);

                if (fabs(tmp_sint_pos - cell_Z) > 1e-6) // a new layer pos
                {

                    tmp_sint_pos = cell_Z;

                    RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                    tempLayerInfo->layerPos = round(cell_Z * 1000.0) / 1000.0;
                    tempLayerInfo->layerType = 0;
                    tempLayerInfo->layerThickness = round(cellDz * 1000.0) / 1000.0;
                    region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                }
            }
        }
        std::sort(region_container.at(ecal_position)->Layers.begin(), region_container.at(ecal_position)->Layers.end(), CmpLayerPos); // sort by layer pos

        int TotalRegLayerNums = region_container.at(ecal_position)->Layers.size();
        spdlog::get(LOG_NAME)->info("region {} has {} layers !", ecal_position, TotalRegLayerNums);
        region_container.at(ecal_position)->TotLayerNum = TotalRegLayerNums;
        for (int layer_id = 0; layer_id < TotalRegLayerNums; layer_id++)
        {
            spdlog::get(LOG_NAME)->info("\tLayer {} in relative position z = {} !", layer_id, region_container.at(ecal_position)->Layers[layer_id]->layerPos);

            TotalLayerNumInAllRegion++;

            if (region_container.at(ecal_position)->Layers[layer_id]->layerType == 0) // if Scintillator
            {
                tmp_name.str("");
                tmp_name << "cell_map_region_" << ecal_position << "_layer" << layer_id;
                TH2I *Scintillator_map = CalCulateMapByTreeFrom_Simulation(CellTree, ecal_position);
                region_container.at(ecal_position)->Layers[layer_id]->cell_map = *Scintillator_map;

                region_container.at(ecal_position)->Layers[layer_id]->cell_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
                region_container.at(ecal_position)->Layers[layer_id]->CellSize = Scintillator_map->GetXaxis()->GetBinWidth(2);
                delete Scintillator_map;
            }
            else if (region_container.at(ecal_position)->Layers[layer_id]->layerType == 1) // if Silicon
            {
                tmp_name.str("");
                tmp_name << "cell_map_region_" << ecal_position << "_layer" << layer_id;
                auto tmpSiliconMap = CalCulateLayerMap(AbsSizeX, region_container.at(ecal_position)->SiTimingCellSize, AbsSizeY, region_container.at(ecal_position)->SiTimingCellSize);
                region_container.at(ecal_position)->Layers[layer_id]->cell_map = *tmpSiliconMap;

                region_container.at(ecal_position)->Layers[layer_id]->cell_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
                region_container.at(ecal_position)->Layers[layer_id]->CellSize = region_container[ecal_position]->Layers[layer_id]->cell_map.GetXaxis()->GetBinWidth(1);
                delete tmpSiliconMap;
            }
        }

        region_container.at(ecal_position)->FrontFace = region_container.at(ecal_position)->Layers.at(0)->layerPos - region_container.at(ecal_position)->Layers.at(0)->layerThickness / 2;
        ////////////////////
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Front face = {}", region_container.at(ecal_position)->FrontFace);
        ///////////////////
        region_container.at(ecal_position)->f_calo = this;
    }
    else
    {
        ModuleStudy = 0; // set moduleStudy = 0;
        RegionStudy = 0;
        int TotalCfgNum = paraTree->GetEntries();
        TotalRawRegion = TotalCfgNum - 1;
        spdlog::get(LOG_NAME)->info("Have {} region.", TotalRawRegion);

        for (int i = 0; i < TotalCfgNum; i++)
        {
            paraTree->GetEntry(i);
            TTree *RegionCellTree = CellTree->CopyTree(Form("type==%d", ecal_position));
            float cell_Z;
            RegionCellTree->SetBranchAddress("z", &cell_Z);
            float cellDz;
            RegionCellTree->SetBranchAddress("dz", &cellDz);
            if (ecal_position == 0)
            {
                if (TotalCfgNum == 2)
                {
                    RegionStudy = 1;
                }
                continue;
            }

            region_container.emplace(ecal_position, new FRegion(ecal_position, ecal_position));

            region_container.at(ecal_position)->SiTimingThickness = SiTiming_thickness;

            if (RegionStudy)
                region_container.at(ecal_position)->SiTimingCellSize = Parameters::Instance()->main_config.SiTimingCellSize;
            else
                region_container.at(ecal_position)->SiTimingCellSize = Parameters::Instance()->module_config.at(ecal_position).SiTimingCellSize;

            auto tmpCell3DMap = CalCulateMapByTreeFrom_Simulation(CellTree, ecal_position);
            region_container.at(ecal_position)->cell3D_map = *tmpCell3DMap;
            delete tmpCell3DMap;
            tmp_name.str("");
            tmp_name << "Cell3DMap_region_" << ecal_position;
            region_container.at(ecal_position)->cell3D_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
            region_container.at(ecal_position)->Cell3DSize = region_container.at(ecal_position)->cell3D_map.GetXaxis()->GetBinWidth(2);

            if (technology == 0) // if SPACAL
            {
                spdlog::get(LOG_NAME)->info("\tregion {} is SPACAL region", ecal_position);
                region_container.at(ecal_position)->Separator_position = cell_separator_position;
                ////////////////////
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Separator_position = {}", region_container.at(ecal_position)->Separator_position);
                ///////////////////
                region_container.at(ecal_position)->detector_type = 0;
                float tmp_sint_pos = -7777;

                for (int cellLoop = 0; cellLoop < RegionCellTree->GetEntries(); cellLoop++)
                {
                    RegionCellTree->GetEntry(cellLoop);

                    if (fabs(tmp_sint_pos - cell_Z) > 1e-6) // a new layer pos
                    {

                        tmp_sint_pos = cell_Z;

                        RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                        tempLayerInfo->layerPos = round(cell_Z * 1000.0) / 1000.0;
                        tempLayerInfo->layerType = 0;
                        tempLayerInfo->layerThickness = round(cellDz * 1000.0) / 1000.0;
                        region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                    }
                }
                if (ConsiderSiTiming && SiTiming_materials->size() > 0) // if exist sitiming layers
                {
                    spdlog::get(LOG_NAME)->info("\t\tregion {} has silicon layers !", ecal_position);
                    region_container.at(ecal_position)->Have_SiTiming = 1;
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "separation_thickness = {}", separation_thickness);
                    float tmp_thickness = -separation_thickness / 2 + cell_separator_position;
                    for (int l = 0; l < SiTiming_materials->size(); l++)
                    {

                        if (SiTiming_materials->at(l) == 3) // material == si
                        {

                            tmp_thickness += SiTiming_layers->at(l) / 2;
                            RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                            tempLayerInfo->layerPos = round(tmp_thickness * 1000.0) / 1000.0;
                            tempLayerInfo->layerType = 1;
                            tempLayerInfo->layerThickness = round(SiTiming_layers->at(l) * 1000.0) / 1000.0;
                            region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                            region_container.at(ecal_position)->SiTimingLayerNum++;
                            tmp_thickness += SiTiming_layers->at(l) / 2;
                        }
                        else
                        {
                            tmp_thickness += SiTiming_layers->at(l);
                        }
                    }
                }
            }
            else if (technology == 1) // if shashlik
            {
                spdlog::get(LOG_NAME)->info("\tregion {} is Shashlik region", ecal_position);
                TTree *tmpTree = AbsorberTree->CopyTree(Form("type==%d", ecal_position), "", 1);
                region_container.at(ecal_position)->Separator_position = tmpTree->GetLeaf("separation_z")->GetValue();
                region_container.at(ecal_position)->detector_type = 1;
                region_container.at(ecal_position)->FiberNumX = shashlik_tile_holes_number_x;
                region_container.at(ecal_position)->FiberNumY = shashlik_tile_holes_number_y;
                ////////////////////
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Separator_position = {}", region_container.at(ecal_position)->Separator_position);
                ///////////////////

                if (ConsiderSiTiming && SiTiming_materials->size() > 0) // if exist sitiming layers
                {
                    spdlog::get(LOG_NAME)->info("\t\tregion {} has silicon layers !", ecal_position);

                    region_container.at(ecal_position)->Have_SiTiming = 1;

                    TTree *tmpTree = ModuleTree->CopyTree(Form("type==%d", ecal_position), "", 1);
                    float module_dz = tmpTree->GetLeaf("dz")->GetValue();

                    float tmp_thickness = -(module_dz) / 2; // 由于SiTiming 层放在shashlik之前
                    for (int l = 0; l < SiTiming_materials->size(); l++)
                    {

                        if (SiTiming_materials->at(l) == 3) // material == si
                        {

                            tmp_thickness += SiTiming_layers->at(l) / 2;
                            RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                            tempLayerInfo->layerPos = round(tmp_thickness * 1000.0) / 1000.0;
                            tempLayerInfo->layerType = 1;
                            tempLayerInfo->layerThickness = round(SiTiming_layers->at(l) * 1000.0) / 1000.0;
                            region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                            region_container.at(ecal_position)->SiTimingLayerNum++;
                            tmp_thickness += SiTiming_layers->at(l) / 2;
                        }
                        else
                        {
                            tmp_thickness += SiTiming_layers->at(l);
                        }
                    }
                }

                float tmp_spacal_pos = -7777;

                for (int cellLoop = 0; cellLoop < RegionCellTree->GetEntries(); cellLoop++)
                {

                    RegionCellTree->GetEntry(cellLoop);

                    if (fabs(tmp_spacal_pos - cell_Z) > 1e-6) // a new layer pos
                    {
                        RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();
                        tempLayerInfo->layerPos = round(cell_Z * 1000.0) / 1000.0;
                        tempLayerInfo->layerType = 0;
                        tempLayerInfo->layerThickness = round(cellDz * 1000.0) / 1000.0;
                        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "emplace_back Layer at region {}, layer pos is {}", ecal_position, cell_Z);
                        region_container.at(ecal_position)->Layers.emplace_back(tempLayerInfo);
                        tmp_spacal_pos = cell_Z;
                    }
                }
            }

            std::sort(region_container.at(ecal_position)->Layers.begin(), region_container.at(ecal_position)->Layers.end(), CmpLayerPos); // sort by layer pos

            int TotalRegLayerNums = region_container.at(ecal_position)->Layers.size();
            spdlog::get(LOG_NAME)->info("region {} has {} layers !", ecal_position, TotalRegLayerNums);
            region_container.at(ecal_position)->TotLayerNum = TotalRegLayerNums;
            for (int layer_id = 0; layer_id < region_container.at(ecal_position)->Layers.size(); layer_id++)
            {
                spdlog::get(LOG_NAME)->info("\tLayer {} in relative position z = {} !", layer_id, region_container.at(ecal_position)->Layers[layer_id]->layerPos);

                TotalLayerNumInAllRegion++;

                if (region_container.at(ecal_position)->Layers[layer_id]->layerType == 0) // if Scintillator
                {
                    tmp_name.str("");
                    tmp_name << "cell_map_region_" << ecal_position << "_layer" << layer_id;
                    TH2I *Scintillator_map = CalCulateMapByTreeFrom_Simulation(CellTree, ecal_position);
                    region_container.at(ecal_position)->Layers[layer_id]->cell_map = *Scintillator_map;
                    region_container.at(ecal_position)->Layers[layer_id]->cell_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
                    region_container.at(ecal_position)->Layers[layer_id]->CellSize = Scintillator_map->GetXaxis()->GetBinWidth(2);
                    delete Scintillator_map;
                }
                else if (region_container.at(ecal_position)->Layers[layer_id]->layerType == 1) // if Silicon
                {
                    tmp_name.str("");
                    tmp_name << "cell_map_region_" << ecal_position << "_layer" << layer_id;
                    auto tmpSiliconMap = CalCulateLayerMap(AbsSizeX, region_container.at(ecal_position)->SiTimingCellSize, AbsSizeY, region_container.at(ecal_position)->SiTimingCellSize);
                    region_container.at(ecal_position)->Layers[layer_id]->cell_map = *tmpSiliconMap;

                    region_container.at(ecal_position)->Layers[layer_id]->cell_map.SetNameTitle(tmp_name.str().c_str(), tmp_name.str().c_str());
                    region_container.at(ecal_position)->Layers[layer_id]->CellSize = region_container[ecal_position]->Layers[layer_id]->cell_map.GetXaxis()->GetBinWidth(1);
                    delete tmpSiliconMap;
                }
            }
            region_container.at(ecal_position)->FrontFace = region_container.at(ecal_position)->Layers.at(0)->layerPos - region_container.at(ecal_position)->Layers.at(0)->layerThickness / 2;
            ////////////////////
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Front face = {}", region_container.at(ecal_position)->FrontFace);
            ///////////////////
            region_container.at(ecal_position)->f_calo = this;
        }
    }
}

void Mixture_Calo::ConstructModule()
{
    ////////////////////////////////////////////////////
    //                Get Module info
    ///////////////////////////////////////////////////
    TTree *moduleTree = (TTree *)Sim_file->Get("modules");
    float module_x;
    moduleTree->SetBranchAddress("x", &module_x);
    float module_y;
    moduleTree->SetBranchAddress("y", &module_y);
    float module_z;
    moduleTree->SetBranchAddress("z", &module_z);
    float module_dx;
    moduleTree->SetBranchAddress("dx", &module_dx);
    float module_dy;
    moduleTree->SetBranchAddress("dy", &module_dy);
    float module_dz;
    moduleTree->SetBranchAddress("dz", &module_dz);
    int module_id;
    moduleTree->SetBranchAddress("ID", &module_id);
    int module_type;
    moduleTree->SetBranchAddress("type", &module_type);
    float shift_z;
    moduleTree->SetBranchAddress("shift_z", &shift_z);

    bool angles_in_modules_found = 0;
    float module_ax = 0.;
    float module_ay = 0.;
    float module_az = 0.;
    if (moduleTree->GetBranch("angle_x") != NULL)
    {
        angles_in_modules_found = 1;
    }

    if (angles_in_modules_found)
    {
        moduleTree->SetBranchAddress("angle_x", &module_ax);
        moduleTree->SetBranchAddress("angle_y", &module_ay);
        moduleTree->SetBranchAddress("angle_z", &module_az);
    }

    if (moduleTree->GetEntries() == 1) // if only have one module
        ModuleStudy = 1;

    std::stringstream tmp_name;
    for (int i = 0; i < moduleTree->GetEntries(); i++)
    {
        moduleTree->GetEntry(i);
        //////////////////
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "Geometry of Module {} in region {}", module_id, module_type);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t X = {}", module_x);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Y = {}", module_y);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Z = {}", module_z);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Dx = {}", module_dx);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Dy = {}", module_dy);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Dz = {}", module_dz);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Ax = {}", module_ax);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Ay = {}", module_ay);
        SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "\t Az = {}", module_az);
        //////////////////

        if (fabs(region_container.at(module_type)->RegionShift - 7777) < 1e-5)
        {
            region_container.at(module_type)->RegionShift = module_z + shift_z;

            spdlog::get(LOG_NAME)->info("Region {}, z Shift :{}", module_type, region_container.at(module_type)->RegionShift);
        }
        if (!module_container.count(module_id))
        {

            module_container.emplace(module_id, new FModule(module_type, module_id, module_x, module_y, module_z, module_dx, module_dy, module_dz, module_ax, module_ay, module_az));
            module_container[module_id]->f_region = region_container.at(module_type);
            module_container[module_id]->f_region->module_container.emplace_back(module_container[module_id]);
            module_container[module_id]->f_calo = this;

            for (int layer_id = 0; layer_id < region_container.at(module_type)->Layers.size(); layer_id++)
            {
                module_container[module_id]->v_layer.emplace_back(new FLayer(layer_id, region_container.at(module_type)->Layers[layer_id]->layerPos));
                module_container[module_id]->v_layer.at(layer_id)->f_module = module_container[module_id];
                module_container[module_id]->v_layer.at(layer_id)->layerType = region_container.at(module_type)->Layers[layer_id]->layerType;
                module_container[module_id]->v_layer.at(layer_id)->layerThickness = region_container.at(module_type)->Layers[layer_id]->layerThickness;

                TH2I tmp_cell_map = region_container.at(module_type)->Layers[layer_id]->cell_map;

                // //std::cout << tmp_cell_map.GetName() << std::endl;

                int tmp_xbin = tmp_cell_map.GetNbinsX();
                int tmp_ybin = tmp_cell_map.GetNbinsY();
                module_container[module_id]->v_layer[layer_id]->v_cell.resize(tmp_xbin * tmp_ybin);
                for (int x_loop = 1; x_loop <= tmp_xbin; x_loop++) // loop cell map
                    for (int y_loop = 1; y_loop <= tmp_ybin; y_loop++)
                    {
                        float tmp_cell_x = tmp_cell_map.GetXaxis()->GetBinCenter(x_loop);
                        float tmp_cell_y = tmp_cell_map.GetYaxis()->GetBinCenter(y_loop);
                        float tmp_cell_z = region_container.at(module_type)->Layers[layer_id]->layerPos;
                        float tmp_cell_dz = region_container.at(module_type)->Layers[layer_id]->layerThickness;
                        float tmp_cell_xsize = tmp_cell_map.GetXaxis()->GetBinWidth(x_loop);
                        float tmp_cell_ysize = tmp_cell_map.GetYaxis()->GetBinWidth(y_loop);
                        int tmp_cell_id = tmp_cell_map.GetBinContent(tmp_cell_map.GetBin(x_loop, y_loop));
                        // //std::cout << " cell : " << tmp_cell_id << " x : " << tmp_cell_x + module_x << " y : " << tmp_cell_y + module_y << std::endl;
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id] = new FCell(tmp_cell_id, tmp_cell_x, tmp_cell_y, tmp_cell_z, tmp_cell_xsize, tmp_cell_ysize, tmp_cell_dz, module_ax, module_ay, module_az);
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherModule(module_container[module_id]);
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherLayer(module_container[module_id]->v_layer.at(layer_id));
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetLayerID(layer_id);
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetGlobal_id(FCell::CalculateGlobalId(module_type, module_id, tmp_cell_id, layer_id));

                        // //std::cout << "module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->Global_id : " << module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->Global_id << std::endl;
                    }
            }
        }
        else
        {
            module_id = module_container.size();
            module_container.emplace(module_id, new FModule(module_type, module_id, module_x, module_y, module_z, module_dx, module_dy, module_dz, module_ax, module_ay, module_az));
            module_container[module_id]->f_region = region_container[module_type];
            module_container[module_id]->f_region->module_container.emplace_back(module_container[module_id]);
            module_container[module_id]->f_calo = this;

            for (int layer_id = 0; layer_id < region_container.at(module_type)->Layers.size(); layer_id++)
            {
                module_container[module_id]->v_layer.emplace_back(new FLayer(layer_id, region_container.at(module_type)->Layers[layer_id]->layerPos));
                module_container[module_id]->v_layer.at(layer_id)->f_module = module_container[module_id];
                module_container[module_id]->v_layer.at(layer_id)->layerType = region_container.at(module_type)->Layers[layer_id]->layerType;
                module_container[module_id]->v_layer.at(layer_id)->layerThickness = region_container.at(module_type)->Layers[layer_id]->layerThickness;

                TH2I tmp_cell_map = region_container.at(module_type)->Layers[layer_id]->cell_map;

                // //std::cout << tmp_cell_map.GetName() << std::endl;

                int tmp_xbin = tmp_cell_map.GetNbinsX();
                int tmp_ybin = tmp_cell_map.GetNbinsY();
                // //std::cout << "tmp_xbin " << tmp_xbin << std::endl;
                // //std::cout << "tmp_ybin " << tmp_xbin << std::endl;
                module_container[module_id]->v_layer[layer_id]->v_cell.resize(tmp_xbin * tmp_ybin);
                for (int x_loop = 1; x_loop <= tmp_xbin; x_loop++) // loop cell map
                    for (int y_loop = 1; y_loop <= tmp_ybin; y_loop++)
                    {
                        // //std::cout << "x_loop " << x_loop << std::endl;
                        // //std::cout << "y_loop " << y_loop << std::endl;
                        float tmp_cell_x = tmp_cell_map.GetXaxis()->GetBinCenter(x_loop);
                        float tmp_cell_y = tmp_cell_map.GetYaxis()->GetBinCenter(y_loop);
                        float tmp_cell_z = region_container.at(module_type)->Layers[layer_id]->layerPos;
                        float tmp_cell_dz = region_container.at(module_type)->Layers[layer_id]->layerThickness;
                        float tmp_cell_xsize = tmp_cell_map.GetXaxis()->GetBinWidth(x_loop);
                        float tmp_cell_ysize = tmp_cell_map.GetYaxis()->GetBinWidth(y_loop);
                        int tmp_cell_id = tmp_cell_map.GetBinContent(tmp_cell_map.GetBin(x_loop, y_loop));
                        // //std::cout << " cell : " << tmp_cell_id << " x : " << tmp_cell_x + module_x << " y : " << tmp_cell_y + module_y << std::endl;
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id] = new FCell(tmp_cell_id, tmp_cell_x, tmp_cell_y, tmp_cell_z, tmp_cell_xsize, tmp_cell_ysize, tmp_cell_dz, module_ax, module_ay, module_az);
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherModule(module_container[module_id]);
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherLayer(module_container[module_id]->v_layer.at(layer_id));
                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetLayerID(layer_id);

                        module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->SetGlobal_id(FCell::CalculateGlobalId(module_type, module_id, tmp_cell_id, layer_id));

                        // //std::cout << "module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->Global_id : " << module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->GetID() << std::endl;
                    }
            }
        }
    }
    // std::cout << module_container.size() << std::endl;
}

void Mixture_Calo::Construct()
{

    std::stringstream tmp_name;

    ((TH2I *)GeoFile->Get("moduleMap"))->Copy(module_map);

    ProgramType = Parameters::Instance()->ProgramType;
    CluFlow = Parameters::Instance()->CluFlow;
    int seed = time(NULL);
    CalRand = new TRandom(seed);

    TTree *CaloTree = (TTree *)GeoFile->Get("Calo");
    float CaloX;
    CaloTree->SetBranchAddress("x", &CaloX);
    float CaloY;
    CaloTree->SetBranchAddress("y", &CaloY);
    float CaloZ;
    CaloTree->SetBranchAddress("x", &CaloZ);
    float CaloDx;
    CaloTree->SetBranchAddress("dx", &CaloDx);
    float CaloDy;
    CaloTree->SetBranchAddress("dy", &CaloDy);
    float CaloDz;
    CaloTree->SetBranchAddress("dz", &CaloDz);
    CaloTree->GetEntry(0);
    X = CaloX;
    Y = CaloY;
    Z = CaloZ;
    Dx = CaloDx;
    Dy = CaloDy;
    Dz = CaloDz;

    TTree *RegionTree = (TTree *)GeoFile->Get("Region");
    int regionID;
    int RegionType;
    int Have_SiTiming;
    float SiTimingThickness;
    int SiTimingLayerNum;
    float SiTimingCellSize;
    float Cell3DSize;
    float Separator_position;
    int detector_type;
    float RegionShift;
    int TotLayerNum;
    float FrontFace;
    float r_ax;
    float r_ay;
    float r_az;
    float FiberNumX;
    float FiberNumY;

    std::vector<int> *layerId = 0;
    std::vector<float> *layerPos = 0;
    std::vector<int> *layerType = 0;
    std::vector<float> *layerThickness = 0;

    RegionTree->SetBranchAddress("regionID", &regionID);
    RegionTree->SetBranchAddress("RegionType", &RegionType);
    RegionTree->SetBranchAddress("Have_SiTiming", &Have_SiTiming);
    RegionTree->SetBranchAddress("SiTimingLayerNum", &SiTimingLayerNum);
    RegionTree->SetBranchAddress("detector_type", &detector_type);
    RegionTree->SetBranchAddress("SiTimingThickness", &SiTimingThickness);
    RegionTree->SetBranchAddress("SiTimingCellSize", &SiTimingCellSize);
    RegionTree->SetBranchAddress("Cell3DSize", &Cell3DSize);
    RegionTree->SetBranchAddress("Separator_position", &Separator_position);
    RegionTree->SetBranchAddress("RegionShift", &RegionShift);
    RegionTree->SetBranchAddress("TotLayerNum", &TotLayerNum);
    RegionTree->SetBranchAddress("FrontFace", &FrontFace);
    RegionTree->SetBranchAddress("layerId", &layerId);
    RegionTree->SetBranchAddress("layerPos", &layerPos);
    RegionTree->SetBranchAddress("layerType", &layerType);
    RegionTree->SetBranchAddress("layerThickness", &layerThickness);
    RegionTree->SetBranchAddress("ax", &r_ax);
    RegionTree->SetBranchAddress("ay", &r_ay);
    RegionTree->SetBranchAddress("az", &r_az);
    RegionTree->SetBranchAddress("FiberNumX", &FiberNumX);
    RegionTree->SetBranchAddress("FiberNumY", &FiberNumY);

    for (int loop = 0; loop < RegionTree->GetEntries(); loop++)
    {
        RegionTree->GetEntry(loop);
        spdlog::get(LOG_NAME)->info("region {} has {} layers !", regionID, layerId->size());
        region_container.emplace(regionID, new FRegion(RegionType, regionID));

        region_container.at(regionID)->Have_SiTiming = Have_SiTiming;
        region_container.at(regionID)->SiTimingLayerNum = SiTimingLayerNum;
        region_container.at(regionID)->detector_type = detector_type;
        region_container.at(regionID)->SiTimingThickness = SiTimingThickness;
        region_container.at(regionID)->SiTimingCellSize = SiTimingCellSize;
        region_container.at(regionID)->Cell3DSize = Cell3DSize;
        region_container.at(regionID)->Separator_position = Separator_position;
        region_container.at(regionID)->RegionShift = RegionShift;
        region_container.at(regionID)->TotLayerNum = TotLayerNum;
        region_container.at(regionID)->FrontFace = FrontFace;
        region_container.at(regionID)->ax = r_ax;
        region_container.at(regionID)->ay = r_ay;
        region_container.at(regionID)->az = r_az;
        region_container.at(regionID)->FiberNumX = FiberNumX;
        region_container.at(regionID)->FiberNumY = FiberNumY;
        region_container.at(regionID)->f_calo = this;

        tmp_name.str("");
        tmp_name << "Region" << regionID << "_Cell3DMap";
        TH2I *Current3DMap = (TH2I *)GeoFile->Get(tmp_name.str().c_str());
        Current3DMap->Copy(region_container.at(regionID)->cell3D_map);
        region_container.at(regionID)->CellNx = Current3DMap->GetXaxis()->GetNbins();
        region_container.at(regionID)->CellNy = Current3DMap->GetYaxis()->GetNbins();

        for (int loopLayer = 0; loopLayer < layerId->size(); loopLayer++)
        {
            TotalLayerNumInAllRegion++;
            RegionLayerInfo *tempLayerInfo = new RegionLayerInfo();

            tempLayerInfo->layerType = layerType->at(loopLayer);
            tempLayerInfo->layerThickness = layerThickness->at(loopLayer);
            tempLayerInfo->layerPos = layerPos->at(loopLayer);
            tmp_name.str("");
            tmp_name << "Region" << regionID << "_Layer" << layerId->at(loopLayer) << "_CellMap";
            TH2I *CurrentMap = (TH2I *)GeoFile->Get(tmp_name.str().c_str());
            CurrentMap->Copy(tempLayerInfo->cell_map);
            tempLayerInfo->CellSize = CurrentMap->GetXaxis()->GetBinWidth(1);

            region_container.at(regionID)->Layers.emplace_back(tempLayerInfo);
            spdlog::get(LOG_NAME)->info("\tLayer {} in relative position z = {} !", layerId->at(loopLayer), layerPos->at(loopLayer));
        }
    }

    TTree *ModuleTree = (TTree *)GeoFile->Get("Module");
    int moduleID;
    float moduleX;
    float moduleY;
    float moduleZ;
    float moduleDx;
    float moduleDy;
    float moduleDz;
    float moduleAx;
    float moduleAy;
    float moduleAz;
    int moduleType;
    int moduleRegionID;

    ModuleTree->SetBranchAddress("moduleID", &moduleID);
    ModuleTree->SetBranchAddress("regionID", &moduleRegionID);
    ModuleTree->SetBranchAddress("x", &moduleX);
    ModuleTree->SetBranchAddress("y", &moduleY);
    ModuleTree->SetBranchAddress("z", &moduleZ);
    ModuleTree->SetBranchAddress("dx", &moduleDx);
    ModuleTree->SetBranchAddress("dy", &moduleDy);
    ModuleTree->SetBranchAddress("dz", &moduleDz);
    ModuleTree->SetBranchAddress("ax", &moduleAx);
    ModuleTree->SetBranchAddress("ay", &moduleAy);
    ModuleTree->SetBranchAddress("az", &moduleAz);
    ModuleTree->SetBranchAddress("moduleType", &moduleType);
    for (int loop = 0; loop < ModuleTree->GetEntries(); loop++)
    {
        ModuleTree->GetEntry(loop);
#if 0
        //std::cout << " loop : " << loop << std::endl;
#endif
        module_container.emplace(moduleID, new FModule(moduleType, moduleID, moduleX, moduleY, moduleZ, moduleDx, moduleDy, moduleDz, moduleAx, moduleAy, moduleAz));
        module_container.at(moduleID)->f_region = region_container.at(moduleRegionID);
        module_container.at(moduleID)->f_region->module_container.emplace_back(module_container[moduleID]);
        module_container.at(moduleID)->f_calo = this;

        for (int layer_id = 0; layer_id < region_container.at(moduleRegionID)->Layers.size(); layer_id++)
        {
#if 0
            //std::cout << "layer : " << layer_id << std::endl;
#endif
            module_container.at(moduleID)->v_layer.emplace_back(new FLayer(layer_id, region_container.at(moduleRegionID)->Layers[layer_id]->layerPos));
            module_container.at(moduleID)->v_layer.at(layer_id)->f_module = module_container.at(moduleID);
            module_container.at(moduleID)->v_layer.at(layer_id)->layerType = region_container.at(moduleRegionID)->Layers[layer_id]->layerType;
            module_container.at(moduleID)->v_layer.at(layer_id)->layerThickness = region_container.at(moduleRegionID)->Layers[layer_id]->layerThickness;
            TH2I tmp_cell_map = region_container.at(moduleRegionID)->Layers.at(layer_id)->cell_map;

            // //std::cout << tmp_cell_map.GetName() << std::endl;
            int tmp_xbin = tmp_cell_map.GetNbinsX();
            int tmp_ybin = tmp_cell_map.GetNbinsY();
            module_container.at(moduleID)->v_layer.at(layer_id)->v_cell.resize(tmp_xbin * tmp_ybin);
            for (int x_loop = 1; x_loop <= tmp_xbin; x_loop++) // loop cell map
                for (int y_loop = 1; y_loop <= tmp_ybin; y_loop++)
                {
                    float tmp_cell_x = tmp_cell_map.GetXaxis()->GetBinCenter(x_loop);
                    float tmp_cell_y = tmp_cell_map.GetYaxis()->GetBinCenter(y_loop);
                    float tmp_cell_z = region_container.at(moduleRegionID)->Layers[layer_id]->layerPos;
                    float tmp_cell_dz = region_container.at(moduleRegionID)->Layers[layer_id]->layerThickness;
                    float tmp_cell_xsize = tmp_cell_map.GetXaxis()->GetBinWidth(x_loop);
                    float tmp_cell_ysize = tmp_cell_map.GetYaxis()->GetBinWidth(y_loop);
                    int tmp_cell_id = tmp_cell_map.GetBinContent(tmp_cell_map.GetBin(x_loop, y_loop));
#if 0
                    //std::cout << " cell : " << tmp_cell_id << " x : " << tmp_cell_x + moduleX << " y : " << tmp_cell_y + moduleY << std::endl;
#endif
                    module_container.at(moduleID)->v_layer[layer_id]->v_cell[tmp_cell_id] = new FCell(tmp_cell_id, tmp_cell_x, tmp_cell_y, tmp_cell_z, tmp_cell_xsize, tmp_cell_ysize, tmp_cell_dz, moduleAx, moduleAy, moduleAz);
                    module_container.at(moduleID)->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherModule(module_container.at(moduleID));
                    module_container.at(moduleID)->v_layer[layer_id]->v_cell[tmp_cell_id]->SetMotherLayer(module_container.at(moduleID)->v_layer.at(layer_id));
                    module_container.at(moduleID)->v_layer[layer_id]->v_cell[tmp_cell_id]->SetLayerID(layer_id);

                    module_container.at(moduleID)->v_layer[layer_id]->v_cell[tmp_cell_id]->SetGlobal_id(FCell::CalculateGlobalId(moduleType, moduleID, tmp_cell_id, layer_id));

                    // //std::cout << "module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->Global_id : " << module_container[module_id]->v_layer[layer_id]->v_cell[tmp_cell_id]->Global_id << std::endl;
                }
        }
    }
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "module_container.size() = {}", module_container.size());

    CaloTree->Delete();
    ModuleTree->Delete();
    RegionTree->Delete();
}

void Mixture_Calo::RecRegion()
{
    module_map.Copy(region_map);
    region_map.SetName("RegionMap");
    region_map.SetTitle("RegionMap");

    module_map.Copy(RawRegionMap);
    RawRegionMap.SetName("RawRegionMap");
    RawRegionMap.SetTitle("RawRegionMap");

    module_map.Copy(regionType_map);
    regionType_map.SetName("RegionTypeMap");
    regionType_map.SetTitle("RegionTypeMap");

    std::map<uint64, FRegion *> NewRegionList;
    std::map<uint64, int> NewTypeList;
    // int FirstRegionID = region_container.begin()->first;
    int RegionType = 0;
    int GlobalId = 0;
    for (auto moduleSeq : module_container)
    {
        FModule *CurrentModule = moduleSeq.second;
        float detector_type = CurrentModule->f_region->detector_type;
        float Cell3DSize = CurrentModule->f_region->Cell3DSize;
        int tmpNXCell = CurrentModule->f_region->cell3D_map.GetXaxis()->GetNbins();
        int tmpNYCell = CurrentModule->f_region->cell3D_map.GetYaxis()->GetNbins();
        float tmpAx = CurrentModule->Ax();
        float tmpAy = CurrentModule->Ay();
        float tmpAz = CurrentModule->Az();
        // //std::cout << "Cell3DSize " << Cell3DSize << " tmpNXCell " << tmpNXCell << " tmpNYCell " << tmpNYCell << std::endl;
        uint64 tmpFullId = CurrentModule->f_region->CalculateFullId(detector_type, tmpAx, tmpAy, tmpAz, tmpNXCell, tmpNYCell);
        // //std::cout << tmpFullId << std::endl;
        uint64 tmpTypeId = CurrentModule->f_region->CalculateTypeId(detector_type, Cell3DSize, tmpAx, tmpAy, tmpAz);
        int tmp_module_bin = module_map.FindBin(CurrentModule->X(), CurrentModule->Y());
        RawRegionMap.SetBinContent(tmp_module_bin, CurrentModule->f_region->type);
        if (!NewRegionList.count(tmpFullId))
        {
            GlobalId++;
            if (!NewTypeList.count(tmpTypeId))
            {
                RegionType++;
                NewTypeList.emplace(tmpTypeId, RegionType);
                auto NewRegionType = new s_RegionType(RegionType);
                NewRegionType->LayerNum = CurrentModule->f_region->TotLayerNum;
                m_RegionType[RegionType] = NewRegionType;
            }
            spdlog::get(LOG_NAME)->info("\tFind new region!");
            // std::cout << "detector_type " << detector_type << " Cell3DSize " << Cell3DSize << " tmpNXCell " << tmpNXCell << " tmpNYCell " << tmpNYCell << std::endl;
            spdlog::get(LOG_NAME)->info("\t\tOld Region Type = {}, Old Region id = {}.", CurrentModule->f_region->type, CurrentModule->f_region->GlobalId);
            spdlog::get(LOG_NAME)->info("\t\tNew Region Type = {}, New Region id = {}.", NewTypeList.at(tmpTypeId), GlobalId);
            FRegion *tmpRegion = new FRegion(NewTypeList.at(tmpTypeId), GlobalId);
            NewRegionList.emplace(tmpFullId, tmpRegion);

            tmpRegion->using3DSeed = CurrentModule->f_region->using3DSeed;
            tmpRegion->AlloCell3D_CAllBack = CurrentModule->f_region->AlloCell3D_CAllBack;
            tmpRegion->WindowShape = CurrentModule->f_region->WindowShape;
            tmpRegion->Seed3DCut = CurrentModule->f_region->Seed3DCut;
            tmpRegion->Seed3DMethod = CurrentModule->f_region->Seed3DMethod;
            tmpRegion->SiTimingNoise = CurrentModule->f_region->SiTimingNoise;
            // tmpRegion->UsingPar = CurrentModule->f_region->UsingPar;
            // tmpRegion->DoSCorr = CurrentModule->f_region->DoSCorr;
            tmpRegion->CombineSiEnWhenCali = CurrentModule->f_region->CombineSiEnWhenCali;
            tmpRegion->CombineEnergyPoint2R = CurrentModule->f_region->CombineEnergyPoint2R;
            tmpRegion->CombineEnergyPoint2T = CurrentModule->f_region->CombineEnergyPoint2T;
            tmpRegion->EnableLayerInfo = CurrentModule->f_region->EnableLayerInfo;

            tmpRegion->Have_SiTiming = CurrentModule->f_region->Have_SiTiming;
            tmpRegion->SiTimingLayerNum = CurrentModule->f_region->SiTimingLayerNum;
            tmpRegion->detector_type = CurrentModule->f_region->detector_type;
            tmpRegion->SiTimingThickness = CurrentModule->f_region->SiTimingThickness;
            tmpRegion->SiTimingCellSize = CurrentModule->f_region->SiTimingCellSize;
            tmpRegion->Cell3DSize = CurrentModule->f_region->Cell3DSize;
            tmpRegion->Separator_position = CurrentModule->f_region->Separator_position;
            tmpRegion->RegionShift = CurrentModule->f_region->RegionShift;
            tmpRegion->TotLayerNum = CurrentModule->f_region->TotLayerNum;
            tmpRegion->FrontFace = CurrentModule->f_region->FrontFace;
            tmpRegion->SplitLayer = CurrentModule->f_region->SplitLayer;
            tmpRegion->FiberNumX = CurrentModule->f_region->FiberNumX;
            tmpRegion->FiberNumY = CurrentModule->f_region->FiberNumY;
            tmpRegion->CellNx = tmpNXCell;
            tmpRegion->CellNy = tmpNYCell;
            tmpRegion->ax = tmpAx;
            tmpRegion->ay = tmpAy;
            tmpRegion->az = tmpAz;
            tmpRegion->Ro.Clear();

            tmpRegion->Ro.RotateX(tmpAx / 180. * TMath::Pi());
            tmpRegion->Ro.RotateY(tmpAy / 180. * TMath::Pi());
            tmpRegion->Ro.RotateZ(tmpAz / 180. * TMath::Pi());

            spdlog::get(LOG_NAME)->info("\t\tRegion Ax = {}, Ay = {}, Az = {}", tmpRegion->ax, tmpRegion->ay, tmpRegion->az);

            tmpRegion->cell3D_map = CurrentModule->f_region->cell3D_map;
            tmpRegion->CaliFile = CurrentModule->f_region->CaliFile;
            tmpRegion->CaliFileName = CurrentModule->f_region->CaliFileName;
            tmpRegion->EnPerMIP = CurrentModule->f_region->EnPerMIP;
            tmpRegion->Layers = CurrentModule->f_region->Layers;
            tmpRegion->EventInfo = CurrentModule->f_region->EventInfo;
            tmpRegion->f_calo = this;
            //////////////////More CaliPar

            tmpRegion->CluECaliFun = CurrentModule->f_region->CluECaliFun;
            tmpRegion->CluType0SCorX = CurrentModule->f_region->CluType0SCorX;
            tmpRegion->CluType0SCorY = CurrentModule->f_region->CluType0SCorY;
            tmpRegion->CluTypeP1SCorX = CurrentModule->f_region->CluTypeP1SCorX;
            tmpRegion->CluTypeP1SCorY = CurrentModule->f_region->CluTypeP1SCorY;
            tmpRegion->CluTypeM1SCorX = CurrentModule->f_region->CluTypeM1SCorX;
            tmpRegion->CluTypeM1SCorY = CurrentModule->f_region->CluTypeM1SCorY;
            tmpRegion->CluTypeP2SCorX = CurrentModule->f_region->CluTypeP2SCorX;
            tmpRegion->CluTypeP2SCorY = CurrentModule->f_region->CluTypeP2SCorY;
            tmpRegion->CluTypeM2SCorX = CurrentModule->f_region->CluTypeM2SCorX;
            tmpRegion->CluTypeM2SCorY = CurrentModule->f_region->CluTypeM2SCorY;
            tmpRegion->CluTypeP3SCorX = CurrentModule->f_region->CluTypeP3SCorX;
            tmpRegion->CluTypeP3SCorY = CurrentModule->f_region->CluTypeP3SCorY;
            tmpRegion->CluTypeM3SCorX = CurrentModule->f_region->CluTypeM3SCorX;
            tmpRegion->CluTypeM3SCorY = CurrentModule->f_region->CluTypeM3SCorY;

            tmpRegion->LCorrFun = CurrentModule->f_region->LCorrFun;
            tmpRegion->TShiftFun = CurrentModule->f_region->TShiftFun;
            tmpRegion->EResFun = CurrentModule->f_region->EResFun;
            tmpRegion->TResFun = CurrentModule->f_region->TResFun;
            tmpRegion->PosXResFun = CurrentModule->f_region->PosXResFun;
            tmpRegion->PosYResFun = CurrentModule->f_region->PosYResFun;
            tmpRegion->TransversalProfile_Cell_N = CurrentModule->f_region->TransversalProfile_Cell_N;
            tmpRegion->TransversalProfile_Cell_P = CurrentModule->f_region->TransversalProfile_Cell_P;
            tmpRegion->TransversalProfile_Seed_Down_Corner = CurrentModule->f_region->TransversalProfile_Seed_Down_Corner;
            tmpRegion->TransversalProfile_Seed_Up_Edge = CurrentModule->f_region->TransversalProfile_Seed_Up_Edge;
            tmpRegion->TransversalProfile_Seed_Down_Edge = CurrentModule->f_region->TransversalProfile_Seed_Down_Edge;
            tmpRegion->TransversalProfile_Seed_Up_Corner = CurrentModule->f_region->TransversalProfile_Seed_Up_Corner;
            tmpRegion->TransversalProfile_Seed_Up_UpCorner = CurrentModule->f_region->TransversalProfile_Seed_Up_UpCorner;
        }
        CurrentModule->f_region = NewRegionList.at(tmpFullId);
        CurrentModule->type = RegionType;
        NewRegionList.at(tmpFullId)->module_container.emplace_back(CurrentModule);

        region_map.SetBinContent(tmp_module_bin, NewRegionList.at(tmpFullId)->GlobalId);
        regionType_map.SetBinContent(tmp_module_bin, NewRegionList.at(tmpFullId)->type);
    }
    TotalRegionType = RegionType;
    TotalRegionId = GlobalId;
    for (auto region : region_container)
    {
        delete region.second;
    }
    region_container.clear();
    TotalLayerNumInAllRegion = 0;
    for (auto region : NewRegionList)
    {
        FRegion *CurrentRegion = region.second;
        TotalLayerNumInAllRegion += CurrentRegion->Layers.size();

        region_container.emplace(CurrentRegion->GlobalId, CurrentRegion);
        m_RegionType[CurrentRegion->type]->Regions.emplace_back(CurrentRegion);
    }
}
///////////////////////////////////////////////
//      0 : AllocateCellBy_3_3
//      1 : AllocateCellByGradient
///////////////////////////////////////////////

void Mixture_Calo::InitCellGlobalPos()
{

    for (auto regionSeq : region_container)
    {
        int RegionGlobalId = regionSeq.first;
        FRegion *CurrentRegion = regionSeq.second;
        int RegionType = CurrentRegion->type;
        CurrentRegion->CalculateCellGlobalPos();
    }
}
void Mixture_Calo::InitPara()
{

    if (region_container.size() == 1)
    {

        if (region_container.begin()->second->module_container.size() == 1)
            ModuleStudy = 1;
        else
            RegionStudy = 1;
    }

    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "ModuleStudy = {}, RegionStudy = {}", ModuleStudy, RegionStudy);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "region_container.size() = {}", region_container.size());

    for (auto regionSeq : region_container)
    {

        int RegionGlobalId = regionSeq.first;
        FRegion *CurrentRegion = regionSeq.second;
        int RegionType = CurrentRegion->type;

        if (ModuleStudy || RegionStudy)
        {
            CurrentRegion->SiTimingNoise = Parameters::Instance()->main_config.SiTimingNoise;
            CurrentRegion->EnPerMIP = Parameters::Instance()->main_config.EnPerMIP;
            spdlog::get(LOG_NAME)->info("In Region Type {}, SiTimingNoise = {}, EnPerMIP = {}", RegionType, CurrentRegion->SiTimingNoise, CurrentRegion->EnPerMIP);
            if (ProgramType != 1 && ProgramType != 3)
            {

                if (ProgramType == 0 || ProgramType == 4)
                {
                    CurrentRegion->CombineEnergyPoint2R = Parameters::Instance()->main_config.CombineEnergyPoint2R;
                    CurrentRegion->CombineEnergyPoint2T = Parameters::Instance()->main_config.CombineEnergyPoint2T;
                    CurrentRegion->EnableLayerInfo = Parameters::Instance()->main_config.EnableLayerInfo;

                    // CurrentRegion->DoSCorr = Parameters::Instance()->main_config.DoSCor;
                    CurrentRegion->SplitLayer = Parameters::Instance()->main_config.SplitLayer;
                    CurrentRegion->DoS1Cor = Parameters::Instance()->main_config.DoS1Cor;
                }

                // CurrentRegion->UsingPar = Parameters::Instance()->main_config.UsingPar;
                CurrentRegion->CombineSiEnWhenCali = Parameters::Instance()->main_config.CombineSiEnWhenCali;
                if (CluFlow == 2 || CluFlow == 3)
                {
                    CurrentRegion->Seed3DCut = Parameters::Instance()->main_config.Seed3DCut;
                    CurrentRegion->Seed3DMethod = Parameters::Instance()->main_config.Seed3DMethod;
                    CurrentRegion->WindowShape = Parameters::Instance()->main_config.WindowShape;

                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "WindowShape = {}", CurrentRegion->WindowShape);
                    if (CurrentRegion->WindowShape == 0)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_3_3");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_3_3;
                    }
                    else if (CurrentRegion->WindowShape == 1)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_2_2");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_2_2;
                    }
                    else if (CurrentRegion->WindowShape == 2)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_Cross");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_Cross;
                    }
                    else if (CurrentRegion->WindowShape == 3)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DByGradient");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DByGradient;
                    }
                }
            }
            for (int layer_id = 0; layer_id < CurrentRegion->Layers.size(); layer_id++)
            {
                if (ProgramType != 1 && ProgramType != 3)
                {
                    if (CluFlow == 1 || CluFlow == 2)
                    {
                        CurrentRegion->Layers[layer_id]->WindowShape4Layers = Parameters::Instance()->main_config.WindowShape4Layers.at(layer_id);
                        CurrentRegion->Layers[layer_id]->Seed2DCut = Parameters::Instance()->main_config.Seed2DCut.at(layer_id);
                        if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 0)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_3_3", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_3_3;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 1)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_2_2", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_2_2;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 2)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_Cross", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_Cross;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 3)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellByGradient", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellByGradient;
                        }
                        if (CluFlow == 2) // init ReAlloCellCallBack
                        {
                            if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 0)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_3_3", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_3_3;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 1)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_2_2", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_2_2;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 2)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_Cross", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_Cross;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 3)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_Gradient", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_Gradient;
                            }
                        }
                        CurrentRegion->Layers[layer_id]->Seed2DCut = Parameters::Instance()->main_config.Seed2DCut.at(layer_id);
                    }

                    if (ProgramType == 0 || ProgramType == 4)
                    {
                        CurrentRegion->Layers[layer_id]->OnlySeedT = Parameters::Instance()->main_config.OnlySeedT;
                    }
                    // 使用什么函数来cali cell的信号
                    CurrentRegion->Layers[layer_id]->CellECaliFunType = Parameters::Instance()->main_config.CellECaliFunType;
                }
            }
        }
        else
        {

            CurrentRegion->SiTimingNoise = Parameters::Instance()->module_config.at(RegionType).SiTimingNoise;
            CurrentRegion->EnPerMIP = Parameters::Instance()->module_config.at(RegionType).EnPerMIP;
            spdlog::get(LOG_NAME)->info("In Region Type {}, SiTimingNoise = {}, EnPerMIP = {}", RegionType, CurrentRegion->SiTimingNoise, CurrentRegion->EnPerMIP);
            if (ProgramType != 1 && ProgramType != 3)
            {

                if (ProgramType == 0 || ProgramType == 4)
                {

                    // CurrentRegion->DoSCorr = Parameters::Instance()->module_config.at(RegionType).DoSCor;
                    CurrentRegion->CombineEnergyPoint2R = Parameters::Instance()->module_config.at(RegionType).CombineEnergyPoint2R;
                    CurrentRegion->CombineEnergyPoint2T = Parameters::Instance()->module_config.at(RegionType).CombineEnergyPoint2T;
                    CurrentRegion->EnableLayerInfo = Parameters::Instance()->module_config.at(RegionType).EnableLayerInfo;
                    CurrentRegion->SplitLayer = Parameters::Instance()->module_config.at(RegionType).SplitLayer;
                    CurrentRegion->DoS1Cor = Parameters::Instance()->module_config.at(RegionType).DoS1Cor;
                }

                // CurrentRegion->UsingPar = Parameters::Instance()->module_config.at(RegionType).UsingPar;
                CurrentRegion->CombineSiEnWhenCali = Parameters::Instance()->module_config.at(RegionType).CombineSiEnWhenCali;
                if (CluFlow == 2 || CluFlow == 3)
                {
                    CurrentRegion->Seed3DCut = Parameters::Instance()->module_config.at(RegionType).Seed3DCut;
                    CurrentRegion->Seed3DMethod = Parameters::Instance()->module_config.at(RegionType).Seed3DMethod;
                    CurrentRegion->WindowShape = Parameters::Instance()->module_config.at(RegionType).WindowShape;
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "WindowShape = {}", CurrentRegion->WindowShape);
                    if (CurrentRegion->WindowShape == 0)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_3_3");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_3_3;
                    }
                    else if (CurrentRegion->WindowShape == 1)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_2_2");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_2_2;
                    }
                    else if (CurrentRegion->WindowShape == 2)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DBy_Cross");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DBy_Cross;
                    }
                    else if (CurrentRegion->WindowShape == 3)
                    {
                        spdlog::get(LOG_NAME)->info("Using AllocateCell3DByGradient");
                        CurrentRegion->AlloCell3D_CAllBack = AllocateCell3DByGradient;
                    }
                }
            }
            for (int layer_id = 0; layer_id < CurrentRegion->Layers.size(); layer_id++)
            {
                if (ProgramType != 1 && ProgramType != 3)
                {

                    if (CluFlow == 1 || CluFlow == 2)
                    {
                        CurrentRegion->Layers[layer_id]->WindowShape4Layers = Parameters::Instance()->module_config.at(RegionType).WindowShape4Layers.at(layer_id);
                        if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 0)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_3_3", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_3_3;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 1)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_2_2", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_2_2;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 2)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellBy_Cross", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellBy_Cross;
                        }
                        else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 3)
                        {
                            spdlog::get(LOG_NAME)->info("Layer = {}, using AllocateCellByGradient", layer_id);
                            CurrentRegion->Layers[layer_id]->AlloCell_CAllBack = AllocateCellByGradient;
                        }
                        if (CluFlow == 2) // init ReAlloCellCallBack
                        {
                            if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 0)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_3_3", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_3_3;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 1)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_2_2", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_2_2;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 2)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_Cross", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_Cross;
                            }
                            else if (CurrentRegion->Layers[layer_id]->WindowShape4Layers == 3)
                            {
                                spdlog::get(LOG_NAME)->info("Layer = {}, using ReAllocateClu2DBy_Gradient", layer_id);
                                CurrentRegion->Layers[layer_id]->ReAlloCell_CallBack = ReAllocateClu2DBy_Gradient;
                            }
                        }
                        CurrentRegion->Layers[layer_id]->Seed2DCut = Parameters::Instance()->module_config.at(RegionType).Seed2DCut.at(layer_id);
                    }
                    if (ProgramType == 0 || ProgramType == 4)
                    {
                        CurrentRegion->Layers[layer_id]->OnlySeedT = Parameters::Instance()->module_config.at(RegionType).OnlySeedT;
                    }
                    CurrentRegion->Layers[layer_id]->CellECaliFunType = Parameters::Instance()->module_config.at(RegionType).CellECaliFunType;
                }
            }
        }
    }
}

void Mixture_Calo::RecSiliconCell()
{

    TTree *StepTree = (TTree *)Sim_file->Get("shower");
    int event_id = 0;
    StepTree->SetBranchAddress("event", &event_id);
    int moduleType;
    StepTree->SetBranchAddress("moduleType", &moduleType);
    float moduleX;
    StepTree->SetBranchAddress("moduleX", &moduleX);
    float moduleY;
    StepTree->SetBranchAddress("moduleY", &moduleY);
    float moduleZ;
    StepTree->SetBranchAddress("moduleZ", &moduleZ);
    float moduleTheta;
    StepTree->SetBranchAddress("moduleTheta", &moduleTheta);
    float modulePhi;
    StepTree->SetBranchAddress("modulePhi", &modulePhi);
    float modulePsi;
    StepTree->SetBranchAddress("modulePsi", &modulePsi);

    int materialNumber;
    StepTree->SetBranchAddress("materialNumber", &materialNumber);
    std::string *materialName = 0;
    StepTree->SetBranchAddress("materialName", &materialName);

    int isInCrystal;
    StepTree->SetBranchAddress("isInCrystal", &isInCrystal);

    int isInSiTiming;
    StepTree->SetBranchAddress("isInSiTiming", &isInSiTiming);

    float totalEnDep;
    StepTree->SetBranchAddress("totalEnDep", &totalEnDep);
    float x;
    StepTree->SetBranchAddress("x", &x);
    float y;
    StepTree->SetBranchAddress("y", &y);
    float z;
    StepTree->SetBranchAddress("z", &z);
    float px;
    StepTree->SetBranchAddress("px", &px);
    float py;
    StepTree->SetBranchAddress("py", &py);
    float pz;
    StepTree->SetBranchAddress("pz", &pz);
    float t;
    StepTree->SetBranchAddress("t", &t);
    int primaryPDGID;
    StepTree->SetBranchAddress("primaryPDGID", &primaryPDGID);
    float primaryEnergy;
    StepTree->SetBranchAddress("primaryEnergy", &primaryEnergy);

    for (int event = 0; event < max_Eventseq; event++)
    {

        int BeginNum = StepTree->GetEntries(Form("event<%d", event));
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Event {} starts at entry {}", event, BeginNum);
        // TH1F *h_test = new TH1F("test", "test", 1000, -48, -46);
        for (int i = BeginNum; i < StepTree->GetEntries(); i++)
        {
            StepTree->GetEntry(i);
            if (event_id != event)
            {
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Event {} ends at entry {}", event, i - 1);
                break;
            }

            if (!isInSiTiming || (materialNumber != 532 && *materialName != "SiTiming_Si") || totalEnDep <= 0)
                continue;

            int moduleID = GetModuleIDByPos(moduleX, moduleY);
            FModule *CurrenModule = GetModuleByID(moduleID);
            if (CurrenModule == NULL)
                continue;
            FRegion *CurrentRegion = CurrenModule->f_region;
            bool haveSi = CurrentRegion->Have_SiTiming;
            if (!haveSi)
                continue;

            FPoint GlobalPoint((Double_t)x - moduleX, (Double_t)y - moduleY, (Double_t)z - CurrentRegion->RegionShift);

            ROOT::Math::EulerAngles Ro(modulePhi, moduleTheta, modulePsi);
            // Ro.RotateYEulerAngles(modulePhi, moduleTheta, modulePsi);

            auto LocalPoint = Ro * GlobalPoint;

            float ModuleDZ = CurrenModule->Dz();
            if (fabs(LocalPoint.Z()) > ModuleDZ / 2)
                continue;

            // int layer_id = CurrentRegion->Get_layerid(LocalPoint.Z());
            int layer_id = CurrentRegion->InSiTiming(LocalPoint.Z());
            // h_test->Fill(LocalPoint.Z());

            // std::cout << "layer_id " << layer_id << " LocalPoint.Z() " << LocalPoint.Z() << std::endl;
            if (layer_id == -1)
                continue;
            // std::cout << "test" << std::endl;
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "CurrentRegion {}", CurrentRegion->GlobalId);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "x : {}, y {}, z {}", x, y, z);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "x - moduleX : {}, y - moduleY {}, z - moduleZ {}", x - moduleX, y - moduleY, z - CurrentRegion->RegionShift);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "modulePhi : {}, moduleTheta {}, modulePsi {}", modulePhi, moduleTheta, modulePsi);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "LocalPoint.Z() : {}, layer id {}", LocalPoint.Z(), layer_id);
            bool isInSilicon = CurrentRegion->Layers.at(layer_id)->layerType;
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "In region type {}, z pos {}, layer id is {}, (not) in SiTiming {}", CurrentRegion->type, LocalPoint.Z(), layer_id, isInSilicon);
            if (!isInSilicon)
                continue;
            FCell *tmpCell = CurrenModule->GetCellByRelaPos(LocalPoint.X(), LocalPoint.Y(), layer_id);
            float old_energy = tmpCell->GetRawSignal(event_id);
            float old_time = tmpCell->GetRawTime(event_id);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "old_time : {}", old_time);
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "old energy : {}", old_energy);
            tmpCell->SetRawTime(event_id, (t * totalEnDep + old_energy * old_time) / (old_energy + totalEnDep));
            tmpCell->SetRawSignal(event_id, totalEnDep + old_energy);
        }
        // TCanvas *c_test = new TCanvas("test", "test");
        // h_test->Draw();
        // c_test->Print("test.pdf");
    }

    for (int event = 0; event < max_Eventseq; event++)
    {
        for (auto module : module_container)
        {

            FModule *Current_module = module.second;

            // //std::cout << "      In module " << Current_module->GlobalId << std::endl;
            FRegion *Current_Region = module.second->f_region;

            for (auto layer : Current_module->v_layer)
            {

                for (auto Cell : layer->v_cell) // find neighbor for each 2D cell
                {
                    if (layer->layerType == 1)
                    {

                        if (Cell->GetRawSignal(event) == 0)
                        {
                            Cell->SetTime(event, 0);
                        }
                        else
                        {
                            float tmpRawSignal = Cell->GetRawSignal(event);
                            float newRawSignal = round(Cell->GetRawSignal(event) / Current_Region->EnPerMIP);
                            float smearTime = sqrt(pow(5 / 4.5 / (newRawSignal), 2) + pow(Current_Region->SiTimingNoise / 1000, 2));
                            float newTime = CalRand->Gaus(Cell->GetRawTime(event), smearTime);
                            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Cell->GetRawSignal(event) : {}", Cell->GetRawSignal(event));
                            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "EnPerMIP : {}", Current_Region->EnPerMIP);
                            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Cell Time : {}", newTime);
                            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Cell Signal : {}", newRawSignal);
                            Cell->SetTime(event, newTime);
                            Cell->SetRawSignal(event, newRawSignal);
                            Current_Region->EventInfo.at(event_id).AccumulateLayerSig(newRawSignal, layer->id);
                            Current_Region->EventInfo.at(event_id).AccumulateSiliconSig(newRawSignal);
                        }
                    }
                }
            }
        }
    }
}

void Mixture_Calo::FindNeighbor()
{
    GetModuleNeighbours();

    for (auto module : module_container)
    {

        FModule *Current_module = module.second;

        // //std::cout << "      In module " << Current_module->GlobalId << std::endl;
        FRegion *Current_Region = module.second->f_region;
        for (auto cell : module.second->v_Cell3D)
        {
            // std::cout << "test" << std::endl;
            cell->FindNeighbours();
            // std::cout << "test1" << std::endl;
            cell->FindCrossNeighbours();
            // std::cout << "test2" << std::endl;
            cell->FindForkNeighbours();
            // std::cout << "test3" << std::endl;
        }

        for (auto layer : Current_module->v_layer)
        {

            for (auto Cell : layer->v_cell) // find neighbor for each 2D cell
            {
                // std::cout << "test4s" << std::endl;
                Cell->FindNeighbours();
                // std::cout << "test4" << std::endl;
            }
        }
    }
}

void Mixture_Calo::WriteScintillatorData()
{

    TTree *ScintillatorTree = (TTree *)Trig_file->Get("tree");

    int n_modules = module_container.size();
    int totalLight = 0;
    ScintillatorTree->SetBranchAddress("Total_Light", &totalLight);
    std::vector<int> *VMod = 0;
    ScintillatorTree->SetBranchAddress("modulesHit", &VMod);
    int event;
    bool evtFlag = 0;
    if (ScintillatorTree->GetBranch("event") != NULL)
    {
        ScintillatorTree->SetBranchAddress("event", &event);
        spdlog::get(LOG_NAME)->info("\thave event branch in trigger file");

        evtFlag = 1;
    }
    std::vector<int> **VPh = 0;
    std::vector<float> **VTime = 0;
    VPh = new std::vector<int> *[n_modules];
    VTime = new std::vector<float> *[n_modules];
    for (int iMod = 0; iMod < n_modules; iMod++)
    {
        VPh[iMod] = 0;
        VTime[iMod] = 0;
    }

    for (int iMod = 0; iMod < n_modules; iMod++)
    {
        std::string moduleName = "mod";
        std::stringstream smod;
        smod << moduleName << iMod;
        moduleName = smod.str();
        ScintillatorTree->SetBranchAddress((moduleName + "_ph").c_str(), &VPh[iMod]);
        ScintillatorTree->SetBranchAddress((moduleName + "_t").c_str(), &VTime[iMod]);
    }
    int eventNum;

    for (int i = 0; i < ScintillatorTree->GetEntries(); i++)
    {

        ScintillatorTree->GetEntry(i);
        if (evtFlag)
        {

            eventNum = event;
        }
        else
        {
            eventNum = i;
        }
        for (int iMod = 0; iMod < n_modules; iMod++)
        {

            // loop on cells
            /* if want to only write spacal data
            if (module_container.at(iMod)->f_region->detector_type != 0)
                continue;
            */
            FRegion *Current_Region = module_container.at(iMod)->f_region;

            // //std::cout << "Region " << Current_Region->GlobalId << " iMod " << iMod << " VPh[iMod]->size() " << VPh[iMod]->size() << " VTime[iMod]->size() " << VTime[iMod]->size() << std::endl;
            for (size_t iCell = 0; iCell < VPh[iMod]->size(); ++iCell)
            {
                // //std::cout << "in cell : " << iCell << " VPh[iMod]->at(iCell) : " << VPh[iMod]->at(iCell) << std::endl;
                if (Current_Region->detector_type == 1 && Current_Region->Have_SiTiming == 1)
                {
                    if (iCell >= VPh[iMod]->size() / 2 && VPh[iMod]->at(iCell) > 0 && fabs(VTime[iMod]->at(iCell)) < 1e20)
                    {
                        int tmp_layer_id = module_container.at(iMod)->v_layer.size() - 1;
                        Current_Region->EventInfo.at(eventNum).AccumulateLayerSig(VPh[iMod]->at(iCell), tmp_layer_id);
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetRawSignal(eventNum, VPh[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetRawTime(eventNum, VTime[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetTime(eventNum, VTime[iMod]->at(iCell));
                    }
                }
                else
                {
                    if (iCell < VPh[iMod]->size() / 2 && VPh[iMod]->at(iCell) > 0 && fabs(VTime[iMod]->at(iCell)) < 1e20)
                    {
                        Current_Region->EventInfo.at(eventNum).AccumulateLayerSig(VPh[iMod]->at(iCell), 0);
                        // //std::cout << "iCell " << iCell << std::endl;
                        // //std::cout << "max" << max_Eventseq << std::endl;
                        // //std::cout << module_container.at(iMod)->v_layer[0]->v_cell.size() << std::endl;
                        module_container.at(iMod)->v_layer[0]->v_cell[iCell]->SetRawSignal(eventNum, VPh[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[0]->v_cell[iCell]->SetRawTime(eventNum, VTime[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[0]->v_cell[iCell]->SetTime(eventNum, VTime[iMod]->at(iCell));
                    }
                    if (iCell >= VPh[iMod]->size() / 2 && VPh[iMod]->at(iCell) > 0 && fabs(VTime[iMod]->at(iCell)) < 1e20)
                    {
                        int tmp_layer_id = module_container.at(iMod)->v_layer.size() - 1;
                        Current_Region->EventInfo.at(eventNum).AccumulateLayerSig(VPh[iMod]->at(iCell), tmp_layer_id);
                        // //std::cout << "iCell " << iCell - VPh[iMod]->size() / 2 << std::endl;
                        // //std::cout << "max " << max_Eventseq << std::endl;
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetRawSignal(eventNum, VPh[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetRawTime(eventNum, VTime[iMod]->at(iCell));
                        module_container.at(iMod)->v_layer[tmp_layer_id]->v_cell[iCell - VPh[iMod]->size() / 2]->SetTime(eventNum, VTime[iMod]->at(iCell));
                    }
                }
            }
        }
    }
    delete ScintillatorTree;
}

void Mixture_Calo::WritePVInfo_Geant4()
{
    TTree *StepTree = ((TTree *)Sim_file->Get("shower"))->CopyTree(Form("trackID<=2&&processName==\"none\"&&totalEnDep==0&&primaryID==trackID"));

    for (int event = 0; event < max_Eventseq; event++)
    {
        SetPrimaryInfoStatus(1, event);

        TTree *EvtStepTree = StepTree->CopyTree(Form("event==%d", event));

        float x;
        EvtStepTree->SetBranchAddress("x", &x);
        float y;
        EvtStepTree->SetBranchAddress("y", &y);
        float z;
        EvtStepTree->SetBranchAddress("z", &z);
        float px;
        EvtStepTree->SetBranchAddress("px", &px);
        float py;
        EvtStepTree->SetBranchAddress("py", &py);
        float pz;
        EvtStepTree->SetBranchAddress("pz", &pz);
        float t;
        EvtStepTree->SetBranchAddress("t", &t);
        int primaryPDGID;
        EvtStepTree->SetBranchAddress("primaryPDGID", &primaryPDGID);
        float primaryEnergy;
        EvtStepTree->SetBranchAddress("primaryEnergy", &primaryEnergy);

        int tmp_n_PV = EvtStepTree->GetEntries();
        spdlog::get(LOG_NAME)->info("\tEvent {} has {} PV", event, tmp_n_PV);
        for (int i = 0; i < tmp_n_PV; i++)
        {
            spdlog::get(LOG_NAME)->info("\t\tIn PV {}", i);

            EvtStepTree->GetEntry(i);

            SetPrimaryPDGID(primaryPDGID, event);
            SetPrimaryE(primaryEnergy, event);
            float newVx = px / primaryEnergy;
            SetPrimaryVx(px, event);
            float newVy = py / primaryEnergy;
            SetPrimaryVy(py, event);
            float newVz = pz / primaryEnergy;
            SetPrimaryVz(pz, event);
            float Pv_Z = PVZShift;
            float Pv_T = t + (Pv_Z - z) / 299.79 / newVz;
            float Pv_X = (Pv_Z - z) / newVz * newVx + x;
            float Pv_Y = (Pv_Z - z) / newVz * newVy + y;
            SetPrimaryX(Pv_X, event);
            SetPrimaryY(Pv_Y, event);
            SetPrimaryT(Pv_T, event);
            SetPrimaryZ(Pv_Z, event);

            float ReferencePlane = PVZShift + 12620;
            SetHitE(primaryEnergy, event);
            float ReferenceTime = t + (ReferencePlane - z) / 299.79 / newVz;
            SetHitT(ReferenceTime, event);
            float entry_x = (ReferencePlane - z) / newVz * newVx + x;
            SetHitX(entry_x, event);
            float entry_y = (ReferencePlane - z) / newVz * newVy + y;
            SetHitY(entry_y, event);
            SetHitZ(ReferencePlane, event);
            SetHitVx(px, event);
            SetHitVy(py, event);
            SetHitVz(pz, event);

            int HitModule = module_map.GetBinContent(module_map.FindBin(entry_x, entry_y));

            int HitRegionId = module_container.at(HitModule)->f_region->GlobalId;
            int HitRegionType = module_container.at(HitModule)->f_region->type;

            SetHitModule(HitModule, event);
            SetHitRegion(HitRegionId, event);
            SetHitRegionType(HitRegionType, event);

            // SetMatchedClu3D(event, NULL);
            // SetMatchedSubClu3D(event, NULL);
            spdlog::get(LOG_NAME)->info("\t\tParticle is {}, energy is {} MeV", primaryPDGID, primaryEnergy);

            spdlog::get(LOG_NAME)->info("\t\tHitRegion id is {}, HitRegion Type is {} ", HitRegionId, HitRegionType);
            spdlog::get(LOG_NAME)->info("\t\tEntry X {}, Entry Y {} ", entry_x, entry_y);
        }
        Set_nPV(tmp_n_PV, event);
        InitMatchContainer(event);
    }
    delete StepTree;
}

void Mixture_Calo::WritePVInfo_Flux()
{
    spdlog::get(LOG_NAME)->info("using Truth flux file initial Truth event information");
    TTree *TruthTree;
    if (AbPathOrPrefix == 1)
    {
        TruthTree = ((TTree *)TruthFile->Get("tree"))->CopyTree(Form("evtIndex==%d", IntputNum));
    }
    else
    {
        TruthTree = ((TTree *)TruthFile->Get("tree"));
    }
    double prod_vertex_x;
    TruthTree->SetBranchAddress("prod_vertex_x", &prod_vertex_x);
    double prod_vertex_y;
    TruthTree->SetBranchAddress("prod_vertex_y", &prod_vertex_y);
    double prod_vertex_z;
    TruthTree->SetBranchAddress("prod_vertex_z", &prod_vertex_z);
    double entry_x;
    TruthTree->SetBranchAddress("entry_x", &entry_x);
    double entry_y;
    TruthTree->SetBranchAddress("entry_y", &entry_y);
    double entry_z;
    TruthTree->SetBranchAddress("entry_z", &entry_z);
    double timing;
    TruthTree->SetBranchAddress("timing", &timing);
    double px_ture;
    TruthTree->SetBranchAddress("px", &px_ture);
    double py_ture;
    TruthTree->SetBranchAddress("py", &py_ture);
    double pz_ture;
    TruthTree->SetBranchAddress("pz", &pz_ture);
    int pdgID;
    TruthTree->SetBranchAddress("pdgID", &pdgID);
    double eTot;
    TruthTree->SetBranchAddress("eTot", &eTot);

    double pi1px, pi1py, pi1pz, pi1pe, pi1p;
    double pi2px, pi2py, pi2pz, pi2pe, pi2p;

    if (Parameters::Instance()->Bd02PipPimPi0 == 1)
    {

        TruthTree->SetBranchAddress("pi1px", &pi1px);

        TruthTree->SetBranchAddress("pi1py", &pi1py);

        TruthTree->SetBranchAddress("pi1pz", &pi1pz);

        TruthTree->SetBranchAddress("pi1pe", &pi1pe);

        TruthTree->SetBranchAddress("pi1p", &pi1p);

        TruthTree->SetBranchAddress("pi2px", &pi2px);

        TruthTree->SetBranchAddress("pi2py", &pi2py);

        TruthTree->SetBranchAddress("pi2pz", &pi2pz);

        TruthTree->SetBranchAddress("pi2pe", &pi2pe);

        TruthTree->SetBranchAddress("pi2p", &pi2p);
    }

    int tmp_n_PV = TruthTree->GetEntries();
    spdlog::get(LOG_NAME)->info("\tEvent {} has {} PV", 0, tmp_n_PV);
    SetPrimaryInfoStatus(1, 0); // 所有的的事例都属于一个event
    for (int i = 0; i < tmp_n_PV; i++)
    {
        spdlog::get(LOG_NAME)->info("\t\tIn PV {}", i);
        TruthTree->GetEntry(i);

        SetHitE(eTot * 1000, 0, i);

        SetHitT(timing, 0, i);

        SetHitX(entry_x, 0, i);

        SetHitY(entry_y, 0, i);
        float newEntry_Z = entry_z + PVZShift;
        SetHitZ(newEntry_Z, 0, i);

        SetHitVx(px_ture * 1000, 0, i);

        SetHitVy(py_ture * 1000, 0, i);
        SetHitVz(pz_ture * 1000, 0, i);

        SetPrimaryE(eTot * 1000, 0, i);
        SetPrimaryPDGID(pdgID, 0, i);
        SetPrimaryX(prod_vertex_x, 0, i);
        SetPrimaryY(prod_vertex_y, 0, i);
        float Pv_Z = prod_vertex_z + PVZShift;
        SetPrimaryZ(Pv_Z, 0, i);
        SetPrimaryVx(px_ture * 1000, 0, i);
        SetPrimaryVy(py_ture * 1000, 0, i);
        SetPrimaryVz(pz_ture * 1000, 0, i);

        double newVz = pz_ture / eTot;
        float Pv_T = timing + (Pv_Z - newEntry_Z) / 299.79 / newVz;
        SetPrimaryT(Pv_T, 0, i);

        int HitModule = module_map.GetBinContent(module_map.FindBin(entry_x, entry_y));
        int HitRegionId = module_container.at(HitModule)->f_region->GetID();
        int HitRegionType = module_container.at(HitModule)->f_region->GetType();
        SetHitModule(HitModule, 0, i);
        SetHitRegion(HitRegionId, 0, i);
        SetHitRegionType(HitRegionType, 0, i);
        // SetMatchedClu3D(0, NULL, i);
        // SetMatchedSubClu3D(0, NULL, i);
        // std::cout << "(Pv_Z - entry_z) " << (Pv_Z - entry_z) << std::endl;
        // std::cout << "newVz " << newVz << std::endl;
        // std::cout << "Pv_T " << Pv_T << " timing " << timing << std::endl;
        spdlog::get(LOG_NAME)->info("\t\tParticle is {}, energy is {} MeV", pdgID, eTot * 1000);
        spdlog::get(LOG_NAME)->info("\t\tpx : {}, py {}, pz {} MeV", GetPrimaryVx(0, i), GetPrimaryVy(0, i), GetPrimaryVz(0, i));
        spdlog::get(LOG_NAME)->info("\t\tHitRegion id is {}, HitRegion Type is {} ", HitRegionId, HitRegionType);
        spdlog::get(LOG_NAME)->info("\t\tEntry X {}, Entry Y {}, Entry Z {}  ", entry_x, entry_y, GetHitZ(0, i));
        spdlog::get(LOG_NAME)->info("\t\tGen X {}, Gen Y {}, Gen Z {}  ", GetPrimaryX(0, i), GetPrimaryY(0, i), GetPrimaryZ(0, i));
        if (Parameters::Instance()->Bd02PipPimPi0 == 1 && i == 0)
        {
            auto tmpPip = new FPip(pi1px * 1000, pi1py * 1000, pi1pz * 1000, pi1pe * 1000);
            tmpPip->GenT = GetPrimaryT(0, 0);
            tmpPip->GenX = GetPrimaryX(0, 0);
            tmpPip->GenY = GetPrimaryY(0, 0);
            tmpPip->GenZ = GetPrimaryZ(0, 0);

            Get_vPip(0)->emplace_back(tmpPip);
            auto tmpPim = new FPim(pi2px * 1000, pi2py * 1000, pi2pz * 1000, pi2pe * 1000);
            tmpPim->GenT = Pv_T;
            tmpPim->GenX = GetPrimaryX(0, 0);
            tmpPim->GenY = GetPrimaryY(0, 0);
            tmpPim->GenZ = GetPrimaryZ(0, 0);
            Get_vPim(0)->emplace_back(tmpPim);
        }
    }
    Set_nPV(tmp_n_PV, 0);
    InitMatchContainer(0);

    delete TruthTree;
}

void Mixture_Calo::WriteHitInfo_Geant4()
{

    TTree *StepTree = (TTree *)Sim_file->Get("shower");
    int event_id;
    StepTree->SetBranchAddress("event", &event_id);
    int moduleType;
    StepTree->SetBranchAddress("moduleType", &moduleType);

    float moduleX;
    StepTree->SetBranchAddress("moduleX", &moduleX);
    float moduleY;
    StepTree->SetBranchAddress("moduleY", &moduleY);
    float moduleZ;
    StepTree->SetBranchAddress("moduleZ", &moduleZ);
    float moduleTheta;
    StepTree->SetBranchAddress("moduleTheta", &moduleTheta);
    float modulePhi;
    StepTree->SetBranchAddress("modulePhi", &modulePhi);
    float modulePsi;
    StepTree->SetBranchAddress("modulePsi", &modulePsi);

    int materialNumber;
    StepTree->SetBranchAddress("materialNumber", &materialNumber);
    std::string *materialName = 0;
    StepTree->SetBranchAddress("materialName", &materialName);

    int isInCrystal;
    StepTree->SetBranchAddress("isInCrystal", &isInCrystal);

    int isInSiTiming = 0;
    StepTree->SetBranchAddress("isInSiTiming", &isInSiTiming);

    float totalEnDep;
    StepTree->SetBranchAddress("totalEnDep", &totalEnDep);
    float x;
    StepTree->SetBranchAddress("x", &x);
    float y;
    StepTree->SetBranchAddress("y", &y);
    float z;
    StepTree->SetBranchAddress("z", &z);
    float px;
    StepTree->SetBranchAddress("px", &px);
    float py;
    StepTree->SetBranchAddress("py", &py);
    float pz;
    StepTree->SetBranchAddress("pz", &pz);
    float t;
    StepTree->SetBranchAddress("t", &t);
    int primaryPDGID;
    StepTree->SetBranchAddress("primaryPDGID", &primaryPDGID);
    float primaryEnergy;
    StepTree->SetBranchAddress("primaryEnergy", &primaryEnergy);

    for (int event = 0; event < max_Eventseq; event++)
    {

        int BeginNum = StepTree->GetEntries(Form("event<%d", event));
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Event {} starts at entry {}", event, BeginNum);

        for (int i = BeginNum; i < StepTree->GetEntries(); i++)
        {
            StepTree->GetEntry(i);
            if (event_id != event)
            {
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Event {} ends at entry {}", event, i - 1);
                break;
            }
            int moduleID = GetModuleIDByPos(moduleX, moduleY);
            // //std::cout << moduleID << std::endl;
            FModule *CurrenModule = GetModuleByID(moduleID);
            if (CurrenModule == NULL)
                continue;
            FRegion *CurrentRegion = CurrenModule->f_region;

            FPoint GlobalPoint((Double_t)x - moduleX, (Double_t)y - moduleY, (Double_t)z - CurrentRegion->RegionShift);

            ROOT::Math::EulerAngles Ro(modulePhi, moduleTheta, modulePsi);
            // Ro.RotateYEulerAngles(modulePhi, moduleTheta, modulePsi);

            auto LocalPoint = Ro * GlobalPoint;

            float ModuleDZ = CurrenModule->Dz();
            SPDLOG_LOGGER_TRACE(spdlog::get(DEBUG_TRACE_NAME), "In Module: {}, region Type: {}, local x:{}, y:{}, z:{}, module Dz:{}", moduleID, CurrentRegion->type, LocalPoint.X(), LocalPoint.Y(), LocalPoint.Z(), ModuleDZ);
            if (fabs(LocalPoint.Z()) > ModuleDZ / 2)
                continue;

            int detectorType = CurrentRegion->detector_type;

            bool haveSi = CurrentRegion->Have_SiTiming;

            float sepa_pos = CurrentRegion->Separator_position;

            float frontFace = CurrentRegion->FrontFace;
            // //std::cout << "event_id " << event_id << std::endl;
            // GetHitMap(event_id)->Fill(x, y, totalEnDep);

            AccumulateEnDep(totalEnDep, event_id);
            float OldShowerZ = GetShowerZ(event_id);
            SetShowerZ(OldShowerZ + totalEnDep * z, event_id);

            float OldShowerX = GetShowerX(event_id);
            SetShowerX(OldShowerX + totalEnDep * x, event_id);

            float OldShowerY = GetShowerY(event_id);
            SetShowerY(OldShowerY + totalEnDep * y, event_id);
            // //std::cout << "Dep : " << CurrentRegion->EventInfo.at(event_id).GetTotEnDep() << " totalEnDep : " << totalEnDep << std::endl;

            CurrentRegion->EventInfo.at(event_id).AccumulateTotEnDep(totalEnDep);
            // //std::cout << "Region Id : " << CurrentRegion->GlobalId << " totalEnDep: " << totalEnDep << std::endl;
            OldShowerZ = CurrentRegion->EventInfo.at(event_id).GetShowerZ();
            CurrentRegion->EventInfo.at(event_id).SetShowerZ(OldShowerZ + totalEnDep * z);
            OldShowerX = CurrentRegion->EventInfo.at(event_id).GetShowerX();
            CurrentRegion->EventInfo.at(event_id).SetShowerX(OldShowerX + totalEnDep * x);
            OldShowerY = CurrentRegion->EventInfo.at(event_id).GetShowerY();
            CurrentRegion->EventInfo.at(event_id).SetShowerY(OldShowerY + totalEnDep * y);

            int layer_id = CurrentRegion->Get_layerid(LocalPoint.Z());
            int SiTimingLayer = CurrentRegion->InSiTiming(LocalPoint.Z());
            // //std::cout << "LocalPoint.Z() " << LocalPoint.Z() << std::endl;
            // if (totalEnDep > 0)
            //     //std::cout << "layer_id " << layer_id << " SiTimingLayer " << SiTimingLayer << std::endl;
            // spdlog::get(LOG_NAME)->info("In region type {}, z pos {}, layer id is {}, (not) in SiTiming {}", CurrentRegion->type, LocalPoint.Z(), layer_id, isInSiTiming);

            if (layer_id != -1)
            {

                if (!isInSiTiming)
                {
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerZ(z * totalEnDep, layer_id);
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerX(x * totalEnDep, layer_id);
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerY(y * totalEnDep, layer_id);
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerEnDep(totalEnDep, layer_id);
                    if (isInCrystal)
                    {
                        CurrentRegion->EventInfo.at(event_id).AccumulateActiveLayerEnDep(totalEnDep, layer_id);
                    }
                }
            }
            if (SiTimingLayer)
            {
                if (isInSiTiming && (materialNumber == 532 || *materialName == "SiTiming_Si"))
                {
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerZ(z * totalEnDep, SiTimingLayer);
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerX(x * totalEnDep, SiTimingLayer);
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerShowerY(y * totalEnDep, SiTimingLayer);
                    CurrentRegion->EventInfo.at(event_id).AccumulateSiliconEnDep(totalEnDep);
                    CurrentRegion->EventInfo.at(event_id).AccumulateActiveLayerEnDep(totalEnDep, SiTimingLayer);
                }
                CurrentRegion->EventInfo.at(event_id).AccumulateSiTimingEnDep(totalEnDep);
                if (SiTimingLayer == 1)
                {
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerEnDep(totalEnDep, 1);
                }
                else if (SiTimingLayer == 2)
                {
                    CurrentRegion->EventInfo.at(event_id).AccumulateLayerEnDep(totalEnDep, 2);
                }
            }
        }

        // EventTree->Delete();
    }

    std::vector<float> RegionMaxEndep(max_Eventseq, -INFINITY);
    for (int event = 0; event < max_Eventseq; event++)
    {
        float NewShowerZ = GetShowerZ(event) / GetEnDep(event);
        SetShowerZ(NewShowerZ, event);
        float NewShowerX = GetShowerX(event) / GetEnDep(event);
        SetShowerX(NewShowerX, event);
        float NewShowerY = GetShowerY(event) / GetEnDep(event);
        SetShowerY(NewShowerY, event);
        for (auto RegionSeq : region_container)
        {
            int RegionId = RegionSeq.first;
            FRegion *CurrentRegion = RegionSeq.second;

            NewShowerZ = CurrentRegion->EventInfo.at(event).GetShowerZ() / CurrentRegion->EventInfo.at(event).GetTotEnDep();
            CurrentRegion->EventInfo.at(event).SetShowerZ(NewShowerZ);
            NewShowerX = CurrentRegion->EventInfo.at(event).GetShowerX() / CurrentRegion->EventInfo.at(event).GetTotEnDep();
            CurrentRegion->EventInfo.at(event).SetShowerX(NewShowerX);
            NewShowerY = CurrentRegion->EventInfo.at(event).GetShowerY() / CurrentRegion->EventInfo.at(event).GetTotEnDep();
            CurrentRegion->EventInfo.at(event).SetShowerY(NewShowerY);

            if (CurrentRegion->EventInfo.at(event).GetTotEnDep() > RegionMaxEndep.at(event))
            {
                RegionMaxEndep.at(event) = CurrentRegion->EventInfo.at(event).GetTotEnDep();
                SetMaxDepRegion(RegionId, event);
            }
            // //std::cout << " GunEvent.at(evtNum)->endepSiTiming : " << GunEvent.at(evtNum)->endepSiTiming << std::endl;
            int layerCount = 0;
            for (auto layer : CurrentRegion->Layers)
            {

                if (layer->layerType == 0)
                {
                    // //std::cout << "Region " << CurrentRegion->GlobalId << " layer " << layerCount << " Old Shower Z " << CurrentRegion->EventInfo.at(event).GetLayerShowerZ(layerCount)
                    //           << " EnDep " << CurrentRegion->EventInfo.at(event).GetLayerEnDep(layerCount) << std::endl;

                    NewShowerZ = CurrentRegion->EventInfo.at(event).GetLayerShowerZ(layerCount) / CurrentRegion->EventInfo.at(event).GetLayerEnDep(layerCount);
                    // //std::cout << "NewShowerZ " << NewShowerZ << std::endl;
                    CurrentRegion->EventInfo.at(event).SetLayerShowerZ(NewShowerZ, layerCount);
                    NewShowerX = CurrentRegion->EventInfo.at(event).GetLayerShowerX(layerCount) / CurrentRegion->EventInfo.at(event).GetLayerEnDep(layerCount);
                    CurrentRegion->EventInfo.at(event).SetLayerShowerX(NewShowerX, layerCount);
                    NewShowerY = CurrentRegion->EventInfo.at(event).GetLayerShowerY(layerCount) / CurrentRegion->EventInfo.at(event).GetLayerEnDep(layerCount);
                    CurrentRegion->EventInfo.at(event).SetLayerShowerY(NewShowerY, layerCount);
                }
                else if (layer->layerType == 1)
                {
                    NewShowerZ = CurrentRegion->EventInfo.at(event).GetLayerShowerZ(layerCount) / CurrentRegion->EventInfo.at(event).GetActiveLayerEnDep(layerCount);
                    // //std::cout << "NewShowerZ " << NewShowerZ << std::endl;
                    CurrentRegion->EventInfo.at(event).SetLayerShowerZ(NewShowerZ, layerCount);
                    NewShowerX = CurrentRegion->EventInfo.at(event).GetLayerShowerX(layerCount) / CurrentRegion->EventInfo.at(event).GetActiveLayerEnDep(layerCount);
                    CurrentRegion->EventInfo.at(event).SetLayerShowerX(NewShowerX, layerCount);
                    NewShowerY = CurrentRegion->EventInfo.at(event).GetLayerShowerY(layerCount) / CurrentRegion->EventInfo.at(event).GetActiveLayerEnDep(layerCount);
                    CurrentRegion->EventInfo.at(event).SetLayerShowerY(NewShowerY, layerCount);
                }
                layerCount++;
            }
        }
    }
    delete StepTree;
}

void Mixture_Calo::WritePVAndHitInfo_Rec()
{
    std::stringstream tmp_name;
    TTree *ReadoutInfo = (TTree *)ReadoutFile->Get("ReadoutInfo");
    int event;
    ReadoutInfo->SetBranchAddress("event", &event);

    int n_PV;
    ReadoutInfo->SetBranchAddress("n_PV", &n_PV);
    std::vector<float> *PositionOnAbsorberX = 0;
    ReadoutInfo->SetBranchAddress("PositionOnAbsorberX", &PositionOnAbsorberX);
    std::vector<float> *PositionOnAbsorberY = 0;
    ReadoutInfo->SetBranchAddress("PositionOnAbsorberY", &PositionOnAbsorberY);
    std::vector<float> *PositionOnAbsorberZ = 0;
    ReadoutInfo->SetBranchAddress("PositionOnAbsorberZ", &PositionOnAbsorberZ);
    std::vector<float> *MomentumOnAbsorberX = 0;
    ReadoutInfo->SetBranchAddress("MomentumOnAbsorberX", &MomentumOnAbsorberX);
    std::vector<float> *MomentumOnAbsorberY = 0;
    ReadoutInfo->SetBranchAddress("MomentumOnAbsorberY", &MomentumOnAbsorberY);
    std::vector<float> *MomentumOnAbsorberZ = 0;
    ReadoutInfo->SetBranchAddress("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
    std::vector<float> *PositionOnAbsorberT = 0;
    ReadoutInfo->SetBranchAddress("PositionOnAbsorberT", &PositionOnAbsorberT);
    std::vector<float> *EnergyOnAbsorber = 0;
    ReadoutInfo->SetBranchAddress("EnergyOnAbsorber", &EnergyOnAbsorber);

    std::vector<float> *MomentumAtVertexX = 0;
    ReadoutInfo->SetBranchAddress("MomentumAtVertexX", &MomentumAtVertexX);
    std::vector<float> *MomentumAtVertexY = 0;
    ReadoutInfo->SetBranchAddress("MomentumAtVertexY", &MomentumAtVertexY);
    std::vector<float> *MomentumAtVertexZ = 0;
    ReadoutInfo->SetBranchAddress("MomentumAtVertexZ", &MomentumAtVertexZ);
    std::vector<float> *PositionAtVertexX = 0;
    ReadoutInfo->SetBranchAddress("PositionAtVertexX", &PositionAtVertexX);
    std::vector<float> *PositionAtVertexY = 0;
    ReadoutInfo->SetBranchAddress("PositionAtVertexY", &PositionAtVertexY);
    std::vector<float> *PositionAtVertexZ = 0;
    ReadoutInfo->SetBranchAddress("PositionAtVertexZ", &PositionAtVertexZ);
    std::vector<float> *EnergyAtVertex = 0;
    ReadoutInfo->SetBranchAddress("EnergyAtVertex", &EnergyAtVertex);
    std::vector<int> *primaryPDGID = 0;
    ReadoutInfo->SetBranchAddress("primaryPDGID", &primaryPDGID);
    std::vector<int> *HitRegion = 0;
    ReadoutInfo->SetBranchAddress("HitRegion", &HitRegion);
    std::vector<int> *HitRegionType = 0;
    ReadoutInfo->SetBranchAddress("HitRegionType", &HitRegionType);
    std::vector<int> *HitModule = 0;
    ReadoutInfo->SetBranchAddress("HitModule", &HitModule);

    std::vector<float> *TimeAtVertex = 0;
    if (ReadoutInfo->GetBranch("TimeAtVertex") != NULL)
    {
        ReadoutInfo->SetBranchAddress("TimeAtVertex", &TimeAtVertex);
    }

    float TotEnDep;
    ReadoutInfo->SetBranchAddress("TotEnDep", &TotEnDep);
    float ShowerZ;
    ReadoutInfo->SetBranchAddress("ShowerZ", &ShowerZ);
    float ShowerX;
    ReadoutInfo->SetBranchAddress("ShowerX", &ShowerX);
    float ShowerY;
    ReadoutInfo->SetBranchAddress("ShowerY", &ShowerY);
    int MaxDepRegion;
    ReadoutInfo->SetBranchAddress("MaxDepRegion", &MaxDepRegion);

    int TotRegionNum = region_container.size();

    float *RegionTotEnDep = new float[TotRegionNum];
    float *RegionShowerX = new float[TotRegionNum];
    float *RegionShowerY = new float[TotRegionNum];
    float *RegionShowerZ = new float[TotRegionNum];
    float *RegionEnDepSiTiming = new float[TotRegionNum];
    float *RegionEnDepSilicon = new float[TotRegionNum];

    float *RegionLayerSig = new float[TotalLayerNumInAllRegion];
    float *RegionLayerEnDep = new float[TotalLayerNumInAllRegion];
    float *RegionLayerMinT = new float[TotalLayerNumInAllRegion];
    float *RegionLayerSeedT = new float[TotalLayerNumInAllRegion];
    float *RegionLayerPreHitT = new float[TotalLayerNumInAllRegion];
    float *RegionLayerShowerZ = new float[TotalLayerNumInAllRegion];
    float *RegionLayerShowerX = new float[TotalLayerNumInAllRegion];
    float *RegionLayerShowerY = new float[TotalLayerNumInAllRegion];

    int RegionCount = 0;
    int tmpCount = 0;
    for (auto RegionSeq : region_container)
    {
        int CurrentRegionID = RegionSeq.first;
        FRegion *CurrentRegion = RegionSeq.second;

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "TotalEnDep";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionTotEnDep[RegionCount]));
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "EnDepSiTiming";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionEnDepSiTiming[RegionCount]));
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "EnDepSilicon";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionEnDepSilicon[RegionCount]));
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerZ";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionShowerZ[RegionCount]));
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerX";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionShowerX[RegionCount]));
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerY";
        ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionShowerY[RegionCount]));

        int LayerCount = 0;
        for (auto Layer : CurrentRegion->Layers)
        {

            tmp_name.str("");
            tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "EnDep";
            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionLayerEnDep[tmpCount]));

            tmp_name.str("");
            tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "PreHitT";
            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionLayerPreHitT[tmpCount]));

            tmp_name.str("");
            tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerZ";
            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionLayerShowerZ[tmpCount]));

            tmp_name.str("");
            tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerX";
            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionLayerShowerX[tmpCount]));

            tmp_name.str("");
            tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerY";
            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &(RegionLayerShowerY[tmpCount]));

            LayerCount++;
            tmpCount++;
        }
        RegionCount++;
    }

    for (int i = 0; i < max_Eventseq; i++)
    {
        ReadoutInfo->GetEntry(i);
        SetPrimaryInfoStatus(1, event);

        for (int j = 0; j < n_PV; j++)
        {
            SetHitE(EnergyOnAbsorber->at(j), event, j);
            SetHitT(PositionOnAbsorberT->at(j), event, j);
            SetHitX(PositionOnAbsorberX->at(j), event, j);
            SetHitY(PositionOnAbsorberY->at(j), event, j);
            SetHitZ(PositionOnAbsorberZ->at(j), event, j);
            SetHitVx(MomentumOnAbsorberX->at(j), event, j);
            SetHitVy(MomentumOnAbsorberY->at(j), event, j);
            SetHitVz(MomentumOnAbsorberZ->at(j), event, j);
            SetPrimaryE(EnergyAtVertex->at(j), event, j);
            SetPrimaryT(TimeAtVertex->at(j), event, j);
            SetPrimaryPDGID(primaryPDGID->at(j), event, j);
            SetPrimaryX(PositionAtVertexX->at(j), event, j);
            SetPrimaryY(PositionAtVertexY->at(j), event, j);
            SetPrimaryZ(PositionAtVertexZ->at(j), event, j);
            SetPrimaryVx(MomentumAtVertexX->at(j), event, j);
            SetPrimaryVy(MomentumAtVertexY->at(j), event, j);
            SetPrimaryVz(MomentumAtVertexZ->at(j), event, j);

            SetHitRegion(HitRegion->at(j), event, j);
            SetHitRegionType(HitRegionType->at(j), event, j);
            SetHitModule(HitModule->at(j), event, j);

            // SetMatchedClu3D(event, NULL, j);
            // SetMatchedSubClu3D(event, NULL, j);
        }
        SetMaxDepRegion(MaxDepRegion, event);
        SetEnDep(TotEnDep, event);
        SetShowerZ(ShowerZ, event);
        SetShowerX(ShowerX, event);
        SetShowerY(ShowerY, event);
        InitMatchContainer(event);

        RegionCount = 0;

        tmpCount = 0;
        for (auto RegionSeq : region_container)
        {
            int CurrentRegionID = RegionSeq.first;
            FRegion *CurrentRegion = RegionSeq.second;

            CurrentRegion->EventInfo.at(event).SetShowerZ(RegionShowerZ[RegionCount]);
            CurrentRegion->EventInfo.at(event).SetShowerX(RegionShowerX[RegionCount]);
            CurrentRegion->EventInfo.at(event).SetShowerY(RegionShowerY[RegionCount]);
            CurrentRegion->EventInfo.at(event).SetTotEnDep(RegionTotEnDep[RegionCount]);
            CurrentRegion->EventInfo.at(event).SetSiliconSig(RegionEnDepSilicon[RegionCount]);
            CurrentRegion->EventInfo.at(event).SetSiTimingEnDep(RegionEnDepSiTiming[RegionCount]);
            int LayerCount = 0;
            for (auto Layer : CurrentRegion->Layers)
            {

                // //std::cout << "size() : " << CurrentRegion->EventInfo.size() << " Event : " << Event << std::endl;

                CurrentRegion->EventInfo.at(event).SetLayerEnDep(RegionLayerEnDep[tmpCount], LayerCount);
                CurrentRegion->EventInfo.at(event).SetLayerShowerZ(RegionLayerShowerZ[tmpCount], LayerCount);
                CurrentRegion->EventInfo.at(event).SetLayerShowerX(RegionLayerShowerX[tmpCount], LayerCount);
                CurrentRegion->EventInfo.at(event).SetLayerShowerY(RegionLayerShowerY[tmpCount], LayerCount);
                CurrentRegion->EventInfo.at(event).SetLayerSig(RegionLayerSig[tmpCount], LayerCount);

                LayerCount++;
                tmpCount++;
            }
            RegionCount++;
        }
    }
}

void Mixture_Calo::WriteData()
{

    std::stringstream tmp_name;
    TTree *ReadoutInfo = (TTree *)ReadoutFile->Get("ReadoutInfo");
    int Event;
    ReadoutInfo->SetBranchAddress("event", &Event);

    int CellMapCount = 0;
    for (auto moduleSeq : module_container)
    {

        int moduleId = moduleSeq.first;

        FModule *CurrentModule = moduleSeq.second;

        for (auto LayerSeq : CurrentModule->v_layer)
        {
            CellMapCount++;
        }
    }

    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "CellMapCount : {}", CellMapCount);

    std::vector<float> **v_Signal = 0;
    std::vector<float> **v_Time = 0;
    std::vector<float> **v_Energy = 0;
    std::vector<float> **v_RawTime = 0;
    v_Signal = new std::vector<float> *[CellMapCount];
    v_Time = new std::vector<float> *[CellMapCount];
    v_Energy = new std::vector<float> *[CellMapCount];
    v_RawTime = new std::vector<float> *[CellMapCount];

    for (int i = 0; i < CellMapCount; i++)
    {
        v_Signal[i] = 0;
        v_Time[i] = 0;
        v_Energy[i] = 0;
        v_RawTime[i] = 0;
    }

    int Count = 0;
    for (auto moduleSeq : module_container)
    {
        int moduleId = moduleSeq.first;

        FModule *CurrentModule = moduleSeq.second;

        for (auto LayerSeq : CurrentModule->v_layer)
        {
            // //std::cout << "moduleId : " << moduleId << "  LayerSeq->id " << LayerSeq->id << std::endl;
            // //std::cout << "Count " << Count << std::endl;
            //  //std::cout << "v_Signal[Count] " << v_Signal[Count] << std::endl;
            tmp_name.str("");
            tmp_name << "mod" << moduleId << "_Layer" << LayerSeq->id << "RawSignal";

            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &v_Signal[Count]);

            tmp_name.str("");
            tmp_name << "mod" << moduleId << "_Layer" << LayerSeq->id << "Time";

            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &v_Time[Count]);

            tmp_name.str("");
            tmp_name << "mod" << moduleId << "_Layer" << LayerSeq->id << "RawTime";

            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &v_RawTime[Count]);

            tmp_name.str("");
            tmp_name << "mod" << moduleId << "_Layer" << LayerSeq->id << "Energy";

            ReadoutInfo->SetBranchAddress(tmp_name.str().c_str(), &v_Energy[Count]);
            Count++;
        }
    }

    for (int loop = 0; loop < ReadoutInfo->GetEntries(); loop++)
    {

        ReadoutInfo->GetEntry(loop);

        CellMapCount = 0;
        for (auto moduleSeq : module_container)
        {

            int moduleId = moduleSeq.first;

            FModule *CurrentModule = moduleSeq.second;

            for (auto LayerSeq : CurrentModule->v_layer)
            {
                // //std::cout << "moduleId : " << moduleId << "  LayerSeq->id " << LayerSeq->id << std::endl;
                //  //std::cout << "v_Signal[CellMapCount]->size() " << v_Signal[CellMapCount]->size() << std::endl;
                for (int CellCount = 0; CellCount < v_Signal[CellMapCount]->size(); CellCount++)
                {
                    // //std::cout << "CellCount : " << CellCount << " LayerSeq->v_cell.size() " << LayerSeq->v_cell.size() << " v_Time[CellMapCount]->size() " << v_Time[CellMapCount]->size() << std::endl;

                    LayerSeq->v_cell.at(CellCount)->SetRawSignal(Event, v_Signal[CellMapCount]->at(CellCount));
                    LayerSeq->v_cell.at(CellCount)->SetTime(Event, v_Time[CellMapCount]->at(CellCount));
                    LayerSeq->v_cell.at(CellCount)->SetEnergy(Event, v_Energy[CellMapCount]->at(CellCount));
                    LayerSeq->v_cell.at(CellCount)->SetRawTime(Event, v_RawTime[CellMapCount]->at(CellCount));
                }

                CellMapCount++;
            }
        }
    }

    delete ReadoutInfo;
}

void Mixture_Calo::InitCaliPara()
{
    std::stringstream tmp_name;
    // TCanvas *c1 = new TCanvas("tmpc");
    TCut a("");
    if (Parameters::Instance()->CalibrationFileName.size() != TotalRegionType)
    {
        spdlog::get(ERROR_NAME)->error("ERROR! Length of CalibrationFileName {} and TotalRegionId {} must be same!!! Aborting...", Parameters::Instance()->CalibrationFileName.size(), TotalRegionType);
        exit(1);
    }
    for (auto region : region_container)
    {
        FRegion *CurrentRegion = region.second;
        int CurrentRegionId = CurrentRegion->GlobalId;
        int CurretnRegionType = CurrentRegion->type;
        int layerNum = CurrentRegion->Layers.size();
        if (!ModuleStudy && !RegionStudy)
        {
            CurrentRegion->CaliFileName = Parameters::Instance()->CalibrationFileName.at(CurretnRegionType - 1);
            CurrentRegion->CaliFile = TFile::Open(CurrentRegion->CaliFileName.c_str());
        }
        else
        {
            CurrentRegion->CaliFileName = Parameters::Instance()->CalibrationFileName.at(0);
            CurrentRegion->CaliFile = TFile::Open(CurrentRegion->CaliFileName.c_str());
        }

        if (ProgramType == 0 || ProgramType == 2 || ProgramType == 4)
        {
            for (int layerCount = 0; layerCount < CurrentRegion->Layers.size(); layerCount++)
            {

                if (CurrentRegion->Layers.at(layerCount)->layerType == 1 && CurrentRegion->CombineSiEnWhenCali)
                {
                    if (CurrentRegion->Layers.at(layerCount)->CellECaliFunType == 0)
                    {
                        CurrentRegion->Layers.at(layerCount)->CellECaliFun = (TF1 *)CurrentRegion->CaliFile->Get("SiTimingCellCaliLinear");
                    }
                    else
                    {
                        CurrentRegion->Layers.at(layerCount)->CellECaliFun = (TF1 *)CurrentRegion->CaliFile->Get("SiTimingCellCaliLog");
                    }
                }
                else
                {
                    if (CurrentRegion->Layers.at(layerCount)->CellECaliFunType == 0)
                    {
                        spdlog::get(LOG_NAME)->info("In Region {}, Using Linear function to calibrate Cells", CurrentRegionId);
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CellCaliLinear";
                        CurrentRegion->Layers.at(layerCount)->CellECaliFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    }
                    else
                    {
                        spdlog::get(LOG_NAME)->info("In Region {}, Using Log function to calibrate Cells", CurrentRegionId);
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CellCaliLog";
                        CurrentRegion->Layers.at(layerCount)->CellECaliFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    }
                }
            }
            if (ProgramType == 0 || ProgramType == 4) // 以下参数仅在正式cluster的时候初始化
            {
                CurrentRegion->CluECaliFun = (TF1 *)CurrentRegion->CaliFile->Get("CluECaliFun");
                CurrentRegion->CluECor2Fun = (TF1 *)CurrentRegion->CaliFile->Get("CluECor2Fun");
                if (ProgramType == 0)
                {
                    tmp_name.str("");
                    tmp_name << "TResFun";
                    CurrentRegion->TResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "EResFun";
                    CurrentRegion->EResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "PosXResFun";
                    CurrentRegion->PosXResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "PosYResFun";
                    CurrentRegion->PosYResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                }
                if (CluFlow == 1 || CluFlow == 2)
                {
                    CurrentRegion->ERatio_FrontE = (TF1 *)CurrentRegion->CaliFile->Get("ERatio_FrontE");
                    CurrentRegion->ERatio_FrontE_Resolution = (TF1 *)CurrentRegion->CaliFile->Get("ERatio_FrontE_Resolution");
                    CurrentRegion->ERatio_E = (TF1 *)CurrentRegion->CaliFile->Get("ERatio_E");
                    CurrentRegion->ERatio_E_Resolution = (TF1 *)CurrentRegion->CaliFile->Get("ERatio_E_Resolution");
                    for (int layerCount = 0; layerCount < CurrentRegion->Layers.size(); layerCount++)
                    {
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "PosXResFun";
                        CurrentRegion->Layers.at(layerCount)->PosXResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "PosYResFun";
                        CurrentRegion->Layers.at(layerCount)->PosYResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluType0SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluType0SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluType0SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluType0SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP1SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP1SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP1SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP1SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM1SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM1SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM1SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM1SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP2SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP2SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP2SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP2SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM2SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM2SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM2SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM2SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP3SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP3SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeP3SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeP3SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM3SCorX";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM3SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "CluTypeM3SCorY";
                        CurrentRegion->Layers.at(layerCount)->CluTypeM3SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "LCorrFun";
                        CurrentRegion->Layers.at(layerCount)->LCorrFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "TShiftFun";
                        CurrentRegion->Layers.at(layerCount)->TShiftFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "TResFun";
                        CurrentRegion->Layers.at(layerCount)->TResFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "S1xCorPFun";
                        CurrentRegion->Layers.at(layerCount)->S1xCorPFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "S1xCorNFun";
                        CurrentRegion->Layers.at(layerCount)->S1xCorNFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "S1yCorPFun";
                        CurrentRegion->Layers.at(layerCount)->S1yCorPFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "Layer" << layerCount << "S1yCorNFun";
                        CurrentRegion->Layers.at(layerCount)->S1yCorNFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        if (ProgramType == 0)
                        {
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Cell_N";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Cell_N = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Cell_P";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Cell_P = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Seed_Down_Corner";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Seed_Down_Corner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Seed_Up_Edge";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Seed_Up_Edge = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Seed_Down_Edge";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Seed_Down_Edge = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Seed_Up_Corner";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Seed_Up_Corner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                            tmp_name.str("");
                            tmp_name << "Layer" << layerCount << "TransversalProfile_Seed_Up_UpCorner";
                            CurrentRegion->Layers.at(layerCount)->TransversalProfile_Seed_Up_UpCorner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        }

                        // tmp_name.str("");
                        // tmp_name << "Layer" << layerCount << "ECaliFun";
                        // CurrentRegion->Layers.at(layerCount)->ECaliFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    }
                }
                else if (CluFlow == 3)
                {
                    tmp_name.str("");
                    tmp_name << "CluType0SCorX";
                    CurrentRegion->CluType0SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluType0SCorY";
                    CurrentRegion->CluType0SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP1SCorX";
                    CurrentRegion->CluTypeP1SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP1SCorY";
                    CurrentRegion->CluTypeP1SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM1SCorX";
                    CurrentRegion->CluTypeM1SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM1SCorY";
                    CurrentRegion->CluTypeM1SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP2SCorX";
                    CurrentRegion->CluTypeP2SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP2SCorY";
                    CurrentRegion->CluTypeP2SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM2SCorX";
                    CurrentRegion->CluTypeM2SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM2SCorY";
                    CurrentRegion->CluTypeM2SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP3SCorX";
                    CurrentRegion->CluTypeP3SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeP3SCorY";
                    CurrentRegion->CluTypeP3SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM3SCorX";
                    CurrentRegion->CluTypeM3SCorX = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "CluTypeM3SCorY";
                    CurrentRegion->CluTypeM3SCorY = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "LCorrFun";
                    CurrentRegion->LCorrFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    tmp_name.str("");
                    tmp_name << "TShiftFun";
                    CurrentRegion->TShiftFun = (TF1 *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());

                    if (ProgramType == 0)
                    {
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Cell_N";
                        CurrentRegion->TransversalProfile_Cell_N = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Cell_P";
                        CurrentRegion->TransversalProfile_Cell_P = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Seed_Down_Corner";
                        CurrentRegion->TransversalProfile_Seed_Down_Corner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Seed_Up_Edge";
                        CurrentRegion->TransversalProfile_Seed_Up_Edge = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Seed_Down_Edge";
                        CurrentRegion->TransversalProfile_Seed_Down_Edge = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Seed_Up_Corner";
                        CurrentRegion->TransversalProfile_Seed_Up_Corner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                        tmp_name.str("");
                        tmp_name << "TransversalProfile_Seed_Up_UpCorner";
                        CurrentRegion->TransversalProfile_Seed_Up_UpCorner = (TH1F *)CurrentRegion->CaliFile->Get(tmp_name.str().c_str());
                    }
                }
            }
        }
    }
    // delete c1;
}

void Mixture_Calo::CalibrateCell()
{
    for (int event = 0; event < max_Eventseq; event++)
    {
        for (auto module : module_container)
        {
            FModule *Current_module = module.second;
            FRegion *Current_Region = module.second->f_region;

            // std::cout << "in region : " << Current_Region->GlobalId << " module : " << Current_module->GlobalId << std::endl;
            for (auto layer : Current_module->v_layer)
            {
                int layer_id = layer->id;

                for (auto Cell : layer->v_cell)
                {
                    // std::cout << "in layer " << layer->id << " cell : " << Cell->GetID() << " Cell->energy.size()" << Cell->energy.size() << std::endl;

                    if (Cell->GetRawSignal(event) > 0)
                    {
                        float Current_RawSignal = Cell->GetRawSignal(event);
                        float CaliEnergy = Current_Region->Layers.at(layer_id)->CellECaliFun->Eval(Current_RawSignal);
                        Cell->SetEnergy(event, CaliEnergy);

                        float et = Cell->GetEnergy(event) * sqrt(pow(Cell->X() - 0, 2) + pow(Cell->Y() - 0, 2)) / sqrt(pow(Cell->X() - 0, 2) + pow(Cell->Y() - 0, 2) + pow(Cell->Z() + 12836, 2));

                        Cell->SetEt(event, et);
                        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Module {}", Current_module->GetID());
                        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Layer {}", layer_id);
                        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tevent : {}", event);
                        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\t\tCell {}, RawSignal = {}, CaliE = {}", Cell->GetID(), Current_RawSignal, CaliEnergy);
                    }
                    else
                    {
                        Cell->SetEnergy(event, 0);
                        Cell->SetEt(event, 0);
                    }
                }
            }
        }
    }
}

void Mixture_Calo::ConstructCell3D()
{
    for (auto module : module_container)
    {
        FModule *Current_module = module.second;
        FRegion *Current_Region = module.second->f_region;
        int region_id = Current_Region->GlobalId;
        float module_x = Current_module->X();
        float module_y = Current_module->Y();
        float module_z = Current_module->Z();
        float module_Dz = Current_module->Dz();
        float module_Ax = Current_module->Ax();
        float module_Ay = Current_module->Ay();
        float module_Az = Current_module->Az();
        int module_id = Current_module->GlobalId;
        int tmp_xbin = Current_Region->cell3D_map.GetNbinsX();
        int tmp_ybin = Current_Region->cell3D_map.GetNbinsY();
        Current_module->v_Cell3D.resize(tmp_xbin * tmp_ybin);
        for (int x_loop = 1; x_loop <= tmp_xbin; x_loop++) // loop cell map
            for (int y_loop = 1; y_loop <= tmp_ybin; y_loop++)
            {
                float tmp_cell_x = Current_Region->cell3D_map.GetXaxis()->GetBinCenter(x_loop);
                float tmp_cell_y = Current_Region->cell3D_map.GetYaxis()->GetBinCenter(y_loop);
                float tmp_cell_xsize = Current_Region->cell3D_map.GetXaxis()->GetBinWidth(x_loop);
                float tmp_cell_ysize = Current_Region->cell3D_map.GetYaxis()->GetBinWidth(y_loop);
                float tmp_cell_id = Current_Region->cell3D_map.GetBinContent(Current_Region->cell3D_map.GetBin(x_loop, y_loop));
                // //std::cout << "module : " << module_id << " cell3D : " << tmp_cell_id << " x : " << tmp_cell_x + module_x << " y : " << tmp_cell_y + module_y << std::endl;
                Current_module->v_Cell3D[tmp_cell_id] = new FCell3D(tmp_cell_id, tmp_cell_x, tmp_cell_y, 0, tmp_cell_xsize, tmp_cell_ysize, module_Dz, module_Ax, module_Ay, module_Az);
                // //std::cout << "Current_module->v_Cell3D[tmp_cell_id] : " << Current_module->v_Cell3D[tmp_cell_id]->GetID() << std::endl;

                Current_module->v_Cell3D[tmp_cell_id]->SetMotherModule(Current_module);

                Current_module->v_Cell3D[tmp_cell_id]->SetGlobal_id(FCell::CalculateGlobalId(region_id, module_id, tmp_cell_id));

                Current_module->v_Cell3D[tmp_cell_id]->InitEventInfo();
                Current_module->v_Cell3D[tmp_cell_id]->v_Cell.resize(Current_Region->TotLayerNum);
                FPoint AbsoluteSeedPos = Current_Region->Local2Global(tmp_cell_x, tmp_cell_y, 0);
                float SeedGlobalx = AbsoluteSeedPos.X() + module_x;
                float SeedGlobaly = AbsoluteSeedPos.Y() + module_y;
                float SeedGlobalz = AbsoluteSeedPos.Z() + module_z;
                Current_module->v_Cell3D[tmp_cell_id]->SetGlobalPosition(SeedGlobalx, SeedGlobaly, SeedGlobalz);
            }

        for (auto layer : Current_module->v_layer)
        {
            for (auto cell : layer->v_cell)
            {
                float cell_xpos = cell->LocalX();
                float cell_ypos = cell->LocalY();
                int Cell3DId = Current_Region->GetCell3DLocalIdByRelaPos(cell_xpos, cell_ypos);
                cell->SetMotherCell3D(Current_module->v_Cell3D.at(Cell3DId));
                Current_module->v_Cell3D.at(Cell3DId)->v_Cell.at(layer->id).emplace_back(cell);
                for (int event = 0; event < max_Eventseq; event++)
                {
                    // //std::cout << "event : " << event << std::endl;
                    float CurrentEnergy = cell->GetEnergy(event);
                    float CurrentTime = cell->GetTime(event);

                    float NewE = CurrentEnergy + Current_module->v_Cell3D[Cell3DId]->GetEnergy(event);
                    float NewZ3D = CurrentEnergy * cell->LocalZ() + Current_module->v_Cell3D[Cell3DId]->GetZ3D(event);
                    float NewTime = CurrentEnergy * CurrentTime + Current_module->v_Cell3D[Cell3DId]->GetTime(event);

                    Current_module->v_Cell3D[Cell3DId]->SetEnergy(event, NewE);
                    Current_module->v_Cell3D[Cell3DId]->SetZ3D(event, NewZ3D);
                    Current_module->v_Cell3D[Cell3DId]->SetTime(event, NewTime);
                }
            }
        }
        for (int event = 0; event < max_Eventseq; event++)
        {
            for (auto Cell3D : Current_module->v_Cell3D)
            {

                float CurrentEnergy = Cell3D->GetEnergy(event);
                float NewZ3D = Cell3D->GetZ3D(event) / CurrentEnergy;
                float NewTime = Cell3D->GetTime(event) / CurrentEnergy;
                Cell3D->SetZ3D(event, NewZ3D);
                Cell3D->SetTime(event, NewTime);
                float et = Cell3D->GetEnergy(event) * sqrt(pow(Cell3D->X() - 0, 2) + pow(Cell3D->Y() - 0, 2)) / sqrt(pow(Cell3D->X() - 0, 2) + pow(Cell3D->Y() - 0, 2) + pow(Cell3D->GetZ3D(event) + 12836., 2));

                Cell3D->SetEt(event, et);
            }
        }
    }
}

void Mixture_Calo::Seeding3D(int event) // finding seed in 3D cell
{

    int SeedCount = 0;

    for (auto module : module_container)
    {
        FModule *Current_module = module.second;
        FRegion *Current_Region = Current_module->f_region;
        // std::cout << Current_Region->Seed3DCut << std::endl;

        if (Current_Region->Seed3DMethod == 0)
        {

            for (auto LMax : Current_module->GetEventInfo(event)->LocalMaxCell3D)
            {
                // std::cout << "LMax->GetEt(event) " << LMax->GetEt(event) << std::endl;
                if (LMax->GetEt(event) > Current_Region->Seed3DCut)
                {
                    Current_module->GetEventInfo(event)->Seed3D.insert(LMax);
                    Get_sSeed3D(event)->insert(LMax);
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "In Region {}, Module {}", Current_Region->GlobalId, Current_module->GetID());
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tFind Seed3D E : {}, Et : {}, X : {}, Y : {}, Z : {}, Type : {}", LMax->GetEnergy(event), LMax->GetEt(event), LMax->X(), LMax->Y(), LMax->Z(), LMax->IsLocalMax(event));
                    SeedCount++;
                    // std::cout << "0 LMax : " << LMax->GetEnergy(event) << std::endl;
                }
            }
        }
        else if (Current_Region->Seed3DMethod == 1)
        {

            for (auto LMax : Current_module->GetEventInfo(event)->LocalMaxCell3D_Cross)
            {
                if (LMax->GetEt(event) > Current_Region->Seed3DCut)
                {
                    Current_module->GetEventInfo(event)->Seed3D.insert(LMax);
                    Get_sSeed3D(event)->insert(LMax);
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "In Region {}, Module {}", Current_Region->GlobalId, Current_module->GetID());
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tFind Seed3D E : {}, Et : {}, X : {}, Y : {}, Z : {}, Type : {}", LMax->GetEnergy(event), LMax->GetEt(event), LMax->X(), LMax->Y(), LMax->Z(), LMax->IsLocalMax(event));
                    SeedCount++;
                }
            }
        }
        else if (Current_Region->Seed3DMethod == 2)
        {

            for (auto LMax : Current_module->GetEventInfo(event)->LocalMaxCell3D_Fork)
            {
                if (LMax->GetEt(event) > Current_Region->Seed3DCut)
                {
                    Current_module->GetEventInfo(event)->Seed3D.insert(LMax);
                    Get_sSeed3D(event)->insert(LMax);
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "In Region {}, Module {}", Current_Region->GlobalId, Current_module->GetID());
                    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tFind Seed3D E : {}, Et : {}, X : {}, Y : {}, Z : {}, Type : {}", LMax->GetEnergy(event), LMax->GetEt(event), LMax->X(), LMax->Y(), LMax->Z(), LMax->IsLocalMax(event));
                    SeedCount++;
                }
            }
        }
    }
    std::cout << "SeedCount " << SeedCount << std::endl;
    spdlog::get(LOG_NAME)->info("In event : {}, has {} Seed3D", event, Get_sSeed3D(event)->size());
}

void Mixture_Calo::Seeding(int event)
{

        for (auto module : module_container)
        {
            FModule *Current_module = module.second;
            FRegion *Current_Region = module.second->f_region;
            for (auto layer : Current_module->v_layer)
            {

                std::vector<FCell *> seedSerial;

                for (auto LMax : layer->EventInfo.at(event).LocalMaxCell)
                {
                    if (LMax->GetEt(event) > Current_Region->Layers[layer->id]->Seed2DCut)
                    {
                        layer->EventInfo.at(event).Seed2D.insert(LMax);
                        Get_sSeed2D(event, Current_Region->GetType(), layer->id)->insert(LMax);
                    }
                }
            }
        }
        std::vector<int> layerSeedCount(2, 0);
        for (auto RegionType : m_RegionType)
        {
            auto regionType = RegionType.first;
            auto RegionInfo = RegionType.second;
            for (int layer = 0; layer < RegionInfo->LayerNum; layer++)
            {
                spdlog::get(LOG_NAME)->info("In event : {}, region Type : {}, layer : {}, has {} Seed2D", event, regionType, layer, Get_sSeed2D(event, regionType, layer)->size());
                layerSeedCount[layer] = layerSeedCount[layer] + Get_sSeed2D(event, regionType, layer)->size();
            }
        }
        spdlog::get(LOG_NAME)->info("In event : {}, layer : 0, has {} Seed2D", event, layerSeedCount[0]);
        spdlog::get(LOG_NAME)->info("In event : {}, layer : 1, has {} Seed2D", event, layerSeedCount[1]);
}

void Mixture_Calo::ContructSeed3D(int event) // combing 2D seed, then get 3d seed
{
    // std::cout << "  Constructing 3D Seed................" << std::endl;
}

void Mixture_Calo::AllocateCell3D(int event)
{
    for (auto module : module_container)
    {
        FModule *Current_module = module.second;
        FRegion *Current_Region = module.second->f_region;
        for (int event = 0; event < max_Eventseq; event++)
        {

            for (auto seed : Current_module->GetEventInfo(event)->Seed3D)
            {
                auto tmpClu3D = Current_Region->AlloCell3D_CAllBack(seed, event);
                if (CluFlow == 3)
                {
                    Get_vClu3D(event)->emplace_back(tmpClu3D);
                }
                Current_module->GetEventInfo(event)->Cluster3D.emplace_back(tmpClu3D);
            }
        }
    }
}

void Mixture_Calo::AllocateCell(int event)
{

    for (auto module : module_container)
    {
        FModule *Current_module = module.second;
        FRegion *Current_Region = module.second->f_region;
        for (auto layer : Current_module->v_layer)
        {

            for (auto seed : layer->EventInfo.at(event).Seed2D)
            {
                // Current_Region->Layers.at(layer->id)->AlloCell_CAllBack(seed, event);
                auto tmpClu2D = seed->ConstructClu2D(event);
                layer->EventInfo.at(event).Cluster2D.emplace_back(tmpClu2D);
                Current_Region->EventInfo.at(event).v_Cluster2D[layer->id].emplace_back(tmpClu2D);

                Get_vClu2D(event, Current_Region->type, layer->id)->emplace_back(tmpClu2D);
                // std::cout << "layer " << layer->id << " size " << Get_vClu2D(event, Current_Region->type, layer->id)->size() << std::endl;
            }
        }
    }
}

std::vector<FCluster2D *> Mixture_Calo::GetMatchingClu2D(FCluster2D *Clu2D, int layer)
{
    if (Clu2D->layer_id == layer)
    {
        spdlog::get(ERROR_NAME)->error("Can't find Matching Clu2D in same layer ({},{})", Clu2D->layer_id, layer);
        exit(0);
    }
    std::vector<FCluster2D *> Result;
    auto Clu2DModule = Clu2D->f_module;
    auto Clu2DRegion = Clu2D->f_region;
    float LayerFrontFace = Clu2DRegion->Layers.at(layer)->layerThickness + Clu2DRegion->Layers.at(layer)->layerPos + Clu2DRegion->RegionShift;

    float PVX = 0, PVY = 0, PVZ = -12836;
    float entry_x = (((LayerFrontFace - PVZ) / (Clu2D->Z() - PVZ)) * (Clu2D->X() - PVX) + PVX);
    float entry_y = (((LayerFrontFace - PVZ) / (Clu2D->Z() - PVZ)) * (Clu2D->Y() - PVY) + PVY);
    float Clu2DSeedXSize = Clu2D->Seed2D->Dx();
    float Clu2DSeedYSize = Clu2D->Seed2D->Dy();
    int event = Clu2D->event;

    for (auto tmpClu2D : Clu2DModule->v_layer.at(layer)->EventInfo.at(event).Cluster2D)
    {
        if (abs(tmpClu2D->X() - entry_x) < 1.5 * Clu2DSeedXSize && abs(tmpClu2D->Y() - entry_y) < 1.5 * Clu2DSeedYSize)
        {
            Result.emplace_back(tmpClu2D);
        }
    }

    for (auto NeiModule : Clu2DModule->Neighbours)
    {
        if (NeiModule->f_region->type != Clu2DModule->f_region->type) // only consider Clu2D with same type of region
        {
            continue;
        }

        for (auto tmpClu2D : NeiModule->v_layer.at(layer)->EventInfo.at(event).Cluster2D)
        {
            if (abs(tmpClu2D->X() - entry_x) < 1.5 * Clu2DSeedXSize && abs(tmpClu2D->Y() - entry_y) < 1.5 * Clu2DSeedYSize)
            {
                Result.emplace_back(tmpClu2D);
            }
        }
    }
    return Result;
}

void Mixture_Calo::ConstructCluster3D(int event)
{

    int Clu2DCount = 0;
    int Clu3DCount = 0;
    for (auto RegionSeq : region_container)
    {
        int RegionId = RegionSeq.first;
        auto tmpRegion = RegionSeq.second;
        // Start from Clu2D in front layer
        for (auto Clu2D : tmpRegion->EventInfo.at(event).v_Cluster2D.at(0))
        {
            Clu2DCount++;
            FCluster3D *tmpClu3D = new FCluster3D(event);
            tmpClu3D->v_Cluster2D.emplace_back(Clu2D);
            tmpClu3D->f_module = Clu2D->f_module;
            tmpClu3D->f_region = Clu2D->f_region;
            Clu2D->f_Clu3D = tmpClu3D;
            bool FailedTag = 0;
            for (int layer = 1; layer < tmpRegion->TotLayerNum; layer++)
            {
                auto v_MathcingCandidates = GetMatchingClu2D(Clu2D, layer);
                std::map<float, FCluster2D *> m_MathcingCandidates;
                for (auto MathcingCandidate : v_MathcingCandidates)
                {
                    float Probability = MatchProbabilityClu2D({Clu2D, MathcingCandidate});
                    m_MathcingCandidates.emplace(Probability, MathcingCandidate);
                }
                if (m_MathcingCandidates.size() > 0)
                {
                    tmpClu3D->v_Cluster2D.emplace_back(m_MathcingCandidates.rbegin()->second); // rbegin() means the max probability
                    m_MathcingCandidates.rbegin()->second->f_Clu3D = tmpClu3D;
                }
                else
                {
                    FailedTag = 1;
                    break;
                }
            }
            if (FailedTag != 1)
            {
                Clu3DCount++;
                tmpClu3D->RecRawInfoByClu2D();
                tmpClu3D->f_module->GetEventInfo(event)->Cluster3D.emplace_back(tmpClu3D);
                Get_vClu3D(event)->emplace_back(tmpClu3D);
            }
            else
            {
                delete tmpClu3D;
            }
        }
    }
    // std::cout << "Clu2DCount " << Clu2DCount << std::endl;
    // std::cout << "Clu3DCount " << Clu3DCount << std::endl;
}

void Mixture_Calo::ReAllo2DCell(int event)
{

        for (int i = 0; i < Get_nPV(event); i++)
        {
            SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tPrimaryE : {}", GetPrimaryE(event, i));
        }

        for (auto module : module_container)
        {
            FModule *CurrenModule = module.second;
            FRegion *tmpRegion = CurrenModule->f_region;
            for (auto Cluster3D : CurrenModule->GetEventInfo(event)->Cluster3D)
            {
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tBefore reallocation Clu3D E : {}", Cluster3D->RawEnergy());
                for (auto Clu2D : Cluster3D->v_Cluster2D)
                {
                    // for (auto v_CellSeq : Clu2D->Cluster_Cell)
                    // {
                    //     int CellType = v_CellSeq.first;
                    //     auto v_Cell = v_CellSeq.second;
                    //     for (auto Cell : v_Cell)
                    //     {
                    //         Cell->RemoveMotherClu2D(Clu2D);
                    //     }
                    // }
                    spdlog::get(LOG_NAME)->info("\t\tIn Region : {}", Clu2D->f_region->GlobalId);
                    spdlog::get(LOG_NAME)->info("\t\tBefore reallocation Clu2D E : {}", Clu2D->RawEnergy());
                    Clu2D->ReAllocateCell();
                    spdlog::get(LOG_NAME)->info("\t\tAfter reallocation Clu2D E : {}", Clu2D->RawEnergy());
                }
                Cluster3D->RecRawInfoByClu2D();

                if (Parameters::Instance()->ProgramType == 0)
                {
                    float tmpTruERatio = tmpRegion->ERatio_E->Eval(Cluster3D->Energy() / 1000);
                    float tmpERatioResolution = tmpRegion->ERatio_E_Resolution->Eval(Cluster3D->Energy() / 1000);
                    float tmpRecERatio;
                    if (Cluster3D->v_Cluster2D.at(1)->Energy() == 0)
                    {
                        tmpRecERatio = 2.5;
                    }
                    else
                    {
                        tmpRecERatio = Cluster3D->v_Cluster2D.at(0)->Energy() / Cluster3D->v_Cluster2D.at(1)->Energy();
                    }
                    // std::cout << "tmpTruERatio " << tmpTruERatio << std::endl;
                    // std::cout << "tmpERatioResolution " << tmpERatioResolution << std::endl;
                    // std::cout << "tmpRecERatio " << tmpRecERatio << std::endl;
                    if (abs(tmpRecERatio - tmpTruERatio) < 3 * tmpERatioResolution)
                    {
                        Get_vClu3D(event)->emplace_back(Cluster3D);
                        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "\tAfter reallocation Clu3D E : {}", Cluster3D->RawEnergy());
                    }
                    else
                    {
                        spdlog::get(LOG_NAME)->info("Not pass layerERatio Cut");
                    }
                }
                else
                {
                    Get_vClu3D(event)->emplace_back(Cluster3D);
                }
            }
        }
        // std::sort(Get_vClu3D(event)->begin(), Get_vClu3D(event)->end());
        // auto last = std::unique(Get_vClu3D(event)->begin(), Get_vClu3D(event)->end());
        // Get_vClu3D(event)->erase(last, Get_vClu3D(event)->end());
        spdlog::get(LOG_NAME)->info("In event {}, Rec {} Clu3D", event, Get_vClu3D(event)->size());
}

void Mixture_Calo::SplitOverlapClu3D(int event)
{

        for (size_t i = 0; i < Get_vClu3D(event)->size(); i++)
        {
            auto Clu3D1 = Get_vClu3D(event)->at(i);
            for (size_t j = i + 1; j < Get_vClu3D(event)->size(); j++)
            {
                auto Clu3D2 = Get_vClu3D(event)->at(j);
                // //std::cout << "test2" << std::endl;
                // if (Clu3D1->f_region->type == Clu3D2->f_region->type)
                {
                    // //std::cout << "test1" << std::endl;
                    if (CluFlow == 2)
                    {
                        int Flag = 0;
                        for (int i = 0; i < Clu3D1->f_region->TotLayerNum; i++)
                        {
                            // //std::cout << "test " << std::endl;
                            auto Clu2D1 = Clu3D1->v_Cluster2D.at(i);
                            auto Clu2D2 = Clu3D2->v_Cluster2D.at(i);
                            if (SplitClu2DCellE({Clu2D1, Clu2D2}))
                            {
                                Flag = 1;
                                // Clu2D1->PrintCellInfo();
                                // Clu2D2->PrintCellInfo();
                            }
                        }
                        if (Flag == 1)
                        {
                            Clu3D1->RecRawInfoByClu2D();
                            Clu3D2->RecRawInfoByClu2D();
                            // Clu3D1->ECorr();
                            // Clu3D1->CorrectionPos();
                            // Clu3D2->ECorr();
                            // Clu3D2->CorrectionPos();
                        }
                    }
                    else if (CluFlow == 3)
                    {

                        SplitClu3DCell3DE({Clu3D1, Clu3D2});
                    }
                }
            }
        }
}

void Mixture_Calo::SplitOverlapClu2D(int event)
{

        for (auto RegionTypeSeq : m_RegionType)
        {
            int type = RegionTypeSeq.first;
            auto s_Type = RegionTypeSeq.second;
            for (int layer = 0; layer < s_Type->LayerNum; layer++)
            {
                auto v_Clu2D = Get_vClu2D(event, type, layer);
                for (int i = 0; i < v_Clu2D->size(); i++)
                {
                    for (int j = i + 1; j < v_Clu2D->size(); j++)
                    {
                        auto Clu2D1 = v_Clu2D->at(i);
                        auto Clu2D2 = v_Clu2D->at(j);
                        SplitClu2DCellE({Clu2D1, Clu2D2});
                    }
                }
            }
        }
}

void Mixture_Calo::CorrectCluster3DEnergy(int event)
{

        for (auto ModuleSeq : module_container)
        {
            FModule *CurrentModule = ModuleSeq.second;
            FRegion *CurrentRegion = CurrentModule->f_region;

            for (auto Cluster3D : CurrentModule->GetEventInfo(event)->Cluster3D)
            {
                // //std::cout << "test e cali b" << Cluster3D->RawE << std::endl;
                Cluster3D->ECorr();
                // //std::cout << "test e cali a" << Cluster3D->RawE << std::endl;
            }
        }
}

void Mixture_Calo::CorrectCluster3DPosition(int event)
{

    for (auto ModuleSeq : module_container)
    {
        FModule *CurrentModule = ModuleSeq.second;
        FRegion *CurrentRegion = CurrentModule->f_region;

        for (auto Cluster3D : CurrentModule->GetEventInfo(event)->Cluster3D)
        {

            Cluster3D->CorrectionPos();
        }
    }
}

void Mixture_Calo::CaculateClusterTime(int event)
{
    for (auto module : module_container)
    {
        FModule *CurrenModule = module.second;
        for (int event = 0; event < max_Eventseq; event++)
        {

            for (auto Cluster3D : CurrenModule->GetEventInfo(event)->Cluster3D)
            {
                Cluster3D->TCorr();
            }
        }
    }
}

void Mixture_Calo::CorrectCluster3D(int event)
{
    // //std::cout << "      First Calibrating Cluster3D Energy " << std::endl;
    // CorrectCluster3DEnergy();
    // //std::cout << "      Split Cluster3D Cell Energy Cluster3D.   The Clu3D Energy Be Reset Please Recorrect Clu3D Energy" << std::endl;
    // SplitOverlapClu3D();
    spdlog::get(LOG_NAME)->info("\tCalibrating Cluster3D Energy");
    CorrectCluster3DEnergy(event);
    spdlog::get(LOG_NAME)->info("\tCalibrating Cluster3D Position");

    CorrectCluster3DPosition(event);
    spdlog::get(LOG_NAME)->info("\tCaculating Cluster Time");

    CaculateClusterTime(event);
}

void Mixture_Calo::RecGamma()
{
    for (int event = 0; event < max_Eventseq; event++)
    {

        for (auto Cluster3D : *Get_vClu3D(event)) // Direct Gamma
        {
            // //std::cout << "test gamma " << Cluster3D->RawE << std::endl;
            FGamma *tmpGamma = new FGamma(Cluster3D);
            Get_vGamma(event)->emplace_back(tmpGamma);

            if (CluFlow == 0) // Sub Gamma
            {
            }
            else if (CluFlow == 1 || CluFlow == 2) //
            {
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Split cluster 3D by cluster 2D");
                SplitClu3DByClu2D(Cluster3D);
            }
            else if (CluFlow == 3) // 只构造3DCell
            {
                SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Split cluster 3D by cluster 3D");
                SplitClu3D(Cluster3D);
            }
        }

        for (auto SubClu3DPair : *Get_vSubClu3D(event))
        {
            FGamma *tmpGamma1 = new FGamma(SubClu3DPair.first, 1);
            FGamma *tmpGamma2 = new FGamma(SubClu3DPair.second, 1);
            std::pair<FGamma *, FGamma *> tmpGammaPair(tmpGamma1, tmpGamma2);
            Get_vSubGamma(event)->emplace_back(tmpGammaPair);
        }
    }
}

void Mixture_Calo::RecElectron()
{
}

void Mixture_Calo::RecResolvedPi0()
{
    spdlog::get(LOG_NAME)->info("\tRec Gamma before Rec Pi0");

    // RecGamma();
    //  std::cout << Get_vGamma(0)->size() << std::endl;
    for (int event = 0; event < max_Eventseq; event++)
    {
        for (size_t i = 0; i < Get_vGamma(event)->size(); i++)
        {
            for (size_t j = i + 1; j < Get_vGamma(event)->size(); j++)
            {
                auto Gamma1 = Get_vGamma(event)->at(i);
                auto Gamma2 = Get_vGamma(event)->at(j);
                if ((Gamma1->time - Gamma2->time) < 300)
                {
                    std::pair<FGamma *, FGamma *> tmpGammaPair = {Gamma1, Gamma2};
                    FPi0 *tmpPi0 = new FPi0(tmpGammaPair);
                    // std::cout << "tmpPi0->M() " << tmpPi0->M() << std::endl;
                    if (tmpPi0->M() < 50 || tmpPi0->M() > 250)
                    {
                        delete tmpPi0;
                        continue;
                    }
                    tmpPi0->Type = 0;
                    Get_vPi0(event)->emplace_back(tmpPi0);
                    Get_vResolvedPi0(event)->emplace_back(tmpPi0);
                }
            }
        }
    }
}

void Mixture_Calo::RecMergedPi0()
{
    for (int event = 0; event < max_Eventseq; event++)
    {

        for (auto SubGammaPair : *Get_vSubGamma(event))
        {

            FPi0 *tmpPi0 = new FPi0(SubGammaPair);
            // std::cout << "tmpPi0->M() " << tmpPi0->M() << std::endl;
            if (tmpPi0->M() < 50 || tmpPi0->M() > 250)
            {
                delete tmpPi0;
                continue;
            }
            tmpPi0->Type = 1;
            Get_vPi0(event)->emplace_back(tmpPi0);
            Get_vMergedPi0(event)->emplace_back(tmpPi0);
        }
    }
}

void Mixture_Calo::EvalDist2Charge()
{
    auto ChargeTree = (TTree *)ChargeSampleFile->Get("tree");
    double entry_x;
    ChargeTree->SetBranchAddress("entry_x", &entry_x);
    double entry_y;
    ChargeTree->SetBranchAddress("entry_y", &entry_y);
    double eTot;
    ChargeTree->SetBranchAddress("eTot", &eTot);

    for (auto Cluster3D : *Get_vClu3D(0))
    {
        for (int i = 0; i < ChargeTree->GetEntries(); i++)
        {
            ChargeTree->GetEntry(i);
            if (abs(Cluster3D->Energy() - eTot * 1000) / (eTot * 1000) < 0.5)
            {
                float tmpDist = sqrt(pow(entry_x - Cluster3D->X(), 2) + pow(entry_y - Cluster3D->Y(), 2));
                if (Cluster3D->GetMinDist2Charge() > tmpDist)
                {
                    // std::cout << "tmpDist " << tmpDist << std::endl;
                    Cluster3D->SetMinDist2Charge(tmpDist);
                }
            }
        }
    }

    for (auto SubClu3DPair : *Get_vSubClu3D(0))
    {
        for (int i = 0; i < ChargeTree->GetEntries(); i++)
        {
            auto Clu3D1 = SubClu3DPair.first;
            auto Clu3D2 = SubClu3DPair.second;
            if (abs(Clu3D1->Energy() - eTot * 1000) / (eTot * 1000) < 0.5)
            {
                float tmpDist = sqrt(pow(entry_x - Clu3D1->X(), 2) + pow(entry_y - Clu3D1->Y(), 2));
                if (Clu3D1->GetMinDist2Charge() > tmpDist)
                {
                    Clu3D1->SetMinDist2Charge(tmpDist);
                }
            }

            if (abs(Clu3D2->Energy() - eTot * 1000) / (eTot * 1000) < 0.5)
            {
                float tmpDist = sqrt(pow(entry_x - Clu3D2->X(), 2) + pow(entry_y - Clu3D2->Y(), 2));
                if (Clu3D2->GetMinDist2Charge() > tmpDist)
                {
                    Clu3D2->SetMinDist2Charge(tmpDist);
                }
            }
        }
    }
}

void Mixture_Calo::Matching()
{
    for (int event = 0; event < max_Eventseq; event++)
    {
        spdlog::get(LOG_NAME)->info("In event {}, Number of Dirct Clu3Ds {}", event, Get_vClu3D(event)->size());
        spdlog::get(LOG_NAME)->info("In event {}, Number of Sub Clu3Ds pair {}", event, Get_vSubClu3D(event)->size());
        // Get_vSubClu3D(event)->at(0).second->v_Cluster2D.at(1)->PrintCellInfo();
        spdlog::get(LOG_NAME)->info("In event {}, Number of Pi0s {}", event, Get_vPi0(event)->size());
        // std::cout << Get_vPi0(event)->at(0)->Type << std::endl;

        for (auto Cluster3D : *Get_vClu3D(event))
        {

            Try2MatchClu3D(event, Cluster3D);
            // std::cout << Cluster3D->Energy() << std::endl;
        }
        for (auto SubCluster3D : *Get_vSubClu3D(event))
        {
            Try2MatchClu3D(event, SubCluster3D.first);
            Try2MatchClu3D(event, SubCluster3D.second);
        }

        if (Get_nPV(event) == 2)
        {
            if (GetMatchedClu3D(event, 0) != GetMatchedClu3D(event, 1))
            {
                if (GetMatchedClu3D(event, 0) != NULL)
                    GetMatchedClu3D(event, 0)->MatchedNum = 0;
                if (GetMatchedClu3D(event, 1) != NULL)
                    GetMatchedClu3D(event, 1)->MatchedNum = 1;
            }
            else if (GetMatchedClu3D(event, 0) != NULL)
            {
                if (GetMatchedClu3D(event, 0)->GetDist2Tru(0) > GetMatchedClu3D(event, 0)->GetDist2Tru(1))
                {
                    if (GetMatchedClu3DInfo(event, 0)->PrevClu3D == NULL)
                    {
                        GetMatchedClu3D(event, 0)->MatchedNum = 1;
                    }
                    else
                    {
                        GetMatchedClu3DInfo(event, 0)->Clu3D = GetMatchedClu3DInfo(event, 0)->PrevClu3D;
                        GetMatchedClu3DInfo(event, 0)->Dist2Tru = GetMatchedClu3DInfo(event, 0)->Clu3D->GetDist2Tru(0);
                        GetMatchedClu3DInfo(event, 0)->DeltaE_E = GetMatchedClu3DInfo(event, 0)->Clu3D->GetDeltaE_E2PV(0);
                        GetMatchedClu3D(event, 0)->MatchedNum = 0;
                        GetMatchedClu3D(event, 1)->MatchedNum = 1;
                    }
                }
                else
                {
                    if (GetMatchedClu3DInfo(event, 1)->PrevClu3D == NULL)
                    {
                        GetMatchedClu3D(event, 0)->MatchedNum = 0;
                    }
                    else
                    {
                        GetMatchedClu3DInfo(event, 1)->Clu3D = GetMatchedClu3DInfo(event, 1)->PrevClu3D;
                        GetMatchedClu3DInfo(event, 1)->Dist2Tru = GetMatchedClu3DInfo(event, 1)->Clu3D->GetDist2Tru(0);
                        GetMatchedClu3DInfo(event, 1)->DeltaE_E = GetMatchedClu3DInfo(event, 1)->Clu3D->GetDeltaE_E2PV(0);
                        GetMatchedClu3D(event, 0)->MatchedNum = 0;
                        GetMatchedClu3D(event, 1)->MatchedNum = 1;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < Get_nPV(event); i++)
            {
                if (GetMatchedClu3D(event, i) != NULL)
                    GetMatchedClu3D(event, i)->MatchedNum = i;
            }
        }
        ////////////////////////////////////
        if (Parameters::Instance()->ConstructPi0)
        {
            for (auto SubCluster3D : *Get_vSubClu3D(event))
            {
                Try2MatchSubClu3D(event, SubCluster3D);
            }
            if (GetMatchedSubClu3DInfo(event)->Active == 1)
            {
                GetMatchedSubClu3D(event, 0)->MatchedNum = 0;
                GetMatchedSubClu3D(event, 1)->MatchedNum = 1;
            }
        }
    }
    if (Parameters::Instance()->ConstructGamma == 1 || Parameters::Instance()->ConstructPi0 == 1)
    {
        spdlog::get(LOG_NAME)->info("Matching Gamma...");
        MatchGamma();
    }
    if (Parameters::Instance()->ConstructPi0 == 1)
    {
        spdlog::get(LOG_NAME)->info("Matching Pi0...");
        MatchPi0();
    }
}

void Mixture_Calo::MatchGamma()
{

    for (int event = 0; event < max_Eventseq; event++)
    {
        for (auto Gamma : *Get_vGamma(event))
        {
            if (Gamma->f_Clu3D->MatchedNum >= 0)
            {
                Gamma->MatchedNum = Gamma->f_Clu3D->MatchedNum;
                // std::cout << "Gamma->f_Clu3D " << Gamma->f_Clu3D << std::endl;
                //  std::cout << "Gamma Match " << Gamma->MatchedNum << std::endl;
            }
        }
        // std::cout<<"Get_vSubGamma(event) "<<Get_vSubGamma(event)->size()<<std::endl;
        for (auto SubGamma : *Get_vSubGamma(event))
        {
            // std::cout << SubGamma->Energy() << std::endl;
            // std::cout << "SubGamma->f_Clu3D->MatchedNum " << SubGamma->f_Clu3D->MatchedNum << std::endl;
            if (SubGamma.first->f_Clu3D->MatchedNum >= 0)
            {
                // //std::cout << "SubGamma->f_Clu3D.at(0)->MatchedNum " << SubGamma->f_Clu3D.at(0)->MatchedNum << std::endl;
                SubGamma.first->MatchedNum = SubGamma.first->f_Clu3D->MatchedNum;
            }
            if (SubGamma.second->f_Clu3D->MatchedNum >= 0)
            {
                // //std::cout << "SubGamma->f_Clu3D.at(0)->MatchedNum " << SubGamma->f_Clu3D.at(0)->MatchedNum << std::endl;
                SubGamma.second->MatchedNum = SubGamma.second->f_Clu3D->MatchedNum;
            }
            // if (SubGamma.first->f_Clu3D == GetMatchedClu3D(event, 1))
            // {
            //     std::cout << "SubGamma.first->Energy() " << SubGamma.first->Energy() << std::endl;
            // }
            // if (SubGamma.second->f_Clu3D == GetMatchedClu3D(event, 1))
            // {
            //     std::cout << "SubGamma.second->Energy() " << SubGamma.second->Energy() << std::endl;
            // }
        }
    }
}

void Mixture_Calo::MatchPi0()
{
    for (int event = 0; event < max_Eventseq; event++)
    {
        for (auto Pi0 : *Get_vPi0(event))
        {
            auto SubGamma1 = Pi0->Sub_Gamma.first;
            auto SubGamma2 = Pi0->Sub_Gamma.second;
            // std::cout << "SubGamma1->MatchedNum " << SubGamma1->MatchedNum << std::endl;
            // std::cout << "SubGamma2->MatchedNum " << SubGamma2->MatchedNum << std::endl;
            if (SubGamma1->MatchedNum >= 0 && SubGamma2->MatchedNum >= 0)
            {
                Pi0->MatchedNum = 1;
            }
        }
    }
}

void Mixture_Calo::WriteTree()
{
    std::stringstream tmpName;
    for (int event = 0; event < max_Eventseq; event++)
    {

        spdlog::get(LOG_NAME)->info("\tFilling  Information in event : {}", event);
        CreateTree::Instance()->Clear();
        CreateTree::Instance()->Run = IntputNum;
        CreateTree::Instance()->Event = event;
        CreateTree::Instance()->CluFlow = CluFlow;

        std::vector<FParticle *> TruSignal;
        if (Parameters::Instance()->SaveAll || Parameters::Instance()->SavePrimaryInfo)
        {
            spdlog::get(LOG_NAME)->info("\t\tFilling PV Information");
            CreateTree::Instance()->ParticleGun = ParticleGun;
            CreateTree::Instance()->n_PV = Get_nPV(event);

            for (int i = 0; i < Get_nPV(event); i++)
            {
                FParticle *tmpParticle = new FParticle();
                tmpParticle->SetPxPyPzE(GetPrimaryVx(event, i), GetPrimaryVy(event, i), GetPrimaryVz(event, i), GetPrimaryE(event, i));

                CreateTree::Instance()->primaryPDGID.emplace_back(GetPrimaryPDGID(event, i));

                CreateTree::Instance()->PositionOnAbsorberX.emplace_back(GetHitX(event, i));
                CreateTree::Instance()->PositionOnAbsorberY.emplace_back(GetHitY(event, i));
                CreateTree::Instance()->PositionOnAbsorberZ.emplace_back(GetHitZ(event, i));
                CreateTree::Instance()->PositionOnAbsorberT.emplace_back(GetHitT(event, i));
                CreateTree::Instance()->MomentumOnAbsorberX.emplace_back(GetHitVx(event, i));
                CreateTree::Instance()->MomentumOnAbsorberY.emplace_back(GetHitVy(event, i));
                CreateTree::Instance()->MomentumOnAbsorberZ.emplace_back(GetHitVz(event, i));
                CreateTree::Instance()->EnergyOnAbsorber.emplace_back(GetHitE(event, i));

                CreateTree::Instance()->EnergyAtVertex.emplace_back(GetPrimaryE(event, i));
                CreateTree::Instance()->PositionAtVertexX.emplace_back(GetPrimaryX(event, i));
                CreateTree::Instance()->PositionAtVertexY.emplace_back(GetPrimaryY(event, i));
                CreateTree::Instance()->PositionAtVertexZ.emplace_back(GetPrimaryZ(event, i));

                CreateTree::Instance()->MomentumAtVertexX.emplace_back(GetPrimaryVx(event, i));
                CreateTree::Instance()->MomentumAtVertexY.emplace_back(GetPrimaryVy(event, i));
                CreateTree::Instance()->MomentumAtVertexZ.emplace_back(GetPrimaryVz(event, i));

                CreateTree::Instance()->TimeAtVertex.emplace_back(GetPrimaryT(event, i));

                CreateTree::Instance()->HitRegion.emplace_back(GetHitRegion(event, i));
                CreateTree::Instance()->HitRegionType.emplace_back(GetHitRegionType(event, i));
                CreateTree::Instance()->HitModule.emplace_back(GetHitModule(event, i));
                TruSignal.emplace_back(tmpParticle);
            }
        }
        if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveRawInfo)
        {
            spdlog::get(LOG_NAME)->info("\t\tFilling Hit Information");
            CreateTree::Instance()->TotEnDep = GetEnDep(event);
            CreateTree::Instance()->ShowerX = GetShowerX(event);
            CreateTree::Instance()->ShowerY = GetShowerY(event);
            CreateTree::Instance()->ShowerZ = GetShowerZ(event);
            // CreateTree::Instance()->MaxDepRegion = EventInfo.at(event).GetMaxDepRegion();

            int RegionCount = 0;
            int tmpCount = 0;

            for (auto RegionSeq : region_container)
            {
                int CurrentRegionID = RegionSeq.first;
                FRegion *CurrentRegion = RegionSeq.second;
                CreateTree::Instance()->RegionTotEnDep[RegionCount] = CurrentRegion->EventInfo.at(event).GetTotEnDep();
                CreateTree::Instance()->RegionEnDepSiTiming[RegionCount] = CurrentRegion->EventInfo.at(event).GetSiTimingEnDep();
                CreateTree::Instance()->RegionSiliconSig[RegionCount] = CurrentRegion->EventInfo.at(event).GetSiliconSig();
                CreateTree::Instance()->RegionEnDepSilicon[RegionCount] = CurrentRegion->EventInfo.at(event).GetSiliconEnDep();
                CreateTree::Instance()->RegionShowerZ[RegionCount] = CurrentRegion->EventInfo.at(event).GetShowerZ();
                CreateTree::Instance()->RegionShowerX[RegionCount] = CurrentRegion->EventInfo.at(event).GetShowerX();
                CreateTree::Instance()->RegionShowerY[RegionCount] = CurrentRegion->EventInfo.at(event).GetShowerY();
                CreateTree::Instance()->RegionTotLayerNum[RegionCount] = CurrentRegion->TotLayerNum;
                CreateTree::Instance()->RegionFrontFace[RegionCount] = CurrentRegion->FrontFace;
                int LayerCount = 0;
                for (auto Layer : CurrentRegion->Layers)
                {
                    CreateTree::Instance()->RegionLayerEnDep[tmpCount] = CurrentRegion->EventInfo.at(event).GetLayerEnDep(LayerCount);
                    CreateTree::Instance()->RegionActiveLayerEnDep[tmpCount] = CurrentRegion->EventInfo.at(event).GetActiveLayerEnDep(LayerCount);
                    CreateTree::Instance()->RegionLayerSig[tmpCount] = CurrentRegion->EventInfo.at(event).GetLayerSig(LayerCount);
                    CreateTree::Instance()->RegionLayerShowerZ[tmpCount] = CurrentRegion->EventInfo.at(event).GetLayerShowerZ(LayerCount);
                    CreateTree::Instance()->RegionLayerShowerX[tmpCount] = CurrentRegion->EventInfo.at(event).GetLayerShowerX(LayerCount);
                    CreateTree::Instance()->RegionLayerShowerY[tmpCount] = CurrentRegion->EventInfo.at(event).GetLayerShowerY(LayerCount);
                    CreateTree::Instance()->RegionLayerFrontFace[tmpCount] = Layer->layerPos - Layer->layerThickness / 2;
                    CreateTree::Instance()->RegionLayerThickness[tmpCount] = Layer->layerThickness;
                    CreateTree::Instance()->RegionLayerPos[tmpCount] = Layer->layerPos;

                    LayerCount++;
                    tmpCount++;
                }
                RegionCount++;
            }
        }
        if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCellInfo)
        {
            spdlog::get(LOG_NAME)->info("\t\tFilling Cell Information");
            // //std::cout << CreateTree::Instance()->CellEnergy.size() << std::endl;

            int tmpCount = 0;
            for (auto moduleSeq : module_container)
            {
                int moduleId = moduleSeq.first;
                FModule *CurrentModule = moduleSeq.second;
                for (auto Layer : CurrentModule->v_layer)
                {
                    CreateTree::Instance()->CellRawSignal.at(tmpCount).clear();
                    CreateTree::Instance()->CellTime.at(tmpCount).clear();
                    CreateTree::Instance()->CellEnergy.at(tmpCount).clear();
                    CreateTree::Instance()->CellRawTime.at(tmpCount).clear();
                    // //std::cout << "module : " << moduleId << " Layer : " << Layer->id << std::endl;
                    // //std::cout << "In event : " << event << " tmpCount : " << tmpCount << std::endl;
                    for (auto Cell : Layer->v_cell)
                    {
                        CreateTree::Instance()->CellRawSignal.at(tmpCount).emplace_back(Cell->GetRawSignal(event));
                        CreateTree::Instance()->CellEnergy.at(tmpCount).emplace_back(Cell->GetEnergy(event));
                        CreateTree::Instance()->CellRawTime.at(tmpCount).emplace_back(Cell->GetRawTime(event));
                        CreateTree::Instance()->CellTime.at(tmpCount).emplace_back(Cell->GetTime(event));
                    }
                    tmpCount++;
                }
            }
            // //std::cout << "total module : " << module_container.size() << std::endl;
        }
        if ((Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCluster3D))
        {
            spdlog::get(LOG_NAME)->info("\t\tFilling Cluster Information");
            if (ProgramType == 2 || ProgramType == 4)
            {
                FRegion *HitRegion = region_container.at(GetHitRegion(event));
                FModule *HitModule = GetModuleByID(GetHitModule(event));
                float TruX = GetShowerX(event);
                float TruY = GetShowerY(event);
                float TruZ = GetShowerX(event);
                float TruEnDep = GetEnDep(event);

                CreateTree::Instance()->TruX = TruX;
                CreateTree::Instance()->TruY = TruY;
                CreateTree::Instance()->TruZ = TruZ;
                CreateTree::Instance()->TruDep = TruEnDep;

                FPoint TruLocalPos = HitRegion->Global2Local(TruX - HitModule->X(), TruY - HitModule->Y(), TruZ - HitModule->Z());
                float TmpDeltaZ = TruLocalPos.Z() - HitRegion->FrontFace;
                float ThetaX = atan((TruX - GetPrimaryX(event)) / (TruZ - GetPrimaryZ(event)));
                float ThetaY = atan((TruY - GetPrimaryY(event)) / (TruZ - GetPrimaryZ(event)));
                float Ax = fabs(ThetaX) + abs(HitRegion->ax) / 180. * TMath::Pi();
                float Ay = fabs(ThetaY) + abs(HitRegion->ay) / 180. * TMath::Pi();
                float TmpShowerDepth = sqrt(pow(TmpDeltaZ, 2) + pow(TmpDeltaZ * tan(Ax), 2) + pow(TmpDeltaZ * tan(Ay), 2));
                CreateTree::Instance()->TruShowerDepth = TmpShowerDepth;
                CreateTree::Instance()->TruLocalX = TruLocalPos.X();
                CreateTree::Instance()->TruLocalX = TruLocalPos.Y();
                CreateTree::Instance()->TruLocalX = TruLocalPos.Z();
            }

            for (auto moduleSeq : module_container)
            {
                FModule *CurrentModule = moduleSeq.second;
                FRegion *CurrentRegion = CurrentModule->f_region;

                for (auto Cluster3D : CurrentModule->GetEventInfo(event)->Cluster3D)
                {
                    CreateTree::Instance()->Clu3DModule = Cluster3D->f_module->GetID();
                    CreateTree::Instance()->Clu3DRegion = Cluster3D->f_region->GlobalId;
                    CreateTree::Instance()->Clu3DRegionType = Cluster3D->f_region->type;
                    CreateTree::Instance()->Clu3DTypeX = Cluster3D->XType;
                    CreateTree::Instance()->Clu3DTypeY = Cluster3D->YType;
                    CreateTree::Instance()->Clu3Denergy = Cluster3D->energy;
                    CreateTree::Instance()->Clu3DTmpCorE = Cluster3D->tmpCorE;
                    CreateTree::Instance()->Clu3Dtime = Cluster3D->time;
                    CreateTree::Instance()->Clu3Dx = Cluster3D->X();
                    CreateTree::Instance()->Clu3Dy = Cluster3D->Y();
                    CreateTree::Instance()->Clu3Dz = Cluster3D->Z();
                    CreateTree::Instance()->Clu3DEntryX = Cluster3D->EntryX();
                    CreateTree::Instance()->Clu3DEntryY = Cluster3D->EntryY();
                    CreateTree::Instance()->Clu3DEntryZ = Cluster3D->EntryZ();
                    CreateTree::Instance()->Clu3DRawX = Cluster3D->RawX();
                    CreateTree::Instance()->Clu3DRawY = Cluster3D->RawY();
                    CreateTree::Instance()->Clu3DRawZ = Cluster3D->RawZ();
                    CreateTree::Instance()->Clu3DRawEnergy = Cluster3D->RawE;
                    CreateTree::Instance()->Clu3DRawTime = Cluster3D->time;
                    if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 3)
                    {
                        CreateTree::Instance()->Seed3Det = Cluster3D->Seed3Det;
                        // //std::cout << "Cluster3D->Seed3D->X() " << Cluster3D->Seed3D->X() << std::endl;
                        // //std::cout << "Cluster3D->Seed3D->Y() " << Cluster3D->Seed3D->Y() << std::endl;
                        CreateTree::Instance()->Seed3Dx = Cluster3D->Seed3D->X();
                        CreateTree::Instance()->Seed3Dy = Cluster3D->Seed3D->Y();
                        CreateTree::Instance()->Seed3Dz = Cluster3D->Seed3D->Z();
                        CreateTree::Instance()->Seed3DLocalX = Cluster3D->Seed3D->LocalX();
                        CreateTree::Instance()->Seed3DLocalY = Cluster3D->Seed3D->LocalY();
                        CreateTree::Instance()->Seed3DLocalZ = Cluster3D->Seed3D->LocalZ();
                        CreateTree::Instance()->Seed3Ddx = Cluster3D->Seed3D->Dx();
                        CreateTree::Instance()->Seed3Ddy = Cluster3D->Seed3D->Dy();
                        CreateTree::Instance()->Seed3Ddz = Cluster3D->Seed3D->Dz();
                        CreateTree::Instance()->Seed3DE = Cluster3D->Seed3D->GetEnergy(event);
                        CreateTree::Instance()->Seed3DT = Cluster3D->Seed3D->GetTime(event);
                        CreateTree::Instance()->Clu3DE3 = Cluster3D->E3;
                        CreateTree::Instance()->Clu3DMatchIndex = Cluster3D->MatchedNum;

                        CreateTree::Instance()->Cell3DE.clear();
                        CreateTree::Instance()->Cell3DX.clear();
                        CreateTree::Instance()->Cell3DY.clear();
                        CreateTree::Instance()->Cell3DZ.clear();
                        CreateTree::Instance()->Cell3DT.clear();
                        CreateTree::Instance()->Cell3DType.clear();
                    }
                    int tmpCell3DNum = 0;
                    for (auto v_Cell3DSeq : Cluster3D->Cluster_Cell3D)
                    {
                        int Cell3DType = v_Cell3DSeq.first;
                        auto v_Cell3D = v_Cell3DSeq.second;
                        for (auto Cell3D : v_Cell3D)
                        {
                            CreateTree::Instance()->Cell3DE.emplace_back(Cell3D->GetEnergy(event));
                            CreateTree::Instance()->Cell3DX.emplace_back(Cell3D->X());
                            CreateTree::Instance()->Cell3DY.emplace_back(Cell3D->Y());
                            CreateTree::Instance()->Cell3DZ.emplace_back(Cell3D->Z());
                            CreateTree::Instance()->Cell3DT.emplace_back(Cell3D->GetTime(event));
                            CreateTree::Instance()->Cell3DType.emplace_back(Cell3DType);
                            tmpCell3DNum++;
                        }
                    }
                    CreateTree::Instance()->Cell3DNum = tmpCell3DNum;
                    if ((CluFlow == 1 || CluFlow == 2))
                    {
                        std::vector<float> tmpLayerTime;
                        std::vector<float> tmpLayerEnergy;
                        std::vector<float> tmpLayerRawE;
                        std::vector<int> tmpLayerId;
                        std::vector<float> tmpLayerPos;
                        std::vector<float> tmpLayerTickness;
                        std::vector<int> tmpModule;
                        std::vector<int> tmpRegion;
                        std::vector<int> tmpRegionType;

                        std::vector<float> tmpLayerAX;
                        std::vector<float> tmpLayerAY;
                        std::vector<float> tmpLayerAZ;
                        std::vector<float> tmpModuleX;
                        std::vector<float> tmpModuleY;
                        std::vector<float> tmpModuleZ;
                        std::vector<int> tmpNextTypeX;
                        std::vector<int> tmpNextTypeY;
                        std::vector<float> tmpLayerRawX;
                        std::vector<float> tmpLayerRawY;
                        std::vector<float> tmpLayerRawZ;
                        std::vector<float> tmpClu2DS0CorX;
                        std::vector<float> tmpClu2DS0CorY;
                        std::vector<float> tmpLayerX;
                        std::vector<float> tmpLayerY;
                        std::vector<float> tmpLayerZ;
                        std::vector<float> tmpLayerLocalX;
                        std::vector<float> tmpLayerLocalY;
                        std::vector<float> tmpLayerLocalZ;
                        std::vector<float> tmpLayerSeedX;
                        std::vector<float> tmpLayerSeedY;
                        std::vector<float> tmpLayerSeedZ;
                        std::vector<float> tmpLayerSeedE;
                        std::vector<float> tmpLayerSeedT;
                        std::vector<float> tmpLayerCellDx;
                        std::vector<float> tmpLayerCellDy;
                        std::vector<float> tmpLayerE3;
                        std::vector<float> tmpLayerE5;
                        std::vector<float> tmpLayerE7;
                        std::vector<float> tmpLayerE9;
                        std::vector<int> tmpCellNum;
                        std::vector<float> tmpCellT;
                        std::vector<float> tmpCellX;
                        std::vector<float> tmpCellY;
                        std::vector<float> tmpCellZ;
                        std::vector<float> tmpCellE;
                        std::vector<int> tmpCellType;
                        /////////////////////////////
                        std::vector<float> tmpLayerTruX;
                        std::vector<float> tmpLayerTruY;
                        std::vector<float> tmpLayerTruZ;
                        std::vector<float> tmpLayerTruDep;
                        std::vector<float> tmpLayerShowerDepth;
                        std::vector<float> tmpLayerTruLocalX;
                        std::vector<float> tmpLayerTruLocalY;
                        std::vector<float> tmpLayerTruLocalZ;
                        ////////////////////////////

                        //////////////////////////
                        int layerCount = 0;

                        for (auto CurrentClu2D : Cluster3D->v_Cluster2D)
                        {
                            // //std::cout << "layerCount " << layerCount << std::endl;
                            tmpLayerId.emplace_back(layerCount);
                            tmpLayerPos.emplace_back(CurrentRegion->Layers.at(layerCount)->layerPos);
                            tmpLayerTickness.emplace_back(CurrentRegion->Layers.at(layerCount)->layerThickness);

                            // float CellDx = CurrentRegion->Layers.at(layerCount)->cell_map.GetXaxis()->GetBinWidth(1);
                            // float CellDy = CurrentRegion->Layers.at(layerCount)->cell_map.GetYaxis()->GetBinWidth(1);
                            // tmpLayerCellDx.emplace_back(CellDx);
                            // tmpLayerCellDy.emplace_back(CellDy);

                            FRegion *Clu2DRegion = CurrentClu2D->f_region;
                            FModule *Clu2DModule = CurrentClu2D->f_module;

                            float Clu2DModuleX = Clu2DModule->X();
                            float Clu2DModuleY = Clu2DModule->Y();
                            float Clu2DModuleZ = Clu2DModule->Z();
                            // //std::cout << "CurrentClu2D->Seed2D " << CurrentClu2D->Seed2D << std::endl;
                            float Seed2DX = CurrentClu2D->Seed2D->X();
                            float Seed2DY = CurrentClu2D->Seed2D->Y();
                            float Seed2DZ = CurrentClu2D->Seed2D->Z();

                            tmpLayerCellDx.emplace_back(CurrentClu2D->Seed2D->Dx());
                            tmpLayerCellDy.emplace_back(CurrentClu2D->Seed2D->Dy());

                            tmpModule.emplace_back(Clu2DModule->GetID());
                            tmpRegion.emplace_back(Clu2DRegion->GlobalId);
                            tmpRegionType.emplace_back(Clu2DRegion->type);

                            tmpModuleX.emplace_back(Clu2DModuleX);
                            tmpModuleY.emplace_back(Clu2DModuleY);
                            tmpModuleZ.emplace_back(Clu2DModuleZ);

                            // int testY_p = GetRegionTypeByPos(Seed2DX + CellDx, Seed2DY);
                            tmpNextTypeX.emplace_back(CurrentClu2D->XType);

                            // //std::cout << "Seed2D->GetID() " << CurrentClu2D->Seed2D->GetID() << " clu2D module " << Clu2DModule->GetID() << std::endl;
                            tmpNextTypeY.emplace_back(CurrentClu2D->YType);

                            tmpLayerAX.emplace_back(Clu2DRegion->ax);
                            tmpLayerAY.emplace_back(Clu2DRegion->ay);
                            tmpLayerAZ.emplace_back(Clu2DRegion->az);
                            // //std::cout << CurrentClu2D->Seed2D << std::endl;
                            tmpLayerTime.emplace_back(CurrentClu2D->time);
                            tmpLayerEnergy.emplace_back(CurrentClu2D->Energy());
                            tmpLayerRawE.emplace_back(CurrentClu2D->RawEnergy());
                            tmpLayerRawX.emplace_back(CurrentClu2D->RawX());
                            tmpLayerRawY.emplace_back(CurrentClu2D->RawY());
                            tmpLayerRawZ.emplace_back(CurrentClu2D->RawZ());

                            tmpClu2DS0CorX.emplace_back(CurrentClu2D->S0CorX());
                            tmpClu2DS0CorY.emplace_back(CurrentClu2D->S0CorY());
                            tmpLayerX.emplace_back(CurrentClu2D->X());
                            tmpLayerY.emplace_back(CurrentClu2D->Y());
                            tmpLayerZ.emplace_back(CurrentClu2D->Z());
                            //////??????????/ check
                            FPoint LocalPos = Clu2DRegion->Global2Local(CurrentClu2D->RawX() - Clu2DModuleX, CurrentClu2D->RawY() - Clu2DModuleY, CurrentClu2D->RawZ() - Clu2DModuleZ);
                            tmpLayerLocalX.emplace_back(CurrentClu2D->LocalX());
                            tmpLayerLocalY.emplace_back(CurrentClu2D->LocalY());
                            tmpLayerLocalZ.emplace_back(LocalPos.Z());

                            tmpLayerSeedX.emplace_back(Seed2DX);
                            tmpLayerSeedY.emplace_back(Seed2DY);
                            tmpLayerSeedZ.emplace_back(Seed2DZ);
                            tmpLayerSeedE.emplace_back(CurrentClu2D->Seed2D->GetEnergy(event));
                            tmpLayerSeedT.emplace_back(CurrentClu2D->Seed2D->GetTime(event));
                            tmpLayerE3.emplace_back(CurrentClu2D->E3);
                            tmpLayerE5.emplace_back(CurrentClu2D->E5);
                            tmpLayerE7.emplace_back(CurrentClu2D->E7);
                            tmpLayerE9.emplace_back(CurrentClu2D->E9);
                            int tmpCellCount = 0;
                            for (auto v_CellSeq : CurrentClu2D->Cluster_Cell)
                            {
                                int CurCellType = v_CellSeq.first;
                                auto v_Cell = v_CellSeq.second;
                                for (auto CurCell : v_Cell)
                                {
                                    tmpCellE.emplace_back(CurCell->GetEnergy(event));
                                    // //std::cout << "CurCell " << CurCell << std::endl;
                                    // //std::cout << "CurCell->X() " << CurCell->X() << std::endl;
                                    tmpCellX.emplace_back(CurCell->X());
                                    tmpCellY.emplace_back(CurCell->Y());
                                    tmpCellZ.emplace_back(CurCell->Z());
                                    tmpCellT.emplace_back(CurCell->GetTime(event));
                                    tmpCellType.emplace_back(CurCellType);
                                    tmpCellCount++;
                                }
                            }
                            // //std::cout << "tmpCellCount " << tmpCellCount << std::endl;
                            tmpCellNum.emplace_back(tmpCellCount);

                            if (ProgramType == 2 || ProgramType == 4)
                            {

                                float LayerTruX = 0;
                                float LayerTruY = 0;
                                float LayerTruZ = 0;
                                float LayerTruEnDep = 0;
                                for (auto LoopReg : region_container)
                                {
                                    FRegion *CurReg = LoopReg.second;
                                    if (CurReg->type == Clu2DRegion->type)
                                    {
                                        // //std::cout << "CurReg " << CurReg->GlobalId << std::endl;
                                        float CuRegDep = CurReg->EventInfo.at(event).GetLayerEnDep(layerCount);
                                        float CuRegSig = CurReg->EventInfo.at(event).GetLayerSig(layerCount);
                                        if (CuRegDep == 0 || CuRegSig == 0)
                                            continue;
                                        LayerTruX += CurReg->EventInfo.at(event).GetLayerShowerX(layerCount) * CuRegDep;
                                        LayerTruY += CurReg->EventInfo.at(event).GetLayerShowerY(layerCount) * CuRegDep;
                                        LayerTruZ += CurReg->EventInfo.at(event).GetLayerShowerZ(layerCount) * CuRegDep;
                                        LayerTruEnDep += CuRegDep;
                                    }
                                }
                                // //std::cout << "TruEnDep " << TruEnDep << std::endl;
                                // if (layerCount == 0)
                                //     //std::cout << "layer " << layerCount << " TruX " << LayerTruX << " TruEnDep " << LayerTruEnDep << std::endl;
                                LayerTruX /= LayerTruEnDep;
                                LayerTruY /= LayerTruEnDep;
                                LayerTruZ /= LayerTruEnDep;
                                // //std::cout << "TruX " << TruX << std::endl;
                                tmpLayerTruX.emplace_back(LayerTruX);
                                tmpLayerTruY.emplace_back(LayerTruY);
                                tmpLayerTruZ.emplace_back(LayerTruZ);
                                tmpLayerTruDep.emplace_back(LayerTruEnDep);
                                FPoint TruLocalPos = Clu2DRegion->Global2Local(LayerTruX - Clu2DModuleX, LayerTruY - Clu2DModuleY, LayerTruZ - Clu2DModuleZ);
                                float TmpDeltaZ = TruLocalPos.Z() - Clu2DRegion->FrontFace;
                                // float ThetaX = atan((EventInfo.at(event).GetShowerX() - EventInfo.at(event).GetPrimaryX()) / (EventInfo.at(event).GetShowerZ() - EventInfo.at(event).GetPrimaryZ()));
                                // float ThetaY = atan((EventInfo.at(event).GetShowerY() - EventInfo.at(event).GetPrimaryY()) / (EventInfo.at(event).GetShowerZ() - EventInfo.at(event).GetPrimaryZ()));
                                float ThetaX = atan((GetShowerX(event) - GetPrimaryX(event)) / (GetShowerX(event) - GetPrimaryZ(event)));
                                float ThetaY = atan((GetShowerY(event) - GetPrimaryY(event)) / (GetShowerX(event) - GetPrimaryZ(event)));
                                float Ax = fabs(ThetaX) + abs(Clu2DRegion->ax) / 180. * TMath::Pi();
                                float Ay = fabs(ThetaY) + abs(Clu2DRegion->ay) / 180. * TMath::Pi();
                                float TmpShowerDepth = sqrt(pow(TmpDeltaZ, 2) + pow(TmpDeltaZ * tan(Ax), 2) + pow(TmpDeltaZ * tan(Ay), 2));
                                tmpLayerShowerDepth.emplace_back(TmpShowerDepth);
                                tmpLayerTruLocalX.emplace_back(TruLocalPos.X());
                                tmpLayerTruLocalY.emplace_back(TruLocalPos.Y());
                                tmpLayerTruLocalZ.emplace_back(TruLocalPos.Z());
                            }

                            layerCount++;
                        }
                        CreateTree::Instance()->Clu2DModule = tmpModule;
                        CreateTree::Instance()->Clu2DRegion = tmpRegion;
                        CreateTree::Instance()->Clu2DRegionType = tmpRegionType;
                        CreateTree::Instance()->Modulex = tmpModuleX;
                        CreateTree::Instance()->Moduley = tmpModuleY;
                        CreateTree::Instance()->Modulez = tmpModuleZ;
                        CreateTree::Instance()->Clu2DTypeX = tmpNextTypeX;
                        CreateTree::Instance()->Clu2DTypeY = tmpNextTypeY;
                        CreateTree::Instance()->Clu2DAx = tmpLayerAX;
                        CreateTree::Instance()->Clu2DAy = tmpLayerAY;
                        CreateTree::Instance()->Clu2DAz = tmpLayerAZ;
                        CreateTree::Instance()->Clu2DEnergy = tmpLayerEnergy;
                        CreateTree::Instance()->Clu2DRawE = tmpLayerRawE;
                        CreateTree::Instance()->Clu2DTime = tmpLayerTime;
                        CreateTree::Instance()->Clu2D_id = tmpLayerId;
                        // CreateTree::Instance()->region_id = tmpRegionId;
                        CreateTree::Instance()->Clu2DPos = tmpLayerPos;
                        CreateTree::Instance()->Clu2DThickness = tmpLayerTickness;
                        CreateTree::Instance()->Clu2DS0CorX = tmpClu2DS0CorX;
                        CreateTree::Instance()->Clu2DS0CorY = tmpClu2DS0CorY;
                        CreateTree::Instance()->Clu2DX = tmpLayerX;
                        CreateTree::Instance()->Clu2DY = tmpLayerY;
                        CreateTree::Instance()->Clu2DZ = tmpLayerZ;
                        CreateTree::Instance()->Clu2DLocalX = tmpLayerLocalX;
                        CreateTree::Instance()->Clu2DLocalY = tmpLayerLocalY;
                        CreateTree::Instance()->Clu2DLocalZ = tmpLayerLocalZ;
                        CreateTree::Instance()->Clu2DRawX = tmpLayerRawX;
                        CreateTree::Instance()->Clu2DRawY = tmpLayerRawY;
                        CreateTree::Instance()->Clu2DRawZ = tmpLayerRawZ;
                        CreateTree::Instance()->Clu2DSeedX = tmpLayerSeedX;
                        CreateTree::Instance()->Clu2DSeedY = tmpLayerSeedY;
                        CreateTree::Instance()->Clu2DSeedZ = tmpLayerSeedZ;
                        CreateTree::Instance()->Clu2DSeedE = tmpLayerSeedE;
                        CreateTree::Instance()->Clu2DSeedT = tmpLayerSeedT;
                        CreateTree::Instance()->Clu2DCellDx = tmpLayerCellDx;
                        CreateTree::Instance()->Clu2DCellDy = tmpLayerCellDy;
                        CreateTree::Instance()->Clu2DE3 = tmpLayerE3;
                        CreateTree::Instance()->Clu2DE5 = tmpLayerE5;
                        CreateTree::Instance()->Clu2DE7 = tmpLayerE7;
                        CreateTree::Instance()->Clu2DE9 = tmpLayerE9;
                        // //std::cout << "tmpCellNum.size() " << tmpCellNum.size() << std::endl;
                        CreateTree::Instance()->Clu2DCellNum = tmpCellNum;
                        CreateTree::Instance()->CellType = tmpCellType;
                        CreateTree::Instance()->CellE = tmpCellE;
                        CreateTree::Instance()->CellT = tmpCellT;
                        CreateTree::Instance()->CellX = tmpCellX;
                        CreateTree::Instance()->CellY = tmpCellY;
                        CreateTree::Instance()->CellZ = tmpCellZ;

                        //////////////////////
                        if (ProgramType == 2 || ProgramType == 4)
                        {
                            CreateTree::Instance()->LayerTruX = tmpLayerTruX;
                            CreateTree::Instance()->LayerTruY = tmpLayerTruY;
                            CreateTree::Instance()->LayerTruZ = tmpLayerTruZ;
                            CreateTree::Instance()->LayerTruDep = tmpLayerTruDep;
                            CreateTree::Instance()->LayerTruShowerDepth = tmpLayerShowerDepth;
                            CreateTree::Instance()->LayerTruLocalX = tmpLayerTruLocalX;
                            CreateTree::Instance()->LayerTruLocalY = tmpLayerTruLocalY;
                            CreateTree::Instance()->LayerTruLocalZ = tmpLayerTruLocalZ;
                        }
                        //////////////////////
                    }
                    CreateTree::Instance()->OutTree->Fill();
                }
            }
        }
        if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCellInfo)
        {

            CreateTree::Instance()->ReadoutInfo->Fill();
        }
        if (ProgramType == 1)
        {
            CreateTree::Instance()->OutTree->Fill();
        }
        if (Parameters::Instance()->InitTruthInfo == 1 && Parameters::Instance()->ConstructPi0 == 1)
        {
            auto TruPi0 = new FPi0({(FGamma *)TruSignal.at(0), (FGamma *)TruSignal.at(1)});
            // //std::cout << "EntryX1 " << EventInfo.at(event).GetHitX(0) << " EntryY1 : " << EventInfo.at(event).GetHitY(0) << std::endl;
            auto HitModule1 = GetModuleByPos(GetHitX(event, 0), GetHitY(event, 0));
            // std::cout << "HitModule1 " << HitModule1 << std::endl;
            float HitX1 = (GetHitX(event, 0) - 0) / (GetHitZ(event, 0) + 12836) * (HitModule1->Z() + 12836);
            float HitY1 = (GetHitY(event, 0) - 0) / (GetHitZ(event, 0) + 12836) * (HitModule1->Z() + 12836);
            // //std::cout << "HitX1 " << HitX1 << " HitY1 : " << HitY1 << std::endl;
            auto HitModule2 = GetModuleByPos(GetHitX(event, 1), GetHitY(event, 1));
            float HitX2 = (GetHitX(event, 1) - 0) / (GetHitZ(event, 1) + 12836) * (HitModule2->Z() + 12836);
            float HitY2 = (GetHitY(event, 1) - 0) / (GetHitZ(event, 1) + 12836) * (HitModule2->Z() + 12836);
            // //std::cout << "HitX2 : " << HitX2 << " HitY2 : " << HitY2 << std::endl;
            auto HitCell3D1 = GetCell3DByPos(HitX1, HitY1);
            auto HitCell3D2 = GetCell3DByPos(HitX2, HitY2);
            float dist = sqrt(pow(GetHitX(event, 1) - GetHitX(event, 0), 2) + pow(GetHitY(event, 1) - GetHitY(event, 0), 2));
            CreateTree::Instance()->Pi0_TruType = 0;
            // if (GetHitModule(event,0) == GetHitModule(event,1))
            // {
            //     float X12 = abs(HitCell3D1->LocalX() + HitCell3D1->MotherModule()->X() - HitCell3D2->LocalX() - HitCell3D2->MotherModule()->X());
            //     float Y12 = abs(HitCell3D1->LocalY() + HitCell3D1->MotherModule()->Y() - HitCell3D2->LocalY() - HitCell3D2->MotherModule()->Y());
            //     float tmpDx = (HitCell3D1->Dx() + HitCell3D2->Dx()) / 2;
            //     float tmpDy = (HitCell3D1->Dy() + HitCell3D2->Dy()) / 2;

            //     if (X12 < (1.7 * tmpDx) && Y12 < (1.7 * tmpDy))
            //         CreateTree::Instance()->Pi0_TruType = 1;
            // }

            // float PV_Pt = sqrt(pow(EventInfos.at(event)->GetPrimaryVx(0) + EventInfos.at(event)->GetPrimaryVx(1), 2) + pow(EventInfos.at(event)->GetPrimaryVy(0) + EventInfos.at(event)->GetPrimaryVy(1), 2));
            // float PV0_Pt = sqrt(pow(EventInfos.at(event)->GetPrimaryVx(0), 2) + pow(EventInfos.at(event)->GetPrimaryVy(0), 2));
            // float PV1_Pt = sqrt(pow(EventInfos.at(event)->GetPrimaryVx(1), 2) + pow(EventInfos.at(event)->GetPrimaryVy(1), 2));
            // if (PV_Pt > 1000 && PV0_Pt > 200 && PV1_Pt > 200)

            float tmpDx = (HitCell3D1->Dx() + HitCell3D2->Dx()) / 2;
            float tmpDy = (HitCell3D1->Dy() + HitCell3D2->Dy()) / 2;
            float Dist = sqrt(pow(HitX1 - HitX2, 2) + pow(HitY1 - HitY2, 2));

            if (Dist <= (tmpDx + tmpDy) * sqrt(2.))
            {

                if (GetMatchedSubClu3D(event, 0) != NULL && GetMatchedSubClu3D(event, 1) != NULL)
                {
                    // std::cout << "1" << std::endl;
                    CreateTree::Instance()->Pi0_TruType = 1;
                }
                else if ((GetMatchedClu3D(event, 0) == NULL || GetMatchedClu3D(event, 1) == NULL))
                {
                    // std::cout << "2" << std::endl;
                    CreateTree::Instance()->Pi0_TruType = 1;
                }
                // else if ((GetMatchedClu3D(event, 0) == NULL || GetMatchedClu3D(event, 1) == NULL))
                // {
                //     // std::cout << "3" << std::endl;
                //     float tmpClu3DE;
                //     if (GetMatchedClu3D(event, 0) != NULL)
                //     {
                //         tmpClu3DE = GetMatchedClu3D(event, 0)->Energy();
                //     }
                //     else if (GetMatchedClu3D(event, 1) != NULL)
                //     {
                //         tmpClu3DE = GetMatchedClu3D(event, 1)->Energy();
                //     }
                //     if (tmpClu3DE > GetPrimaryE(event, 0) && tmpClu3DE > GetPrimaryE(event, 1) && tmpClu3DE > 0.8 * (GetPrimaryE(event, 0) + GetPrimaryE(event, 1)))
                //         CreateTree::Instance()->Pi0_TruType = 1;
                // }
            }

            // else if (Get_vClu3D(event)->size() == 1)
            // {
            //     CreateTree::Instance()->Pi0_TruType = 1;
            // }
            CreateTree::Instance()->HitDist = dist;
            CreateTree::Instance()->Pi0_TruM = TruPi0->M();
            CreateTree::Instance()->Pi0_TruE = TruPi0->E();
            CreateTree::Instance()->Pi0_TruPx = TruPi0->Px();
            CreateTree::Instance()->Pi0_TruPy = TruPi0->Py();
            CreateTree::Instance()->Pi0_TruPz = TruPi0->Pz();
            CreateTree::Instance()->Pi0_TruPt = TruPi0->Pt();
            CreateTree::Instance()->Pi0_Gamma1_TruModule = GetHitModule(event, 0);
            CreateTree::Instance()->Pi0_Gamma1_TruRegionId = GetHitRegion(event, 0);
            CreateTree::Instance()->Pi0_Gamma1_TruRegionType = GetHitRegionType(event, 0);
            CreateTree::Instance()->Pi0_Gamma2_TruModule = GetHitModule(event, 1);
            CreateTree::Instance()->Pi0_Gamma2_TruRegionId = GetHitRegion(event, 1);
            CreateTree::Instance()->Pi0_Gamma2_TruRegionType = GetHitRegionType(event, 1);
            CreateTree::Instance()->Pi0_Gamma1_TruE = TruSignal.at(0)->E();
            CreateTree::Instance()->Pi0_Gamma2_TruE = TruSignal.at(1)->E();
            CreateTree::Instance()->Pi0_Gamma1_TruPx = TruSignal.at(0)->Px();
            CreateTree::Instance()->Pi0_Gamma2_TruPx = TruSignal.at(1)->Px();
            CreateTree::Instance()->Pi0_Gamma1_TruPy = TruSignal.at(0)->Py();
            CreateTree::Instance()->Pi0_Gamma2_TruPy = TruSignal.at(1)->Py();
            CreateTree::Instance()->Pi0_Gamma1_TruPz = TruSignal.at(0)->Pz();
            CreateTree::Instance()->Pi0_Gamma2_TruPz = TruSignal.at(1)->Pz();
            CreateTree::Instance()->Pi0_Gamma1_TruPt = TruSignal.at(0)->Pt();
            CreateTree::Instance()->Pi0_Gamma2_TruPt = TruSignal.at(1)->Pt();
        }
        if (Parameters::Instance()->SavePi0)
        {
            spdlog::get(LOG_NAME)->info("\tFilling Pi0 Tree");

            for (auto Pi0 : *Get_vPi0(event))
            {
                CreateTree::Instance()->Pi0_MatchIndex = Pi0->MatchedNum;
                CreateTree::Instance()->Pi0_Type = Pi0->Type;
                CreateTree::Instance()->Pi0_M = Pi0->M();
                CreateTree::Instance()->Pi0_E = Pi0->E();
                CreateTree::Instance()->Pi0_Px = Pi0->Px();
                CreateTree::Instance()->Pi0_Py = Pi0->Py();
                CreateTree::Instance()->Pi0_Pz = Pi0->Pz();
                CreateTree::Instance()->Pi0_Pt = Pi0->Pt();
                CreateTree::Instance()->Pi0_Gamma1_Module = Pi0->Sub_Gamma.first->f_module->GetID();
                CreateTree::Instance()->Pi0_Gamma1_RegionId = Pi0->Sub_Gamma.first->f_region->GlobalId;
                CreateTree::Instance()->Pi0_Gamma1_RegionType = Pi0->Sub_Gamma.first->f_region->type;
                CreateTree::Instance()->Pi0_Gamma1_E = Pi0->Sub_Gamma.first->E();
                CreateTree::Instance()->Pi0_Gamma1_Px = Pi0->Sub_Gamma.first->Px();
                CreateTree::Instance()->Pi0_Gamma1_Py = Pi0->Sub_Gamma.first->Py();
                CreateTree::Instance()->Pi0_Gamma1_Pz = Pi0->Sub_Gamma.first->Pz();
                CreateTree::Instance()->Pi0_Gamma1_Pt = Pi0->Sub_Gamma.first->Pt();
                CreateTree::Instance()->Pi0_Gamma1_MatchIndex = Pi0->Sub_Gamma.first->MatchedNum;
                CreateTree::Instance()->Pi0_Gamma2_Module = Pi0->Sub_Gamma.second->f_module->GetID();
                CreateTree::Instance()->Pi0_Gamma2_RegionId = Pi0->Sub_Gamma.second->f_region->GlobalId;
                CreateTree::Instance()->Pi0_Gamma2_RegionType = Pi0->Sub_Gamma.second->f_region->type;
                CreateTree::Instance()->Pi0_Gamma2_E = Pi0->Sub_Gamma.second->E();
                CreateTree::Instance()->Pi0_Gamma2_Px = Pi0->Sub_Gamma.second->Px();
                CreateTree::Instance()->Pi0_Gamma2_Py = Pi0->Sub_Gamma.second->Py();
                CreateTree::Instance()->Pi0_Gamma2_Pz = Pi0->Sub_Gamma.second->Pz();
                CreateTree::Instance()->Pi0_Gamma2_Pt = Pi0->Sub_Gamma.second->Pt();
                CreateTree::Instance()->Pi0_Gamma2_MatchIndex = Pi0->Sub_Gamma.second->MatchedNum;

                CreateTree::Instance()->Pi0->Fill();
            }
        }
        if (Parameters::Instance()->SaveGamma)
        {
            // std::cout << Get_vGamma(event)->size() << std::endl;
            for (auto Gamma : *Get_vGamma(event))
            {
                if (Gamma->MatchedNum >= 0)
                {

                    int MatchedNum = Gamma->MatchedNum;
                    // std::cout << "MatchedNum " << MatchedNum << std::endl;
                    CreateTree::Instance()->Gamma_Tru_Region = GetHitRegion(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_RegionType = GetHitRegionType(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_px = GetPrimaryVx(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_py = GetPrimaryVy(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_pz = GetPrimaryVz(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_E = GetPrimaryE(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_T = GetHitT(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Region = Gamma->f_region->GetID();
                    CreateTree::Instance()->Gamma_RegionType = Gamma->f_region->GetType();
                    CreateTree::Instance()->Gamma_px = Gamma->Px();
                    CreateTree::Instance()->Gamma_py = Gamma->Py();
                    CreateTree::Instance()->Gamma_pz = Gamma->Pz();
                    CreateTree::Instance()->Gamma_E = Gamma->E();
                    CreateTree::Instance()->Gamma_T = Gamma->time;
                    CreateTree::Instance()->Gamma_Type = 0;
                    CreateTree::Instance()->Gamma->Fill();
                }
            }

            for (auto SubGamma : *Get_vSubGamma(event))
            {
                if (SubGamma.first->MatchedNum >= 0)
                {
                    int MatchedNum = SubGamma.first->MatchedNum;
                    CreateTree::Instance()->Gamma_Tru_Region = GetHitRegion(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_RegionType = GetHitRegionType(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_px = GetPrimaryVx(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_py = GetPrimaryVy(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_pz = GetPrimaryVz(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_E = GetPrimaryE(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_T = GetHitT(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Region = SubGamma.first->f_region->GetID();
                    CreateTree::Instance()->Gamma_RegionType = SubGamma.first->f_region->GetType();
                    CreateTree::Instance()->Gamma_px = SubGamma.first->Px();
                    CreateTree::Instance()->Gamma_py = SubGamma.first->Py();
                    CreateTree::Instance()->Gamma_pz = SubGamma.first->Pz();
                    CreateTree::Instance()->Gamma_E = SubGamma.first->E();
                    CreateTree::Instance()->Gamma_T = SubGamma.first->time;
                    CreateTree::Instance()->Gamma_Type = 1;
                    CreateTree::Instance()->Gamma->Fill();
                }
                if (SubGamma.second->MatchedNum >= 0)
                {
                    int MatchedNum = SubGamma.second->MatchedNum;
                    CreateTree::Instance()->Gamma_Tru_Region = GetHitRegion(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_RegionType = GetHitRegionType(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_px = GetPrimaryVx(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_py = GetPrimaryVy(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_pz = GetPrimaryVz(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_E = GetPrimaryE(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Tru_T = GetHitT(event, MatchedNum);
                    CreateTree::Instance()->Gamma_Region = SubGamma.second->f_region->GetID();
                    CreateTree::Instance()->Gamma_RegionType = SubGamma.second->f_region->GetType();
                    CreateTree::Instance()->Gamma_px = SubGamma.second->Px();
                    CreateTree::Instance()->Gamma_py = SubGamma.second->Py();
                    CreateTree::Instance()->Gamma_pz = SubGamma.second->Pz();
                    CreateTree::Instance()->Gamma_E = SubGamma.second->E();
                    CreateTree::Instance()->Gamma_T = SubGamma.second->time;
                    CreateTree::Instance()->Gamma_Type = 1;
                    CreateTree::Instance()->Gamma->Fill();
                }
            }
        }
        if (1)
        {
            CreateTree::Instance()->Seed3DNum = Get_sSeed3D(event)->size();
            CreateTree::Instance()->LMNum_Cross = Get_sLMCell3D_Cross(event)->size();
            CreateTree::Instance()->LMNum_Fork = Get_sLMCell3D_Fork(event)->size();
            CreateTree::Instance()->LMNum_3_3 = Get_sLMCell3D(event)->size();
            CreateTree::Instance()->ComputationTime = GetComputeTime(event);
            // CreateTree::Instance()->NLM_MaxInCross = GetNLM_MaxInCross(event);
            // CreateTree::Instance()->NLM_MaxInFork = GetNLM_MaxInFork(event);
            CreateTree::Instance()->Parameters->Fill();
        }
        CreateTree::Instance()->PrimaryTree->Fill();
    }

    if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveGeometry)
    {
        spdlog::get(LOG_NAME)->info("\tFilling Geometry Information................");
        std::string GeoFileName = Parameters::Instance()->OutputPath + "/SimGeo.root";
        TFile *Geo_file = TFile::Open(GeoFileName.c_str());
        if (!Geo_file)
        {
            //std::cout << "No Geo file" << std::endl;
            Geo_file = new TFile(GeoFileName.c_str(), "recreate");

            TTree *CaloTree;

            float CaloX;
            float CaloY;
            float CaloZ;
            float CaloDx;
            float CaloDy;
            float CaloDz;

            Geo_file->cd();
            CaloTree = new TTree("Calo", "Calo");

            CaloTree->Branch("x", &CaloX, "x/F");
            CaloTree->Branch("y", &CaloY, "y/F");
            CaloTree->Branch("z", &CaloZ, "z/F");
            CaloTree->Branch("dx", &CaloDx, "dx/F");
            CaloTree->Branch("dy", &CaloDy, "dy/F");
            CaloTree->Branch("dz", &CaloDz, "dz/F");
            CaloX = X;
            CaloY = Y;
            CaloZ = Z;
            CaloDx = Dx;
            CaloDy = Dy;
            CaloDz = Dz;
            CaloTree->Fill();
            CaloTree->Write();
            module_map.Write("moduleMap");
            region_map.Write("regionIdMap");
            regionType_map.Write("regionTypeMap");

            TTree *RegionTree;
            int regionID;
            int RegionType;
            int Have_SiTiming;
            float SiTimingThickness;
            int SiTimingLayerNum;
            float SiTimingCellSize;
            float Cell3DSize;
            float Separator_position;
            int detector_type;
            float RegionShift;
            int TotLayerNum;
            float FrontFace;
            float r_ax;
            float r_ay;
            float r_az;
            float FiberNumX;
            float FiberNumY;
            std::vector<int> layerId;
            std::vector<float> layerPos;
            std::vector<int> layerType;
            std::vector<float> layerThickness;
            Geo_file->cd();
            RegionTree = new TTree("Region", "Region");
            RegionTree->Branch("regionID", &regionID, "regionID/I");
            RegionTree->Branch("RegionType", &RegionType, "RegionType/I");
            RegionTree->Branch("Have_SiTiming", &Have_SiTiming, "Have_SiTiming/I");
            RegionTree->Branch("TotLayerNum", &TotLayerNum, "TotLayerNum/I");
            RegionTree->Branch("SiTimingLayerNum", &SiTimingLayerNum, "SiTimingLayerNum/I");
            RegionTree->Branch("detector_type", &detector_type, "detector_type/I");
            RegionTree->Branch("SiTimingThickness", &SiTimingThickness, "SiTimingThickness/F");
            RegionTree->Branch("SiTimingCellSize", &SiTimingCellSize, "SiTimingCellSize/F");
            RegionTree->Branch("Cell3DSize", &Cell3DSize, "Cell3DSize/F");
            RegionTree->Branch("Separator_position", &Separator_position, "Separator_position/F");
            RegionTree->Branch("RegionShift", &RegionShift, "RegionShift/F");
            RegionTree->Branch("FrontFace", &FrontFace, "FrontFace/F");
            RegionTree->Branch("ax", &r_ax, "ax/F");
            RegionTree->Branch("ay", &r_ay, "ay/F");
            RegionTree->Branch("az", &r_az, "az/F");
            RegionTree->Branch("FiberNumX", &FiberNumX, "FiberNumX/F");
            RegionTree->Branch("FiberNumY", &FiberNumY, "FiberNumY/F");
            RegionTree->Branch("layerId", &layerId);
            RegionTree->Branch("layerPos", &layerPos);
            RegionTree->Branch("layerType", &layerType);
            RegionTree->Branch("layerThickness", &layerThickness);
            for (auto regionGeo : region_container)
            {

                FRegion *CurrentRegion = regionGeo.second;
                regionID = CurrentRegion->GlobalId;
                RegionType = CurrentRegion->type;
                Have_SiTiming = CurrentRegion->Have_SiTiming;
                SiTimingLayerNum = CurrentRegion->SiTimingLayerNum;
                detector_type = CurrentRegion->detector_type;
                SiTimingThickness = CurrentRegion->SiTimingThickness;
                SiTimingCellSize = CurrentRegion->SiTimingCellSize;
                Cell3DSize = CurrentRegion->Cell3DSize;
                Separator_position = CurrentRegion->Separator_position;
                RegionShift = CurrentRegion->RegionShift;
                TotLayerNum = CurrentRegion->TotLayerNum;
                FrontFace = CurrentRegion->FrontFace;
                r_ax = CurrentRegion->ax;
                r_ay = CurrentRegion->ay;
                r_az = CurrentRegion->az;
                FiberNumX = CurrentRegion->FiberNumX;
                FiberNumY = CurrentRegion->FiberNumY;
                int tmpLayerID = 0;
                layerId.clear();
                layerPos.clear();
                layerType.clear();
                layerThickness.clear();
                tmpName.str("");
                tmpName << "Region" << regionID << "_Cell3DMap";
                CurrentRegion->cell3D_map.Write(tmpName.str().c_str());
                for (auto CurrentLayerInfo : CurrentRegion->Layers)
                {
                    layerId.emplace_back(tmpLayerID);
                    layerPos.emplace_back(CurrentLayerInfo->layerPos);
                    layerType.emplace_back(CurrentLayerInfo->layerType);
                    layerThickness.emplace_back(CurrentLayerInfo->layerThickness);

                    tmpName.str("");
                    tmpName << "Region" << regionID << "_Layer" << tmpLayerID << "_CellMap";
                    CurrentLayerInfo->cell_map.Write(tmpName.str().c_str());
                    tmpLayerID++;
                }
                RegionTree->Fill();
            }
            RegionTree->Write();

            ///////////////////////
            TTree *ModuleTree;
            int moduleID;
            float x;
            float y;
            float z;
            float dx;
            float dy;
            float dz;
            float ax;
            float ay;
            float az;
            int moduleType;

            Geo_file->cd();
            ModuleTree = new TTree("Module", "Module");
            ModuleTree->Branch("moduleID", &moduleID, "moduleID/I");
            ModuleTree->Branch("regionID", &regionID, "regionID/I");
            ModuleTree->Branch("x", &x, "x/F");
            ModuleTree->Branch("y", &y, "y/F");
            ModuleTree->Branch("z", &z, "z/F");
            ModuleTree->Branch("dx", &dx, "dx/F");
            ModuleTree->Branch("dy", &dy, "dy/F");
            ModuleTree->Branch("dz", &dz, "dz/F");
            ModuleTree->Branch("ax", &ax, "ax/F");
            ModuleTree->Branch("ay", &ay, "ay/F");
            ModuleTree->Branch("az", &az, "az/F");
            ModuleTree->Branch("moduleType", &moduleType, "moduleType/I");
            for (auto moduleGeo : module_container)
            {
                moduleID = moduleGeo.first;
                FModule *CurrentModule = moduleGeo.second;
                x = CurrentModule->x;
                y = CurrentModule->y;
                z = CurrentModule->z;
                dx = CurrentModule->dx;
                dy = CurrentModule->dy;
                dz = CurrentModule->dz;
                ax = CurrentModule->ax;
                ay = CurrentModule->ay;
                az = CurrentModule->az;
                moduleType = CurrentModule->type;
                regionID = CurrentModule->f_region->GlobalId;
                ModuleTree->Fill();
            }
            ModuleTree->Write();
        }
        Geo_file->Close();
    }
}

void Mixture_Calo::Calibration()
{

    // if (!ParticleGun)
    // {
    //     //std::cout << "ERROR!!! Calibration can only run in particle gun " << std::endl;
    //     exit(-1);
    // }

    // //std::cout << "  Calculating  Cell Calibration Parameters ..........." << std::endl;
    // CalculateCellCaliPara();
    spdlog::get(LOG_NAME)->info("\tFilling Tree");

    WriteTree();
}

void Mixture_Calo::Clustering()
{

    spdlog::get(LOG_NAME)->info("\tInitical calibration parameters ................");
    InitCaliPara();
    spdlog::get(LOG_NAME)->info("\tCalibrate Cell energy................");
    CalibrateCell();

    auto startTime = std::chrono::high_resolution_clock::now();
    spdlog::get(LOG_NAME)->info("\tConstructing 3D Cell ...");
    ConstructCell3D();
    spdlog::get(LOG_NAME)->info("Find neighbor for cells and modules");
    FindNeighbor();

    spdlog::get(LOG_NAME)->info("\tCluFlow : {}", CluFlow);

    for (int event = 0; event < max_Eventseq; event++)
    {
        spdlog::get(LOG_NAME)->info("event : {}", event);
        spdlog::get(LOG_NAME)->info("\tFinding Local Maximum For Modules ...");
        FindLocalMaxCell3D(event);
        spdlog::get(LOG_NAME)->info("\tFinding Local Maximum For Each Layers ...");
        FindLocalMaxCell2D(event);
        if (CluFlow == 0)
        {
        }
        else if (CluFlow == 1)
        {

            spdlog::get(LOG_NAME)->info("\tSeeding ...");
            Seeding(event);
            spdlog::get(LOG_NAME)->info("\tAllocating Cell...");
            AllocateCell(event);

            if (ProgramType == 0)
            {
                spdlog::get(LOG_NAME)->info("\tSplit Cell Energy...");
                SplitOverlapClu2D(event);
            }
            spdlog::get(LOG_NAME)->info("\tConstruct Cluster3D");
            ConstructCluster3D(event);
            spdlog::get(LOG_NAME)->info("\tCalibrating Cluster3D");
            CorrectCluster3D(event);
        }
        else if (CluFlow == 2)
        {

            if (Parameters::Instance()->SaveHitMap == 1)
            {
                // std::cout << "test" << std::endl;
                SaveReadOutMap(event);
            }

            spdlog::get(LOG_NAME)->info("\tSeeding 3D ...");
            Seeding3D(event);
            spdlog::get(LOG_NAME)->info("\tAllocating 3D Cell...");
            AllocateCell3D(event);
            if (ProgramType == 0)
            {
                spdlog::get(LOG_NAME)->info("\tSplit Cell Energy...");
                SplitOverlapClu3D(event);
            }
            spdlog::get(LOG_NAME)->info("\tReAllocating 2D Cell...");
            ReAllo2DCell(event);
            if (ProgramType == 0)
            {
                spdlog::get(LOG_NAME)->info("\tSplit Cell Energy...");
                SplitOverlapClu3D(event);
            }

            spdlog::get(LOG_NAME)->info("\tCalibrating Cluster3D");
            CorrectCluster3D(event);
        }
        else if (CluFlow == 3)
        {
            spdlog::get(LOG_NAME)->info("\tSeeding 3D ...");
            Seeding3D(event);
            spdlog::get(LOG_NAME)->info("\tAllocating 3D Cell...");
            AllocateCell3D(event);
            if (ProgramType == 0)
            {
                spdlog::get(LOG_NAME)->info("\tSplit Cell Energy...");
                SplitOverlapClu3D(event);
            }
            spdlog::get(LOG_NAME)->info("\tCalibrating Cluster3D");
            CorrectCluster3D(event);
        }
        auto endTime = std::chrono::high_resolution_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
        SetComputeTime(event, duration);
    }
}

void Mixture_Calo::Calibration2()
{
    // if (!ParticleGun)
    // {
    //     //std::cout << "ERROR!!! Calibration can only run in particle gun " << std::endl;
    //     exit(-1);
    // }

    spdlog::get(LOG_NAME)->info("\tInitical calibration parameters ................");
    InitCaliPara();
    spdlog::get(LOG_NAME)->info("\tCalibrate Cell energy................");
    CalibrateCell();

    auto startTime = std::chrono::high_resolution_clock::now();
    spdlog::get(LOG_NAME)->info("\tConstructing 3D Cell ...");
    ConstructCell3D();
    spdlog::get(LOG_NAME)->info("Find neighbor for cells and modules");
    FindNeighbor();
    spdlog::get(LOG_NAME)->info("\tCluFlow : {}", CluFlow);

    for (int event = 0; event < max_Eventseq; event++)
    {
        spdlog::get(LOG_NAME)->info("event : {}", event);
        spdlog::get(LOG_NAME)->info("\tFinding Local Maximum For Modules ...");
        FindLocalMaxCell3D(event);
        spdlog::get(LOG_NAME)->info("\tFinding Local Maximum For Each Layers ...");
        FindLocalMaxCell2D(event);

        if (CluFlow == 2)
        {

            spdlog::get(LOG_NAME)->info("\tSeeding 3D ...........");
            Seeding3D(event);
            spdlog::get(LOG_NAME)->info("\tAllocating 3D Cell................");
            AllocateCell3D(event);
            spdlog::get(LOG_NAME)->info("\tReAllocating 2D Cell................");
            ReAllo2DCell(event);
        }
        else if (CluFlow == 3)
        {

            spdlog::get(LOG_NAME)->info("\tSeeding 3D ...........");
            Seeding3D(event);
            spdlog::get(LOG_NAME)->info("\tAllocating 3D Cell................");
            AllocateCell3D(event);
        }
    }
    // //std::cout << "  Caculating Cluster 2D Time" << std::endl;
    // CaculateCluster2DTime();
    //  //std::cout << "  Caculating Cluster X , Y" << std::endl;
    //   ReWeightLayerPos();
    /*
    //std::cout << "  Splitting energy" << std::endl;
    SplitOverlapClu3D();
    */
    // //std::cout << "  Caculating Cluster Time" << std::endl;
    // CaculateClusterTime();
    spdlog::get(LOG_NAME)->info("\tFilling Tree");
    WriteTree();
}
