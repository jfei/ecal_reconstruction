#include "FCalorimeter.hh"
#include "iostream"
#include "FCell3D.hh"
#include "FModule.hh"
#include "Parameters.hh"
#include "FRegion.hh"
#include "FLayer.hh"
#include "ModuleEventInfo.hh"

FModule *FCalorimeter::GetModuleByPos(float x_pos, float y_pos, float z_pos)
{
    int tmpModuleId = GetModuleIDByPos(x_pos, y_pos, z_pos);
    if (tmpModuleId >= 0)
        return GetModuleByID(tmpModuleId);
    else
        return NULL;
}

FModule *FCalorimeter::GetModuleByPos(float x_pos, float y_pos)
{
    int tmpModuleId = GetModuleIDByPos(x_pos, y_pos);
    if (tmpModuleId >= 0)
        return GetModuleByID(tmpModuleId);
    else
        return NULL;
}

FModule *FCalorimeter::GetModuleByID(int moduleId)
{
    if (moduleId >= 0)
        return module_container.at(moduleId);
    else
        return NULL;
}

bool IsInBeamPipe(int module_id, float x_pos, float y_pos)
{
    if (module_id == 0 && fabs(x_pos) <= 250 && fabs(y_pos) <= 250) // some step in the boundary of beam pipe
    {
        return 1;
    }
    else
        return 0;
}

bool FCalorimeter::OutofECal(float x_pos, float y_pos)
{
    float xMin = module_map.GetXaxis()->GetXmin();
    float xMax = module_map.GetXaxis()->GetXmax();

    // 获取Y轴范围
    float yMin = module_map.GetYaxis()->GetXmin();
    float yMax = module_map.GetYaxis()->GetXmax();

    if (x_pos >= xMin && x_pos <= xMax && y_pos >= yMin && y_pos <= yMax)
    {
        return 0;
    }

    return 1;
}

int FCalorimeter::GetModuleIDByPos(float x_pos, float y_pos)
{
    int tmp_module_id;
    if (ModuleStudy) // if only have one module , so module id is 0
    {
        tmp_module_id = 0;
        return tmp_module_id;
    }
    if (OutofECal(x_pos, y_pos))
        return -1;
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    if (IsInBeamPipe(tmp_module_id, x_pos, y_pos))
        return -1;

    return tmp_module_id;
}

int FCalorimeter::GetModuleIDByPos(float x_pos, float y_pos, float z_pos)
{
    int tmp_module_id;
    if (ModuleStudy) // if only have one module , so module id is 0
    {
        tmp_module_id = 0;
        return tmp_module_id;
    }
    if (OutofECal(x_pos, y_pos))
        return -1;
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    if (tmp_module_id == 0)
    {
        if (IsInBeamPipe(tmp_module_id, x_pos, y_pos)) // some step in the boundary of beam pipe
        {
            int Xbin, Ybin, Zbin;
            module_map.GetBinXYZ(tmp_module_bin, Xbin, Ybin, Zbin);

            float XCenter = module_map.GetXaxis()->GetBinCenter(Xbin);

            float YCenter = module_map.GetYaxis()->GetBinCenter(Ybin);

            if (abs(x_pos) > abs(y_pos))
            {
                if (x_pos < -200)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(XCenter - 100, y_pos));
                }
                if (x_pos > 200)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(XCenter + 100, y_pos));
                }
            }
            else
            {
                if (y_pos < -200)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, YCenter - 100));
                }
                if (y_pos > 200)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, YCenter + 100));
                }
            }

            FModule *CurrentModule = module_container[tmp_module_id];
            FRegion *CurrentRegion = CurrentModule->f_region;
            float Ax = CurrentModule->Ax();
            float Ay = CurrentModule->Ay();
            float Az = CurrentModule->Az();
            float M_dx = CurrentModule->Dx();
            float M_dy = CurrentModule->Dy();
            float M_x = CurrentModule->X();
            float M_y = CurrentModule->Y();
            float M_z = CurrentModule->Z();

            // FPoint ModuleCenter_Local = CurrentRegion->Global2Local(0, 0, z_pos - M_z);
            float Local_M_x = 0;
            float Local_M_y = 0;
            FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);
            float Local_x = NewPos.X();
            float Local_y = NewPos.Y();
            if (abs(Local_x - Local_M_x) > M_dx / 2 && abs(Local_y - Local_M_y) <= M_dy / 2) // 其实已经超出范围
            {
                if (Local_x > Local_M_x)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(M_x + M_dx, y_pos));
                }
                if (Local_x < Local_M_x)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(M_x - M_dx, y_pos));
                }
            }
            if (abs(Local_y - Local_M_y) > M_dy / 2 && abs(Local_x - Local_M_x) <= M_dx / 2)
            {
                if (Local_y > Local_M_y)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, M_y + M_dy));
                }
                if (Local_y < Local_M_y)
                {
                    tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, M_y - M_dy));
                }
            }
            if (tmp_module_id == 0)
            {
                return -77;
            }
            else
            {
                return tmp_module_id;
            }
        }
        else
        {
            return -77;
        }
    }
    else
    {
        FModule *CurrentModule = module_container[tmp_module_id];
        FRegion *CurrentRegion = CurrentModule->f_region;
        float Ax = CurrentModule->Ax();
        float Ay = CurrentModule->Ay();
        float Az = CurrentModule->Az();
        float M_dx = CurrentModule->Dx();
        float M_dy = CurrentModule->Dy();
        float M_x = CurrentModule->X();
        float M_y = CurrentModule->Y();
        float M_z = CurrentModule->Z();

        // FPoint ModuleCenter_Local = CurrentRegion->Global2Local(0, 0, z_pos - M_z);
        float Local_M_x = 0;
        float Local_M_y = 0;
        FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);
        float Local_x = NewPos.X();
        float Local_y = NewPos.Y();
        // std::cout << "Local_x " << Local_x << " Local_y " << Local_x << std::endl;
        if (abs(Local_x - Local_M_x) > M_dx / 2 && abs(Local_y - Local_M_y) <= M_dy / 2) // 其实已经超出范围
        {
            // std::cout << " x out " << std::endl;
            if (Local_x > Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(M_x + M_dx, y_pos));
            }
            if (Local_x < Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(M_x - M_dx, y_pos));
            }
        }
        if (abs(Local_y - Local_M_y) > M_dy / 2 && abs(Local_x - Local_M_x) <= M_dx / 2)
        {
            // std::cout << " y out " << std::endl;
            // std::cout << "Local_y " << Local_y << std::endl;
            if (Local_y > Local_M_y)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, M_y + M_dy));
            }
            if (Local_y < Local_M_y)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos, M_y - M_dy));
            }
        }
        if (abs(Local_y - Local_M_y) > M_dy / 2 && abs(Local_x - Local_M_x) > M_dx / 2)
        {
            // std::cout << " y out " << std::endl;
            // std::cout << "Local_y " << Local_y << std::endl;
            if (Local_y > Local_M_y && Local_x > Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos + M_dx, M_y + M_dy));
            }
            if (Local_y < Local_M_y && Local_x > Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos + M_dx, M_y - M_dy));
            }
            if (Local_y > Local_M_y && Local_x < Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos - M_dx, M_y + M_dy));
            }
            if (Local_y < Local_M_y && Local_x < Local_M_x)
            {
                tmp_module_id = module_map.GetBinContent(module_map.FindBin(x_pos - M_dx, M_y - M_dy));
            }
        }
        if (tmp_module_id == 0)
        {
            return -77;
        }
        else
        {
            return tmp_module_id;
        }
    }
}

int FCalorimeter::GetRegionIDByPos(float x_pos, float y_pos, float z_pos)
{
    FRegion *tmpRegion = GetRegionByPos(x_pos, y_pos, z_pos);
    if (tmpRegion != NULL)
        return tmpRegion->GlobalId;
    else
        return -77;
}

int FCalorimeter::GetRegionTypeByPos(float x_pos, float y_pos, float z_pos)
{
    FRegion *tmpRegion = GetRegionByPos(x_pos, y_pos, z_pos);
    if (tmpRegion != NULL)
        return tmpRegion->type;
    else
        return -77;
}

int FCalorimeter::GetRegionIDByPos(float x_pos, float y_pos)
{
    FRegion *tmpRegion = GetRegionByPos(x_pos, y_pos);
    if (tmpRegion != NULL)
        return tmpRegion->GlobalId;
    else
        return -77;
}

int FCalorimeter::GetRegionTypeByPos(float x_pos, float y_pos)
{
    FRegion *tmpRegion = GetRegionByPos(x_pos, y_pos);
    if (tmpRegion != NULL)
        return tmpRegion->type;
    else
        return -77;
}

FRegion *FCalorimeter::GetRegionByPos(float x_pos, float y_pos, float z_pos)
{
    FModule *tmpModule = GetModuleByPos(x_pos, y_pos, z_pos);
    if (tmpModule != NULL)
        return tmpModule->f_region;
    else
    {
        return NULL;
    }
}

FRegion *FCalorimeter::GetRegionByPos(float x_pos, float y_pos)
{
    FModule *tmpModule = GetModuleByPos(x_pos, y_pos);
    if (tmpModule != NULL)
        return tmpModule->f_region;
    else
    {
        return NULL;
    }
}

int FCalorimeter::GetCellLocalIDByPos(float x_pos, float y_pos, float z_pos) // 输入全局坐标
{
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    int tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    FModule *CurrentModule = module_container[tmp_module_id];
    FRegion *CurrentRegion = CurrentModule->f_region;
    float Ax = CurrentModule->Ax();
    float Ay = CurrentModule->Ay();
    float Az = CurrentModule->Az();
    float M_x = CurrentModule->X();
    float M_y = CurrentModule->Y();
    float M_z = CurrentModule->Z();
    FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);
    // std::cout << "module_container[tmp_module_id]->f_region : " << module_container[tmp_module_id]->f_region->GlobalId << std::endl;
    return CurrentRegion->GetCellLocalIdByRelaPos(NewPos.X(), NewPos.Y(), NewPos.Z());
}

uint64 FCalorimeter::GetCellGlobalIDByPos(float x_pos, float y_pos, float z_pos)
{
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    int tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    FModule *CurrentModule = module_container[tmp_module_id];
    FRegion *CurrentRegion = CurrentModule->f_region;
    float Ax = CurrentModule->Ax();
    float Ay = CurrentModule->Ay();
    float Az = CurrentModule->Az();
    float M_x = CurrentModule->X();
    float M_y = CurrentModule->Y();
    float M_z = CurrentModule->Z();
    FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);

    int tmp_layer_id = module_container[tmp_module_id]->f_region->Get_layerid(NewPos.Z());
    if (tmp_layer_id == -1)
    {
        // std::cout << "doesn't have this layer (" << NewPos.Z() << ") in this module" << std::endl;
        return -1;
    }

    int tmp_cell_id = CurrentRegion->GetCellLocalIdByRelaPos(NewPos.X(), NewPos.Y(), NewPos.Z());
    if (tmp_cell_id < 0)
        return -1;
    uint64 tmp_Global_id = module_container[tmp_module_id]->v_layer[tmp_layer_id]->v_cell[tmp_cell_id]->GetGlobalId();

    return tmp_Global_id;
}

FCell *FCalorimeter::GetCellByPos(float x_pos, float y_pos, float z_pos)
{

    int tmp_module_id = GetModuleIDByPos(x_pos, y_pos, z_pos);
    if (tmp_module_id < 0)
    {
        // std::cout << "Out of ECal Range" << std::endl;
        return NULL;
    }

    FModule *CurrentModule = module_container[tmp_module_id];
    FRegion *CurrentRegion = CurrentModule->f_region;
    float Ax = CurrentModule->Ax();
    float Ay = CurrentModule->Ay();
    float Az = CurrentModule->Az();
    float M_dx = CurrentModule->Dx();
    float M_dy = CurrentModule->Dy();
    float M_x = CurrentModule->X();
    float M_y = CurrentModule->Y();
    float M_z = CurrentModule->Z();

    FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);

    int tmp_layer_id = module_container[tmp_module_id]->f_region->Get_layerid(NewPos.Z());
#if 0
    std::cout << "Absolute x " << x_pos - M_x << " Absolute y " << y_pos - M_y << " Absolute z " << z_pos - M_z << std::endl;
    std::cout << "NewPos.X() " << NewPos.X() << " NewPos.Y() " << NewPos.Y() << " NewPos.Z() " << NewPos.Z() << std::endl;
#endif
    if (tmp_layer_id == -1)
    {
        // std::cout << "no layer (" << NewPos.Z() << ") in this module (region : " << CurrentRegion->GlobalId << ")" << std::endl;
        return NULL;
    }
    // if (tmp_module_id == 3164 && tmp_layer_id == 1)
    // {

    //     std::cout << "l x : " << NewPos.X() << " l.y " << NewPos.Y() << " l.z " << NewPos.Z() << std::endl;
    // }
    int tmp_cell_id = CurrentRegion->GetCellLocalIdByRelaPos(NewPos.X(), NewPos.Y(), NewPos.Z());
    if (tmp_cell_id < 0)
        return NULL;
    return module_container[tmp_module_id]->v_layer[tmp_layer_id]->v_cell[tmp_cell_id];
}

FCell3D *FCalorimeter::GetCell3DByPos(float x_pos, float y_pos, float z_pos)
{
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    int tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    FModule *CurrentModule = module_container[tmp_module_id];
    FRegion *CurrentRegion = CurrentModule->f_region;
    float Ax = CurrentModule->Ax();
    float Ay = CurrentModule->Ay();
    float Az = CurrentModule->Az();
    float M_x = CurrentModule->X();
    float M_y = CurrentModule->Y();
    float M_z = CurrentModule->Z();
    FPoint NewPos = CurrentRegion->Global2Local(x_pos - M_x, y_pos - M_y, z_pos - M_z);

    int tmp_cell_id = CurrentRegion->GetCell3DLocalIdByRelaPos(NewPos.X(), NewPos.Y());
    return module_container[tmp_module_id]->v_Cell3D[tmp_cell_id];
}

FCell3D *FCalorimeter::GetCell3DByPos(float x_pos, float y_pos)
{
    int tmp_module_bin = module_map.FindBin(x_pos, y_pos);
    int tmp_module_id = module_map.GetBinContent(tmp_module_bin);
    FModule *CurrentModule = module_container[tmp_module_id];
    FRegion *CurrentRegion = CurrentModule->f_region;
    float Ax = CurrentModule->Ax();
    float Ay = CurrentModule->Ay();
    float Az = CurrentModule->Az();
    float M_x = CurrentModule->X();
    float M_y = CurrentModule->Y();
    float M_z = CurrentModule->Z();

    int tmp_cell_id = CurrentRegion->GetCell3DLocalIdByRelaPos(x_pos - M_x, y_pos - M_y);
    return module_container[tmp_module_id]->v_Cell3D[tmp_cell_id];
}

void FCalorimeter::FindLocalMaxCell3D(int event)
{

    for (auto module : module_container)
    {

        for (auto cell : module.second->v_Cell3D)
        {
            // std::cout << "Finding Cell Neighbours" << std::endl;

            if (cell->GetEnergy(event) > 0)
            {

                if (cell->IsLocalMax(event) > 0)
                {
                    // std::cout << "event : " << event << " cell->GetID() " << cell->GetID() << std::endl;
                    module.second->GetEventInfo(event)->LocalMaxCell3D.insert(cell);
                    Get_sLMCell3D(event)->insert(cell);
                }
                if (cell->IsLocalMax_Cross(event) > 0)
                {
                    module.second->GetEventInfo(event)->LocalMaxCell3D_Cross.insert(cell);
                    Get_sLMCell3D_Cross(event)->insert(cell);
                    // if (cell->IsLocalMax(event) == 0)
                    // {
                    //     SetNLM_MaxInCross(event, GetNLM_MaxInCross(event) + 1);
                    // }
                }
                if (cell->IsLocalMax_Fork(event) > 0)
                {
                    module.second->GetEventInfo(event)->LocalMaxCell3D_Fork.insert(cell);
                    Get_sLMCell3D_Fork(event)->insert(cell);
                    // if (cell->IsLocalMax(event) == 0)
                    // {
                    //     SetNLM_MaxInFork(event, GetNLM_MaxInFork(event) + 1);
                    // }
                }
            }
        }
    }
}

void FCalorimeter::FindLocalMaxCell2D(int event)
{

        for (auto module : module_container)
        {
            for (auto layer : module.second->v_layer)
            {
                for (auto cell : layer->v_cell)
                {

                    if (cell->IsLocalMax(event) > 0)
                    {
                        layer->EventInfo.at(event).LocalMaxCell.insert(cell);
                    }
                }
            }
        }
}

void FCalorimeter::GetModuleNeighbours()
{

    for (auto module : module_container)
    {
        FModule *CurrentModule = module.second;
        // std::cout << "In module : " << CurrentModule->GlobalId << std::endl;

        CurrentModule->FindNeighbours();
    }
}
