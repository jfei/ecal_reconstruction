#include "FModule.hh"
#include "FCell3D.hh"
#include "FCell.hh"
#include "iostream"
#include <sstream>
#include "TGraphErrors.h"
#include "TTree.h"
#include "TFile.h"
#include "FCalorimeter.hh"
#include "FRegion.hh"
#include "FLayer.hh"
#include "ModuleEventInfo.hh"

void FModule::FindNeighbours()
{
    float Mx = x;
    float My = y;
    float Mz = z;
    float Mdx = dx;
    float Mdy = dy;

    // if (GetID() == 3001)
    // {
    //     std::cout << "Mx " << Mx << " My " << My << " Mz " << Mz << std::endl;
    //     std::cout << "Mdx " << Mdx << " Mdy " << Mdy << std::endl;
    // }
    for (int x_loop = -1; x_loop <= 1; x_loop++)
        for (int y_loop = -1; y_loop <= 1; y_loop++)
        {
            if (x_loop != 0 || y_loop != 0)
            {

                float Nbx = Mx + x_loop * 120;
                float Nby = My + y_loop * 120;

                FModule *NbModule = f_calo->GetModuleByPos(Nbx, Nby);
                // if (GetID() == 3001)
                // {
                //     std::cout << "Nbx : " << Nbx << " Nby : " << Nby << std::endl;
                //     std::cout << "NbModule " << NbModule->GetID() << std::endl;
                // }
                if (NbModule != NULL && NbModule != this)
                    Neighbours.emplace_back(NbModule);
            }
        }
    // std::cout << "finish" << std::endl;
}

void FModule::InitEventInfo()
{
    for (int loop = 0; loop < f_calo->max_Eventseq; loop++)
    {
        FModuleEventInfo *tmpEventInfo = new FModuleEventInfo(loop);

        EventInfo.emplace_back(tmpEventInfo);
    }
}

FModuleEventInfo *FModule::GetEventInfo(int event)
{
    return this->EventInfo.at(event);
}

FCell *FModule::GetCellByRelaPos(float x_pos, float y_pos, int layer_id)
{
    int CellLocalId = f_region->GetCellLocalIdByRelaPos(x_pos, y_pos, layer_id);
    if (CellLocalId == -1)
    {
        spdlog::get(ERROR_NAME)->error("Cell Id {} illegal", CellLocalId);
        exit(0);
    }
    return v_layer.at(layer_id)->v_cell.at(CellLocalId);
}

FCell *FModule::GetCellByRelaPos(float x_pos, float y_pos, float z_pos)
{
    int layer_id = f_region->Get_layerid(z_pos);

    if (layer_id == -1) // if current region doesn't have this layer
    {
        spdlog::get(ERROR_NAME)->error("layer Id {} illegal", layer_id);
        exit(0);
    }
    int CellLocalId = f_region->GetCellLocalIdByRelaPos(x_pos, y_pos, layer_id);
    if (CellLocalId == -1)
    {
        spdlog::get(ERROR_NAME)->error("Cell Id {} illegal", CellLocalId);
        exit(0);
    }
    return v_layer.at(layer_id)->v_cell.at(CellLocalId);
}

FCell3D *FModule::GetCell3DByRelaPos(float x_pos, float y_pos)
{
    int Cell3DLocalId = f_region->GetCell3DLocalIdByRelaPos(x_pos, y_pos);
    return v_Cell3D.at(Cell3DLocalId);
}