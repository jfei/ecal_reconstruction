#include "FLayer.hh"
#include "FRegion.hh"
#include "FModule.hh"
#include "FCluster2D.hh"
#include "FCell.hh"
#include "FCalorimeter.hh"
int FLayer::GetCellLocalIdByRelaPos(float x_pos, float y_pos)
{
    FRegion *CurrentRegion = f_module->f_region;

    auto TmpCellMap = CurrentRegion->Layers[id]->cell_map;
    float cellSize = CurrentRegion->Layers[id]->CellSize;
    int tmpId = CurrentRegion->Layers[id]->cell_map.GetBinContent(TmpCellMap.FindBin(x_pos, y_pos));

    if (tmpId == 0)
    {
        float tmpX = TmpCellMap.GetXaxis()->GetBinLowEdge(1);
        float tmpY = TmpCellMap.GetYaxis()->GetBinLowEdge(1);
        if (abs(x_pos - tmpX) + abs(y_pos - tmpY) > 2 * cellSize)
            return -1;
        else
            return 0;
    }
    else
        return tmpId;


}

void FLayer::InitEventInfo()
{
    for (int loop = 0; loop < f_module->f_calo->max_Eventseq; loop++)
    {
        LayerEventInfo tmpEventInfo;

        tmpEventInfo.SetEventID(loop);
        EventInfo.emplace_back(tmpEventInfo);
    }
}