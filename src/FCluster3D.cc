#include "FLayer.hh"
#include "FCell.hh"
#include "FCluster2D.hh"
#include "FCell.hh"
#include "FRegion.hh"
#include "FCluster3D.hh"
#include "TMath.h"
#include "FModule.hh"
#include "Mixture_Calo.hh"
#include "FCell3D.hh"
#include "FLog.hh"
#include <vector>
#include <algorithm>

void FCluster3D::RecRawInfoByCells()
{
    float Seed3DE = Seed3D->GetEnergy(this);
    float Seed3DX = Seed3D->X();
    float Seed3DY = Seed3D->Y();
    float Seed3DZ = Seed3D->Z();
    RawGlobalPos.SetXYZ(0, 0, 0); // 清空坐标；
    GlobalPos.SetXYZ(0, 0, 0);    // 清空坐标；
    LocalPos.SetXYZ(0, 0, 0);
    RawE = 0;
    E3 = 0;
    float tmpE = 0;
    RawTime = Seed3D->GetTime(event);
    for (auto v_CellSeq : Cluster_Cell3D)
    {
        int CellType = v_CellSeq.first;
        auto v_Cell = v_CellSeq.second;
        for (auto Cell : v_Cell)
        {

            float CellE = Cell->GetEnergy(this);
            if (Cell != Seed3D)
                E3 += CellE;
            float CellX = Cell->X();
            float CellY = Cell->Y();
            float CellZ = Cell->Z();

            if (CellType <= 2)
            {
                float Old_Clu3dX = RawX();
                float Old_Clu3dY = RawY();
                float Old_Clu3dZ = RawZ();
                float New_Clu3dX = Old_Clu3dX + CellX * CellE;
                float New_Clu3dY = Old_Clu3dY + CellY * CellE;
                float New_Clu3dZ = Old_Clu3dZ + CellZ * CellE;
                RawGlobalPos.SetXYZ(New_Clu3dX, New_Clu3dY, New_Clu3dZ);
                GlobalPos.SetXYZ(New_Clu3dX, New_Clu3dY, New_Clu3dZ);

                float CellLocalX = Cell->LocalX();
                float CellLocalY = Cell->LocalY();
                float CellLocalZ = Cell->LocalZ();
                float Old_Clu3dLocalX = LocalX();
                float Old_Clu3dLocalY = LocalY();
                float Old_Clu3dLocalZ = LocalZ();
                float New_Clu3dLocalX = Old_Clu3dLocalX + CellLocalX * CellE;
                float New_Clu3dLocalY = Old_Clu3dLocalY + CellLocalY * CellE;
                float New_Clu3dLocalZ = Old_Clu3dLocalZ + CellLocalZ * CellE;
                LocalPos.SetXYZ(New_Clu3dLocalX, New_Clu3dLocalY, New_Clu3dLocalZ);
                tmpE += CellE;
                // std::cout << "Cell " << Cell->GetID() << " E " << CellE << std::endl;
            }

            RawE += CellE;
            // std::cout << "Cell " << Cell->GetID() << " E " << CellE << std::endl;
        }
    }
    float Old_Clu3dX = RawX();
    float Old_Clu3dY = RawY();
    float Old_Clu3dZ = RawZ();
    GlobalPos.SetXYZ(Old_Clu3dX / tmpE, Old_Clu3dY / tmpE, Old_Clu3dZ / tmpE);
    RawGlobalPos.SetXYZ(Old_Clu3dX / tmpE, Old_Clu3dY / tmpE, Old_Clu3dZ / tmpE);

    float Old_Clu3dLocalX = LocalX();
    float Old_Clu3dLocalY = LocalY();
    float Old_Clu3dLocalZ = LocalZ();
    LocalPos.SetXYZ(Old_Clu3dLocalX / tmpE, Old_Clu3dLocalY / tmpE, Old_Clu3dLocalZ / tmpE);
}

void FCluster3D::RecRawInfoByClu2D()
{
    RawE = 0;
    RawGlobalPos.SetXYZ(0, 0, 0); // 清空坐标；
    for (auto Clu2D : v_Cluster2D)
    {
        RawE += Clu2D->RawEnergy();
        // std::cout << "Clu2D->Cluster_Cell" << Clu2D->Cluster_Cell.size() << std::endl;
        for (auto v_CellSeq : Clu2D->Cluster_Cell)
        {
            int CellType = v_CellSeq.first;
            auto v_Cell = v_CellSeq.second;
            for (auto Cell : v_Cell)
            {
                // Cell->AddMotherClu3D(this);
                float CellE = Cell->GetEnergy(this);
                float CellX = Cell->X();
                float CellY = Cell->Y();
                float CellZ = Cell->Z();
                float Old_Clu3dX = RawX();
                float Old_Clu3dY = RawY();
                float Old_Clu3dZ = RawZ();
                float New_Clu3dX = Old_Clu3dX + CellX * CellE;
                float New_Clu3dY = Old_Clu3dY + CellY * CellE;
                float New_Clu3dZ = Old_Clu3dZ + CellZ * CellE;
                RawGlobalPos.SetXYZ(New_Clu3dX, New_Clu3dY, New_Clu3dZ);
            }
        }
    }
    float Old_Clu3dX = RawX();
    float Old_Clu3dY = RawY();
    float Old_Clu3dZ = RawZ();
    RawGlobalPos.SetXYZ(Old_Clu3dX / RawE, Old_Clu3dY / RawE, Old_Clu3dZ / RawE);
}



void FCluster3D::CalCellFraction()
{
    // Wating..............
}

void FCluster3D::ECorr()
{
    spdlog::get(LOG_NAME)->info("RawE : {}", RawE);
    if (RawE == 0)
    {
        energy = 0;
    }
    else
    {
        if (Parameters::Instance()->RatioECor == 1 && Mixture_Calo::get_instance()->CluFlow != 3)
        {

            // std::cout << "tmpCorE" << tmpCorE << std::endl;
            if (v_Cluster2D.size() == 2)
            {
                TF1 *CluECaliFun = f_region->CluECaliFun;
                TF1 *CluECor2 = f_region->CluECor2Fun;
                // std::cout << "CluECor2 " << CluECor2 << std::endl;
                float X2;
                if (v_Cluster2D.at(1)->RawEnergy() != 0)

                    X2 = v_Cluster2D.at(0)->RawEnergy() / v_Cluster2D.at(1)->RawEnergy();
                else
                    X2 = 4;
                float DeltaE_E2 = CluECaliFun->Eval(X2);
                // std::cout << "DeltaE_E2 " << DeltaE_E2 << std::endl;
                tmpCorE = RawE / (1 - DeltaE_E2); // to do list

                float EGeV = tmpCorE / 1000;
                float DeltaE_E = CluECor2->Eval(EGeV);

                energy = tmpCorE / (1 - DeltaE_E);
            }
            else
            {
                spdlog::get(ERROR_NAME)->error("In region {}, layer number of Cluster is not 2, can not do layer E ratio ECor", f_region->GlobalId);
                exit(0);
            }
        }
        else
        {
            TF1 *CluECaliFun = f_region->CluECaliFun;

            float EGeV = RawE / 1000;
            float DeltaE_E = CluECaliFun->Eval(EGeV);

            float tmpE1 = RawE / (1 - DeltaE_E);
            tmpCorE = tmpE1;
            energy = tmpE1;
        }
        // else if (Mixture_Calo::get_instance()->CluFlow == 2)
        // {
        //     RawE = 0;
        //     spdlog::get(LOG_NAME)->info("E Correct for Clu2D");

        //     for (auto Clu2D : v_Cluster2D)
        //     {
        //         Clu2D->ECorrection();
        //         RawE += Clu2D->Energy();
        //     }
        //     TF1 *CluECaliFun = f_region->CluECaliFun;
        //     float EGeV = RawE / 1000;
        //     float DeltaE_E = CluECaliFun->Eval(EGeV);
        //     energy = RawE / (1 + DeltaE_E); // to do list
        // }
    }
    AfterECor = 1;
    spdlog::get(LOG_NAME)->info("Correct E : {}", energy);
}

void FCluster3D::SCorrection()
{
    if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 1)
    {
        spdlog::get(LOG_NAME)->info("S Correct for Clu2D");

        for (auto Clu2D : v_Cluster2D)
        {
            Clu2D->SCorrection();
        }
    }
    else if (Mixture_Calo::get_instance()->CluFlow == 3)
    {

        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "UnCorrect X : {}", RawX());
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "UnCorrect Y : {}", RawY());
        float cellXsize = Seed3D->Dx();
        float cellYsize = Seed3D->Dy();
        if (abs(RawX() - Seed3D->X()) < 1e-3)
        {
            SetX(Seed3D->X());
        }
        else
        {
            float InputX = (RawX() - Seed3D->X()) / cellXsize;
            TF1 *XSCorFun;
            if (XType == 0)
            {
                XSCorFun = f_region->CluType0SCorX;
            }
            else if (XType == 1)
            {
                XSCorFun = f_region->CluTypeP1SCorX;
            }
            else if (XType == -1)
            {
                XSCorFun = f_region->CluTypeM1SCorX;
            }
            else if (XType == 2)
            {
                XSCorFun = f_region->CluTypeP2SCorX;
            }
            else if (XType == -2)
            {
                XSCorFun = f_region->CluTypeM2SCorX;
            }
            else if (XType == 3)
            {
                XSCorFun = f_region->CluTypeP3SCorX;
            }
            else if (XType == -3)
            {
                XSCorFun = f_region->CluTypeM3SCorX;
            }
            else
            {
                spdlog::get(LOG_NAME)->error("Error XType {}", XType);
                exit(1);
            }
            float OutputX = XSCorFun->Eval(InputX);
            float NewX = OutputX * cellXsize + Seed3D->X();
            SetX(NewX);
        }
        if (abs(RawY() - Seed3D->Y()) < 1e-3)
        {
            SetY(Seed3D->Y());
        }
        else
        {
            float InputY = (RawY() - Seed3D->Y()) / cellYsize;

            TF1 *YSCorFun;

            if (YType == 0)
            {
                YSCorFun = f_region->CluType0SCorY;
            }
            else if (YType == 1)
            {
                YSCorFun = f_region->CluTypeP1SCorY;
            }
            else if (YType == 2)
            {
                YSCorFun = f_region->CluTypeP2SCorY;
            }
            else if (YType == 3)
            {
                YSCorFun = f_region->CluTypeP3SCorY;
            }
            else if (YType == -1)
            {
                YSCorFun = f_region->CluTypeM1SCorY;
            }
            else if (YType == -2)
            {
                YSCorFun = f_region->CluTypeM2SCorY;
            }
            else if (YType == -3)
            {
                YSCorFun = f_region->CluTypeM3SCorY;
            }
            else
            {
                spdlog::get(LOG_NAME)->error("Error YType {}", YType);
                exit(1);
            }

            float OutputY = YSCorFun->Eval(InputY);

            float NewY = OutputY * cellYsize + Seed3D->Y();

            SetY(NewY);
        }
        AfterSCor = 1;
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct X : {}", X());
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct Y : {}", Y());
    }
}

void FCluster3D::LCorrection(int nPV)
{
    if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 1)
    {
        spdlog::get(LOG_NAME)->info("L Correct for Clu2D");
        for (auto Clu2D : v_Cluster2D)
        {
            Clu2D->LCorrection();
        }
    }
    else if (Mixture_Calo::get_instance()->CluFlow == 3)
    {
        TF1 *LCorrFun = f_region->LCorrFun;
        float e = energy / 1000;
        float ShowerDepth;
        ShowerDepth = LCorrFun->Eval(e);
        // std::cout << "ShowerDepth " << ShowerDepth << std::endl;
        float FrontFace = f_region->FrontFace;
        float PositionAtVertexX;
        float PositionAtVertexY;
        float PositionAtVertexZ;
        if (nPV == -1)
        {
            PositionAtVertexX = 0;
            PositionAtVertexY = 0;
            PositionAtVertexZ = -12836.;
        }
        else
        {
            PositionAtVertexZ = Mixture_Calo::get_instance()->GetPrimaryZ(event, nPV);
            PositionAtVertexX = Mixture_Calo::get_instance()->GetPrimaryX(event, nPV);
            PositionAtVertexY = Mixture_Calo::get_instance()->GetPrimaryY(event, nPV);
        }

        float ThetaX = atan((X() - PositionAtVertexX) / (RawZ() - PositionAtVertexZ));
        float ThetaY = atan((Y() - PositionAtVertexY) / (RawZ() - PositionAtVertexZ));
        float NewAx = fabs(ThetaX) + fabs(f_region->ax) / 180. * TMath::Pi();
        float NewAy = fabs(ThetaY) + fabs(f_region->ay) / 180 * TMath::Pi();
        float TmpDeltaZ = ShowerDepth / sqrt(1 + pow(tan(NewAx), 2) + pow(tan(NewAy), 2));
        // std::cout << "TmpDeltaZ " << TmpDeltaZ << std::endl;
        // std::cout << "FrontFace " << FrontFace << std::endl;
        float TmpLocalZ = TmpDeltaZ + FrontFace;
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "LocalX : {} , LocalY : {}", LocalX(), LocalY());
        FPoint GlobalPoint = f_region->Local2Global(LocalX(), LocalY(), TmpLocalZ);
        SetZ(GlobalPoint.Z() + f_module->Z());
        AfterLCor = 1;
        SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Correct Z : {}", Z());
    }
}

void FCluster3D::CorrectionPos(int nPV)
{
    spdlog::get(LOG_NAME)->info("L Correct for Clu3D");
    LCorrection();
    spdlog::get(LOG_NAME)->info("S Correct for Clu3D");
    SCorrection();

    if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 1)
    {
        float NewX = 0;
        float NewY = 0;
        float NewZ = 0;
        float EGeV = energy / 1000;
        float AccumXWeight = 0;
        float AccumYWeight = 0;
        float FrontFaceZ = f_region->f_calo->PVZShift + 12620;
        float PositionAtVertexX;
        float PositionAtVertexY;
        float PositionAtVertexZ;
        if (nPV == -1)
        {
            PositionAtVertexX = 0;
            PositionAtVertexY = 0;
            PositionAtVertexZ = -12836.;
        }
        else
        {
            PositionAtVertexZ = Mixture_Calo::get_instance()->GetPrimaryZ(event, nPV);
            PositionAtVertexX = Mixture_Calo::get_instance()->GetPrimaryX(event, nPV);
            PositionAtVertexY = Mixture_Calo::get_instance()->GetPrimaryY(event, nPV);
        }
        
        int layerCount = 0;
        for (auto Layer : f_region->Layers)
        {
            float tmpRes = 0;
            float XWeight;
            float YWeight;
            if (f_region->CombineEnergyPoint2R)
            {
                XWeight = 1;
                YWeight = 1;
            }
            else
            {
                TF1 *XPosRes = Layer->PosXResFun;
                TF1 *YPosRes = Layer->PosYResFun;
                tmpRes = XPosRes->Eval(EGeV);
                XWeight = 1 / pow(tmpRes, 2);

                tmpRes = YPosRes->Eval(EGeV);
                YWeight = 1 / pow(tmpRes, 2);
            }
            // std::cout << "layerCount " << layerCount << std::endl;
            FCluster2D *CurrentClu2D = v_Cluster2D.at(layerCount);
            // if (CurrentClu2D->RawEnergy() < CurrentClu2D->LayerId() * 40 + 20) // Only valid for Run5_baseline
            // {
            //     XWeight = 0;
            //     YWeight = 0;
            // }
            float LayerX = CurrentClu2D->X();
            float LayerY = CurrentClu2D->Y();
            float LayerZ = CurrentClu2D->Z();
            AccumXWeight += XWeight;
            AccumYWeight += YWeight;
            // std::cout << "(LayerZ - PVZ) " << (LayerZ - PVZ) << std::endl;
            NewX += (((FrontFaceZ - PositionAtVertexZ) / (LayerZ - PositionAtVertexZ)) * (LayerX - PositionAtVertexX) + PositionAtVertexX) * XWeight;
            NewY += (((FrontFaceZ - PositionAtVertexZ) / (LayerZ - PositionAtVertexZ)) * (LayerY - PositionAtVertexY) + PositionAtVertexY) * YWeight;

            layerCount++;
        }
        // std::cout << "NewX " << NewX << " AccumXWeight " << AccumXWeight << std::endl;
        NewX /= AccumXWeight;
        NewY /= AccumYWeight;
        NewZ = FrontFaceZ;
        GlobalPos.SetXYZ(NewX, NewY, NewZ);
        AfterLCor = 1;
        AfterSCor = 1;
    }

    spdlog::get(LOG_NAME)->info("Clu3D Correct X : {}", X());
    spdlog::get(LOG_NAME)->info("Clu3D Correct Y : {}", Y());
    spdlog::get(LOG_NAME)->info("Clu3D Correct Z : {}", Z());
    spdlog::get(LOG_NAME)->info("Calculate entry position");
    CalCulateEntryPos();
    if (Parameters::Instance()->InitTruthInfo == 1)
    {
        spdlog::get(LOG_NAME)->info("Calculate infomation to tru signal");
        CalCulateInfo2Tru();
    }
}

float FCluster3D::TCorr()
{
    if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 1)
    {
        time = 0;
        float AccumWeight = 0;
        float EGeV = energy / 1000;
        for (auto Clu2D : v_Cluster2D)
        {

            Clu2D->TCorrection();
        }
        int layerCount = 0;
        for (auto Layer : f_region->Layers)
        {
            TF1 *TResFun = Layer->TResFun;

            // float TShiftPar0 = Layer.LayerTShiftFormula.at(0);
            // float TShiftPar1 = Layer.LayerTShiftFormula.at(1);

            float tmpRes = TResFun->Eval(EGeV);
            // float tmpTShift = TShiftPar0 + TShiftPar1 * log(EGeV);
            float tmpT = v_Cluster2D.at(layerCount)->GetTime();
            float tmpWeight = 1 / pow(tmpRes, 2);
            time += tmpT * tmpWeight;
            AccumWeight += tmpWeight;

            layerCount++;
        }
        time /= AccumWeight;
    }
    else if (Mixture_Calo::get_instance()->CluFlow == 3)
    {
        float tmpT = Seed3D->GetTime(event);
        float EGeV = energy / 1000;
        // std::cout << "test1" << std::endl;
        TF1 *TShiftFun = f_region->TShiftFun;
        // std::cout << "test2" << std::endl;
        float tmpTShift = TShiftFun->Eval(EGeV);

        time = tmpT - (Seed3D->Z() - f_module->Z()) / 299.79 - tmpTShift;
    }
    AfterTCor = 1;
    return time;
}

int FCluster3D::IsMissingXCell()
{
    float Seed3DLocalX = Seed3D->LocalX();
    float Seed3DLocalY = Seed3D->LocalY();
    float Seed3D_Dx = Seed3D->Dx();
    float Seed3D_Dy = Seed3D->Dy();
    float ModuleDx = f_module->Dx();
    float ModuleDy = f_module->Dy();

    float ModuleX = f_module->X();
    float ModuleY = f_module->Y();
    float ModuleZ = f_module->Z();
    FRegion *ThisRegion = f_region;
    float Seed3DDx = ThisRegion->Cell3DSize;
    float Seed3DDy = ThisRegion->Cell3DSize;

    float ModuleXBinWidth = Mixture_Calo::get_instance()->module_map.GetXaxis()->GetBinWidth(1);
    float ModuleYBinWidth = Mixture_Calo::get_instance()->module_map.GetYaxis()->GetBinWidth(1);
    int NextXType_p = f_region->type;
    int NextXType_n = f_region->type;
    if (Seed3DLocalX > ModuleDx / 2 - Seed3DDx)
    {
        NextXType_p = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX + ModuleXBinWidth, ModuleY);
    }
    if (NextXType_p != f_region->type)
    {
        if (NextXType_p == -77)
            return 77;
        else
            return (NextXType_p);
    }
    if (Seed3DLocalX < -ModuleDx / 2 + Seed3DDx)
    {
        NextXType_n = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX - ModuleXBinWidth, ModuleY);
    }

    if (NextXType_n != f_region->type)
    {
        if (NextXType_n == -77)
            return (-77);
        else
            return (-1 * NextXType_n);
    }

    // float p_E = 0, m_E = 0;

    // for (auto CellSeq : Cluster_Cell3D)
    // {
    //     int CellType = CellSeq.first;
    //     auto v_Cell = CellSeq.second;
    //     for (auto Cell : v_Cell)
    //     {

    //         if (Cell->X() > Seed3D->X())
    //         {
    //             p_E += Cell->GetEnergy(event);
    //         }
    //         else if (Cell->X() < Seed3D->X())
    //         {
    //             m_E += Cell->GetEnergy(event);
    //         }
    //     }
    // }
    // if (p_E / m_E >= 10.)
    // {
    //     return 111;
    // }
    // if (m_E / p_E >= 10.)
    // {
    //     return -111;
    // }
    return (0);
}

int FCluster3D::IsMissingYCell() // 首先是否为最边界cell 边界外是否是另一个类型的region
{
    float Seed3DLocalX = Seed3D->LocalX();
    float Seed3DLocalY = Seed3D->LocalY();
    float Seed3D_Dx = Seed3D->Dx();
    float Seed3D_Dy = Seed3D->Dy();
    float ModuleDx = f_module->Dx();
    float ModuleDy = f_module->Dy();

    float ModuleX = f_module->X();
    float ModuleY = f_module->Y();
    float ModuleZ = f_module->Z();
    FRegion *ThisRegion = f_region;
    float Seed3DDx = ThisRegion->Cell3DSize;
    float Seed3DDy = ThisRegion->Cell3DSize;
    float ModuleXBinWidth = Mixture_Calo::get_instance()->module_map.GetXaxis()->GetBinWidth(1);
    float ModuleYBinWidth = Mixture_Calo::get_instance()->module_map.GetYaxis()->GetBinWidth(1);
    int NextYType_p = f_region->type;
    int NextYType_n = f_region->type;
    // std::cout << "Seed2DLocalY " << Seed2DLocalY << " -ModuleDy / 2 " << -ModuleDy / 2 << " Seed2D_Dy " << Seed2D_Dy << std::endl;
    if (Seed3DLocalY > ModuleDy / 2 - Seed3DDy)
    {
        NextYType_p = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX, ModuleY + ModuleYBinWidth);
    }
    if (NextYType_p != f_region->type)
    {
        if (NextYType_p == -77)
            return (77);
        else
            return (NextYType_p);
    }
    if (Seed3DLocalY < -ModuleDy / 2 + Seed3DDy)
    {
        NextYType_n = Mixture_Calo::get_instance()->GetRegionTypeByPos(ModuleX, ModuleY - ModuleYBinWidth);
        // std::cout << "n module " << Mixture_Calo::get_instance()->GetModuleIDByPos(ModuleX, ModuleY - ModuleYBinWidth) << std::endl;
    }

    // std::cout << "NextYType_n " << NextYType_n << std::endl;

    if (NextYType_n != f_region->type)
    {
        if (NextYType_n == -77)
            return (-77);
        else
            return (-1 * NextYType_n);
    }

    // float p_E = 0, m_E = 0;

    // for (auto CellSeq : Cluster_Cell3D)
    // {
    //     int CellType = CellSeq.first;
    //     auto v_Cell = CellSeq.second;
    //     for (auto Cell : v_Cell)
    //     {

    //         if (Cell->Y() > Seed3D->Y())
    //         {
    //             p_E += Cell->GetEnergy(event);
    //         }
    //         else if (Cell->Y() < Seed3D->Y())
    //         {
    //             m_E += Cell->GetEnergy(event);
    //         }
    //     }
    // }
    // if (p_E / m_E >= 10.)
    // {
    //     return 111;
    // }
    // if (m_E / p_E >= 10.)
    // {
    //     return -111;
    // }

    return (0);
}

void FCluster3D::CalCulateEntryPos(int i)
{
    auto Calo = f_region->f_calo;
    float PVX = 0, PVY = 0, PVZ = -12836;
    float FrontFaceZ = ((Mixture_Calo *)Calo)->PVZShift + 12620;
    if (i >= 0)
    {
        PVX = Calo->GetPrimaryX(event, i);
        PVY = Calo->GetPrimaryY(event, i);
        PVZ = Calo->GetPrimaryZ(event, i);
    }
    float entry_x = (((FrontFaceZ - PVZ) / (Z() - PVZ)) * (X() - PVX) + PVX);
    float entry_y = (((FrontFaceZ - PVZ) / (Z() - PVZ)) * (Y() - PVY) + PVY);
    SetEntryPosition(entry_x, entry_y, FrontFaceZ);
    spdlog::get(LOG_NAME)->info("Clu3D entry X : {}", EntryX());
    spdlog::get(LOG_NAME)->info("Clu3D entry Y : {}", EntryY());
    spdlog::get(LOG_NAME)->info("Clu3D entry Z : {}", EntryZ());
}

void FCluster3D::CalCulateInfo2Tru()
{
    auto Calo = f_region->f_calo;
    Dist2Tru.clear();
    DeltaE_E2Tru.clear();
    for (int i = 0; i < Calo->Get_nPV(event); i++)
    {
        float HitX = Calo->GetHitX(event, i);
        float HitY = Calo->GetHitY(event, i);
        float HitZ = Calo->GetHitZ(event, i);
        float PVE = Calo->GetPrimaryE(event, i);
        float tmpDist = sqrt(pow(EntryX() - HitX, 2) + pow(EntryY() - HitY, 2));
        float tmpDeltaE = abs(Energy() - PVE) / PVE;
        Dist2Tru.emplace_back(tmpDist);
        DeltaE_E2Tru.emplace_back(tmpDeltaE);
    }
}

void FCluster3D::InitCluType()
{
    int XPNeiWithSameType = 0;
    int XNNeiWithSameType = 0;
    int YPNeiWithSameType = 0;
    int YNNeiWithSameType = 0;
    int CellCount = 0;
    int XOutOfModule = 0, XOutOfRegionId = 0, YOutOfModule = 0, YOutOfRegionId = 0, XOutOfRegionType = 0, YOutOfRegionType = 0;
    float Mx = f_module->X();
    float My = f_module->Y();
    float Mdx = f_module->Dx();
    float Mdy = f_module->Dy();
    float Seed3DLocalX = Seed3D->LocalX();
    float Seed3DLocalY = Seed3D->LocalY();
    for (auto v_Cell3DSeq : Cluster_Cell3D)
    {
        int Cell3DType = v_Cell3DSeq.first;
        auto v_Cell3D = v_Cell3DSeq.second;
        for (auto Cell3D : v_Cell3D)
        {
            auto Cell3DMod = Cell3D->MotherModule();
            float Cell3DModX = Cell3DMod->X();
            float Cell3DModY = Cell3DMod->Y();

            if (Cell3DType != 3)
            {
                if (Cell3D->LocalX() + Cell3DModX - Mx - Seed3DLocalX > 1e-3)
                {
                    XPNeiWithSameType += 1;
                }
                else if (Cell3D->LocalX() + Cell3DModX - Mx - Seed3DLocalX < -1e-3)
                {
                    XNNeiWithSameType += 1;
                }

                if (Cell3D->LocalY() + Cell3DModY - My - Seed3DLocalY > 1e-3)
                {
                    YPNeiWithSameType += 1;
                }
                else if (Cell3D->LocalY() + Cell3DModY - My - Seed3DLocalY < -1e-3)
                {
                    YNNeiWithSameType += 1;
                }
            }

            if (Cell3DType == 1)
            {
                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfModule = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfModule = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfModule = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfModule = -1;
                }
            }
            else if (Cell3DType == 2)
            {

                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfRegionId = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfRegionId = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfRegionId = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfRegionId = -1;
                }
            }
            else if (Cell3DType == 3)
            {
                if ((Cell3DModX - Mx) > Mdx / 2)
                {
                    XOutOfRegionType = 1;
                }
                else if ((Cell3DModX - Mx) < -Mdx / 2)
                {
                    XOutOfRegionType = -1;
                }
                if ((Cell3DModY - My) > Mdy / 2)
                {
                    YOutOfRegionType = 1;
                }
                else if ((Cell3DModY - My) < -Mdy / 2)
                {
                    YOutOfRegionType = -1;
                }
            }
            CellCount++;
        }
    }

    Type = 0;
    XType = 0;
    YType = 0;
    // //std::cout << "XOutOfRegionId " << XOutOfRegionId << std::endl;
    if (XOutOfModule != 0 || YOutOfModule != 0)
    {
        Type = 1;

        if (XOutOfModule == 1)
        {
            XType = 1;
        }
        else if (XOutOfModule == -1)
        {
            XType = -1;
        }
        else
            XType = 0;

        if (YOutOfModule == 1)
        {
            YType = 1;
        }
        else if (YOutOfModule == -1)
        {
            YType = -1;
        }
        else
            YType = 0;
    }
    if (XOutOfRegionId != 0 || YOutOfRegionId != 0)
    {
        Type = 2;

        if (XOutOfRegionId == 1)
        {
            XType = 2;
        }
        else if (XOutOfRegionId == -1)
        {
            XType = -2;
        }

        if (YOutOfRegionId == 1)
        {
            YType = 2;
        }
        else if (YOutOfRegionId == -1)
        {
            YType = -2;
        }
    }

    if (XOutOfRegionType != 0 || YOutOfRegionType != 0 || CellCount < 9)
    {
        Type = 3;
        if (XOutOfRegionType || CellCount < 9)
        {
            if (XPNeiWithSameType != XNNeiWithSameType)
            {
                if (XPNeiWithSameType <= 1)
                {
                    XType = 3;
                }
                else if (XNNeiWithSameType <= 1)
                {
                    XType = -3;
                }
            }
        }
        if (YOutOfRegionType || CellCount < 9)
        {
            if (YPNeiWithSameType != YNNeiWithSameType)
            {
                if (YPNeiWithSameType <= 1)
                {
                    YType = 3;
                }
                else if (YNNeiWithSameType <= 1)
                {
                    YType = -3;
                }
            }
        }
    }
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu3D type {}", Type);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu3D x type {}", XType);
    SPDLOG_LOGGER_DEBUG(spdlog::get(DEBUG_TRACE_NAME), "Clu3D y type {}", YType);
}

void FCluster3D::AddCell3D(FCell3D *Cell3D)
{
    int Cell3DType = 0;
    auto Cell3DRegion = Cell3D->MotherModule()->f_region;
    if (f_module->GetID() != Cell3D->MotherModule()->GetID())
    {
        if (Cell3DRegion->type == f_region->type)
        {
            if (Cell3DRegion->GlobalId == f_region->GlobalId)
            {
                Cell3DType = 1;
            }
            else
            {
                Cell3DType = 2;
            }
        }
        else
        {
            Cell3DType = 3;
        }
    }
    if (std::find(Cluster_Cell3D[Cell3DType].begin(), Cluster_Cell3D[Cell3DType].end(), Cell3D) == Cluster_Cell3D[Cell3DType].end())
    {
        Cluster_Cell3D[Cell3DType].emplace_back(Cell3D);
        Cell3D->AddMotherClu3D(this);
        CellNumber++;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Already add this Cell3D in this Clu3D");
    }
}

void FCluster3D::AddCell3D(FCell3D *Cell3D, int Cell3DType)
{
    if (std::find(Cluster_Cell3D[Cell3DType].begin(), Cluster_Cell3D[Cell3DType].end(), Cell3D) == Cluster_Cell3D[Cell3DType].end())
    {
        Cluster_Cell3D[Cell3DType].emplace_back(Cell3D);
        Cell3D->AddMotherClu3D(this);
        CellNumber++;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Already add this Cell3D in this Clu3D");
    }
}

void FCluster3D::RemoveCell3D(FCell3D *Cell3D, int Cell3DType)
{
    auto FindPoint = std::find(Cluster_Cell3D[Cell3DType].begin(), Cluster_Cell3D[Cell3DType].end(), Cell3D);
    if (FindPoint != Cluster_Cell3D[Cell3DType].end())
    {
        Cluster_Cell3D[Cell3DType].erase(FindPoint);
        Cell3D->RemoveMotherClu3D(this);
        CellNumber--;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Not have this Cell3D (type {}) in this Clu3D", Cell3DType);
    }
}

void FCluster3D::RemoveCell3D(FCell3D *Cell3D)
{
    int Cell3DType = 0;
    auto Cell3DRegion = Cell3D->MotherModule()->f_region;
    if (f_module->GetID() != Cell3D->MotherModule()->GetID())
    {
        if (Cell3DRegion->type == f_region->type)
        {
            if (Cell3DRegion->GlobalId == f_region->GlobalId)
            {
                Cell3DType = 1;
            }
            else
            {
                Cell3DType = 2;
            }
        }
        else
        {
            Cell3DType = 3;
        }
    }
    auto FindPoint = std::find(Cluster_Cell3D[Cell3DType].begin(), Cluster_Cell3D[Cell3DType].end(), Cell3D);
    if (FindPoint != Cluster_Cell3D[Cell3DType].end())
    {
        Cluster_Cell3D[Cell3DType].erase(FindPoint);
        Cell3D->RemoveMotherClu3D(this);
        CellNumber--;
    }
    else
    {
        spdlog::get(LOG_NAME)->warn("Not have this Cell3D (type {}) in this Clu3D", Cell3DType);
    }
}

FCluster3D::FCluster3D(FCell3D *i_Seed3D, int i_event)
{
    LocalPos.SetXYZ(0, 0, 0);
    GlobalPos.SetXYZ(0, 0, 0);
    RawGlobalPos.SetXYZ(0, 0, 0);
    time = 0;
    CellNumber = 1;
    event = i_event;
    Seed3D = i_Seed3D;
    f_module = i_Seed3D->MotherModule();
    f_region = f_module->f_region;
    Seed3Det = i_Seed3D->GetEt(event);
    Cluster_Cell3D[0].emplace_back(i_Seed3D);
    i_Seed3D->AddMotherClu3D(this);
}

float FCluster3D::GetERes()
{
    return f_region->EResFun->Eval(Energy() / 1000.);
}

float FCluster3D::GetTRes()
{
    return f_region->TResFun->Eval(Energy() / 1000.);
}

float FCluster3D::GetSeedE()
{
    if (f_region->f_calo->CluFlow == 1 || f_region->f_calo->CluFlow == 2)
    {
        float tmpSeedE = 0;
        for (auto Clu2D : v_Cluster2D)
        {
            tmpSeedE += Clu2D->GetSeedE();
        }
        return tmpSeedE;
    }
    else
    {
        return Seed3D->GetEnergy(this);
    }
}