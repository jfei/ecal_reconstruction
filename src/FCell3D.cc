#include "FCell3D.hh"
#include <stdlib.h>
#include "FLog.hh"
#include "FRegion.hh"
#include "FModule.hh"
#include "CellEventInfo.hh"
#include "FCluster3D.hh"

int FCell3D::IsLocalMax(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (EventInfo.at(event)->IsLocalMax() >= 0)
    {
        return EventInfo.at(event)->IsLocalMax();
    }
    else
    {
        float currentEnergy = GetEnergy(event);
        if (currentEnergy == 0)
        {
            EventInfo.at(event)->SetLocalMaxStatus(0);
            return 0;
        }

        for (auto v_NeiSeq : Neighbours)
        {
            int NeiType = v_NeiSeq.first;
            auto v_Nei = v_NeiSeq.second;

            for (auto Nei : v_Nei)
            {
                if (currentEnergy < Nei->GetEnergy(event))
                {

                    EventInfo.at(event)->SetLocalMaxStatus(0);
                    return 0;
                }
            }
        }

        EventInfo.at(event)->SetLocalMaxStatus(1);
        return 1;
    }
}

int FCell3D::IsLocalMax_Cross(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (EventInfo.at(event)->IsLocalMax_Cross() >= 0)
    {
        return EventInfo.at(event)->IsLocalMax_Cross();
    }
    else
    {
        float currentEnergy = GetEnergy(event);
        if (currentEnergy == 0)
        {
            EventInfo.at(event)->SetLocalMax_Cross(0);
            return 0;
        }

        for (auto v_NeiSeq : CrossNeib)
        {
            int NeiType = v_NeiSeq.first;
            auto v_Nei = v_NeiSeq.second;

            for (auto Nei : v_Nei)
            {
                if (currentEnergy < Nei->GetEnergy(event))
                {

                    EventInfo.at(event)->SetLocalMax_Cross(0);
                    return 0;
                }
            }
        }

        EventInfo.at(event)->SetLocalMax_Cross(1);
        return 1;
    }
}

int FCell3D::IsLocalMax_Fork(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (EventInfo.at(event)->IsLocalMax_Fork() >= 0)
    {
        return EventInfo.at(event)->IsLocalMax_Fork();
    }
    else
    {
        float currentEnergy = GetEnergy(event);
        if (currentEnergy == 0)
        {
            EventInfo.at(event)->SetLocalMax_Fork(0);
            return 0;
        }

        for (auto v_NeiSeq : ForkNeib)
        {
            int NeiType = v_NeiSeq.first;
            auto v_Nei = v_NeiSeq.second;

            for (auto Nei : v_Nei)
            {
                if (currentEnergy < Nei->GetEnergy(event))
                {

                    EventInfo.at(event)->SetLocalMax_Fork(0);
                    return 0;
                }
            }
        }

        EventInfo.at(event)->SetLocalMax_Fork(1);
        return 1;
    }
}

void FCell3D::FindNeighbours()
{
    Neighbours.clear();
    float Mx = MotherModule()->X();
    float My = MotherModule()->Y();
    float Mz = MotherModule()->Z();
    float Mdx = MotherModule()->dx;
    float Mdy = MotherModule()->dy;
    FRegion *MotherRegion = MotherModule()->f_region;
    int regionType = MotherRegion->type;
    int regionId = MotherRegion->GlobalId;

    for (auto cell : MotherModule()->v_Cell3D)
    {
        float tmpCellLocalX = cell->LocalX();
        float tmpCellLocalY = cell->LocalY();
        float tmpCellSize = cell->Dx();
        float tmpCellThickness = cell->Dz();
        if (abs(tmpCellLocalX - LocalX()) <= 1.2 * Dx() && abs(tmpCellLocalY - LocalY()) <= 1.2 * Dy())
        {
            if (cell != this)
                Neighbours[0].emplace_back(cell);
        }
    }
    for (auto ModuleNb : MotherModule()->Neighbours)
    {
        FRegion *NeiRegion = ModuleNb->f_region;
        int NeiRegionType = NeiRegion->type;
        int NeiRegionId = NeiRegion->GlobalId;

        float NbModuleX = ModuleNb->X();
        float NbModuleY = ModuleNb->Y();
        if (NeiRegionType == regionType)
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
                if (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2)
                {
                    if (NeiRegionId == regionId)
                        Neighbours[1].emplace_back(cell);
                    else
                        Neighbours[2].emplace_back(cell);
                }
            }
        }
        else
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;

                if (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2)
                {
                    Neighbours[3].emplace_back(cell);
                }
            }
        }
    }
}

void FCell3D::FindCrossNeighbours()
{
    CrossNeib.clear();
    float Mx = MotherModule()->X();
    float My = MotherModule()->Y();
    float Mz = MotherModule()->Z();
    float Mdx = MotherModule()->dx;
    float Mdy = MotherModule()->dy;
    FRegion *MotherRegion = MotherModule()->f_region;
    int regionType = MotherRegion->type;
    int regionId = MotherRegion->GlobalId;

    for (auto cell : MotherModule()->v_Cell3D)
    {
        float tmpCellLocalX = cell->LocalX();
        float tmpCellLocalY = cell->LocalY();
        float tmpCellSize = cell->Dx();
        float tmpCellThickness = cell->Dz();
        if ((abs(tmpCellLocalX - LocalX()) <= 1.2 * Dx() && abs(tmpCellLocalY - LocalY()) <= 0.5 * Dy()) || abs(tmpCellLocalX - LocalX()) <= 0.5 * Dx() && abs(tmpCellLocalY - LocalY()) <= 1.2 * Dy())
        {
            if (cell != this)
                CrossNeib[0].emplace_back(cell);
        }
    }
    for (auto ModuleNb : MotherModule()->Neighbours)
    {
        FRegion *NeiRegion = ModuleNb->f_region;
        int NeiRegionType = NeiRegion->type;
        int NeiRegionId = NeiRegion->GlobalId;

        float NbModuleX = ModuleNb->X();
        float NbModuleY = ModuleNb->Y();
        if (NeiRegionType == regionType)
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
                if ((abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 4) || (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 4 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2))
                {
                    if (NeiRegionId == regionId)
                        CrossNeib[1].emplace_back(cell);
                    else
                        CrossNeib[2].emplace_back(cell);
                }
            }
        }
        else
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;

                if ((abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 4) || (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 4 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2))
                {
                    CrossNeib[3].emplace_back(cell);
                }
            }
        }
    }
}

void FCell3D::FindForkNeighbours()
{
    ForkNeib.clear();
    float Mx = MotherModule()->X();
    float My = MotherModule()->Y();
    float Mz = MotherModule()->Z();
    float Mdx = MotherModule()->dx;
    float Mdy = MotherModule()->dy;
    FRegion *MotherRegion = MotherModule()->f_region;
    int regionType = MotherRegion->type;
    int regionId = MotherRegion->GlobalId;

    for (auto cell : MotherModule()->v_Cell3D)
    {
        float tmpCellLocalX = cell->LocalX();
        float tmpCellLocalY = cell->LocalY();
        float tmpCellSize = cell->Dx();
        float tmpCellThickness = cell->Dz();
        if ((abs(tmpCellLocalX - LocalX()) <= 1.2 * Dx() && abs(tmpCellLocalX - LocalX()) > 0.5 * Dx()) && abs(tmpCellLocalY - LocalY()) > 0.5 * Dy() && abs(tmpCellLocalY - LocalY()) <= 1.2 * Dy())
        {
            if (cell != this)
                ForkNeib[0].emplace_back(cell);
        }
    }
    for (auto ModuleNb : MotherModule()->Neighbours)
    {
        FRegion *NeiRegion = ModuleNb->f_region;
        int NeiRegionType = NeiRegion->type;
        int NeiRegionId = NeiRegion->GlobalId;

        float NbModuleX = ModuleNb->X();
        float NbModuleY = ModuleNb->Y();
        if (NeiRegionType == regionType)
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
                if ((abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalX - LocalX()) > (Dx() + cell->Dx()) / 4) && (abs(tmpCellLocalY - LocalY()) > (Dy() + cell->Dy()) / 4 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2))
                {
                    if (NeiRegionId == regionId)
                        ForkNeib[1].emplace_back(cell);
                    else
                        ForkNeib[2].emplace_back(cell);
                }
            }
        }
        else
        {
            for (auto cell : ModuleNb->v_Cell3D)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;

                if ((abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalX - LocalX()) > (Dx() + cell->Dx()) / 4) && (abs(tmpCellLocalY - LocalY()) > (Dy() + cell->Dy()) / 4 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2))
                {
                    ForkNeib[3].emplace_back(cell);
                }
            }
        }
    }
    // std::cout << " ForkNeib[0].size() " << ForkNeib[0].size() << std::endl;
    // std::cout << " ForkNeib[1].size() " << ForkNeib[1].size() << std::endl;
    // std::cout << " ForkNeib[2].size() " << ForkNeib[2].size() << std::endl;
    // std::cout << " ForkNeib[3].size() " << ForkNeib[3].size() << std::endl;
}

FCluster3D *FCell3D::ConstructClu3D(int event)
{
    return f_module->f_region->AlloCell3D_CAllBack(this, event);
}

void FCell3D::RemoveMotherClu3D(FCluster3D *Clu3D)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->RemoveMotherClu3D(Clu3D);
}

void FCell3D::AddMotherClu3D(FCluster3D *Clu3D, float fraction)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->AddMotherClu3D(Clu3D, fraction);
}

FCluster3D *FCell3D::GetMotherClu3D(int event, int i)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    FCluster3D *result = EventInfo.at(event)->GetMotherClu3D(i);

    return result;
}

TH1F *FCell3D::GetTranverseProfile(FCluster3D *Clu3D)
{
    TH1F *TransversalProfile;
    float Seed3DX = Clu3D->Seed3D->X();
    float Seed3DY = Clu3D->Seed3D->Y();
    float tmpDeltaX = Clu3D->X() - Seed3DX;
    float tmpDeltaY = Clu3D->Y() - Seed3DY;
    int xSign = Clu3D->X() > 0 ? 1 : -1;
    int ySign = Clu3D->Y() > 0 ? 1 : -1;

    if (this == Clu3D->Seed3D)
    {
        float angle = atan2(tmpDeltaY, tmpDeltaX) * 180 / TMath::Pi();
        if (tmpDeltaX * xSign + tmpDeltaY * ySign > 0)
        {
            if (abs(angle) < 22.5 || (180 - abs(angle)) < 22.5 || abs(90 - abs(angle)) < 22.5)
            {
                TransversalProfile = Clu3D->f_region->TransversalProfile_Seed_Up_Edge;
            }
            else
            {
                if (tmpDeltaX * xSign > 0 && tmpDeltaY * ySign > 0)
                {
                    TransversalProfile = Clu3D->f_region->TransversalProfile_Seed_Up_UpCorner;
                }
                else
                {
                    TransversalProfile = Clu3D->f_region->TransversalProfile_Seed_Up_Corner;
                }
            }
        }
        else
        {
            if (abs(angle) < 22.5 || (180 - abs(angle)) < 22.5 || abs(90 - abs(angle)) < 22.5)
            {
                TransversalProfile = Clu3D->f_region->TransversalProfile_Seed_Down_Edge;
            }
            else
            {
                TransversalProfile = Clu3D->f_region->TransversalProfile_Seed_Down_Corner;
            }
        }
    }
    else
    {
        if (xSign * (Seed3DX - X()) > Clu3D->Seed3D->Dx() / 2. || ySign * (Seed3DY - Y()) > Clu3D->Seed3D->Dy() / 2.)
        {
            TransversalProfile = Clu3D->f_region->TransversalProfile_Cell_P;
        }
        else
        {
            TransversalProfile = Clu3D->f_region->TransversalProfile_Cell_N;
        }
    }
    return TransversalProfile;
}

float FCell3D::GetTruRatioOfClu3D(FCluster3D *Clu3D)
{
    auto Profile = GetTranverseProfile(Clu3D);
    float Distance2Clu = sqrt(pow(X() - Clu3D->X(), 2) + pow(Y() - Clu3D->Y(), 2));
    float Distance2Clu_Cell = Distance2Clu / Clu3D->Seed3D->Dx();
    float Ratio = Profile->GetBinContent(Profile->FindBin(Distance2Clu_Cell));
    return Ratio;
}