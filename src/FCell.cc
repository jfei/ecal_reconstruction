#include "FCell.hh"
#include "FLayer.hh"
#include "FRegion.hh"
#include "FCluster2D.hh"
#include "FCluster3D.hh"
#include "FCell3D.hh"
#include "FModule.hh"
#include "FCalorimeter.hh"
#include "CellEventInfo.hh"

int FCell::IsLocalMax(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (EventInfo.at(event)->IsLocalMax() >= 0)
    {
        return EventInfo.at(event)->IsLocalMax();
    }
    else
    {
        float currentEnergy = GetEnergy(event);
        if (currentEnergy == 0)
        {
            EventInfo.at(event)->SetLocalMaxStatus(0);
            return 0;
        }
        for (auto v_NeiSeq : Neighbours)
        {
            int NeiType = v_NeiSeq.first;
            auto v_Nei = v_NeiSeq.second;
            for (auto Nei : v_Nei)
            {

                if (currentEnergy < Nei->GetEnergy(event))
                {
                    EventInfo.at(event)->SetLocalMaxStatus(0);
                    return 0;
                }
            }
        }
        EventInfo.at(event)->SetLocalMaxStatus(1);
        return 1;
    }
}

void FCell::FindNeighbours_3D_3_3()
{
    Neighbours.clear();
    float Mx = MotherModule()->X();
    float My = MotherModule()->Y();
    float Mz = MotherModule()->Z();
    float Mdx = MotherModule()->dx;
    float Mdy = MotherModule()->dy;
    FRegion *MotherRegion = MotherModule()->f_region;
    int regionType = MotherRegion->type;
    int regionId = MotherRegion->GlobalId;
    float Cell3DSize = f_Cell3D->Dx();

    float Seed3Ddx = Cell3DSize;
    float Seed3Ddy = Cell3DSize;

    for (auto cell : MotherLayer()->v_cell)
    {
        float tmpCellLocalX = cell->LocalX();
        float tmpCellLocalY = cell->LocalY();
        float tmpCellSize = cell->Dx();
        float tmpCellThickness = cell->Dz();
        if (abs(tmpCellLocalX - LocalX()) <= 1.2 * Seed3Ddx && abs(tmpCellLocalY - LocalY()) <= 1.2 * Seed3Ddy)
        {
            if (cell != this)
                Neighbours[0].emplace_back(cell);
        }
    }
    for (auto ModuleNb : MotherModule()->Neighbours)
    {
        FRegion *NeiRegion = ModuleNb->f_region;
        int NeiRegionType = NeiRegion->type;
        int NeiRegionId = NeiRegion->GlobalId;
        if (NeiRegionType != regionType) // 不是一个类型的不添加
            continue;
        float NbModuleX = ModuleNb->X();
        float NbModuleY = ModuleNb->Y();

        for (auto cell : ModuleNb->v_layer.at(layer_id)->v_cell)
        {
            float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
            float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
            if (abs(tmpCellLocalX - LocalX()) <= 1.7 * Seed3Ddx && abs(tmpCellLocalY - LocalY()) <= 1.7 * Seed3Ddy)
            {
                if (NeiRegionId == regionId)
                    Neighbours[1].emplace_back(cell);
                else
                    Neighbours[2].emplace_back(cell);
            }
        }
    }
}

void FCell::FindNeighbours()
{
    Neighbours.clear();
    float Mx = MotherModule()->X();
    float My = MotherModule()->Y();
    float Mz = MotherModule()->Z();
    float Mdx = MotherModule()->dx;
    float Mdy = MotherModule()->dy;
    FRegion *MotherRegion = MotherModule()->f_region;
    int regionType = MotherRegion->type;
    int regionId = MotherRegion->GlobalId;

    for (auto cell : MotherLayer()->v_cell)
    {
        float tmpCellLocalX = cell->LocalX();
        float tmpCellLocalY = cell->LocalY();
        float tmpCellSize = cell->Dx();
        float tmpCellThickness = cell->Dz();
        if (abs(tmpCellLocalX - LocalX()) <= 1.2 * Dx() && abs(tmpCellLocalY - LocalY()) <= 1.2 * Dy())
        {
            if (cell != this)
                Neighbours[0].emplace_back(cell);
        }
    }
    for (auto ModuleNb : MotherModule()->Neighbours)
    {
        FRegion *NeiRegion = ModuleNb->f_region;
        int NeiRegionType = NeiRegion->type;
        int NeiRegionId = NeiRegion->GlobalId;
        float NbModuleX = ModuleNb->X();
        float NbModuleY = ModuleNb->Y();
        if (NeiRegionType == regionType) // 同一个region类型
        {
            // std::cout << "ModuleNb->v_layer.size() " << ModuleNb->v_layer.size() << std::endl;
            // std::cout << "layer_id " << layer_id << std::endl;
            for (auto cell : ModuleNb->v_layer.at(layer_id)->v_cell)
            {
                float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
                if (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2)
                {
                    if (NeiRegionId == regionId)
                        Neighbours[1].emplace_back(cell);
                    else
                        Neighbours[2].emplace_back(cell);
                }
            }
        }
        else
        {
            if (ModuleNb->v_layer.size() == MotherModule()->v_layer.size())
            {
                for (auto cell : ModuleNb->v_layer.at(layer_id)->v_cell)
                {
                    float tmpCellLocalX = cell->LocalX() + NbModuleX - Mx;
                    float tmpCellLocalY = cell->LocalY() + NbModuleY - My;
                    if (abs(tmpCellLocalX - LocalX()) <= (Dx() + cell->Dx()) / 2 + 12.2 && abs(tmpCellLocalY - LocalY()) <= (Dy() + cell->Dy()) / 2 + 12.2)
                    {
                        Neighbours[3].emplace_back(cell);
                    }
                }
            }
        }
    }
}

uint64 FCell::GetGlobalId()
{
    return Global_id;
}

uint64 FCell::CalculateGlobalId(int region_id, int module_id, int localCell_id, int layer_id)
{
    return (uint64)region_id << 56 | (uint64)layer_id << 48 | (uint64)module_id << 32 | (uint64)localCell_id;
}

float FCell::GetEnergy(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {

        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetEnergy();
    if (result < -7000)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        std::cout << "Cell Energy (" << result << ") seem not initial, result may be wrong, please check" << std::endl;
    }
    return result;
}

float FCell::GetEnergy(FCluster3D *Clu3D)
{
    // std::cout << "Clu3D " << Clu3D << std::endl;
    int event = Clu3D->event;
    // std::cout << "event " << event << std::endl;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (this->BelongTo(Clu3D))
        return EventInfo.at(event)->GetE4MotherClu3D(Clu3D);

    return EventInfo.at(event)->GetEnergy();
}

float FCell::GetEnergy(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (this->BelongTo(Clu2D))
        return EventInfo.at(event)->GetE4MotherClu2D(Clu2D);

    return EventInfo.at(event)->GetEnergy();
}

float FCell::GetHistoryEnergy(FCluster3D *Clu3D)
{
    // std::cout << "Clu3D " << Clu3D << std::endl;
    int event = Clu3D->event;
    // std::cout << "event " << event << std::endl;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (Clu3D->f_Clu3D != NULL && this->BelongTo(Clu3D->f_Clu3D))
        return EventInfo.at(event)->GetE4MotherClu3D(Clu3D->f_Clu3D);

    return EventInfo.at(event)->GetEnergy();
}

float FCell::GetHistoryEnergy(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (Clu2D->f_Clu2D != NULL && this->BelongTo(Clu2D->f_Clu2D))
        return EventInfo.at(event)->GetE4MotherClu2D(Clu2D->f_Clu2D);

    return EventInfo.at(event)->GetEnergy();
}

float FCell::GetFraction(FCluster3D *Clu3D)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (this->BelongTo(Clu3D))
    {
        return EventInfo.at(event)->GetFraction4MotherClu3D(Clu3D);
    }

    return 1;
}

float FCell::GetFraction(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (this->BelongTo(Clu2D))
    {
        return EventInfo.at(event)->GetFraction4MotherClu2D(Clu2D);
    }

    return 1;
}

float FCell::GetHistoryFraction(FCluster3D *Clu3D)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }

    if (Clu3D->f_Clu3D != NULL && this->BelongTo(Clu3D->f_Clu3D))
    {
        return EventInfo.at(event)->GetFraction4MotherClu3D(Clu3D->f_Clu3D);
    }

    return 1;
}

float FCell::GetHistoryFraction(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    if (Clu2D->f_Clu2D != NULL && this->BelongTo(Clu2D->f_Clu2D))
    {
        return EventInfo.at(event)->GetFraction4MotherClu2D(Clu2D->f_Clu2D);
    }

    return 1;
}

float FCell::GetRawSignal(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetRawSignal();
    if (result < -7000)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Cell Raw Signal ({}) seem not initial, result may be wrong, please check", result);
    }
    return result;
}

float FCell::GetTime(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetTime();
    if (result < -7000)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Cell Time({})seem not initial, result may be wrong, please check", result);
    }
    return result;
}

float FCell::GetRawTime(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetRawTime();
    if (result < -7000)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Cell Raw Time({}) seem not initial, result may be wrong, please check", result);
    }
    return result;
}

float FCell::GetZ3D(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetZ3D();
    if (result < -INFINITY)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Z3D({}) seem not initial, result may be wrong, please check", result);
    }
    return result;
}

float FCell::GetEt(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    float result = EventInfo.at(event)->GetEt();
    if (result < 0)
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Cell Et seem not initial {}, result may be wrong, please check", result);
    }
    return result;
}

FCluster2D *FCell::GetMotherClu2D(int event, int i)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    FCluster2D *result = EventInfo.at(event)->GetMotherClu2D(i);

    return result;
}

int FCell::GetMotherClu3DNum(int event)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    int result = EventInfo.at(event)->GetMotherClu3DNum();

    return result;
}

void FCell::RemoveMotherClu2D(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->RemoveMotherClu2D(Clu2D);
}

void FCell::AddMotherClu2D(FCluster2D *Clu2D, float fraction)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->AddMotherClu2D(Clu2D, fraction);
}

void FCell::SetRawTime(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetRawTime(input);
}

void FCell::SetRawSignal(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetRawSignal(input);
}

void FCell::SetEnergy(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetEnergy(input);
}

void FCell::SetEnergy(FCluster3D *Clu3D, float input)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetE4MotherClu3D(Clu3D, input);
}

void FCell::SetEnergy(FCluster2D *Clu2D, float input)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetE4MotherClu2D(Clu2D, input);
}

void FCell::SetFraction(FCluster3D *Clu3D, float input)
{
    int event = Clu3D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetFraction4MotherClu3D(Clu3D, input);
}

void FCell::SetFraction(FCluster2D *Clu2D, float input)
{
    int event = Clu2D->event;
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetFraction4MotherClu2D(Clu2D, input);
}

void FCell::SetTime(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetTime(input);
}

void FCell::SetTotEnDep(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetTotEnDep(input);
}

void FCell::SetZ3D(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetZ3D(input);
}

void FCell::SetEt(int event, float input)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->SetEt(input);
}

void FCell::AccumulateTotEnDep(int event, float increment)
{
    if (event >= EventInfo.size())
    {
        spdlog::get(ERROR_NAME)->error("Error !!!!!!!!");
        spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfo.size());
        exit(1);
    }
    if (event != EventInfo.at(event)->GetEventID())
    {
        spdlog::get(LOG_NAME)->warn("WARNING !!!!!!!!");
        spdlog::get(LOG_NAME)->warn("Event serial conflict,{}!={} ,result may be wrong, please check", event, EventInfo.at(event)->GetEventID());
    }
    EventInfo.at(event)->AccumulateTotEnDep(increment);
}

void FCell::InitEventInfo()
{
    for (int loop = 0; loop < f_module->f_calo->max_Eventseq; loop++)
    {
        FCellEventInfo *tmpEventInfo = new FCellEventInfo(loop);

        EventInfo.emplace_back(tmpEventInfo);
    }
}

bool FCell::BelongTo(FCluster3D *Clu3D)
{
    int event = Clu3D->event;
    return EventInfo.at(event)->BelongTo(Clu3D);
}

bool FCell::BelongTo(FCluster2D *Clu2D)
{
    int event = Clu2D->event;
    return EventInfo.at(event)->BelongTo(Clu2D);
}

TH1F *FCell::GetTranverseProfile(FCluster2D *Clu2D)
{
    TH1F *TransversalProfile;
    float Seed2DX = Clu2D->Seed2D->X();
    float Seed2DY = Clu2D->Seed2D->Y();
    float tmpDeltaX = Clu2D->X() - Seed2DX;
    float tmpDeltaY = Clu2D->Y() - Seed2DY;
    int xSign = Clu2D->X() > 0 ? 1 : -1;
    int ySign = Clu2D->Y() > 0 ? 1 : -1;

    if (this == Clu2D->Seed2D)
    {
        float angle = atan2(tmpDeltaY, tmpDeltaX) * 180 / TMath::Pi();
        if (tmpDeltaX * xSign + tmpDeltaY * ySign > 0)
        {
            if (abs(angle) < 22.5 || (180 - abs(angle)) < 22.5 || abs(90 - abs(angle)) < 22.5)
            {
                TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Seed_Up_Edge;
            }
            else
            {
                if (tmpDeltaX * xSign > 0 && tmpDeltaY * ySign > 0)
                {
                    TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Seed_Up_UpCorner;
                }
                else
                {
                    TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Seed_Up_Corner;
                }
            }
        }
        else
        {
            if (abs(angle) < 22.5 || (180 - abs(angle)) < 22.5 || abs(90 - abs(angle)) < 22.5)
            {
                TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Seed_Down_Edge;
            }
            else
            {
                TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Seed_Down_Corner;
            }
        }
    }
    else
    {
        if (xSign * (Seed2DX - X()) > Clu2D->Seed2D->Dx() / 2. || ySign * (Seed2DY - Y()) > Clu2D->Seed2D->Dy() / 2.)
        {
            // std::cout << "1" << std::endl;
            TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Cell_P;
        }
        else
        {
            // std::cout << "2" << std::endl;
            TransversalProfile = Clu2D->f_region->Layers.at(Clu2D->LayerId())->TransversalProfile_Cell_N;
        }
    }
    return TransversalProfile;
}

FCluster2D *FCell::ConstructClu2D(int event)
{
    return f_module->f_region->Layers.at(layer_id)->AlloCell_CAllBack(this, event);
}

float FCell::GetTruRatioOfClu2D(FCluster2D *Clu2D)
{
    auto Profile = GetTranverseProfile(Clu2D);
    float Distance2Clu = sqrt(pow(X() - Clu2D->X(), 2) + pow(Y() - Clu2D->Y(), 2));
    float Distance2Clu_Cell = Distance2Clu / Clu2D->Seed2D->Dx();
    float Ratio = Profile->GetBinContent(Profile->FindBin(Distance2Clu_Cell));
    return Ratio;
}