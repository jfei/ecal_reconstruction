#include "CreateTree.hh"
#include "Mixture_Calo.hh"
#include "FModule.hh"
#include <algorithm>
#include "FLog.hh"
#include "FLayer.hh"
#include "FRegion.hh"

using namespace std;

CreateTree *CreateTree::fInstance = NULL;

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

CreateTree::CreateTree(TString name)
{
  if (fInstance)
  {
    return;
  }

  this->fInstance = this;
  this->fname = name;

  std::stringstream tmp_name;
  std::string tmp_name2;
  Parameters = new TTree("Parameters", "Parameters");
  Parameters->Branch("Run", &Run, "Run/I");
  Parameters->Branch("Event", &Event, "Event/I");
  Parameters->Branch("CluFlow", &CluFlow, "CluFlow/I");
  Parameters->Branch("ComputationTime", &ComputationTime, "ComputationTime/I");
  Parameters->Branch("Seed3DNum", &Seed3DNum, "Seed3DNum/I");
  // Parameters->Branch("NLM_MaxInFork", &NLM_MaxInFork, "NLM_MaxInFork/I");
  // Parameters->Branch("NLM_MaxInCross", &NLM_MaxInCross, "NLM_MaxInCross/I");
  Parameters->Branch("LMNum_3_3", &LMNum_3_3, "LMNum_3_3/I");
  Parameters->Branch("LMNum_Fork", &LMNum_Fork, "LMNum_Fork/I");
  Parameters->Branch("LMNum_Cross", &LMNum_Cross, "LMNum_Cross/I");
  Parameters->Branch("Seed2DNum", &Seed2DNum);

  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveRawInfo)
  {
    int TotRegionLayerNum = Mixture_Calo::get_instance()->TotalLayerNumInAllRegion;
    int TotRegionNum = Mixture_Calo::get_instance()->region_container.size();
    RegionTotEnDep = new float[TotRegionNum];
    RegionTotLayerNum = new int[TotRegionNum];
    RegionEnDepSiTiming = new float[TotRegionNum];
    RegionEnDepSilicon = new float[TotRegionNum];
    RegionSiliconSig = new float[TotRegionNum];
    RegionShowerZ = new float[TotRegionNum];
    RegionShowerX = new float[TotRegionNum];
    RegionShowerY = new float[TotRegionNum];
    RegionFrontFace = new float[TotRegionNum];
    RegionLayerSig = new float[TotRegionLayerNum];
    RegionLayerEnDep = new float[TotRegionLayerNum];
    RegionActiveLayerEnDep = new float[TotRegionLayerNum];
    RegionLayerShowerZ = new float[TotRegionLayerNum];
    RegionLayerShowerX = new float[TotRegionLayerNum];
    RegionLayerShowerY = new float[TotRegionLayerNum];
    RegionLayerFrontFace = new float[TotRegionLayerNum];
    RegionLayerThickness = new float[TotRegionLayerNum];
    RegionLayerPos = new float[TotRegionLayerNum];
  }
  if (Parameters::Instance()->SaveGamma)
  {
    Gamma = new TTree("Gamma", "Gamma");
    Gamma->Branch("Run", &Run, "Run/I");
    Gamma->Branch("Event", &Event, "Event/I");
    Gamma->Branch("Gamma_Type", &Gamma_Type, "Gamma_Type/I");
    Gamma->Branch("Gamma_Tru_Region", &Gamma_Tru_Region, "Gamma_Tru_Region/I");
    Gamma->Branch("Gamma_Tru_RegionType", &Gamma_Tru_RegionType, "Gamma_Tru_RegionType/I");
    Gamma->Branch("Gamma_Region", &Gamma_Region, "Gamma_Region/I");
    Gamma->Branch("Gamma_RegionType", &Gamma_RegionType, "Gamma_RegionType/I");
    Gamma->Branch("Gamma_Tru_px", &Gamma_Tru_px, "Gamma_Tru_px/F");
    Gamma->Branch("Gamma_Tru_py", &Gamma_Tru_py, "Gamma_Tru_py/F");
    Gamma->Branch("Gamma_Tru_pz", &Gamma_Tru_pz, "Gamma_Tru_pz/F");
    Gamma->Branch("Gamma_Tru_E", &Gamma_Tru_E, "Gamma_Tru_E/F");
    Gamma->Branch("Gamma_Tru_T", &Gamma_Tru_T, "Gamma_Tru_T/F");
    Gamma->Branch("Gamma_px", &Gamma_px, "Gamma_px/F");
    Gamma->Branch("Gamma_py", &Gamma_py, "Gamma_py/F");
    Gamma->Branch("Gamma_pz", &Gamma_pz, "Gamma_pz/F");
    Gamma->Branch("Gamma_E", &Gamma_E, "Gamma_E/F");
    Gamma->Branch("Gamma_T", &Gamma_T, "Gamma_T/F");
    Gamma->Branch("Pi0_TruType", &Pi0_TruType, "Pi0_TruType/I");
    Gamma->Branch("HitDist", &HitDist, "HitDist/F");

    SubGamma = new TTree("SubGamma", "SubGamma");
    SubGamma->Branch("Run", &Run, "Run/I");
    SubGamma->Branch("Event", &Event, "Event/I");
    SubGamma->Branch("Gamma_Tru_Region", &Gamma_Tru_Region, "Gamma_Tru_Region/I");
    SubGamma->Branch("Gamma_Tru_RegionType", &Gamma_Tru_RegionType, "Gamma_Tru_RegionType/I");
    SubGamma->Branch("Gamma_Region", &Gamma_Region, "Gamma_Region/I");
    SubGamma->Branch("Gamma_RegionType", &Gamma_RegionType, "Gamma_RegionType/I");
    SubGamma->Branch("Gamma_Tru_px", &Gamma_Tru_px, "Gamma_Tru_px/F");
    SubGamma->Branch("Gamma_Tru_py", &Gamma_Tru_py, "Gamma_Tru_py/F");
    SubGamma->Branch("Gamma_Tru_pz", &Gamma_Tru_pz, "Gamma_Tru_pz/F");
    SubGamma->Branch("Gamma_Tru_E", &Gamma_Tru_E, "Gamma_Tru_E/F");
    SubGamma->Branch("Gamma_Tru_T", &Gamma_Tru_T, "Gamma_Tru_T/F");
    SubGamma->Branch("Gamma_px", &Gamma_px, "Gamma_px/F");
    SubGamma->Branch("Gamma_py", &Gamma_py, "Gamma_py/F");
    SubGamma->Branch("Gamma_pz", &Gamma_pz, "Gamma_pz/F");
    SubGamma->Branch("Gamma_E", &Gamma_E, "Gamma_E/F");
    SubGamma->Branch("Gamma_T", &Gamma_T, "Gamma_T/F");
    SubGamma->Branch("Pi0_TruType", &Pi0_TruType, "Pi0_TruType/I");
    SubGamma->Branch("HitDist", &HitDist, "HitDist/F");
  }
  if (Parameters::Instance()->SavePi0)
  {
    Pi0 = new TTree("Pi0", "Pi0");
    Pi0->Branch("Run", &Run, "Run/I");
    Pi0->Branch("Event", &Event, "Event/I");
    Pi0->Branch("CluFlow", &CluFlow, "CluFlow/I");
    Pi0->Branch("n_PV", &n_PV, "n_PV/I");
    Pi0->Branch("ParticleGun", &ParticleGun, "ParticleGun/I");
    Pi0->Branch("HitDist", &HitDist, "HitDist/F");
    Pi0->Branch("primaryPDGID", &primaryPDGID);
    Pi0->Branch("PositionOnAbsorberX", &PositionOnAbsorberX);
    Pi0->Branch("PositionOnAbsorberY", &PositionOnAbsorberY);
    Pi0->Branch("PositionOnAbsorberZ", &PositionOnAbsorberZ);
    Pi0->Branch("MomentumOnAbsorberX", &MomentumOnAbsorberX);
    Pi0->Branch("MomentumOnAbsorberY", &MomentumOnAbsorberY);
    Pi0->Branch("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
    Pi0->Branch("PositionOnAbsorberT", &PositionOnAbsorberT);
    Pi0->Branch("EnergyOnAbsorber", &EnergyOnAbsorber);
    Pi0->Branch("PositionAtVertexX", &PositionAtVertexX);
    Pi0->Branch("PositionAtVertexY", &PositionAtVertexY);
    Pi0->Branch("PositionAtVertexZ", &PositionAtVertexZ);
    Pi0->Branch("MomentumAtVertexX", &MomentumAtVertexX);
    Pi0->Branch("MomentumAtVertexY", &MomentumAtVertexY);
    Pi0->Branch("MomentumAtVertexZ", &MomentumAtVertexZ);
    Pi0->Branch("TimeAtVertex", &TimeAtVertex);
    Pi0->Branch("EnergyAtVertex", &EnergyAtVertex);
    Pi0->Branch("HitRegion", &HitRegion);
    Pi0->Branch("HitRegionType", &HitRegionType);
    Pi0->Branch("HitModule", &HitModule);
    Pi0->Branch("Pi0_TruType", &Pi0_TruType, "Pi0_TruType/I");
    Pi0->Branch("Pi0_TruM", &Pi0_TruM, "Pi0_TruM/F");
    Pi0->Branch("Pi0_Type", &Pi0_Type, "Pi0_Type/I");
    Pi0->Branch("Pi0_M", &Pi0_M, "Pi0_M/F");
    Pi0->Branch("Pi0_TruM", &Pi0_TruM, "Pi0_TruM/F");
    Pi0->Branch("Pi0_E", &Pi0_E, "Pi0_E/F");
    Pi0->Branch("Pi0_TruE", &Pi0_TruE, "Pi0_TruE/F");
    Pi0->Branch("Pi0_Px", &Pi0_Px, "Pi0_Px/F");
    Pi0->Branch("Pi0_TruPx", &Pi0_TruPx, "Pi0_TruPx/F");
    Pi0->Branch("Pi0_Py", &Pi0_Py, "Pi0_Py/F");
    Pi0->Branch("Pi0_TruPy", &Pi0_TruPy, "Pi0_TruPy/F");
    Pi0->Branch("Pi0_Pz", &Pi0_Pz, "Pi0_Pz/F");
    Pi0->Branch("Pi0_TruPz", &Pi0_TruPz, "Pi0_TruPz/F");
    Pi0->Branch("Pi0_Pt", &Pi0_Pt, "Pi0_Pt/F");
    Pi0->Branch("Pi0_TruPt", &Pi0_TruPt, "Pi0_TruPt/F");
    Pi0->Branch("Pi0_MatchIndex", &Pi0_MatchIndex, "Pi0_MatchIndex/I");
    Pi0->Branch("Pi0_Gamma1_MatchIndex", &Pi0_Gamma1_MatchIndex, "Pi0_Gamma1_MatchIndex/I");
    Pi0->Branch("Pi0_Gamma1_Module", &Pi0_Gamma1_Module, "Pi0_Gamma1_Module/I");
    Pi0->Branch("Pi0_Gamma1_RegionId", &Pi0_Gamma1_RegionId, "Pi0_Gamma1_RegionId/I");
    Pi0->Branch("Pi0_Gamma1_RegionType", &Pi0_Gamma1_RegionType, "Pi0_Gamma1_RegionType/I");
    Pi0->Branch("Pi0_Gamma1_E", &Pi0_Gamma1_E, "Pi0_Gamma1_E/F");
    Pi0->Branch("Pi0_Gamma1_Px", &Pi0_Gamma1_Px, "Pi0_Gamma1_Px/F");
    Pi0->Branch("Pi0_Gamma1_Py", &Pi0_Gamma1_Py, "Pi0_Gamma1_Py/F");
    Pi0->Branch("Pi0_Gamma1_Pz", &Pi0_Gamma1_Pz, "Pi0_Gamma1_Pz/F");
    Pi0->Branch("Pi0_Gamma1_Pt", &Pi0_Gamma1_Pt, "Pi0_Gamma1_Pt/F");
    Pi0->Branch("Pi0_Gamma1_TruModule", &Pi0_Gamma1_TruModule, "Pi0_Gamma1_TruModule/I");
    Pi0->Branch("Pi0_Gamma1_TruRegionId", &Pi0_Gamma1_TruRegionId, "Pi0_Gamma1_TruRegionId/I");
    Pi0->Branch("Pi0_Gamma1_TruRegionType", &Pi0_Gamma1_TruRegionType, "Pi0_Gamma1_TruRegionType/I");
    Pi0->Branch("Pi0_Gamma1_TruE", &Pi0_Gamma1_TruE, "Pi0_Gamma1_TruE/F");
    Pi0->Branch("Pi0_Gamma1_TruPx", &Pi0_Gamma1_TruPx, "Pi0_Gamma1_TruPx/F");
    Pi0->Branch("Pi0_Gamma1_TruPy", &Pi0_Gamma1_TruPy, "Pi0_Gamma1_TruPy/F");
    Pi0->Branch("Pi0_Gamma1_TruPz", &Pi0_Gamma1_TruPz, "Pi0_Gamma1_TruPz/F");
    Pi0->Branch("Pi0_Gamma1_TruPt", &Pi0_Gamma1_TruPt, "Pi0_Gamma1_TruPt/F");
    Pi0->Branch("Pi0_Gamma2_MatchIndex", &Pi0_Gamma2_MatchIndex, "Pi0_Gamma2_MatchIndex/I");
    Pi0->Branch("Pi0_Gamma2_Module", &Pi0_Gamma2_Module, "Pi0_Gamma2_Module/I");
    Pi0->Branch("Pi0_Gamma2_RegionId", &Pi0_Gamma2_RegionId, "Pi0_Gamma2_RegionId/I");
    Pi0->Branch("Pi0_Gamma2_RegionType", &Pi0_Gamma2_RegionType, "Pi0_Gamma2_RegionType/I");
    Pi0->Branch("Pi0_Gamma2_E", &Pi0_Gamma2_E, "Pi0_Gamma2_E/F");
    Pi0->Branch("Pi0_Gamma2_Px", &Pi0_Gamma2_Px, "Pi0_Gamma2_Px/F");
    Pi0->Branch("Pi0_Gamma2_Py", &Pi0_Gamma2_Py, "Pi0_Gamma2_Py/F");
    Pi0->Branch("Pi0_Gamma2_Pz", &Pi0_Gamma2_Pz, "Pi0_Gamma2_Pz/F");
    Pi0->Branch("Pi0_Gamma2_Pt", &Pi0_Gamma2_Pt, "Pi0_Gamma2_Pt/F");
    Pi0->Branch("Pi0_Gamma2_TruModule", &Pi0_Gamma2_TruModule, "Pi0_Gamma2_TruModule/I");
    Pi0->Branch("Pi0_Gamma2_TruRegionId", &Pi0_Gamma2_TruRegionId, "Pi0_Gamma2_TruRegionId/I");
    Pi0->Branch("Pi0_Gamma2_TruRegionType", &Pi0_Gamma2_TruRegionType, "Pi0_Gamma2_TruRegionType/I");
    Pi0->Branch("Pi0_Gamma2_TruE", &Pi0_Gamma2_TruE, "Pi0_Gamma2_TruE/F");
    Pi0->Branch("Pi0_Gamma2_TruPx", &Pi0_Gamma2_TruPx, "Pi0_Gamma2_TruPx/F");
    Pi0->Branch("Pi0_Gamma2_TruPy", &Pi0_Gamma2_TruPy, "Pi0_Gamma2_TruPy/F");
    Pi0->Branch("Pi0_Gamma2_TruPz", &Pi0_Gamma2_TruPz, "Pi0_Gamma2_TruPz/F");
    Pi0->Branch("Pi0_Gamma2_TruPt", &Pi0_Gamma2_TruPt, "Pi0_Gamma2_TruPt/F");
  }

  if (Parameters::Instance()->SavePrimaryInfo)
  {
    PrimaryTree = new TTree("Primary", "Primary");
    PrimaryTree->Branch("Run", &Run, "Run/I");
    PrimaryTree->Branch("Event", &Event, "Event/I");
    PrimaryTree->Branch("CluFlow", &CluFlow, "CluFlow/I");
    PrimaryTree->Branch("n_PV", &n_PV, "n_PV/I");
    PrimaryTree->Branch("ParticleGun", &ParticleGun, "ParticleGun/I");
    PrimaryTree->Branch("HitDist", &HitDist, "HitDist/F");
    PrimaryTree->Branch("primaryPDGID", &primaryPDGID);
    PrimaryTree->Branch("PositionOnAbsorberX", &PositionOnAbsorberX);
    PrimaryTree->Branch("PositionOnAbsorberY", &PositionOnAbsorberY);
    PrimaryTree->Branch("PositionOnAbsorberZ", &PositionOnAbsorberZ);
    PrimaryTree->Branch("MomentumOnAbsorberX", &MomentumOnAbsorberX);
    PrimaryTree->Branch("MomentumOnAbsorberY", &MomentumOnAbsorberY);
    PrimaryTree->Branch("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
    PrimaryTree->Branch("PositionOnAbsorberT", &PositionOnAbsorberT);
    PrimaryTree->Branch("EnergyOnAbsorber", &EnergyOnAbsorber);
    PrimaryTree->Branch("PositionAtVertexX", &PositionAtVertexX);
    PrimaryTree->Branch("PositionAtVertexY", &PositionAtVertexY);
    PrimaryTree->Branch("PositionAtVertexZ", &PositionAtVertexZ);
    PrimaryTree->Branch("MomentumAtVertexX", &MomentumAtVertexX);
    PrimaryTree->Branch("MomentumAtVertexY", &MomentumAtVertexY);
    PrimaryTree->Branch("MomentumAtVertexZ", &MomentumAtVertexZ);
    PrimaryTree->Branch("TimeAtVertex", &TimeAtVertex);
    PrimaryTree->Branch("EnergyAtVertex", &EnergyAtVertex);
    if (Parameters::Instance()->ConstructPi0)
    {
      PrimaryTree->Branch("Pi0_TruType", &Pi0_TruType);
    }
  }
  if (Parameters::Instance()->SaveElectron)
  {
    Electron = new TTree("Electron", "Electron");
    Electron->Branch("n_PV", &n_PV, "n_PV/I");
    Electron->Branch("ParticleGun", &ParticleGun, "ParticleGun/I");
    Electron->Branch("primaryPDGID", &primaryPDGID);
    Electron->Branch("PositionOnAbsorberX", &PositionOnAbsorberX);
    Electron->Branch("PositionOnAbsorberY", &PositionOnAbsorberY);
    Electron->Branch("PositionOnAbsorberZ", &PositionOnAbsorberZ);
    Electron->Branch("MomentumOnAbsorberX", &MomentumOnAbsorberX);
    Electron->Branch("MomentumOnAbsorberY", &MomentumOnAbsorberY);
    Electron->Branch("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
    Electron->Branch("PositionOnAbsorberT", &PositionOnAbsorberT);
    Electron->Branch("EnergyOnAbsorber", &EnergyOnAbsorber);
    Electron->Branch("PositionAtVertexX", &PositionAtVertexX);
    Electron->Branch("PositionAtVertexY", &PositionAtVertexY);
    Electron->Branch("PositionAtVertexZ", &PositionAtVertexZ);
    Electron->Branch("MomentumAtVertexX", &MomentumAtVertexX);
    Electron->Branch("MomentumAtVertexY", &MomentumAtVertexY);
    Electron->Branch("MomentumAtVertexZ", &MomentumAtVertexZ);
    Electron->Branch("TimeAtVertex", &TimeAtVertex);
    Electron->Branch("EnergyAtVertex", &EnergyAtVertex);
    Electron->Branch("HitRegion", &HitRegion);
    Electron->Branch("HitRegionType", &HitRegionType);
    Electron->Branch("HitModule", &HitModule);
  }
  //----------------------------------//
  // ReadoutInfo TREE                      //
  //----------------------------------//
  // saving the all info of Readout in the output file
  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCellInfo)
  {

    ReadoutInfo = new TTree("ReadoutInfo", "ReadoutInfo");
    ReadoutInfo->Branch("Run", &Run, "Run/I");
    ReadoutInfo->Branch("event", &Event, "event/I");
    if (Parameters::Instance()->SaveAll || Parameters::Instance()->SavePrimaryInfo)
    {
      ReadoutInfo->Branch("n_PV", &n_PV, "n_PV/I");
      ReadoutInfo->Branch("ParticleGun", &ParticleGun, "ParticleGun/I");
      ReadoutInfo->Branch("primaryPDGID", &primaryPDGID);
      ReadoutInfo->Branch("PositionOnAbsorberX", &PositionOnAbsorberX);
      ReadoutInfo->Branch("PositionOnAbsorberY", &PositionOnAbsorberY);
      ReadoutInfo->Branch("PositionOnAbsorberZ", &PositionOnAbsorberZ);
      ReadoutInfo->Branch("MomentumOnAbsorberX", &MomentumOnAbsorberX);
      ReadoutInfo->Branch("MomentumOnAbsorberY", &MomentumOnAbsorberY);
      ReadoutInfo->Branch("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
      ReadoutInfo->Branch("PositionOnAbsorberT", &PositionOnAbsorberT);
      ReadoutInfo->Branch("EnergyOnAbsorber", &EnergyOnAbsorber);
      ReadoutInfo->Branch("PositionAtVertexX", &PositionAtVertexX);
      ReadoutInfo->Branch("PositionAtVertexY", &PositionAtVertexY);
      ReadoutInfo->Branch("PositionAtVertexZ", &PositionAtVertexZ);
      ReadoutInfo->Branch("MomentumAtVertexX", &MomentumAtVertexX);
      ReadoutInfo->Branch("MomentumAtVertexY", &MomentumAtVertexY);
      ReadoutInfo->Branch("MomentumAtVertexZ", &MomentumAtVertexZ);
      ReadoutInfo->Branch("TimeAtVertex", &TimeAtVertex);
      ReadoutInfo->Branch("EnergyAtVertex", &EnergyAtVertex);
      ReadoutInfo->Branch("HitRegion", &HitRegion);
      ReadoutInfo->Branch("HitRegionType", &HitRegionType);
      ReadoutInfo->Branch("HitModule", &HitModule);
    }

    for (auto moduleSeq : Mixture_Calo::get_instance()->module_container)
    {
      int CurrentMoudleID = moduleSeq.first;
      FModule *CurrentModule = moduleSeq.second;
      for (auto Layer : CurrentModule->v_layer)
      {
        std::vector<float> tmpSignal;
        std::vector<float> tmpTime;
        std::vector<float> tmpRawTime;
        std::vector<float> tmpEnergy;
        CellRawSignal.emplace_back(tmpSignal);
        CellTime.emplace_back(tmpTime);
        CellEnergy.emplace_back(tmpEnergy);
        CellRawTime.emplace_back(tmpRawTime);
      }
    }
    // std::cout << "CellEnergy.size() : " << CellEnergy.size() << std::endl;
    int tmpCount = 0;
    for (auto moduleSeq : Mixture_Calo::get_instance()->module_container)
    {
      int CurrentMoudleID = moduleSeq.first;
      FModule *CurrentModule = moduleSeq.second;
      for (auto Layer : CurrentModule->v_layer)
      {
        tmp_name.str("");
        tmp_name << "mod" << CurrentMoudleID << "_Layer" << Layer->id << "RawSignal";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(CellRawSignal.at(tmpCount)));

        tmp_name.str("");
        tmp_name << "mod" << CurrentMoudleID << "_Layer" << Layer->id << "Time";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(CellTime.at(tmpCount)));

        tmp_name.str("");
        tmp_name << "mod" << CurrentMoudleID << "_Layer" << Layer->id << "Energy";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(CellEnergy.at(tmpCount)));

        tmp_name.str("");
        tmp_name << "mod" << CurrentMoudleID << "_Layer" << Layer->id << "RawTime";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(CellRawTime.at(tmpCount)));
        tmpCount++;
      }
    }
    /// @Inital vector ////
    /// @param name
    if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveRawInfo)
    {
      ReadoutInfo->Branch("TotEnDep", &TotEnDep, "TotEnDep/F");
      ReadoutInfo->Branch("ShowerZ", &ShowerZ, "ShowerZ/F");
      ReadoutInfo->Branch("ShowerX", &ShowerX, "ShowerX/F");
      ReadoutInfo->Branch("ShowerY", &ShowerY, "ShowerY/F");
      ReadoutInfo->Branch("MaxDepRegion", &MaxDepRegion, "MaxDepRegion/I");

      ////////////////////////

      /// @brief /
      /// @param name
      int RegionCount = 0;
      tmpCount = 0;
      for (auto RegionSeq : Mixture_Calo::get_instance()->region_container)
      {
        int CurrentRegionID = RegionSeq.first;
        FRegion *CurrentRegion = RegionSeq.second;
        //////////////////////////////////
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "TotalEnDep";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionTotEnDep[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "EnDepSiTiming";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionEnDepSiTiming[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "EnDepSilicon";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionEnDepSilicon[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "SiliconSig";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionSiliconSig[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerZ";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionShowerZ[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerX";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionShowerX[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "ShowerY";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionShowerY[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "FrontFace";
        tmp_name2 = tmp_name.str() + "/F";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionFrontFace[RegionCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "TotLayerNum";
        tmp_name2 = tmp_name.str() + "/I";
        ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionTotLayerNum[RegionCount]), tmp_name2.c_str());

        /////////////////////////////////////
        int LayerCount = 0;
        for (auto Layer : CurrentRegion->Layers)
        {
          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "EnDep";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerEnDep[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_ActiveLayer" << LayerCount << "EnDep";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionActiveLayerEnDep[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Sig";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerSig[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerZ";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerShowerZ[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerX";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerShowerX[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerY";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerShowerY[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "FrontFace";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerFrontFace[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Thickness";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerThickness[tmpCount]), tmp_name2.c_str());

          tmp_name.str("");
          tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Pos";
          tmp_name2 = tmp_name.str() + "/F";
          ReadoutInfo->Branch(tmp_name.str().c_str(), &(RegionLayerPos[tmpCount]), tmp_name2.c_str());

          LayerCount++;
          tmpCount++;
        }
        RegionCount++;
      }
    }
  }

  OutTree = new TTree("Tree", "Tree");
  OutTree->Branch("Run", &Run, "Run/I");
  OutTree->Branch("event", &Event, "event/I");
  OutTree->Branch("CluFlow", &CluFlow, "CluFlow/I");
  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SavePrimaryInfo)
  {
    OutTree->Branch("n_PV", &n_PV, "n_PV/I");
    OutTree->Branch("ParticleGun", &ParticleGun, "ParticleGun/I");
    OutTree->Branch("primaryPDGID", &primaryPDGID);
    OutTree->Branch("PositionOnAbsorberX", &PositionOnAbsorberX);
    OutTree->Branch("PositionOnAbsorberY", &PositionOnAbsorberY);
    OutTree->Branch("PositionOnAbsorberZ", &PositionOnAbsorberZ);
    OutTree->Branch("MomentumOnAbsorberX", &MomentumOnAbsorberX);
    OutTree->Branch("MomentumOnAbsorberY", &MomentumOnAbsorberY);
    OutTree->Branch("MomentumOnAbsorberZ", &MomentumOnAbsorberZ);
    OutTree->Branch("PositionOnAbsorberT", &PositionOnAbsorberT);
    OutTree->Branch("EnergyOnAbsorber", &EnergyOnAbsorber);
    OutTree->Branch("PositionAtVertexX", &PositionAtVertexX);
    OutTree->Branch("PositionAtVertexY", &PositionAtVertexY);
    OutTree->Branch("PositionAtVertexZ", &PositionAtVertexZ);
    OutTree->Branch("MomentumAtVertexX", &MomentumAtVertexX);
    OutTree->Branch("MomentumAtVertexY", &MomentumAtVertexY);
    OutTree->Branch("MomentumAtVertexZ", &MomentumAtVertexZ);
    OutTree->Branch("TimeAtVertex", &TimeAtVertex);
    OutTree->Branch("EnergyAtVertex", &EnergyAtVertex);
    OutTree->Branch("HitRegion", &HitRegion);
    OutTree->Branch("HitRegionType", &HitRegionType);
    OutTree->Branch("HitModule", &HitModule);
  }
  ///////////////////////////////////////////////////////////////////////////
  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveRawInfo)
  {
    OutTree->Branch("TotEnDep", &TotEnDep, "TotEnDep/F");
    OutTree->Branch("ShowerZ", &ShowerZ, "ShowerZ/F");
    OutTree->Branch("ShowerX", &ShowerX, "ShowerX/F");
    OutTree->Branch("ShowerY", &ShowerY, "ShowerY/F");
    OutTree->Branch("MaxDepRegion", &MaxDepRegion, "MaxDepRegion/I");

    ////////////////////////

    /// @brief /
    /// @param name
    int RegionCount = 0;
    int tmpCount = 0;
    for (auto RegionSeq : Mixture_Calo::get_instance()->region_container)
    {
      int CurrentRegionID = RegionSeq.first;
      FRegion *CurrentRegion = RegionSeq.second;
      //////////////////////////////////
      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "TotalEnDep";

      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionTotEnDep[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "EnDepSiTiming";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionEnDepSiTiming[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "EnDepSilicon";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionEnDepSilicon[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "SiliconSig";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionSiliconSig[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "ShowerZ";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionShowerZ[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "ShowerX";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionShowerX[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "ShowerY";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionShowerY[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "FrontFace";
      tmp_name2 = tmp_name.str() + "/F";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionFrontFace[RegionCount]), tmp_name2.c_str());

      tmp_name.str("");
      tmp_name << "Region" << CurrentRegionID << "TotLayerNum";
      tmp_name2 = tmp_name.str() + "/I";
      OutTree->Branch(tmp_name.str().c_str(), &(RegionTotLayerNum[RegionCount]), tmp_name2.c_str());
      /////////////////////////////////////
      int LayerCount = 0;
      for (auto Layer : CurrentRegion->Layers)
      {
        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "EnDep";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerEnDep[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_ActiveLayer" << LayerCount << "EnDep";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionActiveLayerEnDep[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Sig";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerSig[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerZ";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerShowerZ[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerX";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerShowerX[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "ShowerY";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerShowerY[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "FrontFace";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerFrontFace[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Thickness";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerThickness[tmpCount]), tmp_name2.c_str());

        tmp_name.str("");
        tmp_name << "Region" << CurrentRegionID << "_Layer" << LayerCount << "Pos";
        tmp_name2 = tmp_name.str() + "/F";
        OutTree->Branch(tmp_name.str().c_str(), &(RegionLayerPos[tmpCount]), tmp_name2.c_str());

        LayerCount++;
        tmpCount++;
      }
      RegionCount++;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////
  if (Mixture_Calo::get_instance()->CluFlow == 1 || Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 3)
  {
    if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCluster3D)
    {
      OutTree->Branch("Clu3DModule", &Clu3DModule, "Clu3DModule/I");
      OutTree->Branch("Clu3DRegion", &Clu3DRegion, "Clu3DRegion/I");
      OutTree->Branch("Clu3DRegionType", &Clu3DRegionType, "Clu3DRegionType/I");
      OutTree->Branch("Clu3DTypeX", &Clu3DTypeX, "Clu3DTypeX/I");
      OutTree->Branch("Clu3DTypeY", &Clu3DTypeY, "Clu3DTypeY/I");
      OutTree->Branch("x", &Clu3Dx, "x/F");
      OutTree->Branch("y", &Clu3Dy, "y/F");
      OutTree->Branch("z", &Clu3Dz, "z/F");
      OutTree->Branch("EntryX", &Clu3DEntryX, "EntryX/F");
      OutTree->Branch("EntryY", &Clu3DEntryY, "EntryY/F");
      OutTree->Branch("EntryZ", &Clu3DEntryZ, "EntryZ/F");
      OutTree->Branch("RawX", &Clu3DRawX, "RawX/F");
      OutTree->Branch("RawY", &Clu3DRawY, "RawY/F");
      OutTree->Branch("RawZ", &Clu3DRawZ, "RawZ/F");
      OutTree->Branch("energy", &Clu3Denergy, "energy/F");
      OutTree->Branch("TmpCorE", &Clu3DTmpCorE, "TmpCorE/F");
      OutTree->Branch("time", &Clu3Dtime, "time/F");
      OutTree->Branch("RawEnergy", &Clu3DRawEnergy, "RawEnergy/F");
      OutTree->Branch("RawTime", &Clu3DRawTime, "RawTime/F");

      if (Mixture_Calo::get_instance()->CluFlow == 2 || Mixture_Calo::get_instance()->CluFlow == 3)
      {
        OutTree->Branch("Seed3Det", &Seed3Det, "Seed3Det/F");
        OutTree->Branch("Seed3DE", &Seed3DE, "Seed3DE/F");
        OutTree->Branch("Seed3DT", &Seed3DT, "Seed3DT/F");
        OutTree->Branch("Seed3Dx", &Seed3Dx, "Seed3Dx/F");
        OutTree->Branch("Seed3Dy", &Seed3Dy, "Seed3Dy/F");
        OutTree->Branch("Seed3Dz", &Seed3Dz, "Seed3Dz/F");
        OutTree->Branch("Seed3DLocalX", &Seed3DLocalX, "Seed3DLocalX/F");
        OutTree->Branch("Seed3DLocalY", &Seed3DLocalY, "Seed3DLocalY/F");
        OutTree->Branch("Seed3DLocalZ", &Seed3DLocalZ, "Seed3DLocalZ/F");
        OutTree->Branch("Seed3Ddx", &Seed3Ddx, "Seed3Ddx/F");
        OutTree->Branch("Seed3Ddy", &Seed3Ddy, "Seed3Ddy/F");
        OutTree->Branch("Seed3Ddz", &Seed3Ddz, "Seed3Ddz/F");
        OutTree->Branch("Clu3DE3", &Clu3DE3, "Clu3DE3/F");
        OutTree->Branch("Cell3DNum", &Cell3DNum, "Cell3DNum/I");
        OutTree->Branch("Cell3DE", &Cell3DE);
        OutTree->Branch("Cell3DT", &Cell3DT);
        OutTree->Branch("Cell3DX", &Cell3DX);
        OutTree->Branch("Cell3DY", &Cell3DY);
        OutTree->Branch("Cell3DZ", &Cell3DZ);
        OutTree->Branch("Cell3DType", &Cell3DType);
        OutTree->Branch("Clu3DMatchIndex", &Clu3DMatchIndex, "Clu3DMatchIndex/I");
      }

      if (Mixture_Calo::get_instance()->ProgramType == 2 || Mixture_Calo::get_instance()->ProgramType == 4) // some truth information
      {
        OutTree->Branch("TruLocalX", &TruLocalX, "TruLocalX/F");
        OutTree->Branch("TruLocalY", &TruLocalY, "TruLocalY/F");
        OutTree->Branch("TruLocalZ", &TruLocalZ, "TruLocalZ/F");
        OutTree->Branch("TruShowerDepth", &TruShowerDepth, "TruShowerDepth/F");
        OutTree->Branch("TruX", &TruX, "TruX/F");
        OutTree->Branch("TruY", &TruY, "TruY/F");
        OutTree->Branch("TruZ", &TruZ, "TruZ/F");
        OutTree->Branch("TruDep", &TruDep, "TruDep/F");
      }
    }
    if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCluster2D)
    {
      OutTree->Branch("Clu2D_id", &Clu2D_id);
      OutTree->Branch("Clu2DModule", &Clu2DModule);
      OutTree->Branch("Clu2DRegion", &Clu2DRegion);
      OutTree->Branch("Clu2DRegionType", &Clu2DRegionType);
      OutTree->Branch("Clu2DTime", &Clu2DTime);
      OutTree->Branch("Clu2DEnergy", &Clu2DEnergy);
      OutTree->Branch("Clu2DRawE", &Clu2DRawE);
      OutTree->Branch("Clu2DPos", &Clu2DPos);
      OutTree->Branch("Clu2DThickness", &Clu2DThickness);
      OutTree->Branch("Clu2DS0CorX", &Clu2DS0CorX);
      OutTree->Branch("Clu2DS0CorY", &Clu2DS0CorY);
      OutTree->Branch("Clu2DX", &Clu2DX);
      OutTree->Branch("Clu2DY", &Clu2DY);
      OutTree->Branch("Clu2DZ", &Clu2DZ);
      OutTree->Branch("Clu2DLocalX", &Clu2DLocalX);
      OutTree->Branch("Clu2DLocalY", &Clu2DLocalY);
      OutTree->Branch("Clu2DLocalZ", &Clu2DLocalZ);
      OutTree->Branch("Clu2DRawX", &Clu2DRawX);
      OutTree->Branch("Clu2DRawY", &Clu2DRawY);
      OutTree->Branch("Clu2DRawZ", &Clu2DRawZ);
      OutTree->Branch("Clu2DSeedX", &Clu2DSeedX);
      OutTree->Branch("Clu2DSeedY", &Clu2DSeedY);
      OutTree->Branch("Clu2DSeedZ", &Clu2DSeedZ);
      OutTree->Branch("Clu2DSeedE", &Clu2DSeedE);
      OutTree->Branch("Clu2DSeedT", &Clu2DSeedT);
      OutTree->Branch("Clu2DCellDx", &Clu2DCellDx);
      OutTree->Branch("Clu2DCellDy", &Clu2DCellDy);
      OutTree->Branch("Clu2DAx", &Clu2DAx);
      OutTree->Branch("Clu2DAy", &Clu2DAy);
      OutTree->Branch("Clu2DAz", &Clu2DAz);
      OutTree->Branch("Modulex", &Modulex);
      OutTree->Branch("Moduley", &Moduley);
      OutTree->Branch("Modulez", &Modulez);
      OutTree->Branch("Clu2DTypeX", &Clu2DTypeX);
      OutTree->Branch("Clu2DTypeY", &Clu2DTypeY);
      OutTree->Branch("Clu2DE3", &Clu2DE3);
      OutTree->Branch("Clu2DE5", &Clu2DE5);
      OutTree->Branch("Clu2DE7", &Clu2DE7);
      OutTree->Branch("Clu2DE9", &Clu2DE9);
      OutTree->Branch("Clu2DCellNum", &Clu2DCellNum);
      OutTree->Branch("CellE", &CellE);
      OutTree->Branch("CellT", &CellT);
      OutTree->Branch("CellX", &CellX);
      OutTree->Branch("CellY", &CellY);
      OutTree->Branch("CellZ", &CellZ);
      OutTree->Branch("CellType", &CellType);
      if (Mixture_Calo::get_instance()->ProgramType == 2 || Mixture_Calo::get_instance()->ProgramType == 4) // some truth information
      {
        OutTree->Branch("LayerTruLocalX", &LayerTruLocalX);
        OutTree->Branch("LayerTruLocalY", &LayerTruLocalY);
        OutTree->Branch("LayerTruLocalZ", &LayerTruLocalZ);
        OutTree->Branch("LayerTruShowerDepth", &LayerTruShowerDepth);
        OutTree->Branch("LayerTruX", &LayerTruX);
        OutTree->Branch("LayerTruY", &LayerTruY);
        OutTree->Branch("LayerTruZ", &LayerTruZ);
        OutTree->Branch("LayerTruDep", &LayerTruDep);
      }
    }
  }

  this->Clear();
}

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

CreateTree::~CreateTree()
{
}

int CreateTree::Fill()
{
  return this->GetTree()->Fill();
}

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

bool CreateTree::Write(TFile *outfile)
{
  spdlog::get(LOG_NAME)->info("Output File is : {}", outfile->GetName());

  outfile->cd();
  Parameters->Write();
  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveCellInfo)
  {
    spdlog::get(LOG_NAME)->info("\tWriting ReadoutInfo Tree To Output File");
    ReadoutInfo->Write();
  }
  else
  {
    spdlog::get(LOG_NAME)->info("\tWriting general Tree To Output File");
    OutTree->Write();
  }
  if (Parameters::Instance()->SavePi0)
  {
    spdlog::get(LOG_NAME)->info("\tWriting Pi0 Tree To Output File");
    Pi0->Write();
  }
  if (Parameters::Instance()->SaveGamma)
  {
    spdlog::get(LOG_NAME)->info("\tWriting Matched Gamma Tree To Output File");
    Gamma->Write();
    // SubGamma->Write();
  }
  if (Parameters::Instance()->SavePrimaryInfo)
  {
    spdlog::get(LOG_NAME)->info("\tWriting Primary Tree To Output File");
    PrimaryTree->Write();
  }
  if (Parameters::Instance()->SaveAll || Parameters::Instance()->SaveHitMap)
  {
    spdlog::get(LOG_NAME)->info("\tWriting HitMap To Output File");

    for (int event = 0; event < Mixture_Calo::get_instance()->max_Eventseq; event++)
    {

      Mixture_Calo::get_instance()->GetHitMap(event)->Write();
      TCanvas *CluMap = new TCanvas("CluMap", "CluMap");
      // Mixture_Calo::get_instance()->region_map.Draw("colz");
      Mixture_Calo::get_instance()->GetHitMap(event)->Draw("colz");
      TString tmpOutName = Parameters::Instance()->OutputPath + "/Run" + std::to_string(Mixture_Calo::get_instance()->IntputNum) + "Evt" + std::to_string(event) + "CluMap.png";
      CluMap->Print(tmpOutName);
    }
  }
  if (Parameters::Instance()->SaveMore == 1)
  {
    spdlog::get(LOG_NAME)->info("\tWriting Info 1 To Output File");
  }
  if (Parameters::Instance()->SaveMore == 2)
  {
    spdlog::get(LOG_NAME)->info("\tWriting RegionMap To Output File");

    Mixture_Calo::get_instance()->region_map.Write();
    Mixture_Calo::get_instance()->regionType_map.Write();
    Mixture_Calo::get_instance()->RawRegionMap.Write();
  }

  return true;
}

void CreateTree::Clear()
{
  Run = 0;
  Event = 0;
  ParticleGun = 0;
  primaryPDGID.clear();
  PositionOnAbsorberX.clear();
  PositionOnAbsorberY.clear();
  PositionOnAbsorberZ.clear();
  MomentumOnAbsorberX.clear();
  MomentumOnAbsorberY.clear();
  MomentumOnAbsorberZ.clear();
  PositionOnAbsorberT.clear();
  EnergyOnAbsorber.clear();
  PositionAtVertexX.clear();
  PositionAtVertexY.clear();
  PositionAtVertexZ.clear();
  MomentumAtVertexX.clear();
  MomentumAtVertexY.clear();
  MomentumAtVertexZ.clear();
  TimeAtVertex.clear();
  EnergyAtVertex.clear();
  HitRegion.clear();
  HitRegionType.clear();
  HitModule.clear();
  TotEnDep = 0;
  MaxDepRegion = 0;
  ShowerZ = 0;
  ShowerX = 0;
  ShowerY = 0;
  Clu3DRegionType = 0;
  Clu3DRegion = 0;
  Clu3DModule = 0;
  CluFlow = 0;
  Clu3Devent = 0;
  Clu3DTypeX = 0;
  Clu3DTypeY = 0;
  Clu3Dx = 0;
  Clu3Dy = 0;
  Clu3Dz = 0;
  Clu3DRawX = 0;
  Clu3DRawY = 0;
  Clu3DRawZ = 0;

  Clu3Dtime = 0;
  Clu3Denergy = 0;
  Clu3DRawTime = 0;
  Clu3DRawEnergy = 0;
  Seed3Det = 0;
  Seed3DT = 0;
  Seed3Dx = 0;
  Seed3Dy = 0;
  Seed3Dz = 0;
  Seed3DLocalX = 0;
  Seed3DLocalY = 0;
  Seed3DLocalZ = 0;
  Seed3Ddx = 0;
  Seed3Ddy = 0;
  Seed3Ddz = 0;
  Seed3DE = 0;
  Clu3DE3 = 0;
}
