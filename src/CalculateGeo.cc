#include "CalculateGeo.hh"
#include <algorithm>
#include <iostream>
#include "FLog.hh"

TH2I *CalCulateMapByTreeFrom_Simulation(TTree *inTree, int Type)
{
    TTree *GeoTree = inTree->CopyTree(Form("type==%d", Type));

    float in_X;
    GeoTree->SetBranchAddress("x", &in_X);
    float in_Y;
    GeoTree->SetBranchAddress("y", &in_Y);
    float in_Z;
    GeoTree->SetBranchAddress("z", &in_Z);
    int in_type;
    GeoTree->SetBranchAddress("type", &in_type);
    float in_Dx;
    GeoTree->SetBranchAddress("dx", &in_Dx);
    float in_Dy;
    GeoTree->SetBranchAddress("dy", &in_Dy);
    float in_Dz;
    GeoTree->SetBranchAddress("dz", &in_Dz);

    // 创建向量来保存x坐标和y坐标
    std::vector<float> xValues;
    std::vector<float> yValues;

    float Dx;
    float Dy;
    // 将x坐标和y坐标分别保存到向量中
    for (int i = 0; i < GeoTree->GetEntries(); i++)
    {
        GeoTree->GetEntry(i);
        if (in_type == Type)
        {
            xValues.push_back(in_X);
            yValues.push_back(in_Y);
            Dx = in_Dx;
            Dy = in_Dy;
        }
    }

    // 对x坐标向量进行排序并去除重复值
    std::sort(xValues.begin(), xValues.end());
    xValues.erase(std::unique(xValues.begin(), xValues.end()), xValues.end());

    // 对y坐标向量进行排序并去除重复值
    std::sort(yValues.begin(), yValues.end());
    yValues.erase(std::unique(yValues.begin(), yValues.end()), yValues.end());
    // 打印去除重复值后的x坐标向量

    // 打印去除重复值后的y坐标向量
    // std::cout << "Unique Y coordinates: ";
    // for (const auto &y : yValues)
    // {
    //     std::cout << y << " ";
    // }
    // std::cout << std::endl;

    const int nXbins = xValues.size();
    Float_t xbins[nXbins + 1];
    if (nXbins > 2)
    {

        xbins[0] = xValues.at(0) - (xValues.at(1) - (xValues.at(2) - xValues.at(1)) / 2 - xValues.at(0));
        // std::cout << "xbins[0] : " << xbins[0] << std::endl;
        int endBin = xValues.size() - 1;
        xbins[nXbins] = xValues.at(endBin) + (xValues.at(endBin) - (xValues.at(endBin - 1) + (xValues.at(endBin - 1) - xValues.at(endBin - 2)) / 2));
        // std::cout << "xbins[nXbins]  : " << xbins[nXbins] << std::endl;
        float innerBinWidth = (xValues.at(2) - xValues.at(1)) / 2;
        for (int i = 0; i < nXbins - 1; i++)
        {
            xbins[i + 1] = xValues.at(i) + innerBinWidth;
        }
    }
    else if (nXbins == 1)
    {
        xbins[0] = xValues.at(0) - Dx / 2.;
        xbins[1] = xValues.at(0) + Dx / 2.;
    }
    else if (nXbins == 2)
    {
        float tmpDx = xValues.at(1) - xValues.at(0);
        xbins[0] = xValues.at(0) - tmpDx / 2;
        xbins[1] = xValues.at(0) + tmpDx / 2;
        xbins[2] = xValues.at(1) + tmpDx / 2;
    }

    const int nYbins = yValues.size();
    Float_t ybins[nYbins + 1];
    if (nYbins > 2)
    {

        ybins[0] = yValues.at(0) - (yValues.at(1) - (yValues.at(2) - yValues.at(1)) / 2 - yValues.at(0));
        // std::cout << "ybins[0] : " << ybins[0] << std::endl;
        int endBin = yValues.size() - 1;
        ybins[nYbins] = yValues.at(endBin) + (yValues.at(endBin) - (yValues.at(endBin - 1) + (yValues.at(endBin - 1) - yValues.at(endBin - 2)) / 2));
        // std::cout << "ybins[nYbins]  : " << ybins[nYbins] << std::endl;
        float innerBinWidth = (yValues.at(2) - yValues.at(1)) / 2;
        for (int i = 0; i < nYbins - 1; i++)
        {
            ybins[i + 1] = yValues.at(i) + innerBinWidth;
            // std::cout << "ybins[" << i + 1 << "] = " << ybins[i + 1] << std::endl;
        }
    }
    else if (nYbins == 1)
    {
        ybins[0] = yValues.at(0) - Dy / 2.;
        ybins[1] = yValues.at(0) + Dy / 2.;
    }
    else if (nYbins == 2)
    {
        float tmpDy = yValues.at(1) - yValues.at(0);
        ybins[0] = yValues.at(0) - tmpDy / 2;
        ybins[1] = yValues.at(0) + tmpDy / 2;
        ybins[2] = yValues.at(1) + tmpDy / 2;
    }

    TH2I *Map = new TH2I(" ", " ", nXbins, xbins, nYbins, ybins);
    int CellId = 0;
    for (int x = 0; x < xValues.size(); x++)
        for (int y = 0; y < yValues.size(); y++)
        {
            Map->Fill(xValues[x], yValues[y], CellId);
            CellId++;
        }
    return Map;
}

TH2I *CalCulateMapByTreeFrom_Simulation(TTree *GeoTree, std::vector<int> TypeList)
{
    float in_X;
    GeoTree->SetBranchAddress("x", &in_X);
    float in_Y;
    GeoTree->SetBranchAddress("y", &in_Y);
    float in_Z;
    GeoTree->SetBranchAddress("z", &in_Z);
    int in_type;
    GeoTree->SetBranchAddress("type", &in_type);
    float in_Dx;
    GeoTree->SetBranchAddress("dx", &in_Dx);
    float in_Dy;
    GeoTree->SetBranchAddress("dy", &in_Dy);
    float in_Dz;
    GeoTree->SetBranchAddress("dz", &in_Dz);

    // 创建向量来保存x坐标和y坐标
    std::vector<double> xValues;
    std::vector<double> yValues;

    // 将x坐标和y坐标分别保存到向量中
    for (int i = 0; i < GeoTree->GetEntries(); i++)
    {
        GeoTree->GetEntry(i);
        if (std::find(TypeList.begin(), TypeList.end(), in_type) != TypeList.end())
        {
            xValues.push_back(in_X);
            yValues.push_back(in_Y);
        }
    }

    // 对x坐标向量进行排序并去除重复值
    std::sort(xValues.begin(), xValues.end());
    xValues.erase(std::unique(xValues.begin(), xValues.end()), xValues.end());

    // 对y坐标向量进行排序并去除重复值
    std::sort(yValues.begin(), yValues.end());
    yValues.erase(std::unique(yValues.begin(), yValues.end()), yValues.end());

    // // 打印去除重复值后的x坐标向量
    // std::cout << "Unique X coordinates: ";
    // for (const auto &x : xValues)
    // {
    //     std::cout << x << " ";
    // }
    // std::cout << std::endl;

    // // 打印去除重复值后的y坐标向量
    // std::cout << "Unique Y coordinates: ";
    // for (const auto &y : yValues)
    // {
    //     std::cout << y << " ";
    // }
    // std::cout << std::endl;

    const int nXbins = xValues.size() + 1;
    Float_t xbins[nXbins];
    xbins[0] = xValues.at(0) - (xValues.at(1) - (xValues.at(2) - xValues.at(1)) / 2 - xValues.at(0));
    //std::cout << "xbins[0] : " << xbins[0] << std::endl;
    int endBin = xValues.size() - 1;
    xbins[nXbins - 1] = xValues.at(endBin) + (xValues.at(endBin) - (xValues.at(endBin - 1) + (xValues.at(endBin - 1) - xValues.at(endBin - 2)) / 2));
    //std::cout << "xbins[nXbins-1]  : " << xbins[nXbins - 1] << std::endl;
    float innerBinWidth = (xValues.at(2) - xValues.at(1)) / 2;
    for (int i = 1; i < nXbins; i++)
    {
        xbins[i] = xValues.at(i - 1) + innerBinWidth;
    }

    const int nYbins = yValues.size() + 1;
    Float_t ybins[nYbins];
    ybins[0] = yValues.at(0) - (yValues.at(1) - (yValues.at(2) - yValues.at(1)) / 2 - yValues.at(0));
    endBin = yValues.size() - 1;
    ybins[nYbins - 1] = yValues.at(endBin) + (yValues.at(endBin) - (yValues.at(endBin - 1) + (yValues.at(endBin - 1) - yValues.at(endBin - 2)) / 2));
    //std::cout << "ybins[nYbins-1]  : " << ybins[nYbins - 1] << std::endl;
    innerBinWidth = (yValues.at(2) - yValues.at(1)) / 2;
    for (int i = 1; i < nYbins; i++)
    {
        ybins[i] = yValues.at(i - 1) + innerBinWidth;
    }

    TH2I *Map = new TH2I(" ", " ", nXbins - 1, xbins, nYbins - 1, ybins);
    int CellId = 0;
    for (int x = 0; x < xValues.size(); x++)
        for (int y = 0; y < yValues.size(); y++)
        {
            Map->Fill(xValues[x], yValues[y], CellId);
            CellId++;
        }
    return Map;
}

TH2I *CalCulateLayerMap(float xRange, float xSize, float yRange, float ySize)
{
    int x_bin = xRange / xSize;
    int y_bin = yRange / ySize;
    float newXSize = xRange / x_bin;
    float newYSize = yRange / y_bin;
    int counts = 0;
    TH2I *Map = new TH2I(" ", " ", x_bin, -xRange / 2, xRange / 2, y_bin, -yRange / 2, yRange / 2);
    for (int x_loop = 1; x_loop <= x_bin; x_loop++)
        for (int y_loop = 1; y_loop <= y_bin; y_loop++)
        {
            int tmpbin = Map->GetBin(x_loop, y_loop);
            Map->SetBinContent(tmpbin, counts);
            counts++;
        }
    return Map;
}