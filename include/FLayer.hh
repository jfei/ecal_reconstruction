#ifndef FJL_Layer_hh
#define FJL_Layer_hh

#include "map"

#include <iomanip>
#include "TCanvas.h"
#include "TH2F.h"
#include "set"

class FCluster2D;
class FCell;

class FRegion;
class FModule;

struct LayerEventInfo //  Except endep, only valid under ActivePrimaryInfo =1
{
    int event;
    std::vector<FCluster2D *> Cluster2D;
    std::set<FCell *> Seed2D;
    std::set<FCell *> LocalMaxCell;
    TH2F *HitMap;
    void SetEventID(int EventID)
    {
        event = EventID;
    }
    int GetEventID()
    {
        return event;
    }
};


class FLayer
{
public:
    FLayer(int m_id, float m_pos_z) : id(m_id), pos_z(m_pos_z)
    {
    }
    FModule *f_module;
    int layerType; // 0 : Scintillator 1: silicon
    int id;
    float pos_z; //
    float layerThickness;
    FModule *Mother_Module;
    std::vector<LayerEventInfo> EventInfo;
    void InitEventInfo();

    std::vector<FCell *> v_cell; // cell vector



    int GetCellLocalIdByRelaPos(float x_pos, float y_pos);


};

#endif