#ifndef FJL_Cluster2D_hh
#define FJL_Cluster2D_hh

#include "FDataType.hh"
#include "iostream"
#include "map"
#include "FPoint.hh"

class FRegion;
class FCluster3D;
class FCell;
class FModule;
class FLayer;

class FCluster2D
{
private:
    /* data */
public:
    FCluster2D(FCell *i_Seed2D, int i_event);
    FCluster2D(int i_event)
    {
        LocalPos.SetXYZ(0, 0, 0);
        GlobalPos.SetXYZ(0, 0, 0);
        RawGlobalPos.SetXYZ(0, 0, 0);
        time = 0;
        CellNumber = 0;
        event = i_event;
    }

    std::vector<float> Dist2Tru;
    std::map<int, std::vector<FCell *>> Cluster_Cell;
    int Type = 0; //compare with seed 0 : All cell in same module , 1 : All cell in same region id, 2: All cell in same region type
    int XType = 0;
    int YType = 0;
    int event;
    int layer_id;
    float layerThickness = 0;
    float energy = 0;
    FPoint LocalPos;
    FPoint GlobalPos;
    FPoint S0CorGlobalPos;
    FPoint EntryPos;
    FPoint RawGlobalPos;
    float ax = 0;
    float ay = 0;
    float az = 0;
    float RawE = 0;
    float time = 0;
    float RawTime = 0;
    float E3 = 0;
    float E5 = 0;
    float E7 = 0;
    float E9 = 0;
    FCell *Seed2D = NULL;
    FRegion *f_region = NULL;
    FModule *f_module = NULL;
    FLayer *f_layer;
    FCluster3D *f_Clu3D = NULL;
    FCluster2D *f_Clu2D=NULL;
    int CellNumber = 0;
    int AfterSCor = 0;
    int AfterLCor = 0;
    int AfterECor = 0;
    int AfterTCor = 0;

    static uint64 CalculateGlobalId(float CellSize, int layer_id, float layer_thick, float ax, float ay, float az);

    // float CalculateTime();

    float inline GetTime()
    {
        return time;
    }
    void inline SetTime(float input)
    {
        time = input;
    }
    float inline GetRawTime()
    {
        return RawTime;
    }
    void inline SetRawTime(float input)
    {
        RawTime = input;
    }

    // void Reconstruction();

    void SCorrection();
    bool S0Correction();
    void S1Correction();
    float LCorrection(int nPV = -1);
    float TCorrection();
    float ECorrection();

    int IsMissingXCell();
    int IsMissingYCell();

    void SetLayerID(int input)
    {
        layer_id = input;
    }
    int LayerId()
    {
        return layer_id;
    }

    float Ax()
    {
        return ax;
    }
    float Ay()
    {
        return ay;
    }
    float Az()
    {
        return az;
    }

    FPoint GetGlobalPosition()
    {
        return GlobalPos;
    }
    void SetGlobalPosition(float i_x, float i_y, float i_z)
    {

        GlobalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetGlobalPosition(FPoint input)
    {

        GlobalPos = input;
    }
    void SetX(float input)
    {
        GlobalPos.SetX(input);
    }
    void SetY(float input)
    {
        GlobalPos.SetY(input);
    }
    void SetZ(float input)
    {
        GlobalPos.SetZ(input);
    }
    float X()
    {
        if (AfterSCor == 1)
            return S0CorGlobalPos.X();
        else if (AfterSCor == 2)
            return GlobalPos.X();
        else
            return RawGlobalPos.X();
    }
    float Y()
    {
        if (AfterSCor == 1)
            return S0CorGlobalPos.Y();
        else if (AfterSCor == 2)
            return GlobalPos.Y();
        else
            return RawGlobalPos.Y();
    }
    float Z()
    {
        if (AfterLCor)
            return GlobalPos.Z();
        else
            return RawGlobalPos.Z();
    }

    void SetEntryPosition(float i_x, float i_y, float i_z)
    {

        EntryPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetEntryPosition(FPoint input)
    {

        EntryPos = input;
    }
    void SetEntryX(float input)
    {
        EntryPos.SetX(input);
    }
    void SetEntryY(float input)
    {
        EntryPos.SetY(input);
    }
    void SetEntryZ(float input)
    {
        EntryPos.SetZ(input);
    }
    float EntryX()
    {
        return EntryPos.X();
    }
    float EntryY()
    {
        return EntryPos.Y();
    }
    float EntryZ()
    {
        return EntryPos.Z();
    }

    FPoint GetLocalPosition()
    {
        return LocalPos;
    }
    void SetLocalPosition(float i_x, float i_y, float i_z)
    {

        LocalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetLocalPosition(FPoint input)
    {

        LocalPos = input;
    }
    float LocalX()
    {
        return LocalPos.X();
    }
    float LocalY()
    {
        return LocalPos.Y();
    }
    float LocalZ()
    {
        return LocalPos.Z();
    }

    FPoint GetRawPosition()
    {
        return RawGlobalPos;
    }
    void SetRawPosition(float i_x, float i_y, float i_z)
    {

        RawGlobalPos.SetXYZ(i_x, i_y, i_z);
    }

    FPoint GetS0CorPosition()
    {
        return S0CorGlobalPos;
    }
    void SetS0CorPosition(float i_x, float i_y, float i_z)
    {

        S0CorGlobalPos.SetXYZ(i_x, i_y, i_z);
    }

    void SetRawPosition(FPoint input)
    {

        RawGlobalPos = input;
    }
    void SetRawX(float input)
    {
        RawGlobalPos.SetX(input);
    }
    void SetRawY(float input)
    {
        RawGlobalPos.SetY(input);
    }
    void SetRawZ(float input)
    {
        RawGlobalPos.SetZ(input);
    }

    void SetS0CorX(float input)
    {
        S0CorGlobalPos.SetX(input);
    }
    void SetS0CorY(float input)
    {
        S0CorGlobalPos.SetY(input);
    }
    void SetS0CorZ(float input)
    {
        S0CorGlobalPos.SetZ(input);
    }

    void SetRawE(float input)
    {
        RawE = input;
    }
    float RawX()
    {
        return RawGlobalPos.X();
    }
    float RawY()
    {
        return RawGlobalPos.Y();
    }
    float RawZ()
    {
        return RawGlobalPos.Z();
    }

    float S0CorX()
    {
        return S0CorGlobalPos.X();
    }
    float S0CorY()
    {
        return S0CorGlobalPos.Y();
    }
    float S0CorZ()
    {
        return S0CorGlobalPos.Z();
    }

    float RawEnergy()
    {
        return RawE;
    }
    void SetEnergy(float input)
    {
        energy = input;
    }
    float Energy()
    {
        if (AfterECor)
            return energy;
        else
            return RawE;
    }
    void RecRawInfoByCells();

    void ReAllocateCell();

    void InitCluType();

    void AddCell2D(FCell *Cell2D);

    void AddCell2D(FCell *Cell2D, int Cell2DType);

    void RemoveCell2D(FCell *Cell2D, int Cell2DType);

    void RemoveCell2D(FCell *Cell2D);

    void ClearCell2D();

    float GetTRes();

    float GetSeedE();

    void PrintCellInfo();
};

#endif