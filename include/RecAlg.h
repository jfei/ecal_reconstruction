#ifndef RecAlg_h
#define RecAlg_h
#include "Mixture_Calo.hh"
#include "TF1.h"
#include "iostream"
#include <map>
#include <stdlib.h>
#include <algorithm>
#include <math.h>
#include <iomanip>

#include "FPoint.hh"

class FCell;
class FCell3D;
class FCluster2D;
class FCluster3D;


void ReAllocateClu2DBy_3_3(FCluster2D *Cluster2D);
void ReAllocateClu2DBy_2_2(FCluster2D *Cluster2D);
void ReAllocateClu2DBy_Cross(FCluster2D *Cluster2D);
void ReAllocateClu2DBy_Gradient(FCluster2D *Cluster2D);
FCluster2D *AllocateCellBy_3_3(FCell *seed, int eventNum);
FCluster2D *AllocateCellBy_2_2(FCell *seed, int eventNum);
FCluster2D *AllocateCellBy_Cross(FCell *seed, int eventNum);
FCluster2D *AllocateCellByGradient(FCell *seed, int eventNum);
FCluster3D *AllocateCell3DBy_3_3(FCell3D *seed, int eventNum);
FCluster3D *AllocateCell3DBy_2_2(FCell3D *seed, int eventNum);
FCluster3D *AllocateCell3DBy_Cross(FCell3D *seed, int eventNum);
FCluster3D *AllocateCell3DByGradient(FCell3D *seed, int eventNum);
FPoint RotatePointByAngle(float x, float y, float z, float ax, float ay, float az);
std::pair<FCluster3D *, FCluster3D *> SplitClu3D(FCluster3D *Clu3D);        // 通常用于merged pi0
std::pair<FCluster2D *, FCluster2D *> SplitClu2D(FCluster2D *Clu2D);        // 通常用于merged pi0
std::pair<FCluster3D *, FCluster3D *> SplitClu3DByClu2D(FCluster3D *Clu3D); // 通常用于merged pi0

float MatchProbabilityClu2D(std::pair<FCluster2D *, FCluster2D *> Clu2DPair);

int SplitClu2DCellE(std::pair<FCluster2D *, FCluster2D *> Clu2DPair);
int SplitClu3DCellE(std::pair<FCluster3D *, FCluster3D *> Clu3DPair);
int SplitClu3DCell3DE(std::pair<FCluster3D *, FCluster3D *> Clu3DPair);

std::pair<TVector3, TVector3> GetLine(std::vector<TVector3> Points, std::vector<TVector3> PointsErr);
TVector3 GetNearestPoint(std::pair<TVector3, TVector3> Line1, std::pair<TVector3, TVector3> Line2);

#endif