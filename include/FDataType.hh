#ifndef FJL_DataType_hh
#define FJL_DataType_hh
#define Fsign(x) (((x) < 0) ? -1 : ((x) > 0))

typedef unsigned char boolean;
typedef unsigned long long uint64;
typedef unsigned long int uint32;
typedef unsigned short uint16;
typedef signed long int int32;
typedef signed short int16;
typedef signed char int8;

#endif