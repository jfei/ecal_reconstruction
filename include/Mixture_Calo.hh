#ifndef FJL_Mixture_Calo_hh
#define FJL_Mixture_Calo_hh

#include "FCalorimeter.hh"
#include "string.h"
#include "FLog.hh"
class FCluster3D;
class FCluster2D;
class Mixture_Calo : public FCalorimeter
{
private:
    Mixture_Calo()
    {
    }
    ~Mixture_Calo()
    {
    }
    static Mixture_Calo *__instance;

    /* data */
public:
    void InitCalo();
    void ConstructFromRawFile();
    void Construct();
    void ConstructCalo();
    void ConstructRegion();
    void ConstructModule();
    void InitPVAndHitInfo();
    void InitEventStructure();
    void InitPara();
    void RecSiliconCell();
    void FindNeighbor();
    void InitCellGlobalPos();
    void RecRegion(); // 根据module类型和旋转角度重新构造region。

    void CalibrateCell();
    void ConstructCell3D();

    void InitCaliPara();

    void Seeding(int event);
    void Seeding3D(int event);
    void ContructSeed3D(int event);
    void AllocateCell3D(int event); // this will construct 3D cluster directly
    void ReAllo2DCell(int event);   // loop Clu3D and redefine layer
    void AllocateCell(int event);   // this will construct 2D cluster

    void ConstructCluster3D(int event);

    void CorrectCluster3D(int event);
    void CaculateClusterTime(int event);
    void CorrectCluster3DEnergy(int event);
    void CorrectCluster3DPosition(int event);

    void SplitOverlapClu3D(int event);
    void SplitOverlapClu2D(int event);

    void Matching();
    void MatchPi0();
    void MatchGamma();

    void Calibration();
    void Calibration2();

    void WriteTree();

    void RecGamma();
    void RecResolvedPi0();
    void RecMergedPi0();
    void RecElectron();

    void EvalDist2Charge();
    std::vector<FCluster2D *> GetMatchingClu2D(FCluster2D *Clu2D, int layer);

    int GeoFilesType;  // 1 : SimFile 2: RecGeoFile
    int TruthFileType; // 1 : SimFile (both primary and hit info) 2 : Flux file (only primary)
    int DataFileType;  // 1: TrigFileName 2 : RecFile
    bool InitTruthInfo;
    bool AbPathOrPrefix = 1;
    bool ConstructSiliconCell;
    bool tagCharge = 0;

    std::string SimFileName = "";
    std::string TrigFileName = "";
    std::string FluxFileName = "";
    std::string RecFileName = "";
    std::string RecGeoFileName = "";
    std::string ChargeSampleFilePrefix = "";

    std::string OutFileName;

    TFile *Trig_file = NULL;
    TFile *Sim_file = NULL;
    TFile *TruthFile = NULL;
    TFile *Step1CaFile = NULL;
    TFile *GeoFile = NULL;
    TFile *ReadoutFile = NULL;
    TFile *ChargeSampleFile = NULL;

    int IntputNum;
    void InitFileName(int my_fileNum)
    {
        IntputNum = my_fileNum;
        GeoFilesType = Parameters::Instance()->GeoFilesType;
        TruthFileType = Parameters::Instance()->TruthFileType;
        DataFileType = Parameters::Instance()->DataFileType;
        InitTruthInfo = Parameters::Instance()->InitTruthInfo;
        ConstructSiliconCell = Parameters::Instance()->ConstructSiliconCell;
        tagCharge = Parameters::Instance()->tagCharge;
        if (tagCharge)
        {
            if (Parameters::Instance()->ChargeSampleFilePrefix.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need ChargeSampleFile , but ChargeSampleFilePrefix is {}", Parameters::Instance()->ChargeSampleFilePrefix);
                exit(0);
            }
            else if (Parameters::Instance()->ChargeSampleFilePrefix.find(".root") == string::npos)
                ChargeSampleFilePrefix = Parameters::Instance()->ChargeSampleFilePrefix + std::to_string(my_fileNum) + ".root";
            else
            {
                spdlog::get(LOG_NAME)->info("please give a prefiex but not {}", Parameters::Instance()->ChargeSampleFilePrefix);
            }

            spdlog::get(LOG_NAME)->info("Charge sample file name = {}", ChargeSampleFilePrefix);
            ChargeSampleFile = TFile::Open(ChargeSampleFilePrefix.c_str());
            if (!ChargeSampleFile)
            {
                spdlog::get(ERROR_NAME)->error("Error opening charge sample file {}, please check", ChargeSampleFilePrefix);
                exit(-1);
            }
        }

        if (GeoFilesType == 1 || (TruthFileType == 1 && InitTruthInfo == 1) || ConstructSiliconCell == 1)
        {
            if (Parameters::Instance()->SimFileName.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need Sim File (default is out*.root), but SimFileName is {}", Parameters::Instance()->SimFileName);
                exit(0);
            }
            else
            {
                if (Parameters::Instance()->SimFileName.find(".root") == string::npos)
                    SimFileName = Parameters::Instance()->InputPath + "/" + Parameters::Instance()->SimFileName + std::to_string(my_fileNum) + ".root";
                else
                    SimFileName = Parameters::Instance()->SimFileName;

                spdlog::get(LOG_NAME)->info("Sim file name = {}", SimFileName);
                Sim_file = TFile::Open(SimFileName.c_str());
                if (!Sim_file)
                {
                    spdlog::get(ERROR_NAME)->error("Error opening sim file {}, please check", SimFileName);
                    exit(-1);
                }
            }
        }
        if (GeoFilesType == 2)
        {
            if (Parameters::Instance()->RecGeoFileName.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need Rec Geometry File (default is SimGeo.root), but RecGeoFileName is {}", Parameters::Instance()->RecGeoFileName);
                exit(0);
            }
            else
            {
                if (Parameters::Instance()->RecGeoFileName.find(".root") == string::npos)
                    RecGeoFileName = Parameters::Instance()->InputPath + "/" + Parameters::Instance()->RecGeoFileName + std::to_string(my_fileNum) + ".root";
                else
                    RecGeoFileName = Parameters::Instance()->RecGeoFileName;

                spdlog::get(LOG_NAME)->info("Rec Geometry file name = {}", RecGeoFileName);
                GeoFile = TFile::Open(RecGeoFileName.c_str());
                if (!GeoFile)
                {
                    spdlog::get(ERROR_NAME)->error("Error opening Geometry file {}, please check", RecGeoFileName);
                    exit(-1);
                }
            }
        }
        if ((TruthFileType == 2 && InitTruthInfo == 1))
        {
            if (Parameters::Instance()->FluxFileName.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need Flux File (default is flux_.root), but FluxFileName is {}", Parameters::Instance()->FluxFileName);
                exit(0);
            }
            else
            {
                if (Parameters::Instance()->FluxFileName.find(".root") == string::npos)
                {
                    FluxFileName = Parameters::Instance()->InputPath + "/" + Parameters::Instance()->FluxFileName + std::to_string(my_fileNum) + ".root";
                    AbPathOrPrefix = 0;
                }
                else
                {
                    FluxFileName = Parameters::Instance()->FluxFileName;
                    AbPathOrPrefix = 1;
                }

                spdlog::get(LOG_NAME)->info("Flux file name = {}", FluxFileName);
                TruthFile = TFile::Open(FluxFileName.c_str());
                if (!TruthFile)
                {
                    spdlog::get(ERROR_NAME)->error("Error opening Truth flux file {}, please check", FluxFileName);
                    exit(-1);
                }
            }
        }
        if ((TruthFileType == 3 && InitTruthInfo == 1) || DataFileType == 2)
        {
            if (Parameters::Instance()->RecFileName.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need Rec File , but RecFileName is {}", Parameters::Instance()->RecFileName);
                exit(0);
            }
            else
            {
                if (Parameters::Instance()->RecFileName.find(".root") == string::npos)
                    RecFileName = Parameters::Instance()->InputPath + "/" + Parameters::Instance()->RecFileName + std::to_string(my_fileNum) + ".root";
                else
                    RecFileName = Parameters::Instance()->RecFileName;

                spdlog::get(LOG_NAME)->info("Rec file name = {}", RecFileName);
                ReadoutFile = TFile::Open(RecFileName.c_str());
                if (!ReadoutFile)
                {
                    spdlog::get(ERROR_NAME)->error("Error opening Readout file {}, please check", RecFileName);
                    exit(-1);
                }
            }
        }
        if (DataFileType == 1 || GeoFilesType == 1)
        {
            if (Parameters::Instance()->TrigFileName.empty())
            {
                spdlog::get(ERROR_NAME)->error("Need Trig File (default is OutTrigd_*.root), but TrigFileName is {}", Parameters::Instance()->TrigFileName);
                exit(0);
            }
            else
            {
                if (Parameters::Instance()->TrigFileName.find(".root") == string::npos)
                    TrigFileName = Parameters::Instance()->InputPath + "/" + Parameters::Instance()->TrigFileName + std::to_string(my_fileNum) + ".root";
                else
                    TrigFileName = Parameters::Instance()->TrigFileName;

                spdlog::get(LOG_NAME)->info("Trigger file name = {}", TrigFileName);
                Trig_file = TFile::Open(TrigFileName.c_str());
                if (!Trig_file)
                {
                    spdlog::get(ERROR_NAME)->error("Error opening Trig file {}, please check", TrigFileName);
                    exit(-1);
                }
            }
        }
        if (Parameters::Instance()->OutputPrefix.find(".root") == string::npos)
            OutFileName = Parameters::Instance()->OutputPath + "/" + Parameters::Instance()->OutputPrefix + std::to_string(my_fileNum) + ".root";
        else
            OutFileName = Parameters::Instance()->OutputPrefix;
    }

    // void ConstructCell(TTree *SPACALCellTree);
    static Mixture_Calo *get_instance()
    {
        if (__instance == 0)
            __instance = new Mixture_Calo();

        return __instance;
    }

    void WriteSiTimingData();
    void WriteScintillatorData();
    void WritePVInfo_Geant4();
    void WritePVInfo_Flux();
    void WriteHitInfo_Geant4();
    void WritePVAndHitInfo_Rec();

    void WriteData();

    void SaveReadOutMap(int event);

    void Clustering();

    void DrawHit(float cell_size, int evtseq);
};

#endif