// Parameters
// Marco Pizzichemi 20.04.2020 marco.pizzichemi@cern.ch

//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef Parameters_h
#define Parameters_h 1

#include <iostream>
#include <string>
#include <fstream>

#include "ConfigFile.hh"


#include "TFile.h"
#include "TTree.h"

#include "Data.hh"

// class for reading multiple input parameters
class Parameters
{

public:
  // ctor
  Parameters();

  //! dtor
  ~Parameters();

  // Parameter about output setting
  bool SaveAll;
  bool SaveCellInfo;
  bool SaveCluster2D; //
  bool SaveCluster3D;
  bool SaveGeometry;
  bool SaveHitMap;
  bool SavePrimaryInfo;
  bool SaveRawInfo;
  int SaveMore = 0;
  bool PGun;

  ////////////////////////
  // Particle
  ///////////////////////
  bool ConstructGamma = 0;
  bool SaveGamma = 0;
  bool ConstructElectron = 0;
  bool SaveElectron = 0;
  bool ConstructPi0 = 0;
  bool SavePi0 = 0;
  //////////////////////

  ////////////////////////////
  // Physical Channel Candiadate
  /////////////////////////////
  std::string B2KstGamma_HardronFile;
  bool B2KstGamma;
  std::string B2D0GammaPi_HardronFile;
  bool B2D0GammaPi;
  std::string Bs02JPsiPi0_HardronFile;
  bool Bs02JPsiPi0;

  bool Bd02PipPimPi0;

   /////////////////////////////

  /////////////////////
  // Input
  ///////////////////////
  std::string InputPath = "1";

  int GeoFilesType;  // 1 : SimFile and trigger file 2: RecGeoFile
  int TruthFileType; // 1 : SimFile (both primary and hit info) 2 : Flux file (only primary) 3 : Rec File
  int DataFileType;  // 1: TrigFileName 2 : RecFile
  bool InitTruthInfo;
  bool tagCharge;

  std::string SimFileName;
  std::string TrigFileName;
  std::string FluxFileName;
  std::string RecFileName;
  std::string RecGeoFileName;
  std::string ChargeSampleFilePrefix;
  /////////////////////////////

  /// Output ////////////////
  std::string OutputPath;

  std::string OutputPrefix;

  std::string Module_Id_Map;
  /////////////////////////

  /////////////////////
  // SiTiming
  ////////////////////
  bool ConstructSiliconCell;
  ////////////////////

  float ecal_position;

  Data_t main_config;

  // structures for module config files
  std::map<int, Data_t> module_config; // Jiale Fei

  // structures for module config files name
  std::vector<std::string> module_config_list;

  // structures for Calibration files name
  std::vector<std::string> CalibrationFileName;

  // reconstruction type
  // 0 : clustering
  // 1 : calibration
  int ProgramType;

  // Seed Type
  //  0 : 2D seed->2DCluster->3DCluster
  //  1 : 2D seed->3Dseed->3DCluster
  //  2 : 3Dseed->3DCluster
  int CluFlow;
  bool RatioECor;

  // ecal module type
  std::vector<int> ecal_type;

  // read main config file
  void ReadMainConfig(std::string fileName);

  // read a module config file
  void ReadModuleConfig(std::string fileName, int ecal_type);

  // routine that actually reads config files
  struct Data_t FillStructure(std::string fileName);

  // print a config structure data
  void PrintConfig(Data_t data);

  // these members are static, which means that
  // no matter how many times this class will be called
  // they will be set only once. Combined with the setting
  // of fInstance in the constructor, and the return if
  // fInstance is already defined, this ensures that parameters
  // will be created only once, and makes it accessible from everywhere
  // in the program, provided that this header is included. Sounds
  // like a dirty trick to me, but it works so let's use it
  static Parameters *Instance() { return fInstance; };
  static Parameters *fInstance;

  void WriteParameters(TFile *outfile);
  void doWriteParameters(TFile *outfile, Data_t parameters);
  void CreateParamTree();

  TTree *paramTTree;
  // struct to save params in output file
  // just a service struct, this struct will not be saved
  // like it is in the ttree, but its values will
  Data_t paramStruct;
};

#endif /*Parameters_h*/
