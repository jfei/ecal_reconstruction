#ifndef FJL_Point_hh
#define FJL_Point_hh
#include "TVector3.h"

class FPoint : public TVector3
{
private:
    /* data */
public:
    using TVector3::TVector3;
    FPoint(float x, float y, float z) : TVector3(x, y, z)
    {
    }

    FPoint(TVector3 Pos) : TVector3(Pos)
    {
    }

    // FPoint operator*(double p)
    // {
    //     return FPoint(this->X() * p, this->Y() * p, this->Z() * p);
    // }
    // friend FPoint operator*(double p, FPoint &a);
    // friend FPoint operator+(FPoint &a, FPoint &b);
    // friend FPoint operator-(FPoint &a, FPoint &b);
    // friend double Dot(FPoint &a, FPoint &b);
    // friend double Length(FPoint &a);
    // friend FPoint Cross(FPoint &a, FPoint &b);
    // FPoint operator/(double p)
    // {
    //     if (p != 0)
    //         return FPoint(this->X() / p, this->Y() / p, this->Z() / p);
    //     else
    //     {
    //         spdlog::get(ERROR_NAME)->error("FPoint: 0 cannot be used as a divisor");
    //         exit(0);
    //     }
    // }
};
// FPoint operator*(double p, FPoint &a)
// {
//     return FPoint(a.X() * p, a.Y() * p, a.Z() * p);
// }

// FPoint operator+(FPoint &a, FPoint &b)
// {
//     return FPoint(a.X() + b.X(), a.Y() + b.Y(), a.Z() + b.Z());
// };
// FPoint operator-(FPoint &a, FPoint &b)
// {
//     return FPoint(a.X() - b.X(), a.Y() - b.Y(), a.Z() - b.Z());
// };

// double Dot(FPoint &a, FPoint &b)
// {
//     return a.X() * b.X() + a.Y() * b.Y() + a.Z() * b.Z();
// }
// double Length(FPoint &a)
// {
//     return sqrt(Dot(a, a));
// }
// FPoint Cross(FPoint &a, FPoint &b)
// {
//     return FPoint(a.Y() * b.Z() - a.Z() * b.Y(), a.Z() * b.X() - a.X() * b.Z(), a.X() * b.Y() - a.Y() * b.X());
// }

#endif