#ifndef FJL_Region_hh
#define FJL_Region_hh

#include "FDataType.hh"
#include "vector"
#include "TH2I.h"
#include "FPoint.hh"
#include "TRotation.h"
#include "FLog.hh"
#include "TF1.h"
#include "TFile.h"

class FCell3D;
class FCell;
class FCluster3D;
class FCluster2D;
class FModule;
class FLayer;
class FCalorimeter;

typedef FCluster3D *(*AlloCell3DCallBack)(FCell3D *, int); // this function should generate cluster 3D
typedef FCluster2D *(*AlloCellCallBack)(FCell *, int);     // this function should generate cluster 2D for layer
typedef void (*ReAlloCellCallBack)(FCluster2D *);          // this function should generate cluster 2D for layer

struct RegionEventInfo //  Except endep, only valid under ActivePrimaryInfo =1
{
private:
    int event = -7;
    bool PrimaryInfoStatus = 0;

    std::vector<float> endep_layer; // layer endep
    std::vector<float> endep_ActiveLayer; // layer endep
    std::vector<float> sig_layer;   // layer sig
    std::vector<float> layer_ShowerZ;
    std::vector<float> layer_ShowerX;
    std::vector<float> layer_ShowerY;

    float endepSiTiming = 0;
    float endepSilicon = 0;
    float Silicon_Sig = 0;
    float endep_total = 0; // total energy deposited in region
    float shower_z = 0;
    float shower_x = 0;
    float shower_y = 0;

public:
    void InitTotLayerNum(int LayerNum) //
    {
        endep_layer.clear();
        endep_ActiveLayer.clear();
        // std::cout << "endep_layer.size() : " << endep_layer.size() << std::endl;
        sig_layer.clear();
        v_Cluster2D.clear();
        layer_ShowerZ.clear();
        layer_ShowerX.clear();
        layer_ShowerY.clear();
        Seed_Num.clear();

        // std::cout << "LayerNum : " << LayerNum << std::endl;
        endep_layer.resize(LayerNum, 0);
        // std::cout << "endep_layer.size() : " << endep_layer.size() << std::endl;
        sig_layer.resize(LayerNum, 0);
        endep_ActiveLayer.resize(LayerNum, 0);
        layer_ShowerZ.resize(LayerNum, 0);
        layer_ShowerX.resize(LayerNum, 0);
        layer_ShowerY.resize(LayerNum, 0);
        Seed_Num.resize(LayerNum, 0);
        v_Cluster2D.resize(LayerNum);
    }
    std::vector<int> Seed_Num;           // first event id second seed num for each layer
    int MaxSeedNumLayerId;               // first event id second layer id;

    int Max2DNumLayerId;                 // first event id second layer id;
    std::vector<std::vector<FCluster2D *>> v_Cluster2D; // map by event

    float SiliconDepEnAccum = 0;

    void SetEventID(int EventID)
    {
        event = EventID;
    }
    int GetEventID()
    {
        return event;
    }
    void SetPrimaryInfoStatus(bool Status)
    {
        PrimaryInfoStatus = Status;
    }
    bool GetPrimaryInfoStatus()
    {
        return PrimaryInfoStatus;
    }

    void AccumulateTotEnDep(float increment)
    {

        endep_total += increment;
    }
    void AccumulateSiTimingEnDep(float increment)
    {

        endepSiTiming += increment;
    }
    void AccumulateSiliconSig(float increment)
    {

        Silicon_Sig += increment;
    }
    void AccumulateSiliconEnDep(float increment)
    {

        endepSilicon += increment;
    }
    void AccumulateLayerEnDep(float increment, int layer_id = 0)
    {

        endep_layer.at(layer_id) += increment;
    }
    void AccumulateActiveLayerEnDep(float increment, int layer_id = 0)
    {

        endep_ActiveLayer.at(layer_id) += increment;
    }
    void AccumulateLayerSig(float increment, int layer_id = 0)
    {
        sig_layer.at(layer_id) += increment;
    }
    /////////////
    void SetLayerEnDep(float input, int layer_id = 0)
    {
        // std::cout << "endep_layer.size() : " << endep_layer.size() << std::endl;
        endep_layer.at(layer_id) = input;
    }
    void SetActiveLayerEnDep(float input, int layer_id = 0)
    {
        // std::cout << "endep_layer.size() : " << endep_layer.size() << std::endl;
        endep_ActiveLayer.at(layer_id) = input;
    }
    void SetLayerSig(float input, int layer_id = 0)
    {
        sig_layer.at(layer_id) = input;
    }
    float GetLayerEnDep(int layer_id = 0)
    {
        return endep_layer.at(layer_id);
    }
    float GetActiveLayerEnDep(int layer_id = 0)
    {
        return endep_ActiveLayer.at(layer_id);
    }
    float GetLayerSig(int layer_id = 0)
    {
        return sig_layer.at(layer_id);
    }
    ////////////////////
    void AccumulateLayerShowerZ(float increment, int layer_id = 0)
    {
        layer_ShowerZ.at(layer_id) += increment;
    }
    void SetLayerShowerZ(float input, int layer_id = 0)
    {
        layer_ShowerZ.at(layer_id) = input;
    }
    float GetLayerShowerZ(int layer_id = 0)
    {
        return layer_ShowerZ.at(layer_id);
    }
    void AccumulateLayerShowerX(float increment, int layer_id = 0)
    {
        layer_ShowerX.at(layer_id) += increment;
    }
    void SetLayerShowerX(float input, int layer_id = 0)
    {
        layer_ShowerX.at(layer_id) = input;
    }
    float GetLayerShowerX(int layer_id = 0)
    {
        return layer_ShowerX.at(layer_id);
    }
    void AccumulateLayerShowerY(float increment, int layer_id = 0)
    {
        layer_ShowerY.at(layer_id) += increment;
    }
    void SetLayerShowerY(float input, int layer_id = 0)
    {
        layer_ShowerY.at(layer_id) = input;
    }
    float GetLayerShowerY(int layer_id = 0)
    {
        return layer_ShowerY.at(layer_id);
    }

    /////////////////
    void ClearEnDep()
    {
        endep_total = 0;
        endepSiTiming = 0;
        endepSilicon = 0;
        SiliconDepEnAccum = 0;
        for (int loop = 0; loop < endep_layer.size(); loop++)
        {
            endep_layer.at(loop) = 0;
        }
    }
    void ClearSig()
    {
        for (int loop = 0; loop < sig_layer.size(); loop++)
        {
            sig_layer.at(loop) = 0;
        }
    }
    void ClearAll()
    {
        ClearEnDep();
        ClearSig();
        for (int loop = 0; loop < sig_layer.size(); loop++)
        {
            Seed_Num.at(loop) = 0;
        }
    }
    void SetTotEnDep(float input)
    {

        endep_total = input;
    }
    float GetTotEnDep()
    {
        return endep_total;
    }
    void SetSiTimingEnDep(float input)
    {
        endepSiTiming = input;
    }
    float GetSiTimingEnDep()
    {
        return endepSiTiming;
    }
    void SetSiliconSig(float input)
    {
        Silicon_Sig = input;
    }
    float GetSiliconSig()
    {
        return Silicon_Sig;
    }
    void SetSiliconEnDep(float input)
    {
        endepSilicon = input;
    }
    float GetSiliconEnDep()
    {
        return endepSilicon;
    }
    float GetShowerZ()
    {
        return shower_z;
    }
    void SetShowerZ(float input)
    {
        shower_z = input;
    }
    float GetShowerX()
    {
        return shower_x;
    }
    void SetShowerX(float input)
    {
        shower_x = input;
    }
    float GetShowerY()
    {
        return shower_y;
    }
    void SetShowerY(float input)
    {
        shower_y = input;
    }
    void ClearShowerZ()
    {
        shower_z = 0;
    }
};

class RegionLayerInfo
{

public:
    RegionLayerInfo() {
    };
    float layerPos; // position relative to module z position
    int layerType;  // 0 : Scintillator layer 1 : Silicon Layer
    float layerThickness;
    int WindowShape4Layers;

    float Seed2DCut = 40;
    float CellSize;
    bool OnlySeedT = 0;
    TH2I cell_map;
    TH1F *TransversalProfile_Cell_N;
    TH1F *TransversalProfile_Cell_P;
    TH1F *TransversalProfile_Seed_Down_Corner;
    TH1F *TransversalProfile_Seed_Down_Edge;
    TH1F *TransversalProfile_Seed_Up_Corner;
    TH1F *TransversalProfile_Seed_Up_UpCorner;
    TH1F *TransversalProfile_Seed_Up_Edge;
    TF1 *CellECaliFun;
    TF1 *PosXResFun;
    TF1 *PosYResFun;
    TF1 *CluType0SCorX;
    TF1 *CluType0SCorY;
    TF1 *CluTypeP1SCorX;
    TF1 *CluTypeP1SCorY;
    TF1 *CluTypeM1SCorX;
    TF1 *CluTypeM1SCorY;
    TF1 *CluTypeP2SCorX;
    TF1 *CluTypeP2SCorY;
    TF1 *CluTypeM2SCorX;
    TF1 *CluTypeM2SCorY;
    TF1 *CluTypeP3SCorX;
    TF1 *CluTypeP3SCorY;
    TF1 *CluTypeM3SCorX;
    TF1 *CluTypeM3SCorY;
    TF1 *S1xCorPFun;
    TF1 *S1xCorNFun;
    TF1 *S1yCorPFun;
    TF1 *S1yCorNFun;
    TF1 *LCorrFun;
    TF1 *TShiftFun;
    TF1 *TResFun;
    TF1 *EResFun;
    TF1 *ECaliFun;

    int CellECaliFunType;
    AlloCellCallBack AlloCell_CAllBack;
    ReAlloCellCallBack ReAlloCell_CallBack;
};

class FRegion
{
private:
    /* data */
public:
    FRegion(int r_type, int r_GlobalId) : type(r_type), GlobalId(r_GlobalId)
    {
    }
    ~FRegion()
    {
    }
    FCalorimeter *f_calo;

    std::string CaliFileName = "";
    TFile *CaliFile = NULL;

    bool Have_SiTiming = 0;
    float SiTimingThickness = 0;
    int SiTimingLayerNum = 0;
    float SiTimingCellSize;
    float SiTimingNoise;
    float EnPerMIP;

    int SplitLayer;

    float Cell3DSize;
    float Separator_position;
    float FrontFace;
    int type;
    int GlobalId;
    int detector_type;    // 0 : SPACAL-SiTiming 1 : Shashlik
    bool using3DSeed = 0; // When Using One Seed For Each Region, not use yet
    float RegionShift = 7777;
    float Seed3DCut;
    int Seed3DMethod;
    int WindowShape;
    int CellNx;
    int CellNy;
    int FiberNumX = 0;
    int FiberNumY = 0;

    float ax;
    float ay;
    float az;
    TRotation Ro;

    int TotLayerNum;
    std::vector<RegionLayerInfo *> Layers;

    TH2I cell3D_map;

    TF1 *CluECaliFun;
    TF1 *CluECor2Fun;
    TF1 *CluType0SCorX;
    TF1 *CluType0SCorY;
    TF1 *CluTypeP1SCorX;
    TF1 *CluTypeP1SCorY;
    TF1 *CluTypeM1SCorX;
    TF1 *CluTypeM1SCorY;
    TF1 *CluTypeP2SCorX;
    TF1 *CluTypeP2SCorY;
    TF1 *CluTypeM2SCorX;
    TF1 *CluTypeM2SCorY;
    TF1 *CluTypeP3SCorX;
    TF1 *CluTypeP3SCorY;
    TF1 *CluTypeM3SCorX;
    TF1 *CluTypeM3SCorY;
    TF1 *LCorrFun;
    TF1 *TShiftFun;
    TF1 *TResFun;
    TF1 *PosXResFun;
    TF1 *PosYResFun;
    TF1 *EResFun;
    TH1F *TransversalProfile_Cell_N;
    TH1F *TransversalProfile_Cell_P;
    TH1F *TransversalProfile_Seed_Down_Corner;
    TH1F *TransversalProfile_Seed_Down_Edge;
    TH1F *TransversalProfile_Seed_Up_Corner;
    TH1F *TransversalProfile_Seed_Up_UpCorner;
    TH1F *TransversalProfile_Seed_Up_Edge;
    TF1 *ERatio_FrontE;
    TF1 *ERatio_FrontE_Resolution;
    TF1 *ERatio_E;
    TF1 *ERatio_E_Resolution;
    // bool DoSCorr;
    bool DoS1Cor;
    // bool UsingPar;
    bool CombineSiEnWhenCali;
    bool CombineEnergyPoint2R;
    bool CombineEnergyPoint2T;

    bool EnableLayerInfo;

    double leak_ratioP0;
    double leak_ratioP1;
    std::vector<float> range;

    std::vector<FModule *> module_container;
    std::vector<RegionEventInfo> EventInfo;
    void InitEventInfo();

    int GetCellLocalIdByRelaPos(float x_pos, float y_pos, float z_pos);
    int GetCellLocalIdByRelaPos(float x_pos, float y_pos, int layer_id);
    int GetCellLocalIdByRelaPos(double x_pos, double y_pos, double z_pos);
    int GetCell3DLocalIdByRelaPos(float x_pos, float y_pos);

    FPoint Global2Local(float GlobalX, float GlobalY, float GlobalZ);
    FPoint Local2Global(float LocalX, float LocalY, float LocalZ);

    FPoint Global2Local(FPoint GlobalPos);
    FPoint Local2Global(FPoint LocalPos);

    void CalculateCellGlobalPos();

    int GetID()
    {
        return GlobalId;
    }

    int GetType()
    {
        return type;
    }

    int Get_layerid(float layer_Z)
    {
        int Zid;
        if (Layers.size() == 0) // if layer not initial
        {
            spdlog::get(ERROR_NAME)->error("layer not initial");
            return -1;
        }
        for (int i = 0; i < Layers.size(); i++)
        {

            if (fabs(layer_Z - Layers[i]->layerPos) <= Layers[i]->layerThickness / 2)
            {
                Zid = i;
                break;
            }
            if (i == Layers.size() - 1)
            {
                Zid = -1;
            }
        }

        return Zid;
    }

    int InSiTiming(float layer_Z)
    {
        if (Have_SiTiming == 0)
            return 0;
        if (fabs(layer_Z - Separator_position) <= SiTimingThickness / 2)
        {
            if ((layer_Z - Separator_position) < 0)
                return 1;
            if ((layer_Z - Separator_position) >= 0)
                return 2;
        }
            return 0;
    }

    uint64 CalculateFullId(int type, float ax, float ay, float az, int Nx, int Ny);
    uint64 CalculateTypeId(int type, float CellSize, float ax, float ay, float az);

    TH2I *Get_cell_mapBylayerPos(float layer_Z)
    {
        int tmp_layer_id;
        tmp_layer_id = Get_layerid(layer_Z);
        if (tmp_layer_id == -1)
            return 0;
        return &(Layers[Get_layerid(layer_Z)]->cell_map);
    }

    AlloCell3DCallBack AlloCell3D_CAllBack;
};

#endif