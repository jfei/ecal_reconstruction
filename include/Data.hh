#include "iostream"
#include "vector"
#include "TTree.h"

// a structure to hold all the config file keys
struct Data_t
{
  int ecal_position;
  float SiTimingNoise;
  float SiTimingCellSize;
  float EnPerMIP;
  float Seed3DCut;
  int Seed3DMethod;

  int WindowShape;

  int SplitLayer; // 用哪一层的2Dcluster来分割3Dcluster

  std::vector<int> WindowShape4Layers;

  std::vector<float> Seed2DCut;
  std::vector<float> EnergyPer;
  std::vector<float> ScanRadius;
  bool OnlySeedT;

  bool DoS1Cor;

  bool EnableLayerInfo;


  bool CombineSiEnWhenCali;
  // bool UsingPar;
  bool DoSCor;
  int CellECaliFunType;
  double SiliconCellTCali;

  bool CombineEnergyPoint2T;


  bool CombineEnergyPoint2R;

};
