#ifndef FJL_Cell_hh
#define FJL_Cell_hh

#include "FDataType.hh"
#include "vector"
#include "cmath"
#include "FPoint.hh"
#include "map"
#include "FLog.hh"
#include "TH1F.h"

class FCluster3D;
class FCluster2D;
class FCell3D;
class FModule;
class FLayer;
class FCellEventInfo;

class FCell
{

public:
    FCell(int c_id, float c_x, float c_y, float c_z, float c_dx, float c_dy, float c_dz, float c_ax, float c_ay, float c_az) : dy(c_dy), dx(c_dx), dz(c_dz),
                                                                                                                               ay(c_ay), ax(c_ax), az(c_az),
                                                                                                                               id(c_id)
    {
        SetLocalPosition(c_x, c_y, c_z);
    }

protected:
    std::vector<FCellEventInfo *> EventInfo;
    FLayer *f_layer;
    FModule *f_module;
    FCell3D *f_Cell3D;
    int id;
    uint64 Global_id;

    FPoint LocalPos;
    FPoint GlobalPos;

    float dx;
    float dy;
    float dz;
    float ax;
    float ay;
    float az;

    int layer_id;

public:
    FCluster2D *ConstructClu2D(int event);
    void InitEventInfo();

    float GetEnergy(int event);

    float GetRawSignal(int event);

    float GetTime(int event);

    float GetRawTime(int event);

    float GetZ3D(int event);

    float GetEt(int event);

    int GetMotherClu3DNum(int event);

    bool BelongTo(FCluster3D *Clu3D);

    bool BelongTo(FCluster2D *Clu2D);

    void RemoveMotherClu2D(FCluster2D *Clu2D);

    void AddMotherClu2D(FCluster2D *Clu2D, float fraction = -1);

        float GetEnergy(FCluster3D *Clu3D);

    float GetEnergy(FCluster2D *Clu2D);

    float GetFraction(FCluster3D *Clu3D);

    float GetFraction(FCluster2D *Clu2D);

    float GetHistoryEnergy(FCluster3D *Clu3D);

    float GetHistoryEnergy(FCluster2D *Clu2D);

    float GetHistoryFraction(FCluster3D *Clu3D);

    float GetHistoryFraction(FCluster2D *Clu2D);

    FCluster2D *GetMotherClu2D(int event, int i = 0);

    void SetEnergy(int event, float input);

    void SetEnergy(FCluster3D *Clu3D, float input);

    void SetEnergy(FCluster2D *Clu2D, float input);

    void SetFraction(FCluster3D *Clu3D, float input);

    void SetFraction(FCluster2D *Clu2D, float input);

    void SetRawSignal(int event, float input);

    void SetTime(int event, float input);

    void SetRawTime(int event, float input);

    void SetTotEnDep(int event, float input);

    void SetZ3D(int event, float input);

    void SetEt(int event, float input);

    void AccumulateTotEnDep(int event, float increment);

    std::map<int, std::vector<FCell *>> Neighbours; // 0:Nei in same module 1:Nei in same type and id region 2: Nei in same type region

    virtual int IsLocalMax(int event);

    virtual void FindNeighbours();

    void FindNeighbours_3D_3_3();

    virtual uint64 GetGlobalId();

    static uint64 CalculateGlobalId(int region_id, int module_id, int localCell_id, int layer_id = 0);

    // virtual void FindNeighbor();

    void SetGlobal_id(uint64 f_id)
    {
        Global_id = f_id;
    }

    FPoint GetGlobalPosition()
    {
        return GlobalPos;
    }
    void SetGlobalPosition(float i_x, float i_y, float i_z)
    {

        GlobalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetGlobalPosition(FPoint input)
    {

        GlobalPos = input;
    }
    void SetX(float input)
    {
        GlobalPos.SetX(input);
    }
    void SetY(float input)
    {
        GlobalPos.SetY(input);
    }
    void SetZ(float input)
    {
        GlobalPos.SetZ(input);
    }
    void SetDx(float input)
    {
        dx = input;
    }
    void SetDy(float input)
    {
        dy = input;
    }
    void SetDz(float input)
    {
        dz = input;
    }
    void SetLayerID(int input)
    {
        layer_id = input;
    }
    float X()
    {
        return GlobalPos.X();
    }
    float Y()
    {
        return GlobalPos.Y();
    }
    float Z()
    {
        return GlobalPos.Z();
    }
    float Ax()
    {
        return ax;
    }
    float Ay()
    {
        return ay;
    }
    float Az()
    {
        return az;
    }
    FPoint GetLocalPosition()
    {
        return LocalPos;
    }
    void SetLocalPosition(float i_x, float i_y, float i_z)
    {

        LocalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetLocalPosition(FPoint input)
    {

        LocalPos = input;
    }
    float LocalX()
    {
        return LocalPos.X();
    }
    float LocalY()
    {
        return LocalPos.Y();
    }
    float LocalZ()
    {
        return LocalPos.Z();
    }
    float Dx()
    {
        return dx;
    }
    float Dy()
    {
        return dy;
    }
    float Dz()
    {
        return dz;
    }
    int LayerId()
    {
        return layer_id;
    }
    FLayer *MotherLayer()
    {
        return f_layer;
    }
    FCell3D *MotherCell3D()
    {
        return f_Cell3D;
    }
    FModule *MotherModule()
    {
        return f_module;
    }
    void SetMotherLayer(FLayer *input)
    {
        f_layer = input;
    }
    void SetMotherCell3D(FCell3D *input)
    {
        f_Cell3D = input;
    }
    void SetMotherModule(FModule *input)
    {
        f_module = input;
    }
    FLayer *MotherVolume()
    {
        return MotherLayer();
    }
    int GetID()
    {
        return id;
    }
    void SetID(int input)
    {
        id = input;
    }

    TH1F *GetTranverseProfile(FCluster2D *Clu2D);
    float GetTruRatioOfClu2D(FCluster2D *Clu2D);
};
#endif
