#ifndef FJL_Cluster3D_hh
#define FJL_Cluster3D_hh

#include "FDataType.hh"
#include "FPoint.hh"
#include "iostream"

class FLayer;
class FRegion;
class FModule;
class FCell;
class FCell3D;
class FCluster2D;

class FCluster3D
{
private:
    /* data */
public:
    FCluster3D(int i_event)
    {
        LocalPos.SetXYZ(0, 0, 0);
        GlobalPos.SetXYZ(0, 0, 0);
        RawGlobalPos.SetXYZ(0, 0, 0);
        time = 0;
        CellNumber = 0;
        event = i_event;
    }
    FCluster3D(FCell3D *i_Seed3D, int i_event);
    std::vector<float> Dist2Tru;
    std::vector<float> DeltaE_E2Tru;
    std::map<int, std::vector<FCell3D *>> Cluster_Cell3D; // Comparison with seed 0:Cell in same module 1:Nei in same type and id region 2: Nei in same type region
    std::vector<FCluster2D *> v_Cluster2D; // first region
    FCell3D *Seed3D = NULL;
    int event;
    float Seed3Det = 0;
    FPoint LocalPos;
    FPoint GlobalPos;
    FPoint EntryPos;
    FPoint RawGlobalPos;
    float RawE = 0;
    float time = 0;
    float RawTime = 0;
    float energy = 0;
    float tmpCorE = 0;
    float E3 = 0;
    int Type;
    int XType;
    int YType;
    int MatchedNum = -1;
    float MinDist2ChargeTrack = 777777777;

    FCluster3D *f_Clu3D=NULL;
    int CellNumber = 0;
    int AfterSCor = 0;
    int AfterLCor = 0;
    int AfterECor = 0;
    int AfterTCor = 0;

    struct Compare
    {
        bool operator()(const FCluster3D *lhs, const FCluster3D *rhs) const
        {
            if (lhs->v_Cluster2D.size() != rhs->v_Cluster2D.size())
            {
                return lhs->v_Cluster2D.size() < rhs->v_Cluster2D.size(); // 小的在前
            }

            // 检查所有元素是否相同
            for (size_t i = 0; i < lhs->v_Cluster2D.size(); ++i)
            {
                if (lhs->v_Cluster2D[i] != rhs->v_Cluster2D[i])
                {
                    return lhs->v_Cluster2D[i] < rhs->v_Cluster2D[i]; // 对于不同元素，使用小于运算符
                }
            }

            // 如果所有元素都相同，返回 false (认为 lhs 和 rhs 是相等的)
            return false;
        }
    };

    FRegion *f_region = NULL;
    FModule *f_module = NULL;

    float inline GetTime()
    {
        return time;
    }
    void inline SetTime(float input)
    {
        time = input;
    }

    float inline GetRawTime()
    {
        return RawTime;
    }
    void inline SetRawTime(float input)
    {
        RawTime = input;
    }

    void CalCellFraction();

    void ECorr();
    void CorrectionPos(int nPV = -1);
    void SCorrection();
    void LCorrection(int nPV = -1);
    float TCorr();

    int IsMissingXCell();
    int IsMissingYCell();

    FPoint GetGlobalPosition()
    {
        return GlobalPos;
    }
    void SetGlobalPosition(float i_x, float i_y, float i_z)
    {

        GlobalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetGlobalPosition(FPoint input)
    {

        GlobalPos = input;
    }
    void SetX(float input)
    {
        GlobalPos.SetX(input);
    }
    void SetY(float input)
    {
        GlobalPos.SetY(input);
    }
    void SetZ(float input)
    {
        GlobalPos.SetZ(input);
    }
    float X()
    {
        return GlobalPos.X();
    }
    float Y()
    {
        return GlobalPos.Y();
    }
    float Z()
    {
        return GlobalPos.Z();
    }

    void SetEntryPosition(float i_x, float i_y, float i_z)
    {

        EntryPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetEntryPosition(FPoint input)
    {

        EntryPos = input;
    }
    void SetEntryX(float input)
    {
        EntryPos.SetX(input);
    }
    void SetEntryY(float input)
    {
        EntryPos.SetY(input);
    }
    void SetEntryZ(float input)
    {
        EntryPos.SetZ(input);
    }
    float EntryX()
    {
        return EntryPos.X();
    }
    float EntryY()
    {
        return EntryPos.Y();
    }
    float EntryZ()
    {
        return EntryPos.Z();
    }

    FPoint GetLocalPosition()
    {
        return LocalPos;
    }
    void SetLocalPosition(float i_x, float i_y, float i_z)
    {

        LocalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetLocalPosition(FPoint input)
    {

        LocalPos = input;
    }
    float LocalX()
    {
        return LocalPos.X();
    }
    float LocalY()
    {
        return LocalPos.Y();
    }
    float LocalZ()
    {
        return LocalPos.Z();
    }

    FPoint GetRawPosition()
    {
        return RawGlobalPos;
    }
    void SetRawPosition(float i_x, float i_y, float i_z)
    {

        RawGlobalPos.SetXYZ(i_x, i_y, i_z);
    }
    void SetRawPosition(FPoint input)
    {

        RawGlobalPos = input;
    }
    void SetRawX(float input)
    {
        RawGlobalPos.SetX(input);
    }
    void SetRawY(float input)
    {
        RawGlobalPos.SetY(input);
    }
    void SetRawZ(float input)
    {
        RawGlobalPos.SetZ(input);
    }
    float RawX()
    {
        return RawGlobalPos.X();
    }
    float RawY()
    {
        return RawGlobalPos.Y();
    }
    float RawZ()
    {
        return RawGlobalPos.Z();
    }

    float RawEnergy()
    {
        return RawE;
    }
    float Energy()
    {
        if (AfterECor == 1)
            return energy;
        else
            return RawE;
    }
    void SetRawE(float input)
    {
        RawE = input;
    }
    void SetEnergy(float input)
    {
        energy = input;
    }
    void RecRawInfoByCells();

    void RecRawInfoByClu2D();

    void CalCulateInfo2Tru();

    float GetDist2Tru(int i = 0)
    {

        return Dist2Tru.at(i);
    }

    float GetDeltaE_E2PV(int i = 0)
    {
        return DeltaE_E2Tru.at(i);
    }

    void CalCulateEntryPos(int i = -1);

    void InitCluType();

    void AddCell3D(FCell3D *Cell3D);

    void AddCell3D(FCell3D *Cell3D, int Cell3DType);

    void RemoveCell3D(FCell3D *Cell3D);

    void RemoveCell3D(FCell3D *Cell3D, int Cell3DType);

    float GetTRes();

    float GetERes();

    float GetSeedE();

    float GetMinDist2Charge()
    {
        return MinDist2ChargeTrack;
    }

    void SetMinDist2Charge(float input)
    {
        MinDist2ChargeTrack = input;
    }
};
#endif