// Class for creating output ttrees

#ifndef CreateTree_H
#define CreateTree_H 1

#include <iostream>
#include <vector>
#include <map>
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TGraph.h"

#include "Parameters.hh"

class CreateTree
{
private:
  TTree *ftree;
  TString fname;

public:
  CreateTree(TString name);

  ~CreateTree();

  TTree *GetTree() const { return ftree; };
  TString GetName() const { return fname; };

  int Fill();
  bool Write(TFile *);

  void Clear();
  static CreateTree *Instance() { return fInstance; };
  static CreateTree *fInstance;

  int Run;
  int Event;

  // to be filled at the beginning of the event generation only
  TTree *Parameters;
  int ComputationTime;
  int Seed3DNum;
  std::vector<int> Seed2DNum;
  int NLM_MaxInFork;
  int NLM_MaxInCross;
  int LMNum_3_3;
  int LMNum_Fork;
  int LMNum_Cross;

  TTree *OutTree;
  TTree *ReadoutInfo;
  TTree *PrimaryTree;
  TTree *Gamma;
  TTree *SubGamma;
  TTree *Pi0;
  TTree *Electron;

  float HitDist;
  int Pi0_TruType;
  int Pi0_Type;
  float Pi0_TruM;
  float Pi0_M;
  float Pi0_TruE;
  float Pi0_E;
  float Pi0_TruPx;
  float Pi0_Px;
  float Pi0_TruPy;
  float Pi0_Py;
  float Pi0_TruPz;
  float Pi0_Pz;
  float Pi0_TruPt;
  float Pi0_Pt;
  int Pi0_MatchIndex;
  int Pi0_Gamma1_MatchIndex;
  int Pi0_Gamma2_MatchIndex;
  int Pi0_Gamma1_Module;
  int Pi0_Gamma1_RegionId;
  int Pi0_Gamma1_RegionType;
  float Pi0_Gamma1_E;
  float Pi0_Gamma1_Px;
  float Pi0_Gamma1_Py;
  float Pi0_Gamma1_Pz;
  float Pi0_Gamma1_Pt;
  int Pi0_Gamma2_Module;
  int Pi0_Gamma2_RegionId;
  int Pi0_Gamma2_RegionType;
  float Pi0_Gamma2_E;
  float Pi0_Gamma2_Px;
  float Pi0_Gamma2_Py;
  float Pi0_Gamma2_Pz;
  float Pi0_Gamma2_Pt;

  int Pi0_Gamma1_TruModule;
  int Pi0_Gamma1_TruRegionId;
  int Pi0_Gamma1_TruRegionType;
  float Pi0_Gamma1_TruE;
  float Pi0_Gamma1_TruPx;
  float Pi0_Gamma1_TruPy;
  float Pi0_Gamma1_TruPz;
  float Pi0_Gamma1_TruPt;

  int Pi0_Gamma2_TruModule;
  int Pi0_Gamma2_TruRegionId;
  int Pi0_Gamma2_TruRegionType;
  float Pi0_Gamma2_TruE;
  float Pi0_Gamma2_TruPx;
  float Pi0_Gamma2_TruPy;
  float Pi0_Gamma2_TruPz;
  float Pi0_Gamma2_TruPt;
  /////////////////////////
  float Gamma_Tru_px;
  float Gamma_Tru_py;
  float Gamma_Tru_pz;
  float Gamma_Tru_E;
  float Gamma_Tru_T;
  float Gamma_px;
  float Gamma_py;
  float Gamma_pz;
  float Gamma_E;
  float Gamma_T;
  int Gamma_Tru_Region;
  int Gamma_Tru_RegionType;
  int Gamma_Region;
  int Gamma_RegionType;
  int Gamma_Type;
  ////////////////////////

  /// Calo event info /
  int ParticleGun;
  int n_PV;
  std::vector<int> primaryPDGID;
  std::vector<float> PositionOnAbsorberX;
  std::vector<float> PositionOnAbsorberY;
  std::vector<float> PositionOnAbsorberZ;
  std::vector<float> MomentumOnAbsorberX;
  std::vector<float> MomentumOnAbsorberY;
  std::vector<float> MomentumOnAbsorberZ;
  std::vector<float> PositionOnAbsorberT;
  std::vector<float> EnergyOnAbsorber;

  std::vector<float> PositionAtVertexX;
  std::vector<float> PositionAtVertexY;
  std::vector<float> PositionAtVertexZ;
  std::vector<float> MomentumAtVertexX;
  std::vector<float> MomentumAtVertexY;
  std::vector<float> MomentumAtVertexZ;
  std::vector<float> TimeAtVertex;
  std::vector<float> EnergyAtVertex;
  std::vector<int> HitRegion;
  std::vector<int> HitRegionType;
  std::vector<int> HitModule;

  ///////////////////
  float TotEnDep;
  int MaxDepRegion;

  float ShowerZ;
  float ShowerX;
  float ShowerY;
  /// Region event info /

  /// @following size regionNum*LayerNum

  float *RegionLayerSig;
  float *RegionLayerEnDep;
  float *RegionActiveLayerEnDep;
  float *RegionLayerShowerZ;
  float *RegionLayerShowerX;
  float *RegionLayerShowerY;
  float *RegionLayerFrontFace;
  float *RegionLayerThickness;
  float *RegionLayerPos;

  /// @following size regionNum
  float *RegionTotEnDep;
  int *RegionTotLayerNum;
  float *RegionEnDepSiTiming;
  float *RegionEnDepSilicon;
  float *RegionSiliconSig;
  float *RegionShowerZ;
  float *RegionShowerX;
  float *RegionShowerY;
  float *RegionFrontFace;
  /// Module event info/

  /// Cell event info/
  std::vector<std::vector<float>> CellRawSignal;
  std::vector<std::vector<float>> CellEnergy;
  std::vector<std::vector<float>> CellRawTime;
  std::vector<std::vector<float>> CellTime;

  int CluFlow;
  int Clu3DModule;
  int Clu3DRegion;
  int Clu3DRegionType;
  int Clu3Devent;
  int Clu3DTypeX;
  int Clu3DTypeY;
  float Clu3Dx;
  float Clu3Dy;
  float Clu3Dz;
  float Clu3DEntryX;
  float Clu3DEntryY;
  float Clu3DEntryZ;
  float Clu3DRawX;
  float Clu3DRawY;
  float Clu3DRawZ;
  int Clu3DMatchIndex;

  float Clu3Dtime;
  float Clu3Denergy;
  float Clu3DTmpCorE;
  float Clu3DRawTime;
  float Clu3DRawEnergy;
  float Seed3Det;
  float Seed3Dx;
  float Seed3Dy;
  float Seed3Dz;
  float Seed3DLocalX;
  float Seed3DLocalY;
  float Seed3DLocalZ;
  float Seed3Ddx;
  float Seed3Ddy;
  float Seed3Ddz;
  float Seed3DE;
  float Seed3DT;
  float Clu3DE3;
  int Cell3DNum;
  std::vector<float> Cell3DT;
  std::vector<float> Cell3DX;
  std::vector<float> Cell3DY;
  std::vector<float> Cell3DZ;
  std::vector<float> Cell3DE;
  std::vector<int> Cell3DType;

  std::vector<int> Clu2DRegion;
  std::vector<int> Clu2DModule;
  std::vector<int> Clu2DRegionType;
  std::vector<float> Clu2DRawTime;
  std::vector<float> Clu2DTime;
  std::vector<float> Clu2DEnergy;
  std::vector<float> Clu2DRawE;
  std::vector<int> Clu2D_id;
  std::vector<float> Clu2DPos;
  std::vector<float> Clu2DThickness;

  std::vector<float> Clu2DLocalX;
  std::vector<float> Clu2DLocalY;
  std::vector<float> Clu2DLocalZ;
  std::vector<float> Clu2DS0CorX;
  std::vector<float> Clu2DS0CorY;
  std::vector<float> Clu2DX;
  std::vector<float> Clu2DRawX;
  std::vector<float> Clu2DY;
  std::vector<float> Clu2DRawY;
  std::vector<float> Clu2DZ;
  std::vector<float> Clu2DRawZ;
  std::vector<float> Clu2DSeedX;
  std::vector<float> Clu2DSeedY;
  std::vector<float> Clu2DSeedZ;
  std::vector<float> Clu2DSeedT;
  std::vector<float> Clu2DSeedE;
  std::vector<float> Clu2DE3;
  std::vector<float> Clu2DE5;
  std::vector<float> Clu2DE7;
  std::vector<float> Clu2DE9;
  std::vector<float> Clu2DCellDx;
  std::vector<float> Clu2DCellDy;
  std::vector<float> Clu2DAx;
  std::vector<float> Clu2DAy;
  std::vector<float> Clu2DAz;
  std::vector<float> Modulex;
  std::vector<float> Moduley;
  std::vector<float> Modulez;
  std::vector<int> Clu2DTypeX;
  std::vector<int> Clu2DTypeY;
  std::vector<int> Clu2DCellNum;
  std::vector<float> CellT;
  std::vector<float> CellX;
  std::vector<float> CellY;
  std::vector<float> CellZ;
  std::vector<float> CellE;
  std::vector<int> CellType;

  //////////////////////////
  // Truth Info
  //////////////////////////
  /////////////////
  //////  2D  /////
  /////////////////
  std::vector<float> LayerTruLocalX;
  std::vector<float> LayerTruLocalY;
  std::vector<float> LayerTruLocalZ;
  std::vector<float> LayerTruShowerDepth;
  std::vector<float> LayerTruX;
  std::vector<float> LayerTruY;
  std::vector<float> LayerTruZ;
  std::vector<float> LayerTruDep;
  /////////////////
  //////  3D  /////
  /////////////////
  float TruX;
  float TruY;
  float TruZ;
  float TruDep;
  float TruLocalX;
  float TruLocalY;
  float TruLocalZ;
  float TruShowerDepth;
};

#endif
