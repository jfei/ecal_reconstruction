#ifndef FJL_Module_hh
#define FJL_Module_hh
#include "map"
#include <iomanip>
#include "TCanvas.h"
#include "TH2F.h"
#include "set"
class FCalorimeter;
class FRegion;
class FCluster3D;
class FCell3D;
class FLayer;
class FCell;

class FModuleEventInfo;

class FModule
{
private:
    /* data */
public:
    FModule(int m_type, int m_id, float m_x, float m_y, float m_z, float m_dx, float m_dy, float m_dz, float m_ax, float m_ay, float m_az) : x(m_x), y(m_y), z(m_z),
                                                                                                                                             dy(m_dy), dx(m_dx), dz(m_dz),
                                                                                                                                             type(m_type), GlobalId(m_id),
                                                                                                                                             ay(m_ay), ax(m_ax), az(m_az)
    {
    }
    float x;
    float y;
    float z;
    float dx;
    float dy;
    float dz;
    float ax;
    float ay;
    float az;
    int type;
    int GlobalId;
    FRegion *f_region;
    FCalorimeter *f_calo;

    void FindNeighbours();

    std::vector<FModuleEventInfo *> EventInfo;
    void InitEventInfo();
    FModuleEventInfo *GetEventInfo(int event);

    std::vector<FLayer *> v_layer;   // layer vector
    std::vector<FCell3D *> v_Cell3D; // cell vector
    std::vector<FModule *> Neighbours;

    TCanvas *Module_Canvas = NULL;


    int GetID()
    {
        return GlobalId;
    }
    void SetID(int input)
    {
        GlobalId = input;
    }
    float X()
    {
        return x;
    }
    float Y()
    {
        return y;
    }
    float Z()
    {
        return z;
    }
    float Dx()
    {
        return dx;
    }
    float Dy()
    {
        return dy;
    }
    float Dz()
    {
        return dz;
    }
    float Ax()
    {
        return ax;
    }
    float Ay()
    {
        return ay;
    }
    float Az()
    {
        return az;
    }
    void SetX(float input)
    {
        x = input;
    }
    void SetY(float input)
    {
        y = input;
    }
    void SetDx(float input)
    {
        dx = input;
    }
    void SetDy(float input)
    {
        dy = input;
    }
    void SetDz(float input)
    {
        dz = input;
    }

    FCell *GetCellByRelaPos(float x_pos, float y_pos, int layer_id);

    FCell *GetCellByRelaPos(float x_pos, float y_pos, float z_pos);

    FCell3D *GetCell3DByRelaPos(float x_pos, float y_pos);
};
#endif