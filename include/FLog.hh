#ifndef FJL_LOG_hh
#define FJL_LOG_hh

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG
#define LOG_NAME "logger"
#define ERROR_NAME "error_logger"
#define DEBUG_TRACE_NAME "traceAndDebug"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_sinks.h"

#endif
