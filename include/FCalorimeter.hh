#ifndef FJL_Calorimeter_hh
#define FJL_Calorimeter_hh
#include "TFile.h"
#include "FDataType.hh"
#include "TH2I.h"
#include "TH2F.h"
#include "TTree.h"
#include "TH3F.h"

#include "string.h"
#include "Parameters.hh"
#include "TRandom.h"
#include "FLog.hh"
#include "EventInfo.hh"

class FModule;
class FCell3D;
class FGamma;
class FPi0;
class FRegion;
class FParticle;
class FCell;
class FCluster3D;
class FCluster2D;
class MatchClu3DInfo;
class MatchSubClu3DPairInfo;
struct s_RegionType
{

    s_RegionType(int i_type)
    {
        type = i_type;
    }
    int type;
    int LayerNum;
    std::vector<FRegion *> Regions;
};
class FCalorimeter
{
protected:
    std::vector<FEventInfo *> EventInfos;

public:
    TRandom *CalRand;

    FCalorimeter(/* args */)
    {
    }
    ~FCalorimeter()
    {
    }
    float PVZShift = -12836;
    TH2I module_map;
    TH2I region_map;
    TH2I RawRegionMap;
    TH2I regionType_map;

    int TotalRegionType = 0;
    int TotalRegionId = 0;
    int TotalRawRegion = 0;

    std::map<int, FModule *> module_container;
    std::map<int, FRegion *> region_container;
    std::map<int, s_RegionType *> m_RegionType;

    std::map<int, float> MixtureLayer; // int : region type   int : 1 : silicon

    float X;
    float Y;
    float Z;
    float Dx;
    float Dy;
    float Dz;

    bool ModuleStudy = 0;

    bool RegionStudy = 0;

    bool ParticleGun = 0;

    int TotalLayerNumInAllRegion = 0;

    int ProgramType;

    int CluFlow; // 0 : only 3D clu 1 : 2D->3D 2 : 3D->2D

    int max_Eventseq;

    // change layer pos mm to nm then map by int

    virtual void InitCalo() = 0; //

    virtual void InitPVAndHitInfo() = 0;

    virtual void InitEventStructure() = 0;

    virtual void InitPara() = 0;

    virtual void FindNeighbor() = 0;

    virtual void RecRegion() = 0;

    virtual void InitCaliPara() = 0;

    virtual void CalibrateCell() = 0;

    virtual void ConstructCell3D() = 0;

    virtual void WriteData() = 0;

    virtual void FindLocalMaxCell2D(int event);

    virtual void FindLocalMaxCell3D(int event);

    virtual void Seeding3D(int event) = 0;

    virtual void Seeding(int event) = 0;

    virtual void AllocateCell3D(int event) = 0;
    virtual void ReAllo2DCell(int event) = 0; // loop Clu3D and redefine layer

    virtual void SplitOverlapClu3D(int event) = 0;

    virtual void SplitOverlapClu2D(int event) = 0;

    virtual void CorrectCluster3D(int event) = 0;

    virtual void AllocateCell(int event) = 0;

    virtual void Clustering() = 0;
    virtual void Calibration() = 0;

    virtual void Matching() = 0;

    virtual void WriteTree() = 0;

    virtual bool OutofECal(float x_pos, float y_pos);
    virtual FRegion *GetRegionByPos(float x_pos, float y_pos, float z_pos);
    virtual FRegion *GetRegionByPos(float x_pos, float y_pos);
    virtual int GetRegionIDByPos(float x_pos, float y_pos, float z_pos);
    virtual int GetRegionIDByPos(float x_pos, float y_pos);
    virtual int GetRegionTypeByPos(float x_pos, float y_pos, float z_pos);
    virtual int GetRegionTypeByPos(float x_pos, float y_pos);
    virtual int GetModuleIDByPos(float x_pos, float y_pos, float z_pos);
    virtual int GetModuleIDByPos(float x_pos, float y_pos);
    virtual FModule *GetModuleByID(int ModuleId);
    virtual FModule *GetModuleByPos(float x_pos, float y_pos, float z_pos);
    virtual FModule *GetModuleByPos(float x_pos, float y_pos);
    virtual FCell *GetCellByPos(float x_pos, float y_pos, float z_pos);
    virtual int GetCellLocalIDByPos(float x_pos, float y_pos, float z_pos);
    virtual uint64 GetCellGlobalIDByPos(float x_pos, float y_pos, float z_pos);

    virtual FCell3D *GetCell3DByPos(float x_pos, float y_pos, float z_pos);
    virtual FCell3D *GetCell3DByPos(float x_pos, float y_pos);

    virtual void GetModuleNeighbours();
    ///////////////////////////////////////////////
    //////////////////////////////////////////////
    void InitEventInfo()
    {
        for (int loop = 0; loop < max_Eventseq; loop++)
        {
            FEventInfo *tmpEventInfo = new FEventInfo(loop,this);

            std::string HitMapName = "CaloHitMap_" + std::to_string(loop);
            tmpEventInfo->HitMap = new TH2F(HitMapName.c_str(), HitMapName.c_str(), 260, X - Dx / 2, X + Dx / 2, 260, Y - Dy / 2, Y + Dy / 2);
            EventInfos.emplace_back(tmpEventInfo);
        }
    }
    int Get_nPV(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->Get_nPV();
    }
    void Set_nPV(int input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->Set_nPV(input);
    }
    void SetMaxDepRegion(int input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetMaxDepRegion(input);
    }
    int GetMaxDepRegion(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetMaxDepRegion();
    }
    void SetHitRegion(int input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitRegion(input, nPV);
    }
    int GetHitRegion(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitRegion(nPV);
    }
    void SetHitRegionType(int input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitRegionType(input, nPV);
    }
    int GetHitRegionType(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitRegionType(nPV);
    }
    void SetHitModule(int input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitModule(input, nPV);
    }
    int GetHitModule(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitModule(nPV);
    }
    void SetPrimaryInfoStatus(bool input, int event)
    {

        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            return EventInfos.at(event)->SetPrimaryInfoStatus(input);
        }
    }
    bool GetPrimaryInfoStatus(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryInfoStatus();
    }

    void SetShowerX(double input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetShowerX(input);
    }
    double GetShowerX(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetShowerX();
    }
    void SetShowerY(double input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetShowerY(input);
    }
    double GetShowerY(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetShowerY();
    }
    void SetShowerZ(double input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetShowerZ(input);
    }
    double GetShowerZ(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetShowerZ();
    }
    void SetPrimaryX(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryX(input, nPV);
    }
    double GetPrimaryX(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryX(nPV);
    }
    void SetPrimaryY(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryY(input, nPV);
    }
    double GetPrimaryY(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryY(nPV);
    }
    void SetPrimaryZ(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryZ(input, nPV);
    }
    double GetPrimaryZ(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryZ(nPV);
    }
    void SetPrimaryPDGID(int input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryPDGID(input, nPV);
    }
    int GetPrimaryPDGID(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryPDGID(nPV);
    }
    void SetPrimaryT(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryT(input, nPV);
    }
    double GetPrimaryT(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryT(nPV);
    }
    void SetPrimaryE(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryE(input, nPV);
    }
    double GetPrimaryE(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryE(nPV);
    }
    void SetPrimaryVx(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryVx(input, nPV);
    }
    double GetPrimaryVx(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryVx(nPV);
    }
    void SetPrimaryVy(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryVy(input, nPV);
    }
    double GetPrimaryVy(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryVy(nPV);
    }
    void SetPrimaryVz(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetPrimaryVz(input, nPV);
    }
    double GetPrimaryVz(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetPrimaryVz(nPV);
    }
    void SetHitX(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitX(input, nPV);
    }
    double GetHitX(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitX(nPV);
    }
    void SetHitY(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitY(input, nPV);
    }
    double GetHitY(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitY(nPV);
    }
    void SetHitZ(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitZ(input, nPV);
    }
    double GetHitZ(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitZ(nPV);
    }
    void SetHitVx(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitVx(input, nPV);
    }
    double GetHitVx(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitVx(nPV);
    }
    void SetHitVy(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitVy(input, nPV);
    }
    double GetHitVy(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitVy(nPV);
    }
    void SetHitVz(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitVz(input, nPV);
    }
    double GetHitVz(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitVz(nPV);
    }
    void SetHitE(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitE(input, nPV);
    }
    double GetHitE(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitE(nPV);
    }
    void SetHitT(double input, int event, int nPV = -1)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetHitT(input, nPV);
    }
    double GetHitT(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetHitT(nPV);
    }
    void AccumulateEnDep(double input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->AccumulateEnDep(input);
    }
    void SetEnDep(double input, int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->SetEnDep(input);
    }
    double GetEnDep(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetEnDep();
    }
    // void SetMatchedClu3D(int event, FCluster3D *Clu3D, int nPV = -1)
    // {
    //     if (EventInfos.size() < event)
    //     {
    //         spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
    //         exit(0);
    //     }
    //     else
    //         return EventInfos.at(event)->SetMatchedClu3D(Clu3D, nPV);
    // }

    void Try2MatchSubClu3D(int event, std::pair<FCluster3D *, FCluster3D *> Clu3DPair)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            EventInfos.at(event)->Try2MatchSubClu3D(Clu3DPair);
    }

    int Try2MatchClu3D(int event, FCluster3D *Clu3D)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->Try2MatchClu3D(Clu3D);
    }

    FCluster3D *GetMatchedClu3D(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetMatchedClu3D(nPV);
    }

    MatchClu3DInfo *GetMatchedClu3DInfo(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetMatchedClu3DInfo(nPV);
    }

    int n_MatchedClu3DInfo(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->n_MatchedClu3DInfo();
    }

    // void SetMatchedSubClu3D(int event, FCluster3D *Clu3D, int nPV = -1)
    // {
    //     if (EventInfos.size() < event)
    //     {
    //         spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
    //         exit(0);
    //     }
    //     else
    //         return EventInfos.at(event)->SetMatchedSubClu3D(Clu3D, nPV);
    // }

    FCluster3D *GetMatchedSubClu3D(int event, int nPV = 0)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            return EventInfos.at(event)->GetMatchedSubClu3D(nPV);
    }

    MatchSubClu3DPairInfo *GetMatchedSubClu3DInfo(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->GetMatchedSubClu3DInfo();
    }

    void InitMatchContainer(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
            EventInfos.at(event)->InitMatchContainer();
    }

    int n_Cluster3D(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->v_Cluster3D.size();
    }

    std::set<FCell *> *Get_sSeed2D(int event, int regionType, int layer)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
        {
            auto EventInfoPoint = EventInfos.at(event);
            if (EventInfoPoint->mR_vL_sSeed2D.count(regionType))
            {
                if (layer < EventInfoPoint->mR_vL_sSeed2D.at(regionType).size())
                {
                    return &EventInfos.at(event)->mR_vL_sSeed2D.at(regionType).at(layer);
                }
                else
                {
                    spdlog::get(ERROR_NAME)->error("Type {} region doesn't have layer {}, please check", regionType, layer);
                    exit(0);
                }
            }
            else
            {
                spdlog::get(ERROR_NAME)->error("Not have region type {} in Calo, please check", regionType);
                exit(0);
            }
        }
    }


    std::vector<FCluster2D *> *Get_vClu2D(int event, int regionType, int layer)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
        {
            auto EventInfoPoint = EventInfos.at(event);
            if (EventInfoPoint->mR_vL_vClu2D.count(regionType))
            {
                if (layer < EventInfoPoint->mR_vL_vClu2D.at(regionType).size())
                {
                    return &EventInfos.at(event)->mR_vL_vClu2D.at(regionType).at(layer);
                }
                else
                {
                    spdlog::get(ERROR_NAME)->error("Type {} region doesn't have layer {}, please check", regionType, layer);
                    exit(0);
                }
            }
            else
            {
                spdlog::get(ERROR_NAME)->error("Not have region type {} in Calo, please check", regionType);
                exit(0);
            }
        }
    }

    std::set<FCell3D *> *Get_sSeed3D(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->s_Seed3D;
    }

    std::set<FCell3D *> *Get_sLMCell3D(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->LocalMaxCell3D;
    }

    std::set<FCell3D *> *Get_sLMCell3D_Cross(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->LocalMaxCell3D_Cross;
    }

    std::set<FCell3D *> *Get_sLMCell3D_Fork(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->LocalMaxCell3D_Fork;
    }

    std::vector<FCluster3D *> *Get_vClu3D(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_Cluster3D;
    }
    std::vector<std::pair<FCluster3D *, FCluster3D *>> *Get_vSubClu3D(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_SubCluster3D;
    }
    std::vector<FGamma *> *Get_vGamma(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_Gamma;
    }
    std::vector<std::pair<FGamma *, FGamma *>> *Get_vSubGamma(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_SubGamma;
    }
    std::vector<FPi0 *> *Get_vPi0(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_Pi0;
    }
    std::vector<FPi0 *> *Get_vMergedPi0(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_MergedPi0;
    }
    std::vector<FPi0 *> *Get_vResolvedPi0(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_ResolvedPi0;
    }
    std::vector<FPim *> *Get_vPim(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_Pim;
    }
    std::vector<FPip *> *Get_vPip(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return &EventInfos.at(event)->v_Pip;
    }
    TH2F *GetHitMap(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
            return 0;
        }
        else
            return EventInfos.at(event)->HitMap;
    }

    void SetComputeTime(int event, int Duration) // ms
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            EventInfos.at(event)->RecDuration = Duration;
        }
    }

    int GetComputeTime(int event) // ms
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            return EventInfos.at(event)->RecDuration;
        }
    }

    void SetNLM_MaxInFork(int event, int Num)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            EventInfos.at(event)->NLM_MaxInFork = Num;
        }
    }

    void SetNLM_MaxInCross(int event, int Num)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            EventInfos.at(event)->NLM_MaxInCross = Num;
        }
    }

    int GetNLM_MaxInFork(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            return EventInfos.at(event)->NLM_MaxInFork;
        }
    }

    int GetNLM_MaxInCross(int event)
    {
        if (EventInfos.size() < event)
        {
            spdlog::get(ERROR_NAME)->error("Event number {} too large, max event is {}, please check", event, EventInfos.size());
            exit(0);
        }
        else
        {

            return EventInfos.at(event)->NLM_MaxInCross;
        }
    }
};
#endif