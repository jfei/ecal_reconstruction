#ifndef CalculateGeo_h
#define CalculateGeo_h
#include "TTree.h"
#include "TH2I.h"

TH2I *CalCulateMapByTreeFrom_Simulation(TTree *inTree, int Type);
TH2I *CalCulateMapByTreeFrom_Simulation(TTree *inTree, std::vector<int> TypeList);
TH2I *CalCulateLayerMap(float xRange, float xSize, float yRange, float ySize);

#endif
