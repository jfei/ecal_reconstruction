#ifndef Mixture_Draw_hh
#define Mixture_Draw_hh
#include "Mixture_Calo.hh"
#include <sstream>
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "iostream"
#include <iomanip>
void Granularity_diff(Mixture_Calo *my_ECAL, std::vector<float> v_granularity, float Draw_layer); // only work for SiTiming silicon layer

void Draw_PosRes(Mixture_Calo *my_ECAL, std::vector<float> v_granularity);

#endif