#ifndef analysis_alg_h
#define analysis_alg_h
class Mixture_Calo;
class TFile;
void ConstructB2KstGamma(Mixture_Calo *my_ECAL, TFile *OutFile);
void ConstructBd02PipPimPi0(Mixture_Calo *my_ECAL, TFile *OutFile);
void ConstructB2D0Gammapi(Mixture_Calo *my_ECAL, TFile *OutFile);
#endif