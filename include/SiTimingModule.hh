#include "FModule.hh"

class SiTimingModule : public FModule
{
private:
    /* data */
public:
    SiTimingModule(int m_type, int m_id, float m_x, float m_y, float m_dx, float m_dy) : FModule(m_type, m_id, m_x, m_y, m_dx, m_dy)
    {
    }
    ~SiTimingModule();
};
