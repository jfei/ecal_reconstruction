#ifndef FJL_Cell3D_hh
#define FJL_Cell3D_hh
#include "FDataType.hh"
#include "FCell.hh"

class FCell3D : public FCell
{
private:
public:
    FCell3D(int c_id, float c_x, float c_y, float c_z, float c_dx, float c_dy, float c_dz, float c_ax, float c_ay, float c_az) : FCell(c_id, c_x, c_y, c_z, c_dx, c_dy, c_dz, c_ax, c_ay, c_az)
    {
    }
    ~FCell3D();
    FCluster3D *ConstructClu3D(int event);
    std::vector<std::vector<FCell *>> v_Cell;
    std::map<int, std::vector<FCell3D *>> Neighbours; // 0:Nei in same module 1:Nei in same type and id region 2: Nei in same type region
    std::map<int, std::vector<FCell3D *>> CrossNeib;  // 0:Nei in same module 1:Nei in same type and id region 2: Nei in same type region
    std::map<int, std::vector<FCell3D *>> ForkNeib;   // 0:Nei in same module 1:Nei in same type and id region 2: Nei in same type region

    int IsLocalMax(int eventNum); // 0 not 1: yes 2: yes
    int IsLocalMax_Cross(int eventNum); // 0 not 1: yes 2: yes
    int IsLocalMax_Fork(int eventNum);  // 0 not 1: yes 2: yes
    void FindNeighbours();
    void FindCrossNeighbours();
    void FindForkNeighbours();

    void AddMotherClu3D(FCluster3D *Clu3D, float fraction = -1);
    void RemoveMotherClu3D(FCluster3D *Clu3D);
    FCluster3D *GetMotherClu3D(int event, int i = 0);

    TH1F *GetTranverseProfile(FCluster3D *Clu3D);

    float GetTruRatioOfClu3D(FCluster3D *Clu3D);
};

#endif