#ifndef FJL_ModuleEventInfo_hh
#define FJL_ModuleEventInfo_hh
#include "set"
#include "vector"
#include "TH2F.h"
class FCell3D;
class FCluster3D;
struct FModuleEventInfo //  Except endep, only valid under ActivePrimaryInfo =1
{
    int event;
    std::set<FCell3D *> LocalMaxCell3D;
    std::set<FCell3D *> LocalMaxCell3D_Cross;
    std::set<FCell3D *> LocalMaxCell3D_Fork;
    std::set<FCell3D *> Seed3D;
    std::vector<FCluster3D *> Cluster3D;
    TH2F *HitMap;
    void SetEventID(int EventID)
    {
        event = EventID;
    }
    int GetEventID()
    {
        return event;
    }
    FModuleEventInfo(int event)
    {
        SetEventID(event);
    }
};
#endif