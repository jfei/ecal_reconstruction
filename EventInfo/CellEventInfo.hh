#ifndef FJL_CellEventInfo_hh
#define FJL_CellEventInfo_hh
#include "map"
#include "FLog.hh"
class FCluster3D;
class FCluster2D;
class FCellEventInfo //  Except endep, only valid under ActivePrimaryInfo =1
{
protected:
    int event;
    bool InfoStatus = 0;
    std::map<FCluster3D *, float> f_Cluster3D;
    std::map<FCluster2D *, float> f_Cluster2D;
    int Is_LocalMax = -1;
    int Is_LocalMax_Cross = -1;
    int Is_LocalMax_Fork = -1;
    float RawSignal = -7777;
    float energy = -7777;
    float RawTime = -7777;
    float time = -7777;
    float TotEnDep = -1;
    float Z3D = -INFINITY;
    float et = -1;

public:
    FCellEventInfo(int event)
    {
        ClearRaw();
        ClearEnergy();
        ClearTime();
        SetEventID(event);
    }
    void RemoveMotherClu2D(FCluster2D *Clu2D)
    {
        f_Cluster2D.erase(Clu2D);
    }
    void RemoveMotherClu3D(FCluster3D *Clu3D)
    {
        f_Cluster3D.erase(Clu3D);
    }
    void SetEventID(int EventID)
    {
        event = EventID;
    }
    int GetEventID()
    {
        return event;
    }
    void SetLocalMaxStatus(int status)
    {
        Is_LocalMax = status;
    }
    int IsLocalMax()
    {
        return Is_LocalMax;
    }
    void SetLocalMax_Cross(int status)
    {
        Is_LocalMax_Cross = status;
    }
    int IsLocalMax_Cross()
    {
        return Is_LocalMax_Cross;
    }
    void SetLocalMax_Fork(int status)
    {
        Is_LocalMax_Fork = status;
    }
    int IsLocalMax_Fork()
    {
        return Is_LocalMax_Fork;
    }
    void ClearRaw()
    {
        RawSignal = 0;
        RawTime = 0;
        Z3D = 0;
    }
    void ClearEnergy()
    {
        energy = 0;
    }
    void ClearTime()
    {
        time = 0;
    }
    void SetEnergy(float input)
    {
        energy = input;
    }
    void SetTime(float input)
    {
        time = input;
    }
    void SetRawSignal(float input)
    {
        RawSignal = input;
    }
    void SetRawTime(float input)
    {
        RawTime = input;
    }
    float GetRawSignal()
    {
        return RawSignal;
    }
    float GetEnergy()
    {
        return energy;
    }
    float GetRawTime()
    {
        return RawTime;
    }
    float GetTime()
    {
        return time;
    }

    void SetTotEnDep(float input)
    {
        TotEnDep = input;
    }
    void AccumulateTotEnDep(float increment)
    {

        TotEnDep += increment;
    }
    float GetTotEnDep()
    {
        return TotEnDep;
    }
    ////////////////////////////
    void SetZ3D(float input)
    {
        Z3D = input;
    }
    float GetZ3D()
    {
        return Z3D;
    }
    void SetEt(float input)
    {
        et = input;
    }
    float GetEt()
    {
        return et;
    }
    int GetMotherClu3DNum()
    {
        return f_Cluster3D.size();
    }

    bool BelongTo(FCluster3D *Clu3D)
    {
        if (f_Cluster3D.count(Clu3D))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    bool BelongTo(FCluster2D *Clu2D)
    {
        if (f_Cluster2D.count(Clu2D))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    void AddMotherClu3D(FCluster3D *Clu3D, float fraction = -1)
    {
        float tmpFraction;
        if (fraction == -1)
        {
            tmpFraction = 1;
        }
        else
        {
            tmpFraction = fraction;
        }
        if (f_Cluster3D.count(Clu3D))
        {
            spdlog::get(LOG_NAME)->warn("Already add this Clu3D");
        }
        else
        {
            f_Cluster3D.emplace(Clu3D, tmpFraction);
        }
    }

    void AddMotherClu2D(FCluster2D *Clu2D, float fraction = -1)
    {
        float tmpFraction;
        if (fraction == -1)
        {
            tmpFraction = 1;
        }
        else
        {
            tmpFraction = fraction;
        }
        if (f_Cluster2D.count(Clu2D))
        {
            spdlog::get(LOG_NAME)->warn("Already add this Clu2D");
        }
        else
        {
            f_Cluster2D.emplace(Clu2D, tmpFraction);
        }
    }

    FCluster3D *GetMotherClu3D(int i = 0)
    {
        auto it = f_Cluster3D.begin();
        if (it != f_Cluster3D.end())
        {
            auto Element = std::next(it, i);
            if (Element != f_Cluster3D.end())
            {
                return Element->first;
            }
            else
            {
                spdlog::get(ERROR_NAME)->error("Cell belong to {} Clu3Ds, but you input {}", f_Cluster3D.size(), i);
                exit(0);
            }
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("No Clu3D contain this cell!");
            exit(0);
        }
    }

    float GetE4MotherClu3D(FCluster3D *Clu3D)
    {
        if (f_Cluster3D.count(Clu3D))
        {
            return f_Cluster3D.at(Clu3D) * energy;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu3D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    void SetE4MotherClu3D(FCluster3D *Clu3D, float CellE)
    {
        if (f_Cluster3D.count(Clu3D))
        {
            if (energy != 0)
                f_Cluster3D.at(Clu3D) = CellE / energy;
            else
                f_Cluster3D.at(Clu3D) = 1;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu3D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    float GetFraction4MotherClu3D(FCluster3D *Clu3D)
    {
        if (f_Cluster3D.count(Clu3D))
        {
            return f_Cluster3D.at(Clu3D);
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu3D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    void SetFraction4MotherClu3D(FCluster3D *Clu3D, float fraction)
    {
        if (f_Cluster3D.count(Clu3D))
        {
            if (fraction < 0 || fraction > 1)
            {
                spdlog::get(ERROR_NAME)->warn("Fraction must in 0-1, but fraction = {}", fraction);
                if (fraction < 0)
                {
                    f_Cluster3D.at(Clu3D) = 0;
                }
                else
                {
                    f_Cluster3D.at(Clu3D) = 1;
                }
            }
            else
                f_Cluster3D.at(Clu3D) = fraction;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu3D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    FCluster2D *GetMotherClu2D(int i = 0)
    {
        auto it = f_Cluster2D.begin();
        if (it != f_Cluster2D.end())
        {
            auto Element = std::next(it, i);
            if (Element != f_Cluster2D.end())
            {
                return Element->first;
            }
            else
            {
                spdlog::get(ERROR_NAME)->error("Cell belong to {} Clu2Ds, but you input {}", f_Cluster2D.size(), i);
                exit(0);
            }
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("No Clu2D contain this cell!");
            exit(0);
        }
    }

    float GetE4MotherClu2D(FCluster2D *Clu2D)
    {
        if (f_Cluster2D.count(Clu2D))
        {
            return f_Cluster2D.at(Clu2D) * energy;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu2D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    void SetE4MotherClu2D(FCluster2D *Clu2D, float CellE)
    {
        if (f_Cluster2D.count(Clu2D))
        {
            if (energy != 0)
                f_Cluster2D.at(Clu2D) = CellE / energy;
            else
                f_Cluster2D.at(Clu2D) = 1;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu2D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    float GetFraction4MotherClu2D(FCluster2D *Clu2D)
    {
        if (f_Cluster2D.count(Clu2D))
        {
            return f_Cluster2D.at(Clu2D);
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu2D {} not contain in this cell!", 0);
            exit(0);
        }
    }

    void SetFraction4MotherClu2D(FCluster2D *Clu2D, float fraction)
    {
        if (f_Cluster2D.count(Clu2D))
        {
            if (fraction < 0 || fraction > 1)
            {
                spdlog::get(ERROR_NAME)->warn("Fraction must in 0-1, but fraction = {}", fraction);
                if (fraction < 0)
                {
                    f_Cluster2D.at(Clu2D) = 0;
                }
                else
                {
                    f_Cluster2D.at(Clu2D) = 1;
                }
            }
            else
                f_Cluster2D.at(Clu2D) = fraction;
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("Clu2D {} not contain in this cell!", 0);
            exit(0);
        }
    }
};

#endif