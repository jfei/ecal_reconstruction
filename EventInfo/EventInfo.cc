#include "EventInfo.hh"
#include "FCalorimeter.hh"
#include "FCluster3D.hh"
#include "FRegion.hh"
#include "Mixture_Calo.hh"
FEventInfo::FEventInfo(int evt, FCalorimeter *Calo)
{
    ClearEnDep();
    SetEventID(evt);
    for (auto TypeSeq : Calo->m_RegionType)
    {
        mR_vL_vClu2D[TypeSeq.first].resize(TypeSeq.second->LayerNum);
        mR_vL_sSeed2D[TypeSeq.first].resize(TypeSeq.second->LayerNum);
    }
    RecDuration = 0;
    NLM_MaxInFork = 0;
    NLM_MaxInCross = 0;
}

int FEventInfo::Try2MatchClu3D(FCluster3D *tmpClu3D) // success : return number of PV. failed: -1
{
    int MatchPV = -1;
    auto f_region = tmpClu3D->f_region;
    float Cell3DSize = f_region->Cell3DSize;
    for (int i = 0; i < n_PV; i++)
    {
        // if (MatchedClu3DInfos.at(i)->DeltaE_E > MatchedClu3DInfos.at(i)->DeltaE_E_Limit)
        // {
        //     if (tmpClu3D->GetDeltaE_E2PV(i) < MatchedClu3DInfos.at(i)->DeltaE_E)
        //     {
        //         MatchedClu3DInfos.at(i)->PrevClu3D = MatchedClu3DInfos.at(i)->Clu3D;
        //         MatchedClu3DInfos.at(i)->Clu3D = tmpClu3D;
        //         MatchedClu3DInfos.at(i)->Dist2Tru = tmpClu3D->GetDist2Tru(i);
        //         MatchedClu3DInfos.at(i)->DeltaE_E = tmpClu3D->GetDeltaE_E2PV(i);
        //         MatchPV = i;
        //         MatchedClu3DInfos.at(i)->Active = 1;
        //         std::cout << "Pv " << i << std::endl;
        //         std::cout << "1 " << tmpClu3D->Energy() << std::endl;
        //         std::cout << "11 " << tmpClu3D->GetDeltaE_E2PV(i) << std::endl;
        //     }
        // }
        // else
        auto HitRegionId = GetHitRegion(i);
        auto HitRegion = Mixture_Calo::get_instance()->region_container.at(HitRegionId);
        auto TruCell3DSize = HitRegion->Cell3DSize;
        // std::cout << i << std::endl;
        // std::cout << "tmpClu3D->GetDist2Tru(i) " << tmpClu3D->GetDist2Tru(i) << std::endl;
        // std::cout << "Cell3DSize " << Cell3DSize << std::endl;
        // std::cout << "TruCell3DSize " << TruCell3DSize << std::endl;
        // std::cout << "MatchedClu3DInfos.at(i)->Dist_Limit * (Cell3DSize + TruCell3DSize) / 2. " << MatchedClu3DInfos.at(i)->Dist_Limit * (Cell3DSize + TruCell3DSize) / 2. << std::endl;
        if (tmpClu3D->GetDist2Tru(i) < MatchedClu3DInfos.at(i)->Dist_Limit * (Cell3DSize + TruCell3DSize) / 2.)
        {
            if (tmpClu3D->GetDeltaE_E2PV(i) < MatchedClu3DInfos.at(i)->DeltaE_E_Limit)
            {
                if (tmpClu3D->GetDist2Tru(i) < MatchedClu3DInfos.at(i)->Dist2Tru)
                {
                    MatchedClu3DInfos.at(i)->PrevClu3D = MatchedClu3DInfos.at(i)->Clu3D;
                    MatchedClu3DInfos.at(i)->Clu3D = tmpClu3D;
                    MatchedClu3DInfos.at(i)->Dist2Tru = tmpClu3D->GetDist2Tru(i);
                    MatchedClu3DInfos.at(i)->DeltaE_E = tmpClu3D->GetDeltaE_E2PV(i);
                    MatchPV = i;
                    MatchedClu3DInfos.at(i)->Active = 1;
                    // std::cout << "Pv " << i << std::endl;
                    // std::cout << "2 " << tmpClu3D->Energy() << std::endl;
                    // std::cout << "22 " << tmpClu3D->GetDeltaE_E2PV(i) << std::endl;
                }
            }
        }
    }
    return MatchPV;
}

void FEventInfo::Try2MatchSubClu3D(std::pair<FCluster3D *, FCluster3D *> tmpClu3D) // success : return number of PV. failed: -1
{
    int MatchPV = -1;
    FCluster3D *Clu3D1 = tmpClu3D.first;
    FCluster3D *Clu3D2 = tmpClu3D.second;
    float DistClu3D1_0 = Clu3D1->GetDist2Tru(0);
    float DistClu3D1_1 = Clu3D1->GetDist2Tru(1);
    float DistClu3D2_0 = Clu3D2->GetDist2Tru(0);
    float DistClu3D2_1 = Clu3D2->GetDist2Tru(1);
    float DeltaEClu3D1_0 = Clu3D1->GetDeltaE_E2PV(0);
    float DeltaEClu3D1_1 = Clu3D1->GetDeltaE_E2PV(1);
    float DeltaEClu3D2_0 = Clu3D2->GetDeltaE_E2PV(0);
    float DeltaEClu3D2_1 = Clu3D2->GetDeltaE_E2PV(1);

    auto f_region = Clu3D1->f_region;
    float Cell3DSize = f_region->Cell3DSize;

    float DeltaE_E = (DeltaEClu3D1_0 + DeltaEClu3D2_1) < (DeltaEClu3D1_1 + DeltaEClu3D2_0) ? (DeltaEClu3D1_0 + DeltaEClu3D2_1) : (DeltaEClu3D1_1 + DeltaEClu3D2_0);
    int DeltaE_E_Seq = (DeltaEClu3D1_0 + DeltaEClu3D2_1) < (DeltaEClu3D1_1 + DeltaEClu3D2_0) ? 0 : 1;
    float Dist2Tru = (DistClu3D1_0 + DistClu3D2_1) < (DistClu3D1_1 + DistClu3D2_0) ? (DistClu3D1_0 + DistClu3D2_1) / 2. : (DistClu3D1_1 + DistClu3D2_0) / 2.;
    int Dist2Tru_Seq = (DistClu3D1_0 + DistClu3D2_1) < (DistClu3D1_1 + DistClu3D2_0) ? 0 : 1;

    // if (MatchedSubClu3DPairInfos->DeltaE_E > MatchedSubClu3DPairInfos->DeltaE_E_Limit)
    // {
    //     if (DeltaE_E < MatchedSubClu3DPairInfos->DeltaE_E)
    //     {
    //         MatchedSubClu3DPairInfos->DeltaE_E = DeltaE_E;
    //         MatchedSubClu3DPairInfos->PrevSubClu3D_pair = MatchedSubClu3DPairInfos->SubClu3D_pair;
    //         if (DeltaE_E_Seq == 0)
    //         {
    //             MatchedSubClu3DPairInfos->SubClu3D_pair = {Clu3D1, Clu3D2};
    //             MatchedSubClu3DPairInfos->Dist2Tru = (DistClu3D1_0 + DistClu3D2_1);
    //             MatchedSubClu3DPairInfos->Active = 1;
    //         }
    //         else
    //         {
    //             MatchedSubClu3DPairInfos->SubClu3D_pair = {Clu3D2, Clu3D1};
    //             MatchedSubClu3DPairInfos->Dist2Tru = (DistClu3D1_1 + DistClu3D2_0);
    //             MatchedSubClu3DPairInfos->Active = 1;
    //         }
    //     }
    // }
    // else
    auto HitRegionId0 = GetHitRegion(0);
    auto HitRegion0 = Mixture_Calo::get_instance()->region_container.at(HitRegionId0);
    auto TruCell3DSize0 = HitRegion0->Cell3DSize;
    auto HitRegionId1 = GetHitRegion(1);
    auto HitRegion1 = Mixture_Calo::get_instance()->region_container.at(HitRegionId1);
    auto TruCell3DSize1 = HitRegion1->Cell3DSize;
    bool PassDistTag = 0;
    if (Dist2Tru_Seq == 0)
    {

        if (Clu3D1->GetDist2Tru(0) < MatchedSubClu3DPairInfos->Dist_Limit * (Cell3DSize + TruCell3DSize0) / 2.)
        {
            if (Clu3D2->GetDist2Tru(1) < MatchedSubClu3DPairInfos->Dist_Limit * (Cell3DSize + TruCell3DSize1) / 2.)
            {
                PassDistTag = 1;
            }
        }
    }
    else
    {
        if (Clu3D1->GetDist2Tru(1) < MatchedSubClu3DPairInfos->Dist_Limit * (Cell3DSize + TruCell3DSize1) / 2.)
        {
            if (Clu3D2->GetDist2Tru(0) < MatchedSubClu3DPairInfos->Dist_Limit * (Cell3DSize + TruCell3DSize0) / 2.)
            {
                PassDistTag = 1;
            }
        }
    }
    if (PassDistTag)
    {

        if (Dist2Tru < MatchedSubClu3DPairInfos->Dist2Tru)
        {

            if (Dist2Tru_Seq == 0)
            {
                if ((DeltaEClu3D1_0 + DeltaEClu3D2_1) < MatchedSubClu3DPairInfos->DeltaE_E_Limit)
                {
                    MatchedSubClu3DPairInfos->Dist2Tru = Dist2Tru;
                    MatchedSubClu3DPairInfos->PrevSubClu3D_pair = MatchedSubClu3DPairInfos->SubClu3D_pair;
                    MatchedSubClu3DPairInfos->SubClu3D_pair = {Clu3D1, Clu3D2};
                    MatchedSubClu3DPairInfos->DeltaE_E = DeltaEClu3D1_0 + DeltaEClu3D2_1;
                    MatchedSubClu3DPairInfos->Active = 1;
                }
            }
            else
            {
                if ((DeltaEClu3D1_1 + DeltaEClu3D2_0) < MatchedSubClu3DPairInfos->DeltaE_E_Limit)
                {
                    MatchedSubClu3DPairInfos->Dist2Tru = Dist2Tru;
                    MatchedSubClu3DPairInfos->PrevSubClu3D_pair = MatchedSubClu3DPairInfos->SubClu3D_pair;
                    MatchedSubClu3DPairInfos->SubClu3D_pair = {Clu3D2, Clu3D1};
                    MatchedSubClu3DPairInfos->DeltaE_E = (DeltaEClu3D1_1 + DeltaEClu3D2_0);
                    MatchedSubClu3DPairInfos->Active = 1;
                }
            }
        }
    }
}