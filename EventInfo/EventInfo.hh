#ifndef FJL_EventInfo_hh
#define FJL_EventInfo_hh
#include "vector"
#include "TH2F.h"
#include "FLog.hh"
#include "map"
#include "set"
class FPi0;
class FGamma;
class FPim;
class FPip;
class FCluster3D;
class FCluster2D;
class FCalorimeter;
class FPoint;
class FCell3D;
class FCell;
class MatchClu3DInfo
{
public:
    int Active = 0;
    FCluster3D *Clu3D = NULL;
    FCluster3D *PrevClu3D = NULL;
    float Dist2Tru = FLT_MAX;
    float DeltaE_E = FLT_MAX;
    float DeltaE_E_Limit = 2.;
    float Dist_Limit = 1.5;
};

class MatchSubClu3DPairInfo
{
public:
    int Active = 0;
    std::pair<FCluster3D *, FCluster3D *> SubClu3D_pair = {NULL, NULL};
    std::pair<FCluster3D *, FCluster3D *> PrevSubClu3D_pair = {NULL, NULL};
    float Dist2Tru = FLT_MAX;
    float DeltaE_E = FLT_MAX;
    float DeltaE_E_Limit = 2.;
    float Dist_Limit = 1.5;
};
class FEventInfo //  Except endep, only valid under ActivePrimaryInfo =1
{
protected:
    int event;
    bool PrimaryInfoStatus = 0;
    int n_PV = 0;
    std::vector<double> primaryEnergy;
    std::vector<int> primaryPDGID;
    std::vector<double> primaryX;
    std::vector<double> primaryY;
    std::vector<double> primaryZ;
    std::vector<double> primaryT;
    std::vector<double> primaryVx;
    std::vector<double> primaryVy;
    std::vector<double> primaryVz;
    std::vector<double> Hit_x;
    std::vector<double> Hit_y;
    std::vector<double> Hit_z;
    std::vector<double> Hit_t;
    std::vector<double> Hit_vx;
    std::vector<double> Hit_vy;
    std::vector<double> Hit_vz;
    std::vector<double> Hit_e;
    std::vector<MatchClu3DInfo *> MatchedClu3DInfos;
    MatchSubClu3DPairInfo *MatchedSubClu3DPairInfos;
    /////////////////////
    double endep_total = 0; // total energy deposited in module
    int MaxDepRegion = 0;
    std::vector<int> HitRegion;
    std::vector<int> HitRegionType;
    std::vector<int> HitModule;
    double ShowerZ;
    double ShowerX;
    double ShowerY;

public:
    // float RecStartTime;
    // float RecEndTime;
    int RecDuration = 0;
    int NLM_MaxInFork = 0;
    int NLM_MaxInCross = 0;

    TH2F *HitMap;
    std::set<FCell3D *> LocalMaxCell3D;
    std::set<FCell3D *> LocalMaxCell3D_Cross;
    std::set<FCell3D *> LocalMaxCell3D_Fork;
    std::set<FCell3D *> s_Seed3D;
    std::map<int, std::vector<std::set<FCell *>>> mR_vL_sSeed2D;
    std::vector<FCluster3D *> v_Cluster3D;
    std::vector<std::pair<FCluster3D *, FCluster3D *>> v_SubCluster3D;
    // std::vector<FPoint *> ChargeHit;
    std::map<int, std::vector<std::vector<FCluster2D *>>> mR_vL_vClu2D;
    std::vector<FGamma *> v_Gamma;
    std::vector<std::pair<FGamma *, FGamma *>> v_SubGamma;
    std::vector<FPi0 *> v_Pi0;
    std::vector<FPi0 *> v_ResolvedPi0;
    std::vector<FPi0 *> v_MergedPi0;
    std::vector<FPim *> v_Pim;
    std::vector<FPip *> v_Pip;
    FEventInfo(int evt, FCalorimeter *Calo);
    int n_MatchedClu3DInfo()
    {
        return MatchedClu3DInfos.size();
    }
    void InitMatchContainer()
    {
        for (int i = 0; i < n_MatchedClu3DInfo(); i++)
        {
            delete MatchedClu3DInfos.at(i);
        }

        MatchedClu3DInfos.clear();

        for (int i = 0; i < n_PV; i++)
        {
            auto tmpMatchCluInfo = new MatchClu3DInfo();
            MatchedClu3DInfos.emplace_back(tmpMatchCluInfo);
        }
        MatchedSubClu3DPairInfos = new MatchSubClu3DPairInfo();
    }
    int Try2MatchClu3D(FCluster3D *tmpClu3D); // success : return number of PV. failed: -1

    FCluster3D *GetMatchedClu3D(int i)
    {
        if (i >= MatchedClu3DInfos.size())
        {
            spdlog::get(ERROR_NAME)->error("Size of MatchedClu3D {}, but input {}", MatchedClu3DInfos.size(), i);
            exit(0);
        }
        return MatchedClu3DInfos.at(i)->Clu3D;
    }

    MatchClu3DInfo *GetMatchedClu3DInfo(int i)
    {
        if (i >= MatchedClu3DInfos.size())
        {
            spdlog::get(ERROR_NAME)->error("Size of MatchedClu3D {}, but input {}", MatchedClu3DInfos.size(), i);
            exit(0);
        }
        return MatchedClu3DInfos.at(i);
    }
    // void SetMatchedClu3D(FCluster3D *Clu3D, int i = -1)
    // {
    //     if (i < 0 || i >= MatchedClu3DInfos.size())
    //     {
    //         if (i >= 0)
    //             spdlog::get(ERROR_NAME)->warn("Size of MatchedClu3D {}, but input {}, So Clu3D will put in index {}", MatchedClu3D.size(), i, MatchedClu3D.size());
    //         MatchedClu3DInfos.emplace_back(Clu3D);
    //     }
    //     else
    //     {
    //         MatchedClu3DInfos.at(i) = Clu3D;
    //     }
    // }

    FCluster3D *GetMatchedSubClu3D(int i)
    {
        if (i >= 2)
        {
            spdlog::get(ERROR_NAME)->error("Max input =1, but input {}", i);
            exit(0);
        }
        if (i == 0)
            return MatchedSubClu3DPairInfos->SubClu3D_pair.first;
        else
            return MatchedSubClu3DPairInfos->SubClu3D_pair.second;
    }

    MatchSubClu3DPairInfo *GetMatchedSubClu3DInfo()
    {

        return MatchedSubClu3DPairInfos;
    }
    void Try2MatchSubClu3D(std::pair<FCluster3D *, FCluster3D *> tmpClu3D); // success : return number of PV. failed: -1

    // void SetMatchedSubClu3D(FCluster3D *SubClu3D, int i = 0)
    // {
    //     if (i >= MatchedSubClu3D.size())
    //     {
    //         spdlog::get(ERROR_NAME)->warn("Size of MatchedSubClu3D {}, but input {}, So SubClu3D will put in index {}", MatchedSubClu3D.size(), i, MatchedSubClu3D.size());
    //         MatchedSubClu3D.emplace_back(SubClu3D);
    //     }
    //     else
    //     {
    //         MatchedSubClu3D.at(i) = SubClu3D;
    //     }
    // }

    void Set_nPV(int input)
    {
        n_PV = input;
    }
    int Get_nPV()
    {
        return n_PV;
    }
    void SetEventID(int EventID)
    {
        event = EventID;
    }
    int GetEventID()
    {
        return event;
    }
    void SetMaxDepRegion(int input)
    {
        MaxDepRegion = input;
    }
    int GetMaxDepRegion()
    {
        return MaxDepRegion;
    }
    void SetHitRegion(int input, int nPV = -1)
    {

        if (nPV == -1 || nPV >= HitRegion.size())
            HitRegion.emplace_back(input);
        else
            HitRegion.at(nPV) = input;
    }
    int GetHitRegion(int i = 0)
    {
        return HitRegion.at(i);
    }
    void SetHitRegionType(int input, int nPV = -1)
    {

        if (nPV == -1 || nPV >= HitRegionType.size())
            HitRegionType.emplace_back(input);
        else
            HitRegionType.at(nPV) = input;
    }
    int GetHitRegionType(int i = 0)
    {
        return HitRegionType.at(i);
    }
    void SetHitModule(int input, int nPV = -1)
    {
        if (nPV == -1 || nPV >= HitModule.size())
            HitModule.emplace_back(input);
        else
            HitModule.at(nPV) = input;
    }
    int GetHitModule(int i = 0)
    {
        return HitModule.at(i);
    }
    void SetPrimaryInfoStatus(bool Status)
    {
        PrimaryInfoStatus = Status;
    }
    bool GetPrimaryInfoStatus()
    {
        return PrimaryInfoStatus;
    }
    //////////////////////////////
    void SetShowerZ(double input)
    {
        ShowerZ = input;
    }
    double GetShowerZ()
    {
        return ShowerZ;
    }
    void SetShowerX(double input)
    {
        ShowerX = input;
    }
    double GetShowerX()
    {
        return ShowerX;
    }
    void SetShowerY(double input)
    {
        ShowerY = input;
    }
    double GetShowerY()
    {
        return ShowerY;
    }
    ////////////////////////////////
    void SetPrimaryE(double PrimaryE, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryEnergy.size())
            primaryEnergy.emplace_back(PrimaryE);
        else
            primaryEnergy.at(nPV) = PrimaryE;
    }
    double GetPrimaryE(int i = 0)
    {
        return primaryEnergy.at(i);
    }
    void SetPrimaryPDGID(int PrimaryPDGID, int nPV = -1)
    {

        if (nPV == -1 || nPV >= primaryPDGID.size())
            primaryPDGID.emplace_back(PrimaryPDGID);
        else
            primaryPDGID.at(nPV) = PrimaryPDGID;
    }
    int GetPrimaryPDGID(int i = 0)
    {
        return primaryPDGID.at(i);
    }
    void SetPrimaryX(double PrimaryX, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryX.size())
            primaryX.emplace_back(PrimaryX);
        else
            primaryX.at(nPV) = PrimaryX;
    }
    double GetPrimaryX(int i = 0)
    {
        return primaryX.at(i);
    }
    void SetPrimaryY(double PrimaryY, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryY.size())
            primaryY.emplace_back(PrimaryY);
        else
            primaryY.at(nPV) = PrimaryY;
    }
    double GetPrimaryY(int i = 0)
    {
        return primaryY.at(i);
    }
    void SetPrimaryZ(double PrimaryZ, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryZ.size())
            primaryZ.emplace_back(PrimaryZ);
        else
            primaryZ.at(nPV) = PrimaryZ;
    }
    double GetPrimaryZ(int i = 0)
    {
        return primaryZ.at(i);
    }
    void SetPrimaryT(double PrimaryT, int nPV = -1)
    {

        if (nPV == -1 || nPV >= primaryT.size())
            primaryT.emplace_back(PrimaryT);
        else
            primaryT.at(nPV) = PrimaryT;
    }
    double GetPrimaryT(int i = 0)
    {
        return primaryT.at(i);
    }
    void SetPrimaryVx(double PrimaryVx, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryVx.size())
            primaryVx.emplace_back(PrimaryVx);
        else
            primaryVx.at(nPV) = PrimaryVx;
    }
    double GetPrimaryVx(int i = 0)
    {
        return primaryVx.at(i);
    }
    void SetPrimaryVy(double PrimaryVy, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryVy.size())
            primaryVy.emplace_back(PrimaryVy);
        else
            primaryVy.at(nPV) = PrimaryVy;
    }
    double GetPrimaryVy(int i = 0)
    {
        return primaryVy.at(i);
    }
    void SetPrimaryVz(double PrimaryVz, int nPV = -1)
    {
        if (nPV == -1 || nPV >= primaryVz.size())
            primaryVz.emplace_back(PrimaryVz);
        else
            primaryVz.at(nPV) = PrimaryVz;
    }
    double GetPrimaryVz(int i = 0)
    {
        return primaryVz.at(i);
    }
    ///////////////////////
    // function for hit info
    ///////////////////////
    void SetHitX(double HitX, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_x.size())
            Hit_x.emplace_back(HitX);
        else
            Hit_x.at(nPV) = HitX;
    }
    double GetHitX(int i = 0)
    {
        return Hit_x.at(i);
    }

    void SetHitY(double HitY, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_y.size())
            Hit_y.emplace_back(HitY);
        else
            Hit_y.at(nPV) = HitY;
    }
    double GetHitY(int i = 0)
    {
        return Hit_y.at(i);
    }

    void SetHitZ(double HitZ, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_z.size())
            Hit_z.emplace_back(HitZ);
        else
            Hit_z.at(nPV) = HitZ;
    }
    double GetHitZ(int i = 0)
    {
        return Hit_z.at(i);
    }

    void SetHitVx(double HitVx, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_vx.size())
            Hit_vx.emplace_back(HitVx);
        else
            Hit_vx.at(nPV) = HitVx;
    }
    double GetHitVx(int i = 0)
    {
        return Hit_vx.at(i);
    }
    void SetHitVy(double HitVy, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_vy.size())
            Hit_vy.emplace_back(HitVy);
        else
            Hit_vy.at(nPV) = HitVy;
    }
    double GetHitVy(int i = 0)
    {
        return Hit_vy.at(i);
    }
    void SetHitVz(double HitVz, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_vz.size())
            Hit_vz.emplace_back(HitVz);
        else
            Hit_vz.at(nPV) = HitVz;
    }
    double GetHitVz(int i = 0)
    {
        return Hit_vz.at(i);
    }

    void SetHitE(double HitE, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_e.size())
            Hit_e.emplace_back(HitE);
        else
            Hit_e.at(nPV) = HitE;
    }
    double GetHitE(int i = 0)
    {
        return Hit_e.at(i);
    }

    void SetHitT(double HitT, int nPV = -1)
    {
        if (nPV == -1 || nPV >= Hit_t.size())
            Hit_t.emplace_back(HitT);
        else
            Hit_t.at(nPV) = HitT;
    }
    double GetHitT(int i = 0)
    {
        return Hit_t.at(i);
    }
    void AccumulateEnDep(double increment)
    {

        endep_total += increment;
    }
    void ClearEnDep()
    {
        endep_total = 0;
        ShowerX = 0;
        ShowerY = 0;
        ShowerZ = 0;
        for (int i = 0; i < primaryEnergy.size(); i++)
        {
            primaryEnergy.at(i) = 0;

            Hit_e.at(i) = 0;
        }
    }
    void SetEnDep(double input)
    {

        endep_total = input;
    }
    double GetEnDep()
    {
        return endep_total;
    }
};

#endif