#ifndef FJL_Kp_hh
#define FJL_Kp_hh

#include "FDataType.hh"
#include "FParticle.hh"

class FKp : public FParticle
{
private:
    /* data */
public:
    FKp(float px, float py, float pz, float E)
    {
        SetPxPyPzE(px, py, pz, E);
    }
    float GenT;
    float GenX;
    float GenY;
    float GenZ;
};

#endif