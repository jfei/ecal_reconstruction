#include "Bd_PipPimPi0.hh"
#include "FPi0.hh"
#include "FPip.hh"
#include "FPim.hh"
#include "FBd0.hh"
#include "FGamma.hh"
#include "FCluster3D.hh"
#include "FCluster2D.hh"
#include "FRegion.hh"
#include "RecAlg.h"
Bd_PipPimPi0::Bd_PipPimPi0(FPip *i_Pip, FPim *i_Pim, FPi0 *i_Pi0, int IsTru)
{

    Pi0 = i_Pi0;
    Pip = i_Pip;
    Pim = i_Pim;
    Bd0 = new FBd0();

    auto tmpVector = (*i_Pi0 + *i_Pip + *i_Pim);
    Bd0->SetPxPyPzE(tmpVector.Px(), tmpVector.Py(), tmpVector.Pz(), tmpVector.E());
    auto Gamma1 = i_Pi0->Sub_Gamma.first;
    auto Gamma2 = i_Pi0->Sub_Gamma.second;
    float PV_Z = 12860;
    float PV_X = 0., PV_Y = 0.;
    float Dist4Gamma1 = sqrt(pow(Gamma1->HitX - PV_X, 2) + pow(Gamma1->HitY - PV_Y, 2) + pow(Gamma1->HitZ - PV_Z, 2));
    float Dist4Gamma2 = sqrt(pow(Gamma2->HitX - PV_X, 2) + pow(Gamma2->HitY - PV_Y, 2) + pow(Gamma2->HitZ - PV_Z, 2));
    float DeltaT4Gamma1 = Dist4Gamma1 / 299.79; // ns
    float DeltaT4Gamma2 = Dist4Gamma2 / 299.79; // ns
    TGamma1_InPV = Gamma1->time - DeltaT4Gamma1;
    TGamma2_InPV = Gamma2->time - DeltaT4Gamma2;
    DeltaT_Gamma = TGamma1_InPV - TGamma2_InPV;

    if (IsTru == 0 && (Parameters::Instance()->CluFlow == 1 || Parameters::Instance()->CluFlow == 2))
    {
        CalculateNearestPoint1();

        Dist2PiPi1 = sqrt(pow(Pim->GenX - NearestPointOfGamma1.X(), 2) + pow(Pim->GenY - NearestPointOfGamma1.Y(), 2) + pow(Pim->GenZ - NearestPointOfGamma1.Z(), 2));

        CalculateNearestPoint2();
        // std::cout << "Pim->GenZ " << Pim->GenZ << std::endl;
        Dist2PiPi2 = sqrt(pow(Pim->GenX - NearestPointOfGamma2.X(), 2) + pow(Pim->GenY - NearestPointOfGamma2.Y(), 2) + pow(Pim->GenZ - NearestPointOfGamma2.Z(), 2));
    }
}

void Bd_PipPimPi0::CalculateNearestPoint2()
{
    auto Gamma1 = Pi0->Sub_Gamma.first;
    auto Gamma2 = Pi0->Sub_Gamma.second;

    auto Gamma1Clu3D = Gamma1->f_Clu3D;
    auto Gamma2Clu3D = Gamma2->f_Clu3D;

    auto Gamma1Clu2D0 = Gamma1->f_Clu3D->v_Cluster2D.at(0);
    auto Gamma1Clu2D1 = Gamma1->f_Clu3D->v_Cluster2D.at(1);
    auto Gamma2Clu2D0 = Gamma2->f_Clu3D->v_Cluster2D.at(0);
    auto Gamma2Clu2D1 = Gamma2->f_Clu3D->v_Cluster2D.at(1);

    float tmpXErr, tmpYErr;
    auto Gamma1Region = Gamma1->f_region;
    TVector3 a(Gamma1Clu2D0->X(), Gamma1Clu2D0->Y(), Gamma1Clu2D0->Z());
    tmpXErr = Gamma1Region->Layers.at(0)->PosXResFun->Eval(Gamma1->E() / 1000.);
    tmpYErr = Gamma1Region->Layers.at(0)->PosYResFun->Eval(Gamma1->E() / 1000.);
    TVector3 aErr(tmpXErr, tmpYErr, 0);

    TVector3 b(Gamma1Clu2D1->X(), Gamma1Clu2D1->Y(), Gamma1Clu2D1->Z());
    tmpXErr = Gamma1Region->Layers.at(1)->PosXResFun->Eval(Gamma1->E() / 1000.);
    tmpYErr = Gamma1Region->Layers.at(1)->PosYResFun->Eval(Gamma1->E() / 1000.);
    TVector3 bErr(tmpXErr, tmpYErr, 0);

    auto Gamma2Region = Gamma2->f_region;
    TVector3 c(Gamma2Clu2D0->X(), Gamma2Clu2D0->Y(), Gamma2Clu2D0->Z());
    tmpXErr = Gamma2Region->Layers.at(0)->PosXResFun->Eval(Gamma2->E() / 1000.);
    tmpYErr = Gamma2Region->Layers.at(0)->PosYResFun->Eval(Gamma2->E() / 1000.);
    TVector3 cErr(tmpXErr, tmpYErr, 0);

    TVector3 d(Gamma2Clu2D1->X(), Gamma2Clu2D1->Y(), Gamma2Clu2D1->Z());
    tmpXErr = Gamma2Region->Layers.at(1)->PosXResFun->Eval(Gamma2->E() / 1000.);
    tmpYErr = Gamma2Region->Layers.at(1)->PosYResFun->Eval(Gamma2->E() / 1000.);
    TVector3 dErr(tmpXErr, tmpYErr, 0);

    TVector3 EntryPos1(Gamma1Clu3D->X(), Gamma1Clu3D->Z(), Gamma1Clu3D->Z());
    tmpXErr = Gamma1Region->PosXResFun->Eval(Gamma1->E() / 1000.);
    tmpYErr = Gamma1Region->PosYResFun->Eval(Gamma1->E() / 1000.);
    TVector3 EntryPos1Err(tmpXErr, tmpYErr, 0);

    TVector3 EntryPos2(Gamma2Clu3D->X(), Gamma2Clu3D->Z(), Gamma2Clu3D->Z());
    tmpXErr = Gamma2Region->PosXResFun->Eval(Gamma2->E() / 1000.);
    tmpYErr = Gamma2Region->PosYResFun->Eval(Gamma2->E() / 1000.);
    TVector3 EntryPos2Err(tmpXErr, tmpYErr, 0);

    // std::cout << "TVector3 a(" << Gamma1Clu2D0->X() << "," << Gamma1Clu2D0->Y() << "," << Gamma1Clu2D0->Z() << ");" << std::endl;
    // std::cout << "TVector3 aErr(" << aErr.X() << "," << aErr.Y() << "," << aErr.Z() << ");" << std::endl;
    // std::cout << "TVector3 b(" << b.X() << "," << b.Y() << "," << b.Z() << ");" << std::endl;
    // std::cout << "TVector3 bErr(" << bErr.X() << "," << bErr.Y() << "," << bErr.Z() << ");" << std::endl;
    // std::cout << "TVector3 c(" << c.X() << "," << c.Y() << "," << c.Z() << ");" << std::endl;
    // std::cout << "TVector3 cErr(" << cErr.X() << "," << cErr.Y() << "," << cErr.Z() << ");" << std::endl;
    // std::cout << "TVector3 d(" << d.X() << "," << d.Y() << "," << d.Z() << ");" << std::endl;
    // std::cout << "TVector3 dErr(" << dErr.X() << "," << dErr.Y() << "," << dErr.Z() << ");" << std::endl;

    // std::cout << "TVector3 EntryPos1(" << EntryPos1.X() << "," << EntryPos1.Y() << "," << EntryPos1.Z() << ");" << std::endl;
    // std::cout << "TVector3 EntryPos1Err(" << EntryPos1Err.X() << "," << EntryPos1Err.Y() << "," << EntryPos1Err.Z() << ");" << std::endl;

    // std::cout << "TVector3 EntryPos2(" << EntryPos2.X() << "," << EntryPos2.Y() << "," << EntryPos2.Z() << ");" << std::endl;
    // std::cout << "TVector3 EntryPos2Err(" << EntryPos2Err.X() << "," << EntryPos2Err.Y() << "," << EntryPos2Err.Z() << ");" << std::endl;

    // std::cout << "FinalPoint: (" << FinalPoint.X() << ", " << FinalPoint.Y() << ", " << FinalPoint.Z() << ")" << std::endl;

    std::vector<TVector3> Points1, Points2, PointsErr1, PointsErr2;
    Points1.emplace_back(a);
    Points1.emplace_back(b);
    Points1.emplace_back(EntryPos1);
    Points2.emplace_back(c);
    Points2.emplace_back(d);
    Points2.emplace_back(EntryPos2);
    PointsErr1.emplace_back(aErr);
    PointsErr1.emplace_back(bErr);
    PointsErr1.emplace_back(EntryPos1Err);
    PointsErr2.emplace_back(cErr);
    PointsErr2.emplace_back(dErr);
    PointsErr2.emplace_back(EntryPos2Err);
    auto Line1 = GetLine(Points1, PointsErr1);

    auto Line2 = GetLine(Points2, PointsErr2);
    auto FinalPoint = GetNearestPoint(Line1, Line2);

    NearestPointOfGamma2.SetXYZ(FinalPoint.X(), FinalPoint.Y(), FinalPoint.Z());
}

void Bd_PipPimPi0::CalculateNearestPoint1()
{
    auto Gamma1 = Pi0->Sub_Gamma.first;
    auto Gamma2 = Pi0->Sub_Gamma.second;
    auto Gamma1Clu2D0 = Gamma1->f_Clu3D->v_Cluster2D.at(0);
    auto Gamma1Clu2D1 = Gamma1->f_Clu3D->v_Cluster2D.at(1);
    auto Gamma2Clu2D0 = Gamma2->f_Clu3D->v_Cluster2D.at(0);
    auto Gamma2Clu2D1 = Gamma2->f_Clu3D->v_Cluster2D.at(1);

    float tmpXErr, tmpYErr;
    auto Gamma1Region = Gamma1->f_region;
    TVector3 a(Gamma1Clu2D0->X(), Gamma1Clu2D0->Y(), Gamma1Clu2D0->Z());
    tmpXErr = Gamma1Region->Layers.at(0)->PosXResFun->Eval(Gamma1->E() / 1000.);
    tmpYErr = Gamma1Region->Layers.at(0)->PosYResFun->Eval(Gamma1->E() / 1000.);
    TVector3 aErr(tmpXErr, tmpYErr, 0);

    TVector3 b(Gamma1Clu2D1->X(), Gamma1Clu2D1->Y(), Gamma1Clu2D1->Z());
    tmpXErr = Gamma1Region->Layers.at(1)->PosXResFun->Eval(Gamma1->E() / 1000.);
    tmpYErr = Gamma1Region->Layers.at(1)->PosYResFun->Eval(Gamma1->E() / 1000.);
    TVector3 bErr(tmpXErr, tmpYErr, 0);

    auto Gamma2Region = Gamma2->f_region;
    TVector3 c(Gamma2Clu2D0->X(), Gamma2Clu2D0->Y(), Gamma2Clu2D0->Z());
    tmpXErr = Gamma2Region->Layers.at(0)->PosXResFun->Eval(Gamma2->E() / 1000.);
    tmpYErr = Gamma2Region->Layers.at(0)->PosYResFun->Eval(Gamma2->E() / 1000.);
    TVector3 cErr(tmpXErr, tmpYErr, 0);

    TVector3 d(Gamma2Clu2D1->X(), Gamma2Clu2D1->Y(), Gamma2Clu2D1->Z());
    tmpXErr = Gamma2Region->Layers.at(1)->PosXResFun->Eval(Gamma2->E() / 1000.);
    tmpYErr = Gamma2Region->Layers.at(1)->PosYResFun->Eval(Gamma2->E() / 1000.);
    TVector3 dErr(tmpXErr, tmpYErr, 0);

    auto Line1 = std::pair<TVector3, TVector3>{a, b - a};

    auto Line2 = std::pair<TVector3, TVector3>{c, d - c};

    auto FinalPoint = GetNearestPoint(Line1, Line2);

    NearestPointOfGamma1.SetXYZ(FinalPoint.X(), FinalPoint.Y(), FinalPoint.Z());
}