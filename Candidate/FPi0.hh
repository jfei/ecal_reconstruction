#ifndef FJL_Pi0_hh
#define FJL_Pi0_hh

#include "FDataType.hh"
#include "FParticle.hh"

class FGamma;

class FPi0 : public FParticle
{
private:
    /* data */
public:
    FPi0(std::pair<FGamma *, FGamma *> Gammas);

    std::pair<FGamma *, FGamma *> Sub_Gamma;
    int Type; // 0: Resolved 1: merged
};

#endif