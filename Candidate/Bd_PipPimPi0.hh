#ifndef BdPipPimPi0_h
#define BdPipPimPi0_h
#include "string.h"
#include "map"
#include "FPoint.hh"

class FPi0;
class FPip;
class FPim;
class FBd0;

class Bd_PipPimPi0
{
private:
    //////////////////

    //////////////////

public:
    Bd_PipPimPi0(FPip *i_Pip, FPim *i_Pim, FPi0 *i_Pi0, int IsTru = 0);

    int event;
    FPi0 *Pi0;
    FPim *Pim;
    FPip *Pip;
    FBd0 *Bd0;
    float DeltaT_Gamma;
    float TGamma1_InPV;
    float TGamma2_InPV;
    FPoint NearestPointOfGamma1;
    FPoint NearestPointOfGamma2;
    float Dist2PiPi1;
    float Dist2PiPi2;
    void CalculateNearestPoint1();
    void CalculateNearestPoint2();
};
#endif