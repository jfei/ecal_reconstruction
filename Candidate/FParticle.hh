#ifndef FJL_Particle_hh
#define FJL_Particle_hh

#include "FDataType.hh"
#include "TLorentzVector.h"

class FCluster3D;

class FParticle : public TLorentzVector
{
private:
    /* data */
public:
    int event;
    
    int MatchedNum = -1;
};

#endif