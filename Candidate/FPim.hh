#ifndef FJL_Pim_hh
#define FJL_Pim_hh

#include "FDataType.hh"
#include "FParticle.hh"


class FPim : public FParticle
{
private:
    /* data */
public:
    FPim(float px,float py,float pz,float E)
    {
        SetPxPyPzE(px,py,pz,E);
    }
    float GenT;
    float GenX;
    float GenY;
    float GenZ;
};

#endif