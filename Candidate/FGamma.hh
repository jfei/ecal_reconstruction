#ifndef FJL_Gamma_hh
#define FJL_Gamma_hh

#include "FDataType.hh"
#include "FParticle.hh"

class FRegion;
class FModule;
class FCluster3D;

class FGamma : public FParticle
{
private:
    /* data */
public:
    FGamma()
    {
    }
    FGamma(FCluster3D *Clu3D, int i_Type = 0, int nPV = -1);
    FCluster3D *f_Clu3D;
    FRegion *f_region;
    FModule *f_module;
    float time;
    float HitX;
    float HitY;
    float HitZ;
    float GetSeedE(int layer = -1);
    float GetE(int layer = -1);
    float GetMinDist2Charge();
    float GetSeedDx(int layer = -1);
    float GetSeedDy(int layer = -1);
    int Type = 0;
};

#endif