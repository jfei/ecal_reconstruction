#include "FGamma.hh"
#include "Mixture_Calo.hh"
#include "FCluster3D.hh"
#include "FRegion.hh"
#include "FCluster2D.hh"
#include "FCell.hh"
#include "FCell3D.hh"

FGamma::FGamma(FCluster3D *Clu3D, int i_Type, int nPV)
{
    Type = i_Type;
    f_Clu3D = Clu3D;
    Mixture_Calo *my_ECAL = (Mixture_Calo *)Clu3D->f_region->f_calo;
     event = Clu3D->event;
    float cx = Clu3D->X();
    float cy = Clu3D->Y();
    float cz = Clu3D->Z();
    float ce = Clu3D->energy;
    float PVX, PVY, PVZ;

    if (nPV == -1)
    {
        PVX = 0;
        PVY = 0;
        PVZ = -12836;
    }
    else
    {
        PVX = my_ECAL->GetPrimaryX(event, nPV);
        PVY = my_ECAL->GetPrimaryY(event, nPV);
        PVZ = my_ECAL->GetPrimaryZ(event, nPV);
    }
    float px = (cx - PVX) / sqrt((cz - PVZ) * (cz - PVZ) + (cx - PVX) * (cx - PVX) + (cy - PVY) * (cy - PVY)) * ce;
    float py = (cy - PVY) / sqrt((cz - PVZ) * (cz - PVZ) + (cx - PVX) * (cx - PVX) + (cy - PVY) * (cy - PVY)) * ce;
    float pz = (cz - PVZ) / sqrt((cz - PVZ) * (cz - PVZ) + (cx - PVX) * (cx - PVX) + (cy - PVY) * (cy - PVY)) * ce;
    f_region = Clu3D->f_region;
    f_module = Clu3D->f_module;
    SetPxPyPzE(px, py, pz, ce);
    time = Clu3D->GetTime();
    HitX = Clu3D->X();
    HitY = Clu3D->Y();
    HitZ = Clu3D->Z();
    
}

float FGamma::GetSeedE(int i)
{
    if (i == -1)
    {

        return f_Clu3D->GetSeedE();
    }
    else
    {
        if (i < f_Clu3D->v_Cluster2D.size())
        {
            return f_Clu3D->v_Cluster2D.at(i)->GetSeedE();
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("FGamma: Clu3D only have {} Clu2D, but give {}", f_Clu3D->v_Cluster2D.size(), i);
            exit(0);
        }
    }
}

float FGamma::GetE(int i)
{
    if (i == -1)
    {

        return f_Clu3D->Energy();
    }
    else
    {
        if (i < f_Clu3D->v_Cluster2D.size())
        {
            return f_Clu3D->v_Cluster2D.at(i)->Energy();
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("FGamma: Clu3D only have {} Clu2D, but give {}", f_Clu3D->v_Cluster2D.size(), i);
            exit(0);
        }
    }
}

float FGamma::GetMinDist2Charge()
{
    return f_Clu3D->GetMinDist2Charge();
}

float FGamma::GetSeedDx(int i)
{
    if (i == -1)
    {
        if (f_Clu3D->Seed3D != NULL)
            return f_Clu3D->Seed3D->Dx();
        else
            return 0;
    }
    else
    {
        if (i < f_Clu3D->v_Cluster2D.size())
        {
            return f_Clu3D->v_Cluster2D.at(i)->Seed2D->Dx();
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("FGamma: Clu3D only have {} Clu2D, but give {}", f_Clu3D->v_Cluster2D.size(), i);
            exit(0);
        }
    }
}

float FGamma::GetSeedDy(int i)
{
    if (i == -1)
    {

        if (f_Clu3D->Seed3D != NULL)
            return f_Clu3D->Seed3D->Dy();
        else
            return 0;
    }
    else
    {
        if (i < f_Clu3D->v_Cluster2D.size())
        {
            return f_Clu3D->v_Cluster2D.at(i)->Seed2D->Dy();
        }
        else
        {
            spdlog::get(ERROR_NAME)->error("FGamma: Clu3D only have {} Clu2D, but give {}", f_Clu3D->v_Cluster2D.size(), i);
            exit(0);
        }
    }
}