#include "FPi0.hh"
#include "FGamma.hh"
#include "FLog.hh"
FPi0::FPi0(std::pair<FGamma *, FGamma *> Gammas)
{

    auto Gamma1 = Gammas.first;
    auto Gamma2 = Gammas.second;
    if (Gamma1->event != Gamma2->event)
    {
        spdlog::get(ERROR_NAME)->error("Two Gamma should from same event ({},{})", Gamma1->event, Gamma2->event);
    }
    SetPxPyPzE(Gamma1->Px() + Gamma2->Px(), Gamma1->Py() + Gamma2->Py(), Gamma1->Pz() + Gamma2->Pz(), Gamma1->E() + Gamma2->E());
    Sub_Gamma = {Gamma1, Gamma2};
}