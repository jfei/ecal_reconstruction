#ifndef FJL_Pip_hh
#define FJL_Pip_hh

#include "FDataType.hh"
#include "FParticle.hh"

class FPip : public FParticle
{
private:
    /* data */
public:
    FPip(float px,float py,float pz,float E)
    {
        SetPxPyPzE(px,py,pz,E);
    }
    float GenT;

    float GenX;
    float GenY;
    float GenZ;
};

#endif