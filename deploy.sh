CMAKE_ARGS="-DPLATFORM=local"

### SET ENV VARIABLES
if [ -z "$1" ]
  then
    echo "No argument supplied, so compiling with machine compiler..."
else 
  if [ $1 = "lxplus" ]; then
    set --
    echo "Sourcing compiler for lxplus..."
    source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh
    # source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
    # CMAKE_ARGS="-DCMAKE_CXX_COMPILER=`which g++` -DCMAKE_C_COMPILER=`which gcc`"
    CMAKE_ARGS="-DCMAKE_CXX_COMPILER=`which g++` -DCMAKE_C_COMPILER=`which gcc` -DCMAKE_BUILD_TYPE=Release -DPLATFORM=lxplus"
  fi
fi
mkdir -p build

echo "Compiling App..."
cd build
echo "Running cmake command as: cmake $CMAKE_ARGS ../"
cmake $CMAKE_ARGS ../
make -j8
cd ..
echo "Done."


g++ -o ./build/GetElectronSamples ./Script/GenChargeSample/GetElectronSamples.cpp $(root-config --glibs --cflags --libs) -lHist -lCore -lMathCore -lGenVector -lstdc++ 
# g++ -o ./build/app Clustering.cc ./src/FCluster3D.cc ./src/CalculateGeo.cc ./src/AnalysisAlg.cc ./src/FCluster2D.cc ./src/FCell.cc ./src/CreateTree.cc ./src/FCell3D.cc ./src/FCalorimeter.cc ./src/FModule.cc ./src/FRegion.cc ./src/Mixture_Calo.cc ./src/FLayer.cc ./src/Parameters.cc ./src/ConfigFile.cc ./src/RecAlg.C $(root-config --glibs --cflags --libs) -lHist -lCore -lMathCore -lGenVector  -I ./include -lstdc++ 
#g++ ./Script/DrawTimeResolution.C $(root-config --glibs --cflags --libs) -o ./build/DrawTimeResolution
# g++ ./Script/Matching/Matching.C $(root-config --glibs --cflags --libs) -o ./build/Matching -lstdc++
# g++ ./Script/tagCharge/tagCharge.C $(root-config --glibs --cflags --libs) -o ./build/tagCharge -lstdc++