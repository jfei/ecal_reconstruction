SiTimingCellSizes='5 5 5 5 5 5 5 5'
SiTimingNoise='15 15 15 15 15 15 15 15'
Seed3DCut='50 50 50 50 50 50 50 50'
LayerNums='4 4 4 4 4 4 4 4'
ListTypes='4 5 6 7 8 9 10 11'
DoSCor='1 1 1 1 1 1 1 1'
EnableLayerInfo='1 1 1 1 1 1 1 1'
CombineEnergyPoint2T='0 0 0 0 0 0 0 0'
CombineEnergyPoint2R='0 0 0 0 0 0 0 0'
CellECaliFunType='1 1 1 1 1 1 1 1'
CombineSiEnWhenCali='1 1 1 1 1 1 1 1'
IsRotation=1
BASEFOLDEROUT='/Users/fjl/workdir/ecal_reconstruction/Configuration/SiTiming_Rotated'
Step1Folder='/Users/fjl/workdir/ecal_reconstruction/Calibration/SiTiming_rotation/Step1/GetCaliPar'
Step2Folder='/Users/fjl/workdir/ecal_reconstruction/Calibration/SiTiming_rotation/Step2_5mm_15ps/GetCaliPar'

python3 GenCfg.py \
--listTypes ${ListTypes} \
--SiTimingCellSizes ${SiTimingCellSizes} \
--SiTimingNoise ${SiTimingNoise} \
--Seed3DCut ${Seed3DCut} \
--LayerNums ${LayerNums} \
--DoSCor ${DoSCor} \
--EnableLayerInfo ${EnableLayerInfo} \
--SiTimingCellSizes ${SiTimingCellSizes} \
--CombineEnergyPoint2T ${CombineEnergyPoint2T} \
--CombineEnergyPoint2R ${CombineEnergyPoint2R} \
--CellECaliFunType ${CellECaliFunType} \
--CombineSiEnWhenCali ${CombineSiEnWhenCali} \
--IsRotation ${IsRotation} \
--baseFolderOut ${BASEFOLDEROUT} \
--Step1Folder ${Step1Folder} \
--Step2Folder ${Step2Folder}