import math
import os
from posixpath import basename
import stat
import sys
import argparse
import subprocess
from subprocess import Popen, PIPE, STDOUT
import shutil
import random
from shutil import copyfile
RoType2Part = {
    0: 0,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 4,
    6: 4,
    7: 4,
    8: 5,
    9: 5,
    10: 5,
    11: 5,
}

Type2Part = {
    0: 0,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
}
def main(args):   
    ### Parsing args
    parser = argparse.ArgumentParser(description='Python script to prepare optical calibration run')
    parser.add_argument("--listTypes"                             , help='List of module types',
                        nargs="*"           ,
                        type=int            ,
                        default=[0]         , 
    )
    parser.add_argument("--SiTimingCellSizes"                             , help='SiTimingCellSizes / [mm]',
                        nargs="*"           ,
                        type=float            ,
                        default=[0]         , 
    )
    parser.add_argument("--SiTimingNoise"                             , help='adding timing noise /ps',
                        nargs="*"           ,
                        type=float            ,
                        default=[15]         , 
    )
    parser.add_argument("--Seed3DCut"                             , help='Seed3D pt Cut /MeV',
                        nargs="*"           ,
                        type=float            ,
                        default=[50]         , 
    )
    parser.add_argument('--baseFolderOut'    , default=''         , help='output folder'                 , required=True)
    parser.add_argument('--Step1Folder'                                , help='Step1Folder'                  , required=True) 
    parser.add_argument('--Step2Folder'                                , help='Step2Folder'                  , required=True) 
    parser.add_argument('--IsRotation'       , default=1              , help='IsRotation'           , required=True) 
    parser.add_argument("--DoSCor"                             , help='Do S Corretion 0 : no 1 : yes',
                        nargs="*"           ,
                        type=int            ,
                        default=[1]         , 
    ) 
    parser.add_argument("--EnableLayerInfo"                             , help='= 0 : Get Final Pos by whole detector S and L Cor    1 : Get Pos Layer by Layer',
                        nargs="*"           ,
                        type=int            ,
                        default=[1]         , 
    )
    parser.add_argument("--CombineEnergyPoint2T"                             , help='= 0 : Get Final Pos by whole detector S and L Cor    1 : Get Pos Layer by Layer',
                        nargs="*"           ,
                        type=int            ,
                        default=[0]         , 
    )
    parser.add_argument("--CombineEnergyPoint2R"                             , help='= 0 : Get Final Pos by whole detector S and L Cor    1 : Get Pos Layer by Layer',
                        nargs="*"           ,
                        type=int            ,
                        default=[0]         , 
    )
    parser.add_argument("--CellECaliFunType"                             , help='CellECaliFunType =0 : linear 1 : log',
                        nargs="*"           ,
                        type=int            ,
                        default=[1]         , 
    )
    parser.add_argument("--CombineSiEnWhenCali"                             , help='try to combine multi-silicon layers when energy calibration',
                        nargs="*"           ,
                        type=int            ,
                        default=[1]         , 
    )

    parser.add_argument("--LayerNums"                             , help='LayerNums',
                        nargs="*"           ,
                        type=int            ,
                        default=[2]         , 
    )
    ### Read arguments
    args = parser.parse_args()
    
    listTypes                  = args.listTypes
    SiTimingCellSizes                  = args.SiTimingCellSizes
    SiTimingNoise                  = args.SiTimingNoise
    Seed3DCut                  = args.Seed3DCut
    LayerNums                  = args.LayerNums
    baseFolderOut                  = args.baseFolderOut
    Step1Folder                  = args.Step1Folder
    Step2Folder                  = args.Step2Folder
    IsRotation                   = args.IsRotation
    baseFolderOut       = os.path.abspath(baseFolderOut)
    Step1Folder         =os.path.abspath(Step1Folder)
    Step2Folder         =os.path.abspath(Step2Folder)
    DoSCor = args.DoSCor
    EnableLayerInfo = args.EnableLayerInfo
    CombineEnergyPoint2T = args.CombineEnergyPoint2T
    CombineEnergyPoint2R = args.CombineEnergyPoint2R
    CellECaliFunType = args.CellECaliFunType
    CombineSiEnWhenCali = args.CombineSiEnWhenCali

    
    if len(listTypes) != len(SiTimingCellSizes):
          print("ERROR! Lists of types and list of SiTimingCellSize must have the same length!!! Aborting...")
          sys.exit(1)
    if len(listTypes) != len(SiTimingNoise):
          print("ERROR! Lists of types and list of SiTimingNoise must have the same length!!! Aborting...")
          sys.exit(1)
    
      #create output folder
    if not os.path.exists(baseFolderOut):
        print("Creating output folder at %s" %(baseFolderOut))
        os.makedirs(baseFolderOut)
    else:
        print("Output folder exists at %s" %(baseFolderOut))
        
    if not os.path.exists(Step1Folder):
        print("Step1 path doesn't exist %s" %(Step1Folder))
        sys.exit(1)

    if not os.path.exists(Step2Folder):
        print("Step2 path doesn't exist %s" %(Step2Folder))
        sys.exit(1)
        
    for index in range(len(listTypes)):
        Part=0
        if IsRotation :
            Part=RoType2Part[listTypes[index]]
        else:
            Part=Type2Part[listTypes[index]]
        
            
        CfgScript = "Type" + str(listTypes[index]) + ".cfg"
        OutFile = os.path.join(baseFolderOut,CfgScript)
        with open(OutFile, "w") as output:
            
            output.write("UsingPar = 1\n")
            output.write("# Silicon cell size\n")
            output.write("SiTimingCellSize = %.1f\n"%SiTimingCellSizes[index])
            output.write("# Constant term of time noise of silicon cell\n")
            output.write("SiTimingNoise = %.1f\n"%SiTimingNoise[index])
            output.write("# Seed3D pt Cut /MeV\n")
            output.write("Seed3DCut = %.1f\n"%Seed3DCut[index])
            output.write("#Allocate Cell Algorithm For Each Layer\n")
            output.write("# 0 : by fix window\n")
            output.write("# 1 : by gradient\n")
            output.write('Clu2DType=|')
            for layer in range(0,LayerNums[index]):
                output.write('0|')
            output.write('\n')
            output.write("#Only using time of seed\n")
            output.write('OnlySeedT=|')
            for layer in range(0,LayerNums[index]):
                output.write('1|')
            output.write('\n')
            
            output.write("DoSCor = %d\n"%DoSCor[index])  
            output.write("EnableLayerInfo = %d\n"%EnableLayerInfo[index])  
            output.write("CombineEnergyPoint2T = %d\n"%CombineEnergyPoint2T[index])  
            output.write("CombineEnergyPoint2R = %d\n"%CombineEnergyPoint2R[index])  
            output.write("CellECaliFunType = %d\n"%CellECaliFunType[index])  
            output.write("CombineSiEnWhenCali = %d\n"%CombineSiEnWhenCali[index])  
            
            output.write("#CellCali Parameters\n")
            with open("%s/Part%d_CellCaliPar2.txt"%(Step1Folder,Part), "r") as CellCaliFile:
                for line in CellCaliFile:
                    output.write(line)
                CellCaliFile.close()
            output.write("#E Parameters\n")
            with open("%s/Part%d_EPar2.txt"%(Step2Folder,Part), "r") as ECaliFile:
                for line in ECaliFile:
                    output.write(line)
                ECaliFile.close()
            output.write("#T Parameters\n")
            with open("%s/Part%d_TPar2.txt"%(Step2Folder,Part), "r") as TCaliFile:
                for line in TCaliFile:
                    output.write(line)
                TCaliFile.close()   
            output.write("#Layer T Res\n")
            with open("%s/../TestCali/Part%d_LayerTPar2.txt"%(Step2Folder,Part), "r") as TCaliFile:
                for line in TCaliFile:
                    output.write(line)
                TCaliFile.close()  
            output.write("#T Res\n")
            with open("%s/../TestCali/Part%d_TPar2.txt"%(Step2Folder,Part), "r") as TCaliFile:
                for line in TCaliFile:
                    output.write(line)
                TCaliFile.close()  
            output.write("#S Parameters\n")      
            with open("%s/Type%d_SPar2.txt"%(Step2Folder,listTypes[index]), "r") as SCaliFile:
                for line in SCaliFile:
                    output.write(line)
                SCaliFile.close()     
            output.write("#L Parameters\n")       
            with open("%s/Type%d_LPar2.txt"%(Step2Folder,listTypes[index]), "r") as LCaliFile:
                for line in LCaliFile:
                    output.write(line)
                LCaliFile.close()       
            output.write("#Pos Res\n")
            with open("%s/../TestCali/Part%d_LayerXYPar2.txt"%(Step2Folder,Part), "r") as TCaliFile:
                for line in TCaliFile:
                    output.write(line)
                TCaliFile.close()                                  
            output.close()


if __name__ == "__main__":
   main(sys.argv[1:])