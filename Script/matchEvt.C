#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
#include "iostream"
void matchEvt()
{
    TFile *bkfile = new TFile("/mnt/nas/InputFlux/1.5e34/FluxGammaB2KstGamma_MB_Merged_1.5e34.root");
    TTree *bkTree = (TTree *)bkfile->Get("tree");
    TTree *bksTree1 = bkTree->CopyTree("treeIndex>-1");
    TFile *sfile = new TFile("/mnt/nas/InputFlux/1.5e34/FluxGammaFromB2KstGamma_1.5e34.root");
    TTree *sTree = (TTree *)sfile->Get("tree");
    double seTot;
    sTree->SetBranchAddress("eTot", &seTot);
    int loop = 0;
    for (int evt = 0; evt < sTree->GetEntries(); evt++)
    {
        // std::cout << "evt : " << evt << std::endl;
        sTree->GetEntry(evt);
        TTree *bksTree = bksTree1->CopyTree(Form("evtIndex==%d", evt - loop));
        bksTree->GetEntry(0);
        double Currentbks = bksTree->GetLeaf("eTot")->GetValue();

        if (abs(Currentbks - seTot) > 1e-5)
        {

            for (loop = 0; loop < 10; loop++)
            {
                bksTree = bksTree1->CopyTree(Form("evtIndex==%d", evt - loop));
                bksTree->GetEntry(0);
                double Currentbks = bksTree->GetLeaf("eTot")->GetValue();
                if (abs(Currentbks - seTot) <= 1e-5)
                {
                    break;
                }
            }

            //每次发生改变时输出
            std::cout << "in evt : " << evt - loop << " loop : " << loop << std::endl;
        }
    }
}