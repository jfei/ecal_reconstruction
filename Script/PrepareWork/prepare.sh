#!/bin/bash



#########################################
# SOME OPERATIONS...                    #
#########################################
### Store cmd args
PLATFORM=$1
### Prepare some flags
PLATFORM_FLAG=''
### Some variable declaration
RECPATH=''
LISTCALIBRATIONS=''

### Check cmd args number
if [ "$#" -ne 1 ]; then
  echo "You need to provide 1 arguments:"
  echo "./prepare.sh [local|lxplus|grid]"
  exit 1
fi
### Check cmd args values 
## PLATFORM
if [ ${PLATFORM} != "local" ] && [ ${PLATFORM} != "lxplus" ] && [ ${PLATFORM} != "grid" ]&& [ ${PLATFORM} != "slurm" ]; then
  echo "Invalid argument ${PLATFORM} . Valid choices for argument 1 are [local|lxplus|grid]"
  exit 1
fi
### Set env variables, flags etc 
### for PLATFORM (MODALITY needs to be done later)
if [ ${PLATFORM} = "local" ]; then
  PLATFORM_FLAG='--local'
else 
  if [ ${PLATFORM} = "lxplus" ]; then
    PLATFORM_FLAG=''
    source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
  else 
    if [ ${PLATFORM} = "grid" ]; then
      PLATFORM_FLAG='--grid'
      source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
    fi
    if [ ${PLATFORM} = "slurm" ]; then
      PLATFORM_FLAG='--slurm'
    fi
  fi
fi
#########################################




### Modify what you need among these 3 cases, following guidelines above. Obviously you 
### Need to modify only flag relative to your current use case
if [ ${PLATFORM} = "local" ]; then
  RECPATH='/Users/fjl/workdir/ecal_reconstruction'
elif [ ${PLATFORM} = "lxplus" ]; then
  RECPATH='/afs/cern.ch/work/j/jfei/spacal-simulation/'
elif [ ${PLATFORM} = "grid" ]; then
  RECPATH='/home/fjl/ecal_reconstruction'
elif [ ${PLATFORM} = "slurm" ]; then
  RECPATH='/home/fjl/ecal_reconstruction'
fi
#########################################



BASEFOLDEROUT='./'
BASEFOLDERIN='./'
BASEFOLDERJOB='jobs'
LISTDIR='./'

LISTFILES='-1'
LISTQUEUE='testmatch' 
DISK='100GB'    
RecProgram='0' # 0 clustering 1 cell calibration 2 Clu correction 3. Transform format 4. Check resolution
ChargeSampleFilePrefix='/Simulation/FullECALSim/B2pipipi0/11102401/1.5e34/ChargeSample/SPACAL/ElectronSample_'
SimFileName='out' #If it's a full filename and not a prefix, you have to give an absolute path.
TrigFileName='OutTrigd_' #If it's a full filename and not a prefix, you have to give an absolute path.
FluxFileName='./FluxGammaFromB2pipipi0_SPACAL.root'
RecFileName=''
RecGeoFileName=$RECPATH'/Layout/Run5_2024baseline/SimGeo.root' #If it's a full filename and not a prefix, you have to give an absolute path.
TruthFileType='2' # Init Pv and Hit information 1 : From SimFile（out.root)  2:from flux file  3:from rec file (RecProgram==3)
GeoFilesType='2' # Init Geometry 1:from Sim（out.root) and TrigOut_ 2: from Rec Geometry File
DataFileType='1' # Init readout information 1:from TrigOut_ 2:3:from rec file (RecProgram==3)
InitTruthInfo='1' #Whether to initialise Truth information 
listTypes='1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16'
DoS1Cor='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0'
listLayerNum='2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2'
WindowShape='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0' # 0: 3*3 1: 2*2 2: cross 3: gradient
WindowShape4Layers='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0'
Seed2DCut='25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25' # 2*16
Seed3DCut='50' #pt/MeV
LISTCALIBRATIONS=(
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type1_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type2_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type3_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type4_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type5_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type6_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type7_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type8_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type9_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type10_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type11_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type12_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type13_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type14_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type15_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/CorrectionFile/Type16_CaliFile.root"
) 
CluFlow='2'
Candidates='Bd02PipPimPi0'
listSplitLayer='01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01'
tagCharge=1
#
RecFileName_FLAG=''
if [ "${RecFileName}" ]; then
RecFileName_FLAG="--RecFileName ${RecFileName}"
fi
SimFileName_FLAG=''
if [ -n "${SimFileName}" ]; then
  SimFileName_FLAG="--SimFileName ${SimFileName}"
fi
FluxFileName_FLAG=''
if [ -n "${FluxFileName}" ]; then
  FluxFileName_FLAG="--FluxFileName ${FluxFileName}"
fi
TrigFileName_FLAG=''
if [ -n "${TrigFileName}" ]; then
  TrigFileName_FLAG="--TrigFileName ${TrigFileName}"
fi
RecGeoFileName_FLAG=''
if [ -n "${RecGeoFileName}" ]; then
  RecGeoFileName_FLAG="--RecGeoFileName ${RecGeoFileName}"
fi

#########################################

#########################################
### Now we can set some flags...

if [ ${PLATFORM} = "lxplus" ]; then
  QUEUE_FLAG="--listQueue ${LISTQUEUE}"
  if [ "${DISK}" != '' ]; then
    DISK_FLAG="--requestDisk ${DISK}"
  fi
fi
#########################################

#########################################
# THE ACTUAL PREPARE COMMAND            #
#########################################

python3 ${RECPATH}/Script/PrepareWork/PrepareWorkFlow.py ${PLATFORM_FLAG} ${QUEUE_FLAG} ${DISK_FLAG} \
--listCalibrations ${LISTCALIBRATIONS[@]} \
--listFiles ${LISTFILES} \
--build ${RECPATH}/build \
--baseFolderOut ${BASEFOLDEROUT} \
--baseFolderIn ${BASEFOLDERIN} \
--listDir ${LISTDIR} \
--baseFolderJobs ${BASEFOLDERJOB} \
${RecFileName_FLAG} \
${SimFileName_FLAG} \
${FluxFileName_FLAG} \
${TrigFileName_FLAG} \
${RecGeoFileName_FLAG} \
--GeoFilesType ${GeoFilesType} \
--TruthFileType ${TruthFileType} \
--DataFileType ${DataFileType} \
--InitTruthInfo ${InitTruthInfo} \
--RecProgram ${RecProgram} \
--listTypes ${listTypes} \
--Seed3DCut ${Seed3DCut} \
--Seed2DCut ${Seed2DCut} \
--WindowShape ${WindowShape} \
--WindowShape4Layers ${WindowShape4Layers} \
--CluFlow ${CluFlow} \
--listSplitLayer ${listSplitLayer} \
--listLayerNum ${listLayerNum} \
--Candidates ${Candidates}\
--tagCharge ${tagCharge} \
--DoS1Cor ${DoS1Cor} \
--ChargeSampleFilePrefix ${ChargeSampleFilePrefix}



