
import math
import os
import glob
from posixpath import basename
import stat
import sys
import argparse
import subprocess
from subprocess import Popen, PIPE, STDOUT
import shutil
import random
from shutil import copyfile
def main(args):

  ### Parsing args
  parser = argparse.ArgumentParser(description='Python script to prepare optical calibration run')
  parser.add_argument('--baseFolderJobs'   , default=''         , help='Job scripts folder'                , required=True)
  parser.add_argument('--baseFolderOut'    , default=''         , help='Rec output folder'                 , required=True)
  parser.add_argument('--baseFolderIn'    , default=''         , help='Rec input folder'                 , required=True)
  parser.add_argument("--listTypes"                             , help='List of module types',
                       nargs="*"           ,
                       type=int            ,
                       default=[0]         , 
                       required=True  ,
  )
  parser.add_argument('--build'                                 , help='path to rec build folder'       , required=True)
  parser.add_argument('--RecProgram'      ,type=int    , default=3         , help='Default will just save information', required=True)
  parser.add_argument('--CluFlow'      ,type=int    , default=2         , help='Default will just save information')
  parser.add_argument('--RatioECor'      ,type=int    , default=0         , help='1:use ratio do ECor 0:unused ratio do ECor')
  parser.add_argument('--GeoFilesType'      ,type=int    , default=1         , help='1 : SimFile 2: RecGeoFile')
  parser.add_argument('--TruthFileType'      ,type=int    , default=1         , help='1 : SimFile (both primary and hit info) 2 : Flux file (only primary) 3 :Rec File', required=True)
  parser.add_argument('--DataFileType'      ,type=int    , default=1         , help='1: TrigFileName 2 : RecFile', required=True)
  parser.add_argument('--InitTruthInfo'      ,type=int    , default=1         , help='Default using the file from reconstruction software', required=True)
  parser.add_argument('--ConstructSiliconCell'      ,type=int    , default=0        , help='')
  parser.add_argument("--listSiTimingCell"                             , help='List of SiTiming Cell Size',
                       nargs="*"           ,
                       type=float            ,
                       default=[5.]         , 
  )
  parser.add_argument("--listLayerNum"                             , help='Number of layers for each region',
                       nargs="*"           ,
                       type=int            ,
                       default=[2]         , 
  )
  parser.add_argument("--Seed2DCut"                             , help='Cut for Seed2D in each layer, Pt/MeV',
                       nargs="*"           ,
                       type=float            ,
                       default=[25.,25.]         , 
  )
  parser.add_argument("--DoS1Cor"                             , help='DoS1Cor',
                       nargs="*"           ,
                       type=int            ,
                       default=[0]         , 
  )
  parser.add_argument("--WindowShape4Layers"                             , help='Window shape for each layer',
                       nargs="*"           ,
                       type=int            ,
                       default=[0,0]         , 
  )
  parser.add_argument("--WindowShape"                             , help='Window shape for each layer',
                       nargs="*"           ,
                       type=int            ,
                       default=[0]         , 
  )
  parser.add_argument("--Seed3DMethod"                             , help='0: 3_3 1: Cross 2:Fork',
                       nargs="*"           ,
                       type=int            ,
                       default=[0]         , 
  )
  parser.add_argument("--listSiTimingNoise"                             , help='List of constant term of time noise of SiTiming',
                       nargs="*"           ,
                       type=float            ,
                       default=[15.]         , 
  )
  parser.add_argument("--listSplitLayer"                             , help='Split Layer',
                       nargs="*"           ,
                       type=int            ,
                       default=[0]         , 
  )
  parser.add_argument("--listEnPerMIP"                             , help='List of energy per MIPs in one silicon cell',
                       nargs="*"           ,
                       type=float            ,
                       default=[0.084113]         , 
  )
  parser.add_argument('--Seed3DCut'      ,type=float    , default=50         , help='Pt/MeV')
  parser.add_argument('--SimFileName'       , default=""         , help='SimFileName')
  parser.add_argument('--TrigFileName'       , default=""         , help='TrigFileName')
  parser.add_argument('--RecFileName'       , default=""         , help='RecFileName')
  parser.add_argument('--RecGeoFileName'       , default=""         , help='RecGeoFileName')
  parser.add_argument('--FluxFileName'       , default=""         , help='FluxFileName') 
  parser.add_argument('--ChargeSampleFilePrefix'       , default=""         , help='ChargeSampleFilePrefix') 
  parser.add_argument("--listDir"                            , help='List of energy points to simulate',
                       nargs="*"           ,
                       type=str          ,
                       default=[""]         ,
  )
  parser.add_argument("--listFiles"                            , help='How many files in each directory',
                       nargs="*"           ,
                       type=int            ,
                       default=[-1]       ,
  )
  parser.add_argument("--listQueue"                             , help='List of queues per job',
                       nargs="*"           ,
                       type=str            ,
                       default=["workday"] , 
  )
  parser.add_argument("--listCalibrations"                      , help='List of calibration files per module type',
                       nargs="*"           ,
                       type=str            ,
                       default=[""]        , 
  )
  parser.add_argument("--Candidates"                      , help='B2KstGamma, B2D0GammaPi,Bs02JPsiPi0,Bd02PipPimPi0',
                       nargs="*"           ,
                       type=str            ,
                       default=[""]        , 
  )
  parser.add_argument("--ConstructPi0"                      , help='ConstructPi0',
                       type=int            ,
                       default=0        , 
  )
  parser.add_argument("--ConstructElectron"                      , help='ConstructElectron',
                       type=int            ,
                       default=0        , 
  )
  parser.add_argument("--ConstructGamma"                      , help='ConstructGamma',
                       type=int            ,
                       default=0        , 
  )
  parser.add_argument("--Verbose"                      , help='ConstructGamma',
                       type=int            ,
                       default=1        , 
  )
  parser.add_argument('--tagCharge'      ,type=int    , default=0         , help='Wether tag charge cluster')

  # running options
  parser.add_argument('--local'      , action='store_true'      , help='Jobs will be run locally')
  parser.add_argument('--grid'       , action='store_true'      , help='Jobs will be run on grid via ganga')
  parser.add_argument('--slurm'      , action='store_true'      , help='Jobs will be run by slurm')

  parser.add_argument('--requestDisk' )


  
  
  ### Read arguments
  args = parser.parse_args()

  # assign variables
  baseFolderOut              = args.baseFolderOut
  baseFolderIn               = args.baseFolderIn
  baseFolderJobs             = args.baseFolderJobs
  listTypes                  = args.listTypes
  DoS1Cor                   = args.DoS1Cor
  build                      = args.build
  listDir                    = args.listDir
  listFiles                  = args.listFiles
  listQueue                  = args.listQueue
  listCalibrations           = args.listCalibrations
  RecProgram                 = args.RecProgram
  CluFlow                   = args.CluFlow
  RatioECor                   = args.RatioECor
  GeoFilesType               = args.GeoFilesType
  TruthFileType               = args.TruthFileType
  DataFileType               = args.DataFileType
  SimFileName               = args.SimFileName
  TrigFileName               = args.TrigFileName
  RecFileName               = args.RecFileName
  FluxFileName               = args.FluxFileName
  RecGeoFileName               = args.RecGeoFileName
  ChargeSampleFilePrefix               = args.ChargeSampleFilePrefix
  InitTruthInfo               = args.InitTruthInfo
  ConstructSiliconCell         = args.ConstructSiliconCell
  local                      = args.local
  grid                       = args.grid
  slurm                      = args.slurm
  requestDisk                = args.requestDisk     
  tagCharge                  =args.tagCharge
  Candidates                 =args.Candidates
  listSiTimingNoise          = args.listSiTimingNoise
  listSiTimingCell           = args.listSiTimingCell
  listEnPerMIP              =args.listEnPerMIP
  Seed3DCut                  = args.Seed3DCut
  Seed2DCut                  = args.Seed2DCut
  ConstructPi0              =args.ConstructPi0
  ConstructGamma              =args.ConstructGamma
  ConstructElectron              =args.ConstructElectron
  listSplitLayer            =args.listSplitLayer
  WindowShape            =args.WindowShape
  Seed3DMethod            =args.Seed3DMethod
  WindowShape4Layers            =args.WindowShape4Layers
  listLayerNum          = args.listLayerNum
  Verbose            =args.Verbose
  
  
  if (GeoFilesType == 1 or TruthFileType == 1 or ConstructSiliconCell == 1):
    if (SimFileName==''):
      print("ERROR! Need Sim File (default is out*.root)")
      sys.exit(1)
    else:
      print("SimFileName : ",SimFileName)
  if (GeoFilesType == 2):
    if (RecGeoFileName==''):
      print("ERROR! Need Rec Geometry File (default is SimGeo.root)")
      sys.exit(1)
    else:
      print("RecGeoFileName : ",RecGeoFileName)
  if (TruthFileType == 2):
    if (FluxFileName==''):
      print("ERROR! Need Flux File (default is flux_.root)")
      sys.exit(1)
    else:
      print("FluxFileName : ",FluxFileName)  
  if (TruthFileType == 3 or DataFileType==2):
    if (RecFileName==''):
      print("ERROR! Need Rec File")
      sys.exit(1)
    else:
      print("RecFileName : ",RecFileName) 
  if (DataFileType == 1 or GeoFilesType==1):
    if (TrigFileName==''):
      print("Need Trig File (default is OutTrigd_*.root)")
      sys.exit(1)
    else:
      print("TrigFileName : ",TrigFileName)  

  CandidateList = ("B2KstGamma","B2D0GammaPi","Bs02JPsiPi0","Bd02PipPimPi0")
  
  newCandidates=[]
  if(len(Candidates)!=0):
    for Candidate in Candidates:
      if(Candidate in CandidateList):
        newCandidates.append(Candidate)

  if(tagCharge>0 and (ChargeSampleFilePrefix=='')):
      print("ERROR! Please provide the chargeSample!")
      sys.exit(1)
    #check lists size
  if len(listDir) != len(listFiles):
      print("ERROR! Lists of Dir and events must have the same length!!! Aborting...")
      sys.exit(1)
  # check Lists of energies and queues only in lxplus case (irrelevant otherwise)
  if((grid == False) and (local==False) and (slurm==False)):
    if len(listDir) != len(listQueue):
        print("ERROR! Lists of Dir and queues must have the same length!!! Aborting...")
        sys.exit(1)
  
  if(RecProgram!=1 and  RecProgram!=3): # need to do cluster
    if(CluFlow==2 or CluFlow==3): # construct Clu3D firstly
      if len(listTypes) != len(WindowShape):
        print("ERROR! Lists of Types and WindowShape must have the same length!!! Aborting...")
        sys.exit(1)
      if len(listTypes) != len(Seed3DMethod):
        print("ERROR! Lists of Types and Seed3DMethod must have the same length!!! Aborting...")
        sys.exit(1)
    if(CluFlow==1 or CluFlow==2):
      if sum(listLayerNum) != len(WindowShape4Layers):
        print("ERROR! Length of WindowShape4Layers (%d) must be same with the sum of the listLayerNum (%d)!!! Aborting..."%(len(WindowShape4Layers),sum(listLayerNum)))
        sys.exit(1)
      if sum(listLayerNum) != len(Seed2DCut):
        print("ERROR! Length of Seed2DCut (%d) must be same with the sum of the listLayerNum (%d)!!! Aborting..."%(len(Seed2DCut),sum(listLayerNum)))
        sys.exit(1)
      if(RecProgram==0 or RecProgram==4):
        if  len(listTypes) != len(DoS1Cor):
          print("ERROR! Lists of Types and DoS1Cor must have the same length!!! Aborting...")
          sys.exit(1)
    if(RecProgram==0):
      if len(listTypes) != len(listSplitLayer):
          print("ERROR! Lists of Types and SplitLayer must have the same length!!! Aborting...")
          sys.exit(1)
  if(ConstructSiliconCell==1):
    if len(listTypes) != len(listSiTimingCell):
        print("ERROR! Lists of Types and SiTimingCells must have the same length!!! Aborting...")
        sys.exit(1)
        
    if len(listTypes) != len(listEnPerMIP):
        print("ERROR! Lists of Types and EnPerMIP must have the same length!!! Aborting...")
        sys.exit(1)

    if len(listTypes) != len(listSiTimingNoise):
        print("ERROR! Lists of Types and SiTimingNoise must have the same length!!! Aborting...")
        sys.exit(1)


  
  ### abs paths
  ## to be consistent with the past, baseFolderJobs can be just a name for a subfolder 
  ## in current folder, or any specific path 
  ## so the identifier has to be just the last part of the path, it it's a path
  ## otherwise it's the entire name. the path can be absolute
  script_identifier = os.path.basename(baseFolderJobs)
  ## all files are abs paths, so if no path is specified, the path of current folder will be used

  baseFolderJobs   = os.path.abspath(baseFolderJobs)
  baseFolderOut    = os.path.abspath(baseFolderOut)
  baseFolderIn    = os.path.abspath(baseFolderIn)
  build            = os.path.abspath(build)
  if RecGeoFileName.endswith(".root"):
    RecGeoFileName = os.path.abspath(RecGeoFileName)

  if not os.path.exists(baseFolderIn):
    print("ERROR! InputPath %s not exist!!! Aborting..." % baseFolderIn)
    sys.exit(1)

  ## calc args list lengths
  listArgLines = []
  listGevFolders = []
  ListFoldersFileNums=[]
  FilePrefix=''
  if(DataFileType==1):
    FilePrefix=TrigFileName
  else: 
    FilePrefix=RecFileName
  for i in range(len(listFiles)) :
    CurrentPath = os.path.join(baseFolderIn,listDir[i])
    if not os.path.exists(CurrentPath):
        print("ERROR! InputPath %s not exist!!! Aborting..." % CurrentPath)
        sys.exit(1)
    if (listFiles[i]==-1):
        totalNum=0
        ListFileNums=[]
        for fileName in os.listdir(CurrentPath):
          if(fileName.startswith(FilePrefix) and fileName.endswith(".root")):
              totalNum=totalNum+1
              ListFileNums.append(int(fileName.split(".")[0].split(FilePrefix)[1]))
        ListFileNums.sort()
        listArgLines.append(totalNum)
        ListFoldersFileNums.append(ListFileNums)
    else :
        totalNum=0
        ListFileNums=[]
        for fileName in os.listdir(CurrentPath):
          if(fileName.startswith(FilePrefix) and fileName.endswith(".root")):
              totalNum=totalNum+1
              ListFileNums.append(int(fileName.split(".")[0].split(FilePrefix)[1]))
              if(totalNum>=listFiles[i]):
                break
        ListFileNums.sort()
        listArgLines.append(totalNum)
        ListFoldersFileNums.append(ListFileNums)
    listGevFolders.append(CurrentPath)      
  request_disk = False
  if requestDisk is not None:
      request_disk = True
      request_disk_space = requestDisk



  print("")
  print("########################################")
  print("# FEEDBACK                             #")
  print("########################################")
  print("")
  print("baseFolderOut         = %s" % baseFolderOut)
  print("baseFolderJobs        = %s" % baseFolderJobs)
  print("baseFolderIn         = %s" % baseFolderIn)
  print("listTypes            = %s" % listTypes)
  print("DoS1Cor            = %s" % DoS1Cor)
  print("build                 = %s" % build)
  print("listDir            = %s" % listDir)
  print("listFiles            = %s" % listFiles)
  print("listCalibrations      = %s" % listCalibrations)
  print("listArgLines          = %s" % listArgLines)
  print("ListFoldersFileNums          = %s" % ListFoldersFileNums)
  print("local                 = %s" % local)
  print("grid                  = %s" % grid)
  print("slurm                 = %s" % slurm)
  print("RecProgram                 = %s" % RecProgram)
  print("CluFlow                 = %s" % CluFlow)
  print("RatioECor                 = %s" % RatioECor)
  print("GeoFilesType                 = %s" % GeoFilesType)
  print("TruthFileType                  = %s" % TruthFileType)
  print("DataFileType                  = %s" % DataFileType)
  print("SimFileName                 = %s" % SimFileName)
  print("TrigFileName                  = %s" % TrigFileName)
  print("RecFileName                  = %s" % RecFileName)
  print("FluxFileName                  = %s" % FluxFileName)
  print("RecGeoFileName                  = %s" % RecGeoFileName)
  print("InitTruthInfo                  = %s" % InitTruthInfo)
  print("Candidates                 = %s" % Candidates)
  print("tagCharge                  = %s" % tagCharge)
  print("listSiTimingNoise             = %s" % listSiTimingNoise)
  print("listSiTimingCell             = %s" % listSiTimingCell)
  print("listEnPerMIP             = %s" % listEnPerMIP)
  print("ConstructSiliconCell             = %s" % ConstructSiliconCell)
  print("ConstructPi0             = %s" % ConstructPi0)
  print("ConstructGamma             = %s" % ConstructGamma)
  print("ConstructElectron             = %s" % ConstructElectron)
  print("Seed3DCut             = %s" % Seed3DCut)
  print("Seed3DMethod             = %s" % Seed3DMethod)
  print("Seed2DCut             = %s" % Seed2DCut)
  print("WindowShape             = %s" % WindowShape)
  print("WindowShape4Layers             = %s" % WindowShape4Layers)
  print("listLayerNum             = %s" % listLayerNum)
  
  ## make job folders in a subfolder here
  if not os.path.exists(baseFolderJobs):
      print("Creating job folder at %s" %(baseFolderJobs))
      os.makedirs(baseFolderJobs)
  else:
      print("Job folder exists at %s" %(baseFolderJobs))

  #create output folder
  if not os.path.exists(baseFolderOut):
      print("Creating output folder at %s" %(baseFolderOut))
      os.makedirs(baseFolderOut)
  else:
      print("Output folder exists at %s" %(baseFolderOut))




  ##############################
  # Reconstruction Config File
  ##############################
  newSubConfigFile=[]
  tmpLayerCount=0
  if len(listTypes)>0 and listTypes[0]!=0:
        for index in range(len(listTypes)):
          newSubConfigFile.append(baseFolderJobs + "/Type_" +str(listTypes[index])+".cfg")
          with open(newSubConfigFile[index], "w") as output: 
            if (ConstructSiliconCell==1):
              output.write("# setting sitiming cell size \n")
              output.write("SiTimingCellSize = ")
              output.write(str(listSiTimingCell[index]))
              output.write("\n")
              output.write("# adding timing noise /ps \n")
              output.write("SiTimingNoise = ")
              output.write(str(listSiTimingNoise[index]))
              output.write("\n")
              output.write("# Energy per MIPs \n")
              output.write("EnPerMIP = ")
              output.write(str(listEnPerMIP[index]))
              output.write("\n")
            if(RecProgram!=1 and  RecProgram!=3):
              if(CluFlow==2 or CluFlow==3): # construct Clu3D firstly
                output.write("Seed3DCut = %d \n" %Seed3DCut)
                output.write("WindowShape = %d \n" %WindowShape[index])
                output.write("Seed3DMethod = %d \n" %Seed3DMethod[index])
              if(CluFlow==1 or CluFlow==2):
                output.write("WindowShape4Layers = |" )
                for layer in range(listLayerNum[index]):
                  output.write("%d|" %WindowShape4Layers[tmpLayerCount+layer])
                output.write("\n")
                ####################
                output.write("Seed2DCut = |" )
                for layer in range(listLayerNum[index]):
                  output.write("%d|" %Seed2DCut[tmpLayerCount+layer])
                output.write("\n")
              if(RecProgram==0):
                output.write("SplitLayer = %d \n" %listSplitLayer[index])
              if(RecProgram==0 or RecProgram==4):
                output.write("DoS1Cor = %d \n" %DoS1Cor[index])
            tmpLayerCount=tmpLayerCount+listLayerNum[index]
          output.close()   
 

  newRecConfigFile=[]
  for index in range(len(listDir)):
    # create out err log folders, only if --grid is false
      
    ### OUTPUT folder for each energy
    newOutFolder = baseFolderOut + "/" + str(listDir[index])
    if not os.path.exists(newOutFolder):
        os.makedirs(newOutFolder)
    else:
        print("Output subfolder exists at %s" %(newOutFolder))
        
    NewInPath = os.path.join(baseFolderIn,listDir[index])
    #temp file names

    local_out_folder="./" 
    if (local == True) or (grid == True) or (slurm == True):
        local_out_folder=newOutFolder

        


    ### JOB folder for each energy
    newFolder = baseFolderJobs + "/" + str(listDir[index]) 
    if not os.path.exists(newFolder):
        print("Creating job subfolder at %s" %(newFolder))
        os.makedirs(newFolder)
    else:
        print("Output folder exists at %s" %(newFolder))

    
    newRecConfigFile.append(baseFolderJobs +"/"+listDir[index]+ '/Rec_config.cfg')
    with open(newRecConfigFile[index], "w") as output: 
        output.write("#input path \n")
        output.write("InputPath = ")
        if (local == True) or (grid == True) or (slurm == True):
            output.write(NewInPath)
        else:
            output.write(NewInPath)
        output.write("\n")
        ######################
        
        output.write("DataFileType = %d \n"%DataFileType)
        output.write("GeoFilesType = %d \n"%GeoFilesType)
        output.write("TruthFileType = %d \n"%TruthFileType)
        output.write("InitTruthInfo = %d \n"%InitTruthInfo)

        #######################
        if (GeoFilesType == 1 or TruthFileType == 1 or ConstructSiliconCell == 1):
          output.write("SimFileName = %s \n"%SimFileName)
        if (GeoFilesType == 2):
          output.write("RecGeoFileName = %s \n"%RecGeoFileName)
        if (TruthFileType == 2):
          output.write("FluxFileName = %s \n"%FluxFileName)
        if (TruthFileType == 3 or DataFileType==2):
          output.write("RecFileName = %s \n"%RecFileName)
        if (DataFileType == 1 or GeoFilesType==1):
          output.write("TrigFileName = %s \n"%TrigFileName)
        if (ConstructSiliconCell==1):
          output.write("ConstructSiliconCell = %d \n"%ConstructSiliconCell)

        #######################
        output.write("#out path \n")
        output.write("OutputPath = %s \n"%local_out_folder)

        #######################
        output.write("OutputPrefix = ")
        if (local == True) or (grid == True) or (slurm == True):
            output.write("RecOutPut")
        else:
            output.write("RecOutPut.root")
        output.write("\n")
        ######################
        output.write("ProgramType = %d \n" %RecProgram)
        #######################
        #This option will not use
        output.write("CluFlow = %d \n" %CluFlow)
        output.write("RatioECor = %d \n" %RatioECor)
        #######################\
        if(ConstructGamma==1):
          output.write("ConstructGamma = 1 \n")
        if(ConstructElectron==1):
          output.write("ConstructElectron = 1 \n")
        if(ConstructPi0==1):
          output.write("ConstructPi0 = 1 \n")
          output.write("SavePrimaryInfo = 1 \n")
        #######################
        if len(listCalibrations)>0 and listCalibrations[0]!="":
            output.write("CalibrationFileName   = |")
            for i in range(len(listCalibrations)):
                output.write(listCalibrations[i])
                output.write("|")
            output.write("\n")
        if len(listTypes)>0 and listTypes[0]!=0:
            output.write("modules_config   = |")
            for i in range(len(listTypes)):
                output.write(newSubConfigFile[i])
                output.write("|")
            output.write("\n")
            output.write("ecal_type   = |")
            for i in range(len(listTypes)):
                output.write(str(listTypes[i]))
                output.write("|")
            output.write("\n")
        else:
          if (ConstructSiliconCell==1):
            output.write("# setting sitiming cell size \n")
            output.write("SiTimingCellSize = ")
            output.write(str(listSiTimingCell[0]))
            output.write("\n")
            output.write("# adding timing noise /ps \n")
            output.write("SiTimingNoise = ")
            output.write(str(listSiTimingNoise[0]))
            output.write("\n")
            output.write("# Energy per MIPs \n")
            output.write("EnPerMIP = ")
            output.write(str(listEnPerMIP[0]))
            output.write("\n")
          if(RecProgram!=1 and  RecProgram!=3):
            if(CluFlow==2 or CluFlow==3): # construct Clu3D firstly
              output.write("Seed3DCut = %d \n" %Seed3DCut)
              output.write("WindowShape = %d \n" %WindowShape[0])
              output.write("Seed3DMethod = %d \n" %Seed3DMethod[0])
            if(CluFlow==1 or CluFlow==2):
              output.write("WindowShape4Layers = |" )
              for layer in range(listLayerNum[0]):
                output.write("%d|" %WindowShape4Layers[layer])
              output.write("\n")
              ####################
              output.write("Seed2DCut = |" )
              for layer in range(listLayerNum[0]):
                output.write("%d|" %Seed2DCut[layer])
              output.write("\n")
            if(RecProgram==0):
              output.write("SplitLayer = %d \n" %listSplitLayer[0])
            if(RecProgram==0 or RecProgram==4):
                output.write("DoS1Cor = %d \n" %DoS1Cor[0])
             
        if(RecProgram==0):  
          for candidate in newCandidates:
            output.write(candidate)
            output.write("  =1\n")
        if tagCharge==1:
          output.write("tagCharge = 1 \n")
          output.write("ChargeSampleFilePrefix = %s \n"%ChargeSampleFilePrefix)
        output.close()
          
          


    if grid == False:
      outDir = newFolder + "/out"
      errDir = newFolder + "/err"
      logDir = newFolder + "/log"
      if not os.path.exists(outDir):
          os.mkdir(outDir)
      else:
          print("Out folder exists at %s" %(outDir))
      if not os.path.exists(errDir):
          os.mkdir(errDir)
      else:
          print("Err folder exists at %s" %(errDir))
      if not os.path.exists(logDir):
          os.mkdir(logDir)
      else:
          print("Log folder exists at %s" %(errDir))




    # run_script and gpw can be the same for all modalities...
    #########################
    # RUN SCRIPT            #
    #########################
    
    run_script = newFolder + "/run_script.sh"
    with open(run_script, "w") as output:

        output.write("#!/bin/bash\n\n")
        output.write("#parse input, they are written in args.txt\n\n")
        output.write("RecApp=$1\n")
        output.write("RecCONFIG=$2\n")
        output.write("FileNum=$3\n")
        output.write("RecInfoOut=$4\n")


        output.write("set --\n\n")
        if local == False and slurm == False:
            output.write("source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh\n")
        if slurm == True:
          output.write("module load gcc/11\n")
          output.write("source /data/software/geant4/11.1.2/geant4_install/bin/geant4.sh\n")
          output.write("source /data/software/cern_root/root-6.28.12/bin/thisroot.sh\n")
        output.write("### Do Rec SiTiming cell size \n" )  
        output.write("$RecApp -c $RecCONFIG -n $FileNum -v %d\n\n"%(Verbose))
      


        if (local == False) and (grid == False) and (slurm==False):
            output.write("echo \"Copying Rec file...\"\n\n")
            output.write("xrdcp --nopbar RecOutPut.root root://eoslhcb.cern.ch/$RecInfoOut.root\n\n" )
            if len(newCandidates)>0:
              for candidate in newCandidates:
                CandidateAbsPath=os.path.join(newOutFolder,candidate)
                output.write("echo \"Copying %s...\"\n\n"%candidate)
                output.write("xrdcp --nopbar %s$FileNum.root root://eoslhcb.cern.ch/%s$FileNum.root\n\n"%candidate )

        output.close()
    #and make executable
    st = os.stat(run_script)
    os.chmod(run_script, st.st_mode | stat.S_IEXEC)

    # grid modality is different from the others...
    if grid == False:

      #######################################
      # LXPLUS and LOCAL runs
      #######################################
      #create condor files... 
      
      #########################
      # JOBS SCRIPT           #
      #########################
      ## no reason to write jobs.sub in local runs...
      if local == False and slurm == False:
        jobsub = newFolder + "/jobs.sub"
        with open(jobsub, "w") as output:
            output.write("executable              = run_script.sh \n")
            output.write("output                  = out/out.$(ClusterId).$(ProcId).out \n")
            output.write("error                   = err/err.$(ClusterId).$(ProcId).err \n")
            output.write("log                     = log/log.$(ClusterId).log  \n")
            output.write("transfer_output_files   = \"\" \n")
            if(request_disk == True):
                output.write("request_disk            = " + request_disk_space + " \n")
            output.write("+JobFlavour             = \"%s\" \n" %(listQueue[index]))
            output.write("queue arguments from args.txt \n")
            output.close()
      elif local ==False and slurm == True:
        jobsub = newFolder + "/jobs.sh"
        with open(jobsub, "w") as output:
            output.write("#!/bin/bash \n")
            output.write("#SBATCH --job-name=%s \n" %str(listDir[index]))
            output.write("#SBATCH --cpus-per-task=1 \n")   
            output.write("#SBATCH --ntasks=1 \n")   
            output.write("#SBATCH --output=out/slurm-%A.out \n")   
            output.write("for ((i=0;i<$2;i++)) \n")
            output.write("do \n")
            output.write("    if [ -n \"$SLURM_ARRAY_TASK_ID\" ]; then \n")
            output.write("    echo \"This is an array job, task ID: $SLURM_ARRAY_TASK_ID\" \n")
            output.write("    lineNum=$(((SLURM_ARRAY_TASK_ID-1)*$2+$i+$1)) \n")
            output.write("    echo $lineNum \n")
            output.write("    line=$(sed -n ${lineNum}p args.txt)  \n")
            output.write("    ./run_script.sh $line 1> $SLURM_SUBMIT_DIR/log/log_$lineNum.log 2> $SLURM_SUBMIT_DIR/err/err_$lineNum.err &\n")
            output.write("    else \n")
            output.write("    echo \"This is not an array job\" \n")
            output.write("    lineNum=$(($i+$1))   \n")
            output.write("    echo $lineNum \n")
            output.write("    line=$(sed -n ${lineNum}p args.txt) \n")
            output.write("    ./run_script.sh $line 1> $SLURM_SUBMIT_DIR/log/log_$lineNum.log 2> $SLURM_SUBMIT_DIR/err/err_$lineNum.err &\n")
            output.write("    fi \n")
            output.write("done \n")
            output.write("wait \n")
            output.write("echo \"In Array Task $SLURM_ARRAY_TASK_ID, All background processes have completed.\" \n")
            output.close()


      
      #########################
      # ARGS                  #
      #########################
      argsFile = newFolder + "/args.txt"

      with open(argsFile, "w") as output:
        print(index)
        for i in (ListFoldersFileNums[index]):

            output.write("%s/app " % build ) # $1
            output.write("%s " % newRecConfigFile[index] )                   # $2
            output.write("%d " %(i)  )                   # $3
            output.write("%s/RecInfoOut_%d " %(newOutFolder,i)  )                   # $4
            output.write("\n")
        output.close()



  if grid == True:
    grid = False # can not use now
  else:
    if local == True:
      subScript = "runLocal_" + script_identifier + ".py"
      with open(subScript, "w") as output:
        output.write("#!/usr/bin/python3\n")
        output.write("# -*- coding: utf-8 -*-\n")
        output.write("\n")
        output.write("import math\n")
        output.write("import os\n")
        output.write("import stat\n")
        output.write("import sys\n")
        output.write("import argparse\n")
        output.write("import subprocess\n")
        output.write("from subprocess import Popen, PIPE, STDOUT\n")
        output.write("import threading\n")
        output.write("import time\n")
        output.write("import multiprocessing\n")
        output.write("\n")
        output.write("def worker(path,line,count,Dir):\n")
        output.write("  \n")
        output.write("  comm = path + '/%s/' + Dir + '/run_script.sh'\n" %script_identifier)
        output.write("  cmd = [comm]\n")
        output.write("  for i in line.split():\n")
        output.write("    cmd.append(i)\n")
        output.write("  # print(cmd)\n")
        output.write("  logName = path + '/%s/' + Dir + '/log/log_' + str(count) +  '.log'\n" %script_identifier)
        output.write("  log = open(logName, 'w')\n")
        output.write("  errName = path + '/%s/' + Dir + '/err/err_' + str(count) +  '.log'\n" %script_identifier)
        output.write("  err = open(errName, 'w')\n")
        output.write("  subprocess.Popen(cmd,stdout = log,stderr=err).wait()\n")
        output.write("  log.close()\n")
        output.write("  err.close()\n")
        output.write("  return\n")
        output.write("\n")
        output.write("def main(argv):\n")
        output.write("  #parsing args\n")
        output.write("  parser = argparse.ArgumentParser(description='Python script to start jobs in parallel')\n")
        output.write("  parser.add_argument('--num' , default=-1, help='Number of PCs in the farm')\n")
        output.write("  parser.add_argument('--here', default=-1, help='Part of jobs to run on this pc')\n")
        output.write("  parser.add_argument('--cores', default=8, help='Number of cores to use')\n")
        output.write("  args = parser.parse_args()\n")
        output.write("  numberOfPCs = int(args.num)\n")
        output.write("  thisPC      = int(args.here)\n")
        output.write("  cores       = int(args.cores)\n")
        output.write("  splitJobs = False\n")
        output.write("  if numberOfPCs == -1:\n")
        output.write("    if thisPC != -1:\n")
        output.write("      print ('ERROR: if you specify --num you should specify also --here  ')\n")
        output.write("      sys.exit()\n")
        output.write("  if thisPC == -1:\n")
        output.write("    if numberOfPCs != -1:\n")
        output.write("      print ('ERROR: if you specify --num you should specify also --here  ')\n")
        output.write("      sys.exit()\n")
        output.write("  # now both -1 or both a number \n")
        output.write("  if thisPC != -1:\n")
        output.write("    if numberOfPCs != -1:\n")
        output.write("      splitJobs = True\n")
        output.write("  pool = multiprocessing.Pool(cores) \n")
        output.write("  # preparing \n")
        output.write("  DIRs = ['")
        countEnergies = 0
        for Dir in listDir:
          countEnergies = countEnergies + 1
          if countEnergies == len(listDir):
            output.write("%s']\n" % str(Dir) )
          else:
            output.write("%s','" % str(Dir) )
        output.write("  path = os.path.abspath('./')\n")
        output.write("  # take only 1/3 of the jobs if lineNum is specified \n")
        output.write("  processList = []\n")
        output.write("  for Dir in DIRs:\n")
        output.write("    filepath = path + '/%s/' + Dir + '/args.txt'\n" %script_identifier)
        output.write("    countLine = 0\n")
        output.write("    lines = len(open(filepath).readlines())\n")
        output.write("    limit = lines / numberOfPCs\n")
        output.write("    uplimit = limit * thisPC \n")
        output.write("    downlimit = limit * (thisPC -1) \n")
        output.write("    with open(filepath) as fp:\n")
        output.write("      for line in fp:\n")
        output.write("        if splitJobs == True:\n")
        output.write("          if countLine >= downlimit:\n")
        output.write("            if countLine < uplimit:\n")
        output.write("              pool.apply_async(worker, args=(path,line,countLine,Dir))\n")
        output.write("              #print(countLine)\n")
        output.write("        else:\n")
        output.write("          pool.apply_async(worker, args=(path,line,countLine,Dir))\n")
        output.write("        countLine = countLine + 1\n")
        output.write("  pool.close()\n")
        output.write("  pool.join()\n")
        output.write("\n")
        output.write("if __name__ == '__main__':\n")
        output.write("  main(sys.argv[1:])\n")
        output.close()
    elif slurm == True :
      subScript = "runAll_" + script_identifier + ".sh"
      with open(subScript, "w") as output:
          output.write("#!/bin/bash\n\n")
          output.write("subJob=1000 \n")
          output.write("taskPerJob=1 \n")
          output.write("maxJob=10 \n")
          output.write("for i in ")
          for i in listDir:
              output.write("%s " % str(i))
          output.write("\n")
          output.write("do\n")
          output.write("  cd %s/$i\n" % baseFolderJobs)
          output.write("  num_lines=$(wc -l < args.txt)\n")
          output.write("  TotalSubJob=$((subJob*taskPerJob))\n")
          output.write("  loop=$((num_lines/TotalSubJob))\n")
          output.write("  for ((j=0;j<$loop;))\n")
          output.write("  do\n")
          output.write("    running_jobs=$(squeue -u $USER --format=%F | sort | uniq | wc -l)\n")
          output.write("    echo running_jobs $running_jobs\n")
          output.write("    running_Subjobs=$(squeue -u $USER | sort | uniq | wc -l)\n")
          output.write("    echo running_Subjobs $running_Subjobs\n")
          output.write("    if [ $running_jobs -lt $maxJob ] || [ $running_Subjobs -lt 600 ]; then\n")
          output.write("      firstJob=$((j*TotalSubJob+1))\n")
          output.write("      echo \"Submitting jobs for range $firstJob-$((j*TotalSubJob+TotalSubJob))\"\n")
          output.write("      sbatch --array=1-$subJob  jobs.sh $firstJob $taskPerJob \n")
          output.write("      ((j++))\n")
          output.write("    else\n")
          output.write("      echo \"当前运行的作业数量已达到上限，等待中...\"\n")
          output.write("      sleep 60  # 每隔60秒检查一次\n")
          output.write("    fi\n")
          output.write("    sleep 60\n")
          output.write("  done\n")
          output.write("  firstJob=$((loop*TotalSubJob+1))\n")
          output.write("  remainder=$((num_lines-loop*TotalSubJob))\n")
          output.write("  subJob2=$((remainder/taskPerJob))\n")
          output.write("  echo \"Submitting jobs for range $firstJob-$((firstJob-1+subJob2*taskPerJob))\"\n")
          output.write("  sbatch --array=1-$subJob2 jobs.sh $firstJob $taskPerJob\n")
          # output.write("  firstJob=$((firstJob+subJob2*taskPerJob))\n")
          # output.write("  remainder=$((num_lines-firstJob+1))\n")
          # output.write("  echo \"Submitting jobs for range $firstJob-$num_lines\"\n")
          # output.write("  sbatch --ntasks=$remainder jobs.sh $firstJob $remainder\n\n")
          output.write("  cd - \n")
          output.write("done\n")
          output.close()
    else:
      subScript = "runAll_" + script_identifier + ".sh"
      with open(subScript, "w") as output:
          output.write("#!/bin/bash\n\n")
          output.write("for i in ")
          for i in listDir:
              output.write("%s " % str(i))
          output.write("\n")
          output.write("do\n")
          output.write("  cd %s/$i\n" % baseFolderJobs)
          output.write("  condor_submit jobs.sub\n")
          output.write("  cd - \n")
          output.write("done\n")
          output.close()
  #and make executable
  st = os.stat(subScript)
  os.chmod(subScript, st.st_mode | stat.S_IEXEC)

  ### and it's all...
  print("Done. Please run %s to submit all jobs. Bye!" % subScript)


if __name__ == "__main__":
   main(sys.argv[1:])
