from ROOT import *
from array import array
from math import sqrt,atan

import sys


def dogen(lumitype):
   fk = TFile("K_MB_%s.root"%(lumitype))
   tk = fk.Get("Hits")
   nktot = tk.GetEntries()
   
   fpi = TFile("pi_MB_%s.root"%(lumitype))
   tpi = fpi.Get("Hits")
   npitot = tpi.GetEntries()
   
   fs = TFile("Kpi_comb_MB_%s.root"%(lumitype),"recreate")
   ts = TTree("tree","")
   
   
   Kpx_true = array("d",[0.])
   Kpy_true = array("d",[0.])
   Kpz_true = array("d",[0.])
   Kpe_true = array("d",[0.])
   Kp_true = array("d",[0.])
   ts.Branch("Kpx_true",Kpx_true,"Kpx_true/D")
   ts.Branch("Kpy_true",Kpy_true,"Kpy_true/D")
   ts.Branch("Kpz_true",Kpz_true,"Kpz_true/D")
   ts.Branch("Kpe_true",Kpe_true,"Kpe_true/D")
   ts.Branch("Kp_true",Kp_true,"Kp_true/D")
   
   Kpx = array("d",[0.])
   Kpy = array("d",[0.])
   Kpz = array("d",[0.])
   Kpe = array("d",[0.])
   Kp = array("d",[0.])
   ts.Branch("Kpx",Kpx,"Kpx/D")
   ts.Branch("Kpy",Kpy,"Kpy/D")
   ts.Branch("Kpz",Kpz,"Kpz/D")
   ts.Branch("Kpe",Kpe,"Kpe/D")
   ts.Branch("Kp",Kp,"Kp/D")
   
   
   
   pipx_true = array("d",[0.])
   pipy_true = array("d",[0.])
   pipz_true = array("d",[0.])
   pipe_true = array("d",[0.])
   pip_true = array("d",[0.])
   ts.Branch("pipx_true",pipx_true,"pipx_true/D")
   ts.Branch("pipy_true",pipy_true,"pipy_true/D")
   ts.Branch("pipz_true",pipz_true,"pipz_true/D")
   ts.Branch("pipe_true",pipe_true,"pipe_true/D")
   ts.Branch("pip_true",pip_true,"pip_true/D")
   
   pipx = array("d",[0.])
   pipy = array("d",[0.])
   pipz = array("d",[0.])
   pipe = array("d",[0.])
   pip = array("d",[0.])
   ts.Branch("pipx",pipx,"pipx/D")
   ts.Branch("pipy",pipy,"pipy/D")
   ts.Branch("pipz",pipz,"pipz/D")
   ts.Branch("pipe",pipe,"pipe/D")
   ts.Branch("pip",pip,"pip/D")
   
   
   vtxdist = array("d",[0.])
   ts.Branch("vtxdist",vtxdist,"vtxdist/D")
   Ktiming = array("d",[0.])
   ts.Branch("Ktiming",Ktiming,"Ktiming/D")
   pitiming = array("d",[0.])
   ts.Branch("pitiming",pitiming,"pitiming/D")
   MKpi = array("d",[0.])
   ts.Branch("MKpi",MKpi,"MKpi/D")
   
   
   K_prod_vertex_x = array("d",[0.])
   K_prod_vertex_y = array("d",[0.])
   K_prod_vertex_z = array("d",[0.])
   ts.Branch("K_prod_vertex_x",K_prod_vertex_x,"K_prod_vertex_x/D")
   ts.Branch("K_prod_vertex_y",K_prod_vertex_y,"K_prod_vertex_y/D")
   ts.Branch("K_prod_vertex_z",K_prod_vertex_z,"K_prod_vertex_z/D")
   
   pi_prod_vertex_x = array("d",[0.])
   pi_prod_vertex_y = array("d",[0.])
   pi_prod_vertex_z = array("d",[0.])
   ts.Branch("pi_prod_vertex_x",pi_prod_vertex_x,"pi_prod_vertex_x/D")
   ts.Branch("pi_prod_vertex_y",pi_prod_vertex_y,"pi_prod_vertex_y/D")
   ts.Branch("pi_prod_vertex_z",pi_prod_vertex_z,"pi_prod_vertex_z/D")
   
   
   evtNumber = array("i",[0])
   ts.Branch("evtNumber",evtNumber,"evtNumber/I")
   
   
   # Initialize random number generator.
   gRandom.SetSeed()
   gauss, landau = gRandom.Gaus, gRandom.Landau
   
   tempevtN = 1 
   idstart = 0
   idend = 0
   tempN = 0
   
   
   for kk in range(0,nktot):
      tk.GetEntry(kk)
   
      if kk%1==0:
         print "Processing K event ",kk,"; evtNumber = ",tk.evtNumber
   
      K_prod_vertex_x[0] = tk.prod_vertex_x
      K_prod_vertex_y[0] = tk.prod_vertex_y
      K_prod_vertex_z[0] = tk.prod_vertex_z
   
      Kpx_true[0] = tk.px
      Kpy_true[0] = tk.py
      Kpz_true[0] = tk.pz
      Kpe_true[0] = tk.eTot
      Kp_true[0] = sqrt(Kpx_true[0]*Kpx_true[0]+Kpy_true[0]*Kpy_true[0]+Kpz_true[0]*Kpz_true[0]) 
   
      Kpx[0] = Kpx_true[0] + gauss(0.,Kpx_true[0]*0.01)
      Kpy[0] = Kpy_true[0] + gauss(0.,Kpy_true[0]*0.01)
      Kpz[0] = Kpz_true[0] + gauss(0.,Kpz_true[0]*0.01)
      Kpe[0] = sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]+Kpz[0]*Kpz[0]+0.493677*0.493677) 
      Kp[0] = sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]+Kpz[0]*Kpz[0]) 
   
      Ktiming[0] = tk.prod_vertex_timing
   
      evtNumber[0] = tk.evtNumber
   
      if tpi.GetEntries("evtNumber==%i"%(evtNumber[0])) == 0:
         continue
   
      if evtNumber[0] != tempevtN:
         tempevtN = evtNumber[0]
         idstart = idend
   
      tempN = 0
   
      for ii in range(idstart,npitot):
         tpi.GetEntry(ii)
   
         if ii%1==0:
            print "Processing pi event ",ii,"; evtNumber = ",tpi.evtNumber
   
         if tempN == 0 and tpi.evtNumber != evtNumber[0]:
            continue
   
         elif tempN > 0 and tpi.evtNumber != evtNumber[0]:
            idend = ii
            break
   
         else:
            tempN += 1
   
            pi_prod_vertex_x[0] = tpi.prod_vertex_x
            pi_prod_vertex_y[0] = tpi.prod_vertex_y
            pi_prod_vertex_z[0] = tpi.prod_vertex_z
   
            pipx_true[0] = tpi.px
            pipy_true[0] = tpi.py
            pipz_true[0] = tpi.pz
            pipe_true[0] = tpi.eTot
            pip_true[0] = sqrt(pipx_true[0]*pipx_true[0]+pipy_true[0]*pipy_true[0]+pipz_true[0]*pipz_true[0]) 
   
            pipx[0] = pipx_true[0] + gauss(0.,pipx_true[0]*0.01)
            pipy[0] = pipy_true[0] + gauss(0.,pipy_true[0]*0.01)
            pipz[0] = pipz_true[0] + gauss(0.,pipz_true[0]*0.01)
            pipe[0] = sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]+pipz[0]*pipz[0]+0.493677*0.493677) 
            pip[0] = sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]+pipz[0]*pipz[0]) 
   
            MKpi[0] = sqrt(pow(Kpe[0]+pipe[0],2)-pow(Kpx[0]+pipx[0],2)-pow(Kpy[0]+pipy[0],2)-pow(Kpz[0]+pipz[0],2))
   
            if MKpi[0]<792 or MKpi[0]>992:
               continue
   
            pitiming[0] = tpi.prod_vertex_timing
   
            vtxdist[0] = sqrt(pow(pi_prod_vertex_x[0]-K_prod_vertex_x[0],2)+pow(pi_prod_vertex_y[0]-K_prod_vertex_y[0],2)+pow(pi_prod_vertex_z[0]-K_prod_vertex_z[0],2))
   
   
            ts.Fill()
   
   
   
   ts.Write()
   print "Output Entires: ",ts.GetEntries()



hh = sys.argv[-1]
dogen(hh)
#dogen("lumi0.6")





