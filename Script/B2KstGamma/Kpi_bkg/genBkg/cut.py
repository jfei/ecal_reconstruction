from ROOT import *
#for i in range(312,500):

import os,sys

def copytree(pid,fin,fout):
   ch=TChain("GaussGeo.Ecal.Tuple/Hits")
   ch.Add(fin)

   cut="abs(PDGID)==%i&&sqrt(px*px+py*py)>500.&&mother_ID==0"%(pid)

   f=TFile(fout,"recreate")
   print("Processing events: "+str(ch.GetEntries()))
   nt = ch.CopyTree(cut)
   print(nt.GetEntries())
   nt.Write("",1)
   f.Close()


copytree(211,"/eos/experiment/spacal/users/anlp/simulation/MinimumBias/lumi1.5/lumi1.5e34_8.1kMB_Run666.root","pi_MB_Run5.root")
copytree(321,"/eos/experiment/spacal/users/anlp/simulation/MinimumBias/lumi1.5/lumi1.5e34_8.1kMB_Run666.root","K_MB_Run5.root")


#copytree(211,"../../../simulation/MinimumBias/lumi0.6/lumi0.6e34_8.5kMB_Run666.root","pi_MB_lumi0.6.root")
#copytree(321,"../../../simulation/MinimumBias/lumi0.6/lumi0.6e34_8.5kMB_Run666.root","K_MB_lumi0.6.root")

#copytree(211,"../../../simulation/MinimumBias/lumi1.0/lumi1.0e34_8.2kMB_Run666.root","pi_MB_lumi1.0.root")
#copytree(321,"../../../simulation/MinimumBias/lumi1.0/lumi1.0e34_8.2kMB_Run666.root","K_MB_lumi1.0.root")

#copytree(211,"../../simulation/MinimumBias/lumi1.5/lumi1.5e34_8.1kMB_Run666.root","pi_MB_lumi1.5.root")
#copytree(321,"../../../simulation/MinimumBias/lumi1.5/lumi1.5e34_8.1kMB_Run666.root","K_MB_lumi1.5.root")


