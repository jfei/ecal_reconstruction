from ROOT import *
from array import array
from math import sqrt,atan

import sys




def doadd(lumitype, evtstart, evtend):
   # Initialize random number generator.
   gRandom.SetSeed()
   Rndm = gRandom.Rndm
   
   
   fclus = TFile("/Users/fjl/workdir/ecal_reconstruction/PhysicalChannelOut/B2KstGamma/1.5e34/SiTimingRotation/B2KstGamma_1.5e34_Match.root")
   tclus = fclus.Get("tree")
   nclustot = tclus.GetEntries()
   
   fbkg = TFile("../genBkg/Kpi_comb_MB_%s.root"%(lumitype))
   tbkg = fbkg.Get("tree")
   nbkgtot = tbkg.GetEntries()

   tB = TChain()## this is to add true but background K*; using nextevent method (evtNumber+50)
   tB.Add("/Users/fjl/workdir/ecal_reconstruction/B2KstGammaFluxOut/FluxGammaFromB2KstGamma_assignPV_1.5e34.root/tree")

   
   newfile = TFile("SiTimingRotation_addbkg_%s.root"%(lumitype),"recreate")
   #newfile = TFile("Reconstruction_addbkg_%s_%ito%i.root"%(lumitype,evtstart,evtend),"recreate")
   newtree = tclus.CloneTree(0)
   
   
   vtxdist = array("d",[0.])
   newtree.Branch("vtxdist",vtxdist,"vtxdist/D")
   MKpi = array("d",[0.])
   newtree.Branch("MKpi",MKpi,"MKpi/D")
   MKstg = array("d",[0.])
   newtree.Branch("MKstg",MKstg,"MKstg/D")
   orig_timing = array("d",[0.])
   newtree.Branch("orig_timing",orig_timing,"orig_timing/D")
   entry_timing = array("d",[0.])
   newtree.Branch("entry_timing",entry_timing,"entry_timing/D")
   index = array("i",[0])
   newtree.Branch("index",index,"index/I")
   rdm = array("d",[0.])
   newtree.Branch("rdm",rdm,"rdm/D")
   
   
   
   Kpx_true = array("d",[0.])
   Kpy_true = array("d",[0.])
   Kpz_true = array("d",[0.])
   Kpe_true = array("d",[0.])
   Kp_true = array("d",[0.])
   newtree.SetBranchAddress("Kpx_true",Kpx_true)
   newtree.SetBranchAddress("Kpy_true",Kpy_true)
   newtree.SetBranchAddress("Kpz_true",Kpz_true)
   newtree.SetBranchAddress("Kpe_true",Kpe_true)
   newtree.SetBranchAddress("Kp_true",Kp_true)
   
   
   Kpx = array("d",[0.])
   Kpy = array("d",[0.])
   Kpz = array("d",[0.])
   Kpe = array("d",[0.])
   Kp = array("d",[0.])
   newtree.SetBranchAddress("Kpx",Kpx)
   newtree.SetBranchAddress("Kpy",Kpy)
   newtree.SetBranchAddress("Kpz",Kpz)
   newtree.SetBranchAddress("Kpe",Kpe)
   newtree.SetBranchAddress("Kp",Kp)
   
   
   pipx_true = array("d",[0.])
   pipy_true = array("d",[0.])
   pipz_true = array("d",[0.])
   pipe_true = array("d",[0.])
   pip_true = array("d",[0.])
   newtree.SetBranchAddress("pipx_true",pipx_true)
   newtree.SetBranchAddress("pipy_true",pipy_true)
   newtree.SetBranchAddress("pipz_true",pipz_true)
   newtree.SetBranchAddress("pipe_true",pipe_true)
   newtree.SetBranchAddress("pip_true",pip_true)
   
   
   pipx = array("d",[0.])
   pipy = array("d",[0.])
   pipz = array("d",[0.])
   pipe = array("d",[0.])
   pip = array("d",[0.])
   newtree.SetBranchAddress("pipx",pipx)
   newtree.SetBranchAddress("pipy",pipy)
   newtree.SetBranchAddress("pipz",pipz)
   newtree.SetBranchAddress("pipe",pipe)
   newtree.SetBranchAddress("pip",pip)
   
   
   true_prod_vertex_x = array("d",[0.])
   true_prod_vertex_y = array("d",[0.])
   true_prod_vertex_z = array("d",[0.])
   newtree.SetBranchAddress("true_prod_vertex_x",true_prod_vertex_x)
   newtree.SetBranchAddress("true_prod_vertex_y",true_prod_vertex_y)
   newtree.SetBranchAddress("true_prod_vertex_z",true_prod_vertex_z)
   

   idstart = 0
   ntruebkgK = 3 #number of true background K* to combine for each reconstructed cluster
   nadd = 4 #number of Kpi background to combine for each reconstructed cluster
   
   
   #for kk in range(0,20000):
   for kk in range(0,nclustot):
      tclus.GetEntry(kk)

      if kk%1000 == 0:
         print(kk)
   

      if tclus.evtNumber > evtend:
         break

      if tclus.evtNumber>=evtstart:
         if tclus.e < 0.:
            continue

         Kpx_true[0] = tclus.Kpx_true*1000
         Kpy_true[0] = tclus.Kpy_true*1000
         Kpz_true[0] = tclus.Kpz_true*1000
         Kpe_true[0] = tclus.Kpe_true*1000
         Kp_true[0] = tclus.Kp_true*1000
      
         Kpx[0] = tclus.Kpx*1000
         Kpy[0] = tclus.Kpy*1000
         Kpz[0] = tclus.Kpz*1000
         Kpe[0] = tclus.Kpe*1000
         Kp[0] = tclus.Kp*1000
      
         pipx_true[0] = tclus.pipx_true*1000
         pipy_true[0] = tclus.pipy_true*1000
         pipz_true[0] = tclus.pipz_true*1000
         pipe_true[0] = tclus.pipe_true*1000
         pip_true[0] = tclus.pip_true*1000
      
         pipx[0] = tclus.pipx*1000
         pipy[0] = tclus.pipy*1000
         pipz[0] = tclus.pipz*1000
         pipe[0] = tclus.pipe*1000
         pip[0] = tclus.pip*1000
      
         true_prod_vertex_x[0] = tclus.true_prod_vertex_x
         true_prod_vertex_y[0] = tclus.true_prod_vertex_y
         true_prod_vertex_z[0] = tclus.true_prod_vertex_z
      
         MKpi[0] = sqrt(pow(Kpe[0]+pipe[0],2)-pow(Kpx[0]+pipx[0],2)-pow(Kpy[0]+pipy[0],2)-pow(Kpz[0]+pipz[0],2))
         vtxdist[0] = 0.
         index[0] = 1

         entry_x_12640= ((tclus.rec_entry_x-true_prod_vertex_x[0])/12620.)*12640.02+true_prod_vertex_x[0]
         entry_y_12640= ((tclus.rec_entry_y-true_prod_vertex_y[0])/12620.)*12640.02+true_prod_vertex_y[0]

         orig_timing[0] = tclus.true_timing - sqrt(pow(tclus.true_entry_x-tclus.true_prod_vertex_x,2)+pow(tclus.true_entry_y-tclus.true_prod_vertex_y,2)+pow(tclus.true_entry_z-tclus.true_prod_vertex_z,2))*1.E+9/(299792.458*1000.*1000.)
         entry_timing[0] = orig_timing[0] + sqrt(pow(tclus.rec_entry_x-true_prod_vertex_x[0],2)+pow(tclus.rec_entry_y-true_prod_vertex_y[0],2)+pow(12620-true_prod_vertex_z[0],2))*1.E+9/(299792.458*1000.*1000.)
      
         MKstg[0] = sqrt(pow(Kpe[0]+pipe[0]+tclus.e,2)-pow(Kpx[0]+pipx[0]+tclus.px,2)-pow(Kpy[0]+pipy[0]+tclus.py,2)-pow(Kpz[0]+pipz[0]+tclus.pz,2))
         rdm[0] = 0. 
      
         
         if MKstg[0] > 4000. and MKstg[0] < 7500. and MKpi[0] > 792. and MKpi[0] < 992. and sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]) > 500. and sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]) > 500.:
            newtree.Fill()


#############################################################
#############################################################
#############################################################
         for ii in range(ntruebkgK):
            tB.GetEntry(tclus.evtNumber+50+ii)
            Kpx_true[0] = tB.Kpx_true*1000
            Kpy_true[0] = tB.Kpy_true*1000
            Kpz_true[0] = tB.Kpz_true*1000
            Kpe_true[0] = tB.Kpe_true*1000
            Kp_true[0] = tB.Kp_true*1000
      
            Kpx[0] = tB.Kpx*1000
            Kpy[0] = tB.Kpy*1000
            Kpz[0] = tB.Kpz*1000
            Kpe[0] = tB.Kpe*1000
            Kp[0] = tB.Kp*1000
      
            pipx_true[0] = tB.pipx_true*1000
            pipy_true[0] = tB.pipy_true*1000
            pipz_true[0] = tB.pipz_true*1000
            pipe_true[0] = tB.pipe_true*1000
            pip_true[0] = tB.pip_true*1000
      
            pipx[0] = tB.pipx*1000
            pipy[0] = tB.pipy*1000
            pipz[0] = tB.pipz*1000
            pipe[0] = tB.pipe*1000
            pip[0] = tB.pip*1000
      
            true_prod_vertex_x[0] = tB.prod_vertex_x
            true_prod_vertex_y[0] = tB.prod_vertex_y
            true_prod_vertex_z[0] = tB.prod_vertex_z
      
            MKpi[0] = sqrt(pow(Kpe[0]+pipe[0],2)-pow(Kpx[0]+pipx[0],2)-pow(Kpy[0]+pipy[0],2)-pow(Kpz[0]+pipz[0],2))
            vtxdist[0] = 0.
            index[0] = -1

            entry_x_12640= ((tclus.rec_entry_x-true_prod_vertex_x[0])/12620.)*12640.02+true_prod_vertex_x[0]
            entry_y_12640= ((tclus.rec_entry_y-true_prod_vertex_y[0])/12620.)*12640.02+true_prod_vertex_y[0]
            
            orig_timing[0] = tB.timing - sqrt(pow(tB.entry_x-tB.prod_vertex_x,2)+pow(tB.entry_y-tB.prod_vertex_y,2)+pow(12620.-tB.prod_vertex_z,2))*1.E+9/(299792.458*1000.*1000.)


            entry_timing[0] = orig_timing[0] + sqrt(pow(tclus.rec_entry_x-true_prod_vertex_x[0],2)+pow(tclus.rec_entry_y-true_prod_vertex_y[0],2)+pow(12620-true_prod_vertex_z[0],2))*1.E+9/(299792.458*1000.*1000.)
      
            MKstg[0] = sqrt(pow(Kpe[0]+pipe[0]+tclus.e,2)-pow(Kpx[0]+pipx[0]+tclus.px,2)-pow(Kpy[0]+pipy[0]+tclus.py,2)-pow(Kpz[0]+pipz[0]+tclus.pz,2))
            #before addbkg, use the below equation to calculate MKstg
            #sqrt(pow(1000.*Kpe+1000.*pipe+e,2)-pow(1000.*Kpx+1000.*pipx+px,2)-pow(1000.*Kpy+1000.*pipy+py,2)-pow(1000.*Kpz+1000.*pipz+pz,2))
            rdm[0] = -1.*Rndm()
      
            
            if MKstg[0] > 4000. and MKstg[0] < 7500. and MKpi[0] > 792. and MKpi[0] < 992. and sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]) > 500. and sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]) > 500.:
               newtree.Fill()

   
#############################################################
#############################################################
#############################################################
         if idstart+nadd > nbkgtot:
            idstart = 0

         for ii in range(idstart,idstart+nadd):
            tbkg.GetEntry(ii)
      
            Kpx_true[0] = tbkg.Kpx_true
            Kpy_true[0] = tbkg.Kpy_true
            Kpz_true[0] = tbkg.Kpz_true
            Kpe_true[0] = tbkg.Kpe_true
            Kp_true[0] = tbkg.Kp_true
         
            Kpx[0] = tbkg.Kpx
            Kpy[0] = tbkg.Kpy
            Kpz[0] = tbkg.Kpz
            Kpe[0] = tbkg.Kpe
            Kp[0] = tbkg.Kp
         
            pipx_true[0] = tbkg.pipx_true
            pipy_true[0] = tbkg.pipy_true
            pipz_true[0] = tbkg.pipz_true
            pipe_true[0] = tbkg.pipe_true
            pip_true[0] = tbkg.pip_true
         
            pipx[0] = tbkg.pipx
            pipy[0] = tbkg.pipy
            pipz[0] = tbkg.pipz
            pipe[0] = tbkg.pipe
            pip[0] = tbkg.pip
         
            true_prod_vertex_x[0] = tbkg.K_prod_vertex_x
            true_prod_vertex_y[0] = tbkg.K_prod_vertex_y
            true_prod_vertex_z[0] = tbkg.K_prod_vertex_z
         
            MKpi[0] = sqrt(pow(Kpe[0]+pipe[0],2)-pow(Kpx[0]+pipx[0],2)-pow(Kpy[0]+pipy[0],2)-pow(Kpz[0]+pipz[0],2))
            vtxdist[0] = tbkg.vtxdist
            index[0] = 0


            entry_x_12640= ((tclus.rec_entry_x-true_prod_vertex_x[0])/12620.)*12640.02+true_prod_vertex_x[0]
            entry_y_12640= ((tclus.rec_entry_y-true_prod_vertex_y[0])/12620.)*12640.02+true_prod_vertex_y[0]
            orig_timing[0] = tbkg.Ktiming 
            entry_timing[0] = orig_timing[0] + sqrt(pow(tclus.rec_entry_x-true_prod_vertex_x[0],2)+pow(tclus.rec_entry_y-true_prod_vertex_y[0],2)+pow(12620-true_prod_vertex_z[0],2))*1.E+9/(299792.458*1000.*1000.)
         

            MKstg[0] = sqrt(pow(Kpe[0]+pipe[0]+tclus.e,2)-pow(Kpx[0]+pipx[0]+tclus.px,2)-pow(Kpy[0]+pipy[0]+tclus.py,2)-pow(Kpz[0]+pipz[0]+tclus.pz,2))
            rdm[0] = Rndm()
         
            if MKstg[0] > 4000. and MKstg[0] < 7500.:
               newtree.Fill()

            if ii == (idstart+nadd-1):
               idstart = ii+nadd
               break

   
   
   
   
   newtree.Write()
   print("Output Entires: ",newtree.GetEntries())



#lumitype = sys.argv[-3]
#nstart = int(sys.argv[-2])
#nend = int(sys.argv[-1])
#doadd(lumitype,nstart,nend)

doadd("1.5e34",0,100000)
#doadd("Run3",0,100000)
#doadd("U1b",0,100000)
#doadd("lumi0.6",0,100000)
#doadd("lumi1.0",0,100000)
#doadd("lumi1.5",0,100000)




