import sys,os
from ROOT import *


#ptype = all, gamma, p, pi, e, K, mu, hadron, lepton, nogamma
#ecut = 0, 1, ...


def draw(lumitype):
   t = TChain();
   t.Add("./Reconstruction_tagT_%s.root/cluster"%(lumitype))

   h1 = TH1F("h1","h1",100,4000.,7000.)
   h2 = TH1F("h2","h2",100,4000.,7000.)


   t.Draw("sqrt(pow(1000.*Kpe+1000.*pipe+e,2)-pow(1000.*Kpx+1000.*pipx+px,2)-pow(1000.*Kpy+1000.*pipy+py,2)-pow(1000.*Kpz+1000.*pipz+pz,2))>>h1")
   t.Draw("sqrt(pow(1000.*Kpe+1000.*pipe+e,2)-pow(1000.*Kpx+1000.*pipx+px,2)-pow(1000.*Kpy+1000.*pipy+py,2)-pow(1000.*Kpz+1000.*pipz+pz,2))>>h2","pidtag!=22")


   cv1 = TCanvas("cv1","cv1",900,600)
   h1.SetXTitle("#it{M}(#it{K}^{*0}#it{#gamma}) [MeV/#it{c}^{2}]")
   h1.SetYTitle("nEntries")
   h1.SetMinimum(0.)
   h1.SetMaximum(h1.GetMaximum()*1.2)
   h1.Draw()

   h2.SetFillColor(8)
   h2.SetFillStyle(1001)
   h2.Draw("same")

   leg = TLegend(0.6,0.7,0.85,0.9,lumitype)
   leg.AddEntry(h1,"All clusters","l")
   leg.AddEntry(h2,"Charged clusters","f")
   leg.Draw()

   #ppt = TPaveText(0.2,0.8,0.45,0.9,"NBNDC")
   #ppt.AddText("%.0f%% #it{l}_{vtx} < %g mm"%(100.*rdmcut,distcut))
   #ppt.SetFillColor(kWhite)
   #ppt.SetShadowColor(kWhite)
   #ppt.Draw()

   cv1.SaveAs("plots/MKstg_%s.pdf"%(lumitype))



gROOT.ProcessLine(".x ~/functions/lhcbStyle.C");
gStyle.SetMarkerSize(0.3)
#gStyle.SetLineScalePS(1);


#draw("U1b")
#draw("lumi0.6")
#draw("lumi1.0")
#draw("lumi1.5")
draw("lumi0.6_downscoped")
#draw("lumi1.0_downscoped")
#draw("lumi1.5_downscoped")





