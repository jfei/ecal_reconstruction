from ROOT import *
#for i in range(312,500):

import os,sys

def copytree(lumi):
   ch=TChain("tree")
   ch.Add("/Users/fjl/workdir/ecal_reconstruction/Script/B2KstGamma/rmChargeT/charged_sample/FluxGammaB2KstGamma_MB_Merged_1.5e34.root")

   cut="(abs(pdgID)==11||abs(pdgID)==211||abs(pdgID)==321||abs(pdgID)==2212)"

   f=TFile("charged_sample_%s.root"%(lumi),"recreate")
   print("Processing events: "+str(ch.GetEntries()))
   nt = ch.CopyTree(cut)
   print(nt.GetEntries())
   nt.Write("",1)
   f.Close()


copytree("1.5e34")
#copytree("Run3")
#copytree("U1b")
#copytree("lumi0.6")
#copytree("lumi1.0")
#copytree("lumi1.5")


