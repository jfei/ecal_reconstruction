import sys,os
import math
from ROOT import *
from array import array


def domatch(lumi,nstart,nend):
   tflux = TChain()
   tflux.Add("./charged_sample/charged_sample_%s.root/tree"%(lumi))

   t = TChain()
   t.Add("/Users/fjl/workdir/ecal_reconstruction/PhysicalChannelOut/B2KstGamma/1.5e34/SiTimingRotation/B2KstGamma_1.5e34_Match.root/tree")

   newfile = TFile("./rootfiles/Reconstruction_tagT_%s_%i_to_%i.root"%(lumi,nstart,nend),"recreate")
   newtree = t.CloneTree(0)
   
   pidtag = array("i",[0])
   newtree.Branch("pidtag",pidtag,"pidtag/I")


   nentries = t.GetEntries()
   print( "Processing events: ",nentries)

   for jentry in range(nstart,nend):
      t.GetEntry(jentry)
      pidtag[0] = 22

      #nn = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&(abs(pdgID)==11||abs(pdgID)==211||abs(pdgID)==321||abs(pdgID)==2212)&&{e}/1000./eTot>0.75&&{e}/1000./eTot<1.25".format(evtNumber=t.evtNumber,x=t.x,y=t.y,cellSize=t.cellSize,e=t.e))
      nn = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&{e}/1000./eTot<1.25&&charge==-1".format(evtNumber=t.evtNumber,x=t.rec_entry_x,y=t.rec_entry_y,cellSize=t.cellSize,e=t.e))
      nz = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&{e}/1000./eTot<1.25&&charge==1".format(evtNumber=t.evtNumber,x=t.rec_entry_x,y=t.rec_entry_y,cellSize=t.cellSize,e=t.e))
      #print "nn =",nn

      if nn+nz > 0:
         if nn >= nz:
            pidtag[0] = 11
            print("nn>=nz = ",nn-nz)
         else :
            pidtag[0] = -11
            print("nz>nn = ",nz-nn)
            
      if jentry%100 == 0:
         print( jentry)
      newtree.Fill()      



   newtree.Print()
   newtree.AutoSave()
   newfile.Close()





domatch("1.5e34",int(sys.argv[-2]),int(sys.argv[-1]))

