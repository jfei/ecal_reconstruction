import sys,os
from ROOT import *
from math import sqrt
import numpy




def draw(lumitype, tlist):
   vv = numpy.loadtxt(fname="output/output_%s.txt"%(lumitype),delimiter=' ')

   np_signif = sqrt(8331.*2./0.49)*vv[:,2]
   np_signifr = sqrt(8331.*2./0.49)*vv[:,3]
   np_eff = vv[:,4]
   np_effr = vv[:,5]

   array_signif = numpy.array(np_signif)
   array_signifr = numpy.array(np_signifr)
   array_eff = numpy.array(np_eff)
   array_effr = numpy.array(np_effr)

   signif = []
   signifr = []
   eff = []
   effr = []

   for ii in range(20):
      signif.append(array_signif[20*ii:20*ii+20])
      signifr.append(array_signifr[20*ii:20*ii+20])
      eff.append(array_eff[20*ii:20*ii+20])
      effr.append(array_effr[20*ii:20*ii+20])
   

 

   color = [1, 2, 3, 4, 5, 6, 7, 8]
   xmax = 0.
   ymax = 0.

   gr = [TGraph(20,eff[kk-1],signif[kk-1]) for kk in tlist]

   for ii in range(len(gr)):
      gr[ii].SetLineColor(color[ii])
      gr[ii].SetLineWidth(3)
      gr[ii].SetMarkerColor(color[ii])
      #gr[ii].SetMarkerStyle(4)
      if TMath.MaxElement(gr[ii].GetN(),gr[ii].GetX())>xmax:
         xmax = TMath.MaxElement(gr[ii].GetN(),gr[ii].GetX()) 
      if gr[ii].GetHistogram().GetMaximum()>ymax:
         ymax = gr[ii].GetHistogram().GetMaximum()

   cv1 = TCanvas("cv1","cv1",900,600)
   gr[0].GetXaxis().SetTitle("Efficiency")
   gr[0].GetYaxis().SetTitle("#it{S}/#sqrt{#it{S+B}} per fb^{-1}")
   gr[0].GetXaxis().SetLimits(-0.1,xmax*1.2)
   gr[0].GetHistogram().SetMaximum(ymax*1.4)
   gr[0].GetHistogram().SetMinimum(0.)
   gr[0].Draw("AL")
   for ii in range(1,len(gr)):
      gr[ii].Draw("same,L")


   leg = TLegend(0.12,0.46,0.3,0.9)
   for ii in range(len(gr)):
      leg.AddEntry(gr[ii],"#Delta#it{t}/#sigma#it{t}<%i"%(tlist[ii]),"l")

   leg.Draw()

   labeldict = {"U1b": "Upgrade 1b                ",
         "lumi0.6": "Upgrade 2: #it{L}=0.6#times 10^{34} cm^{-2}s^{-1}",
         "lumi1.0": "Upgrade 2: #it{L}=1.0#times 10^{34} cm^{-2}s^{-1}",
         "1.5e34": "Upgrade 2: #it{L}=1.5#times 10^{34} cm^{-2}s^{-1}",
         }

   pt = TPaveText(0.5,0.82,0.9,0.89,"NBNDC")
   pt.AddText(labeldict[lumitype])
   pt.SetFillColor(kWhite)
   pt.SetShadowColor(kWhite)
   pt.Draw()

   gr_ET1 = TGraph() #ET bin number = ET/0.5
   for ii in range(len(gr)):
      gr_ET1.SetPoint(ii,eff[tlist[ii]-1][1],signif[tlist[ii]-1][1])
   #gr_ET1.SetPoint(len(gr),eff[-2][1],signif[-2][1])
   #gr_ET1.SetPoint(len(gr)+1,eff[-1][1],signif[-1][1])
   gr_ET1.SetMarkerStyle(20)
   gr_ET1.SetMarkerSize(2.)
   gr_ET1.SetMarkerColor(kOrange+1)
   gr_ET1.Draw("same,P")

   gr_ET2 = TGraph() #ET bin number = ET/0.5
   for ii in range(len(gr)):
      gr_ET2.SetPoint(ii,eff[tlist[ii]-1][3],signif[tlist[ii]-1][3])
   #gr_ET2.SetPoint(len(gr),eff[-2][3],signif[-2][3])
   #gr_ET2.SetPoint(len(gr)+1,eff[-1][3],signif[-1][3])
   gr_ET2.SetMarkerStyle(47)
   gr_ET2.SetMarkerSize(2.)
   gr_ET2.SetMarkerColor(kViolet+1)
   gr_ET2.Draw("same,P")

   leg2 = TLegend(0.65,0.67,0.85,0.8)
   leg2.AddEntry(gr_ET1,"#it{E}_{T}>1 GeV","P")
   leg2.AddEntry(gr_ET2,"#it{E}_{T}>2 GeV","P")
   leg2.Draw()


   cv1.SaveAs("plots/graph_%s.png"%(lumitype))
   



#gROOT.ProcessLine(".x ~/functions/lhcbStyle.C");
#gStyle.SetMarkerSize(0.3)
#gStyle.SetLineScalePS(1);


draw("1.5e34", [1,3,4,5,7,10])





