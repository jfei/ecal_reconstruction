rm ./Run5RoOut/Part45*.txt
rm ./Run5RoFtOut/Part45*.txt
rm ./SiTimingRoOut/Part45*.txt
rm ./SiTimingRoFtOut/Part45*.txt
for((tCut=1; tCut<=8; tCut++))
do
for ((pt=500; pt<=4000; pt+=100))
do
#root -l -q FitB2KstGamma_SiTimingRo.C\(3,$pt,4\)
root -l -q -b FitB2KstGamma_Run5Ro.C\($tCut,$pt,45\) 
root -l -q -b FitB2KstGamma_Run5Ro_ft.C\($tCut,$pt,45\) 
root -l -q -b FitB2KstGamma_SiTimingRo_ft.C\($tCut,$pt,45\) 
root -l -q -b FitB2KstGamma_SiTimingRo.C\($tCut,$pt,45\)
done
done