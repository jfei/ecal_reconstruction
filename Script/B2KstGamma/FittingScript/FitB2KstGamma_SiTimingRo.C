#include "RooRealVar.h"
#include "TStyle.h"
#include "RooAbsReal.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCrystalBall.h"
#include "RooExponential.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>
#include <streambuf>
#include "TFile.h"
#include "RooFitResult.h"
#include "RooArgList.h"
#include "TChain.h"
#include "TPaveText.h"

using namespace RooFit;
using namespace std;

std::map<int, int> Part_LayerNum = {{1, 2}, {2, 2}, {3, 2}, {4, 4}, {5, 4}};
std::map<int, int> Type_LayerNum = {
	{1, 2},
	{2, 2},
	{3, 2},
	{4, 4},
	{5, 4},
	{6, 4},
	{7, 4},
	{8, 4},
	{9, 4},
	{10, 4},
	{11, 4},
};
std::map<int, std::vector<int>> Part2Type = {{1, {1}}, {2, {2}}, {3, {3}}, {4, {4, 5, 6, 7}}, {5, {8, 9, 10, 11}}};
std::map<int, std::vector<int>> Type2Id = {
	{1, {1}},
	{2, {2}},
	{3, {3}},
	{4, {4, 6, 7}},
	{5, {5, 14, 15}},
	{6, {8, 9, 10}},
	{7, {11, 12, 13}},
	{8, {16, 18, 19}},
	{9, {25, 27, 17}},
	{10, {20, 21, 22}},
	{11, {23, 24, 26}},
};

inline bool exists_test1(const std::string &name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return 1;
	}
	else
	{
		return 0;
	}
}

std::string lumi = "1.5e34";

void FitB2KstGamma_SiTimingRo(double tCut, double ptCut, int part)
{
	// read data
	gStyle->SetOptFit();
	TChain *mytree = new TChain();
	mytree->Add(Form("/Users/fjl/workdir/ecal_reconstruction/PhysicalChannelOut/B2KstGamma/1.5e34/SiTimingRotation/B2KstGamma_1.5e34_Match.root/tree"));

	std::stringstream tmp_name;
	std::string regionCut;
	tmp_name.str("");
	tmp_name << "(";
	int TotRegionNum = 0;
	int Part = part;
	int Type = 0;
	std::vector<int> PartType;
	if (Type == 0)
	{
		if (Part == 45)
		{
			for (int subPart = 4; subPart <= 5; subPart++)
				for (auto type : Part2Type.at(subPart))
				{
					PartType.emplace_back(type);
				}
		}
		else
		{
			for (auto type : Part2Type.at(Part))
			{
				PartType.emplace_back(type);
			}
		}
	}
	if (Type == 0)
	{
		for (int i = 0; i < PartType.size(); i++)
		{
			TotRegionNum += Type2Id.at(PartType.at(i)).size();
			tmp_name << "regionType==" << PartType.at(i);
			if (i == PartType.size() - 1)
			{
				tmp_name << ")";
			}
			else
			{
				tmp_name << "||";
			}
		}
	}
	else
	{
		tmp_name << "regionType==" << Type << ")";
	}
	regionCut = tmp_name.str();

	TTree *newTree = mytree->CopyTree(regionCut.c_str());
	TString sig_cut = Form("pt>=%f&&match==1", ptCut);
	TString all_cut = Form("pidtag==22&&pt>=%f&&abs(true_timing-t)*1000/30<=%f", ptCut, tCut);
	std::cout << sig_cut << std::endl;
	// TTree *tree = mytree->CopyTree(all_cut);
	int nTotSigNum = newTree->GetEntries("match==1");
	int nSigAfterCut = newTree->GetEntries("match==1&&" + all_cut);

	// Bmass
	RooRealVar m("Bmass", "#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma})", 4600., 6400., "MeV/#it{c}^{2}");

	// set data
	RooDataSet data("data", "data", newTree->CopyTree(sig_cut), m);
	RooDataSet dataall("dataall", "dataall", newTree->CopyTree(all_cut), m);
	// Define CrystalBall
	RooRealVar SigMean("SigMean", "SigMean", 5280., 5180., 5600.);
	RooRealVar SigSigma("SigSigma", "SigSigma", 100., 80., 160.);

	RooRealVar alphal("alphal", "alphal", 1., 0., 5.);
	RooRealVar nl("nl", "nl", 1., 0., 10.);

	RooRealVar alphar("alphar", "alphar", 1., 0., 5.);
	RooRealVar nr("nr", "nr", 100., 1., 200.);

	RooCrystalBall sig("sig", "sig", m, SigMean, SigSigma, alphal, nl, alphar, nr);
	RooPlot *mframe = m.frame(RooFit::Title(Form("data frame")));
	data.plotOn(mframe, RooFit::Binning(50));
	sig.fitTo(data, RooFit::SumW2Error(kTRUE), RooFit::NumCPU(16));
	sig.fitTo(data, RooFit::SumW2Error(kTRUE), RooFit::NumCPU(16));
	sig.plotOn(mframe);
	// data.plotOn(mframe, RooFit::Binning(100));
	TCanvas *c1 = new TCanvas("c1", "c1", 900, 600);
	mframe->GetYaxis()->SetTitle("Candidates / (20 MeV/#it{c}^{2})");
	mframe->GetYaxis()->SetTitleOffset(0.88);
	mframe->GetXaxis()->SetTitle("#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma}) [MeV/#it{c}^{2}]");
	mframe->Draw();

	TPaveText *pt = new TPaveText(0.55, 0.6, 0.85, 0.9, "NDC");
	pt->SetLineColor(kWhite);
	pt->SetTextFont(132);
	pt->SetShadowColor(kWhite);
	pt->SetFillColor(kWhite);
	pt->SetTextAlign(12);
	pt->AddText(Form("#Delta#it{t}/#sigma#it{t}(comb) < %.0f ", tCut));
	pt->AddText(Form("#it{E}_{T} > %.1f MeV", ptCut));
	pt->AddText(Form("#it{#mu} = %.2f #pm %.2f MeV/#it{c^{2}}", SigMean.getVal(), SigMean.getError()));
	pt->AddText(Form("#it{#sigma} = %.2f #pm %.2f MeV/#it{c^{2}}", SigSigma.getVal(), SigSigma.getError()));
	pt->Draw();

	c1->SaveAs(Form("SiTimingRoPlots/myKpiTru_sig_ET%.1f_tc%.0f_1.5e34_part%d.png", ptCut, tCut, part));

	///////////////////////////////////////////////////////////////////////////////////
	//							Sig and Bkg
	///////////////////////////////////////////////////////////////////////////////////
	alphal.setConstant(true);
	alphar.setConstant(true);
	nl.setConstant(true);
	nr.setConstant(true);
	float leftLimit = SigMean.getVal() - 300;
	float rightLimit = SigMean.getVal() + 300;
	SigSigma.setRange(SigSigma.getVal() - 2 * SigSigma.getError(), SigSigma.getVal() + 2 * SigSigma.getError());
	SigMean.setRange(SigMean.getVal() - 2 * SigMean.getError(), SigMean.getVal() + 2 * SigMean.getError());

	RooRealVar tau = RooRealVar("tau", "tau", -0.0001, -0.1, -0.);
	RooExponential bkg = RooExponential("bkg", "", m, tau);
	RooRealVar Nsig = RooRealVar("nsig", "Number of signal events", nTotSigNum / 2, 1, nTotSigNum);
	RooRealVar Nbkg = RooRealVar("nbkg", "Number of background events", 100, 0.001, 10000);
	RooAddPdf total = RooAddPdf("total", "sum of signal and background PDF's", RooArgList(sig, bkg), RooArgList(Nsig, Nbkg));

	RooPlot *mframe2 = m.frame(RooFit::Title(Form("data frame")));
	dataall.plotOn(mframe2, RooFit::Binning(50));

	total.fitTo(dataall, RooFit::SumW2Error(kTRUE), RooFit::Extended(true), RooFit::NumCPU(16));
	total.fitTo(dataall, RooFit::SumW2Error(kTRUE), RooFit::Extended(true), RooFit::NumCPU(16));
	total.plotOn(mframe2);
	total.plotOn(mframe2, RooFit::Components("sig"), RooFit::LineColor(kRed), RooFit::LineStyle(kSolid));
	total.plotOn(mframe2, RooFit::Components("bkg"), RooFit::LineColor(kGreen + 2), RooFit::LineStyle(kDashed));

	TCanvas *c2 = new TCanvas("c2", "c2", 900, 600);

	mframe2->GetYaxis()->SetTitle("Candidates / (20 MeV/#it{c}^{2})");
	mframe2->GetYaxis()->SetTitleOffset(0.88);
	mframe2->GetXaxis()->SetTitle("#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma}) [MeV/#it{c}^{2}]");

	mframe2->Draw();

	TPaveText *pt2 = new TPaveText(0.55, 0.6, 0.85, 0.89, "NDC");
	pt2->SetLineColor(kWhite);
	// pt2->SetTextSize(1);
	pt2->SetTextFont(132);
	pt2->SetShadowColor(kWhite);
	pt2->SetFillColor(kWhite);
	pt2->SetTextAlign(12);
	string lumiStr;
	if (lumi == "1.5e34")
	{
		lumiStr = "1.5 #times 10^{34} #it{cm^{-2}s^{-1}}";
	}

	pt2->AddText(Form("Upgrade II : %s", lumiStr.c_str()));
	pt2->AddText(Form("#Delta#it{t}/#sigma#it{t}(comb) < %.0f", tCut));
	pt2->AddText(Form("#it{E}_{T} > %.1f MeV", ptCut));
	pt2->AddText(Form("#it{#mu} = %.2f #pm %.2f MeV/#it{c^{2}}", SigMean.getVal(), SigMean.getError()));
	pt2->AddText(Form("#it{#sigma} = %.2f #pm %.2f MeV/#it{c^{2}}", SigSigma.getVal(), SigSigma.getError()));
	pt2->AddText(regionCut.c_str());
	pt2->AddText(Form("#it{N}_{sig} = %.2f #pm %.2f", Nsig.getVal(), Nsig.getError()));

	m.setRange("Rsignal", SigMean.getVal() - 3 * SigSigma.getVal(), SigMean.getVal() + 3 * SigSigma.getVal());
	cout << "sigma " << SigSigma.getVal() << endl;

	double sig_per = sig.createIntegral(m, RooFit::NormSet(m), Range("Rsignal"))->getVal();
	cout << "sig per " << sig_per << endl;
	double bkg_per = bkg.createIntegral(m, RooFit::NormSet(m), Range("Rsignal"))->getVal();
	cout << "bkg per " << bkg_per << endl;

	double sig_num = Nsig.getVal();
	double bk_num = Nbkg.getVal();
	cout << "sig_num " << sig_num << endl;
	cout << "bk_num " << bk_num << endl;
	std::cout << "nTotSigNum " << nTotSigNum << std::endl;

	float S2B = (sig_num * sig_per) / (bk_num * bkg_per);

	pt2->AddText(Form("#it{N}_{sig}/#it{N}_{bkg} = %.2f ", S2B));
	pt2->Draw();

	/*
		TPaveText pt2 = TPaveText(0.15, 0.8, 0.42, 0.9, "NDC");
		pt2.SetLineColor(kWhite);
		pt2.SetTextFont(132);
		pt2.SetShadowColor(kWhite);
		pt2.SetFillColor(kWhite);
		pt2.SetTextAlign(12);
		pt2.Draw();
	*/
	c2->SaveAs(Form("SiTimingRoPlots/myKpiTru_ET%.1f_tc%.0f_%s_part%d.png", ptCut, tCut, lumi.c_str(), part));

	std::ofstream CaliParFile;
	tmp_name.str("");
	tmp_name << "./SiTimingRoOut/Part" << Part << "_SiTimingRo.txt";
	CaliParFile.open(tmp_name.str(), ios_base::app);
	TString Windows = Form("Bmass>=%f&&Bmass<=%f", leftLimit, rightLimit);
	int SigNum = newTree->GetEntries("match==1&&" + all_cut + "&&" + Windows);
	int BkgNum = newTree->GetEntries("match==0&&" + all_cut + "&&" + Windows);
	CaliParFile << ptCut << " " << tCut << " " << SigNum << " " << BkgNum << " " << nTotSigNum << " " << nSigAfterCut << std::endl;
}
