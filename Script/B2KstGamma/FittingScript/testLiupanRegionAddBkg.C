#include "RooRealVar.h"
#include "TStyle.h"
#include "RooAbsReal.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCrystalBall.h"
#include "RooExponential.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>
#include <streambuf>
#include "TFile.h"
#include "RooFitResult.h"
#include "RooArgList.h"
#include "TChain.h"
#include "TPaveText.h"

using namespace RooFit;
using namespace std;

inline bool exists_test1(const std::string &name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return 1;
	}
	else
	{
		return 0;
	}
}

std::string lumi = "1.5e34";

void testLiupanRegionAddBkg(double tCut, double ptCut, int region)
{
	// read data
	gStyle->SetOptFit();
	TChain *mytree = new TChain();
	// mytree->Add(Form("../Matching/Matching_B2KstGamma_%s.root/tree", lumi.c_str()));
	mytree->Add("/Users/fjl/workdir/full_reconstruction/4_Kpi_bkg/4_tmva/samples/Reconstruction_forTMVA_lumi1.5.root/cluster");
	// TString sig_cut = Form("pt>=%f&&abs(t-true_timing-0.08)<=%f&&match==1", ptCut, tCut);
	// TString all_cut = Form("pt>=%f&&abs(t-true_timing-0.08)<=%f", ptCut, tCut);

	std::string regionCut;
	if (region == 0)
	{
		regionCut = "region>=0";
	}
	else if (region == 5 || region == 6)
	{
		regionCut = "(region==5||region==6)";
	}
	else if (region == 3 || region == 4)
	{
		regionCut = "(region==3||region==4)";
	}
	else if (region == 66)
	{
		regionCut = "(region==6)";
	}
	else if (region == 55)
	{
		regionCut = "(region==5)";
	}
	else
	{
		regionCut = Form("region==%d", region);
	}
	TString sig_cut = Form(("pidtag==22&&sqrt(px*px+py*py)>=%f&&deltaT<=%f&&index==1&&tag==1&&" + regionCut).c_str(), ptCut, tCut);
	TString all_cut = Form(("pidtag==22&&sqrt(px*px+py*py)>=%f&&deltaT<=%f&&" + regionCut).c_str(), ptCut, tCut);
	// TTree *tree = mytree->CopyTree(all_cut);

	// Bmass
	RooRealVar m("MKstg", "#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma})", 4000., 7500., "MeV/#it{c}^{2}");

	// set data
	RooDataSet data("data", "data", mytree->CopyTree(sig_cut), m);
	RooDataSet dataall("dataall", "dataall", mytree->CopyTree(all_cut), m);
	// Define CrystalBall
	RooRealVar SigMean("SigMean", "SigMean", 5280., 5000., 6000.);
	RooRealVar SigSigma("SigSigma", "SigSigma", 100., 50., 300.);

	RooRealVar alphal("alphal", "alphal", 2., 0., 5.);
	RooRealVar nl("nl", "nl", 1.);

	RooRealVar alphar("alphar", "alphar", 2., 0., 10.);
	RooRealVar nr("nr", "nr", 1., 0., 10.);

	RooCrystalBall sig("sig", "sig", m, SigMean, SigSigma, alphal, nl, alphar, nr);
	RooPlot *mframe = m.frame(RooFit::Title(Form("data frame")));
	data.plotOn(mframe, RooFit::Binning(50));
	sig.fitTo(data, RooFit::SumW2Error(kTRUE), RooFit::NumCPU(16));
	sig.plotOn(mframe);

	data.plotOn(mframe, RooFit::Binning(50));
	// data.plotOn(mframe, RooFit::Binning(100));
	TCanvas *c1 = new TCanvas("c1", "c1", 900, 600);
	mframe->GetYaxis()->SetTitle("Candidates / (20 MeV/#it{c}^{2})");
	mframe->GetYaxis()->SetTitleOffset(0.88);
	mframe->GetXaxis()->SetTitle("#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma}) [MeV/#it{c}^{2}]");
	mframe->Draw();

	TPaveText *pt = new TPaveText(0.55, 0.6, 0.85, 0.9, "NDC");
	pt->SetLineColor(kWhite);
	pt->SetTextFont(132);
	pt->SetShadowColor(kWhite);
	pt->SetFillColor(kWhite);
	pt->SetTextAlign(12);
	pt->AddText(Form("#Delta#it{t}/#sigma#it{t}(comb)  < %.0f ", tCut));
	pt->AddText(Form("#it{E}_{T} > %.1f MeV", ptCut));
	pt->AddText(Form("#it{#mu} = %.2f #pm %.2f MeV/#it{c^{2}}", SigMean.getVal(), SigMean.getError()));
	pt->AddText(Form("#it{#sigma} = %.2f #pm %.2f MeV/#it{c^{2}}", SigSigma.getVal(), SigSigma.getError()));
	pt->Draw();

	c1->SaveAs(Form("plots/Anfitplot_sig_ET%.1f_tcomb%.0f_1.5e34_region%d.png", ptCut, tCut, region));

	///////////////////////////////////////////////////////////////////////////////////
	//							Sig and Bkg
	///////////////////////////////////////////////////////////////////////////////////
	alphal.setConstant(true);
	alphar.setConstant(true);
	nl.setConstant(true);
	nr.setConstant(true);

	RooRealVar tau = RooRealVar("#tau", "tau", -0., -0.5, 0.5);
	RooExponential bkg = RooExponential("bkg", "", m, tau);
	RooRealVar Nsig = RooRealVar("nsig", "Number of signal events", 10000, 0, 30000);
	RooRealVar Nbkg = RooRealVar("nbkg", "Number of background events", 10000, 0, 600000);
	RooAddPdf total = RooAddPdf("total", "sum of signal and background PDF's", RooArgList(sig, bkg), RooArgList(Nsig, Nbkg));

	RooPlot *mframe2 = m.frame(RooFit::Title(Form("data frame")));
	dataall.plotOn(mframe2, RooFit::Binning(50));

	total.fitTo(dataall, RooFit::SumW2Error(kTRUE), RooFit::Extended(true), RooFit::NumCPU(16));
	total.plotOn(mframe2);
	total.plotOn(mframe2, RooFit::Components("sig"), RooFit::LineColor(kRed), RooFit::LineStyle(kSolid));
	total.plotOn(mframe2, RooFit::Components("bkg"), RooFit::LineColor(kGreen + 2), RooFit::LineStyle(kDashed));

	TCanvas *c2 = new TCanvas("c2", "c2", 900, 600);

	mframe2->GetYaxis()->SetTitle("Candidates / (20 MeV/#it{c}^{2})");
	mframe2->GetYaxis()->SetTitleOffset(0.88);
	mframe2->GetXaxis()->SetTitle("#it{M}(#it{K}^{+}#it{#pi}^{#minus}#it{#gamma}) [MeV/#it{c}^{2}]");

	mframe2->Draw();

	TPaveText *pt2 = new TPaveText(0.55, 0.6, 0.85, 0.9, "NDC");
	pt2->SetLineColor(kWhite);
	pt2->SetTextFont(132);
	pt2->SetShadowColor(kWhite);
	pt2->SetFillColor(kWhite);
	pt2->SetTextAlign(12);

	pt2->AddText(Form("Upgrade II : lumi %s", lumi.c_str()));
	pt2->AddText(Form("#Delta#it{t}/#sigma#it{t}(comb)  < %.0f ", tCut));
	pt2->AddText(Form("#it{E}_{T} > %.1f MeV", ptCut));
	pt2->AddText(Form("#it{#mu} = %.2f #pm %.2f MeV/#it{c^{2}}", SigMean.getVal(), SigMean.getError()));
	pt2->AddText(Form("#it{#sigma} = %.2f #pm %.2f MeV/#it{c^{2}}", SigSigma.getVal(), SigSigma.getError()));
	pt2->AddText(regionCut.c_str());
	pt2->AddText(Form("#it{N}_{sig} = %.2f #pm %.2f", Nsig.getVal(), Nsig.getError()));
	m.setRange("Rsignal", SigMean.getVal() - 3 * SigSigma.getVal(), SigMean.getVal() + 3 * SigSigma.getVal());
	cout << "sigma " << SigSigma.getVal() << endl;

	double sig_per = sig.createIntegral(m, RooFit::NormSet(m), Range("Rsignal"))->getVal();
	cout << "sig per " << sig_per << endl;
	double bkg_per = bkg.createIntegral(m, RooFit::NormSet(m), Range("Rsignal"))->getVal();
	cout << "bkg per " << bkg_per << endl;

	double sig_num = Nsig.getVal();
	double bk_num = Nbkg.getVal();
	cout << "sig_num " << sig_num << endl;
	cout << "bk_num " << bk_num << endl;

	float S2B = (sig_num * sig_per) / (bk_num * bkg_per);

	pt2->AddText(Form("#it{N}_{sig}/#it{N}_{bkg} = %.2f ", S2B));
	pt2->Draw();

	c2->SaveAs(Form("plots/AnBkgfitplot_ET%.1f_tcomb%.0f_%s_lumidecay_region%d.png", ptCut, tCut, lumi.c_str(), region));
}
