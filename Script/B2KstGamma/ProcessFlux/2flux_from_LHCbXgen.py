from ROOT import *
from array import array

import sys
#print "Ecal: 12520 mm"
#print "Neutron shielding: 12110 mm"
#print "Distance: 12520-12110 = 410 m"

#gROOT.ProcessLine(".x ~/lhcbStyle.C")


def make(lumitype,name):
   ### the original MCDecayTree
   fsig = TFile("/eos/lhcb/user/s/syuxiang/ECalSimulation/FluxFile/new/1.5e34/Gen_%s_Run2000Evt1_%s.root"%(name,lumitype))
   tsig = fsig.Get("mct/MCDecayTree")

   ### the ECAL map
   fgeo=TFile("./ECAL_LS4_option1.root")
   h2D=fgeo.Get("histo_ls4_option1")

   ### the flux extracted from mergeMB with PV assigned
   f_flux = TFile("%sflux_%s.root"%(name,lumitype))
   tflux = f_flux.Get("tree")
   
   ### the new file
   fs=TFile("FluxGammaFrom%s_assignPV_%s.root"%(name,lumitype),"recreate")
   nt = tflux.CloneTree(0)
   
   Kstpx = array("d",[0.])
   Kstpy = array("d",[0.])
   Kstpz = array("d",[0.])
   Kstpe = array("d",[0.])
   nt.Branch("Kstpx",Kstpx,"Kstpx/D")
   nt.Branch("Kstpy",Kstpy,"Kstpy/D")
   nt.Branch("Kstpz",Kstpz,"Kstpz/D")
   nt.Branch("Kstpe",Kstpe,"Kstpe/D")
   
   Kpx_true = array("d",[0.])
   Kpy_true = array("d",[0.])
   Kpz_true = array("d",[0.])
   Kpe_true = array("d",[0.])
   Kp_true = array("d",[0.])
   nt.Branch("Kpx_true",Kpx_true,"Kpx_true/D")
   nt.Branch("Kpy_true",Kpy_true,"Kpy_true/D")
   nt.Branch("Kpz_true",Kpz_true,"Kpz_true/D")
   nt.Branch("Kpe_true",Kpe_true,"Kpe_true/D")
   nt.Branch("Kp_true",Kp_true,"Kp_true/D")
   
   Kpx = array("d",[0.])
   Kpy = array("d",[0.])
   Kpz = array("d",[0.])
   Kpe = array("d",[0.])
   Kp = array("d",[0.])
   nt.Branch("Kpx",Kpx,"Kpx/D")
   nt.Branch("Kpy",Kpy,"Kpy/D")
   nt.Branch("Kpz",Kpz,"Kpz/D")
   nt.Branch("Kpe",Kpe,"Kpe/D")
   nt.Branch("Kp",Kp,"Kp/D")
   
   pipx_true = array("d",[0.])
   pipy_true = array("d",[0.])
   pipz_true = array("d",[0.])
   pipe_true = array("d",[0.])
   pip_true = array("d",[0.])
   nt.Branch("pipx_true",pipx_true,"pipx_true/D")
   nt.Branch("pipy_true",pipy_true,"pipy_true/D")
   nt.Branch("pipz_true",pipz_true,"pipz_true/D")
   nt.Branch("pipe_true",pipe_true,"pipe_true/D")
   nt.Branch("pip_true",pip_true,"pip_true/D")
   
   pipx = array("d",[0.])
   pipy = array("d",[0.])
   pipz = array("d",[0.])
   pipe = array("d",[0.])
   pip = array("d",[0.])
   nt.Branch("pipx",pipx,"pipx/D")
   nt.Branch("pipy",pipy,"pipy/D")
   nt.Branch("pipz",pipz,"pipz/D")
   nt.Branch("pipe",pipe,"pipe/D")
   nt.Branch("pip",pip,"pip/D")
   
   # Initialize random number generator.
   gRandom.SetSeed()
   gauss = gRandom.Gaus
   
   count = 0

   from math import sqrt,atan
   for kk in range(tflux.GetEntries()):
       tflux.GetEntry(kk)

       tsig.GetEntry(tflux.treeIndex)
       #if not abs(tsig.gamma_MC_MOTHER_ID) == 511:continue
       #if tsig.gamma_TRUEP_Z<0.1:continue
       ##if atan(tsig.gamma_TRUEPT/tsig.gamma_TRUEP_Z)>0.25:continue
       #sig_prod_vertex_x = tsig.gamma_TRUEORIGINVERTEX_X
       #sig_prod_vertex_y = tsig.gamma_TRUEORIGINVERTEX_Y
       #sig_prod_vertex_z = tsig.gamma_TRUEORIGINVERTEX_Z
       #sig_px = tsig.gamma_TRUEP_X/1000.
       #sig_py = tsig.gamma_TRUEP_Y/1000.
       #sig_pz = tsig.gamma_TRUEP_Z/1000.
       ##### propagated to z=12620
       #sig_entry_x = sig_px/sig_pz*(12620.-sig_prod_vertex_z) + sig_prod_vertex_x
       #sig_entry_y = sig_py/sig_pz*(12620.-sig_prod_vertex_z) + sig_prod_vertex_y
       #sig_gid = h2D.GetBinContent(h2D.FindBin(sig_entry_x/10.,sig_entry_y/10.))
       #if sig_gid<1. or sig_gid>6.: continue
   
   
       Kstpx[0] = tsig.Kst_TRUEP_X/1000.
       Kstpy[0] = tsig.Kst_TRUEP_Y/1000.
       Kstpz[0] = tsig.Kst_TRUEP_Z/1000.
       Kstpe[0] = tsig.Kst_TRUEP_E/1000.
   
       Kpx_true[0] = tsig.K_TRUEP_X/1000.
       Kpy_true[0] = tsig.K_TRUEP_Y/1000.
       Kpz_true[0] = tsig.K_TRUEP_Z/1000.
       Kpe_true[0] = tsig.K_TRUEP_E/1000.
       Kp_true[0] = sqrt(Kpx_true[0]*Kpx_true[0]+Kpy_true[0]*Kpy_true[0]+Kpz_true[0]*Kpz_true[0]) 
   
       Kpx[0] = Kpx_true[0] + gauss(0.,Kpx_true[0]*0.01)
       Kpy[0] = Kpy_true[0] + gauss(0.,Kpy_true[0]*0.01)
       Kpz[0] = Kpz_true[0] + gauss(0.,Kpz_true[0]*0.01)
       Kpe[0] = sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]+Kpz[0]*Kpz[0]+0.493677*0.493677) 
       Kp[0] = sqrt(Kpx[0]*Kpx[0]+Kpy[0]*Kpy[0]+Kpz[0]*Kpz[0]) 
   
   
       pipx_true[0] = tsig.pi_TRUEP_X/1000.
       pipy_true[0] = tsig.pi_TRUEP_Y/1000.
       pipz_true[0] = tsig.pi_TRUEP_Z/1000.
       pipe_true[0] = tsig.pi_TRUEP_E/1000.
       pip_true[0] = sqrt(pipx_true[0]*pipx_true[0]+pipy_true[0]*pipy_true[0]+pipz_true[0]*pipz_true[0]) 
   
       pipx[0] = pipx_true[0] + gauss(0.,pipx_true[0]*0.01)
       pipy[0] = pipy_true[0] + gauss(0.,pipy_true[0]*0.01)
       pipz[0] = pipz_true[0] + gauss(0.,pipz_true[0]*0.01)
       pipe[0] = sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]+pipz[0]*pipz[0]+0.1395701*0.1395701) 
       pip[0] = sqrt(pipx[0]*pipx[0]+pipy[0]*pipy[0]+pipz[0]*pipz[0]) 
   
   
       print("filling signal ",count)
       nt.Fill()

           
   nt.Write()
   print("Output Entires: ",nt.GetEntries())



#make("Run2")
#make("lumi0.6")
#make("lumi1.0")
make("1.5e34","B2KstGamma")
make("1.5e34","Bs2PhiGamma")

