from ROOT import *
#for i in range(312,500):

import os,sys

def copytree(lumitype,name):
   ch=TChain()
   ch.Add("/eos/lhcb/user/s/syuxiang/ECalSimulation/FluxFile/new/1.5e34/FluxGamma%s_MB_Merged_%s.root/tree"%(name,lumitype))

   cut="treeIndex!=-1"

   f=TFile("%sflux_%s.root"%(name,lumitype),"recreate")
   print("Processing events: ",str(ch.GetEntries()))
   nt = ch.CopyTree(cut)
   print(nt.GetEntries())
   nt.Write("",1)
   f.Close()


#copytree("Run2")
#copytree("Run3")
#copytree("U1b")
#copytree("lumi0.6")
#copytree("lumi1.0")

copytree("1.5e34","B2KstGamma")
copytree("1.5e34","Bs2PhiGamma")


