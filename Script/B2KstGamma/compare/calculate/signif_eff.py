import sys,os
from ROOT import *
from math import sqrt




def draw(lumitype):
   t = TChain()
   t.Add("../../Matching/Matching_B2KstGamma_%s.root/tree"%(lumitype))

   #n1 = double(t.GetEntries("Bmass>4800.&&Bmass<6300.&&pt>2500."))



   ntot = t.GetEntries("Bmass>4800.&&Bmass<6300.&&match==1")


   fw = open("../output/output_%s.txt"%(lumitype),'w')
   
   mean = 5363.
   sigma = 100.


   nsigtot = double(t.GetEntries("Bmass>%g&&Bmass<%g&&match==1&&pidtag==22"%(mean-3.*sigma,mean+3.*sigma)))

   h = TH2F("h","h",20,0.5,20.5,20,0.25,10.25)

   ETcut = 0.
   tcut = 0.


   ns = 0.
   nsb = 0.
   signif = 0.
   signifr = 0.
   sigeff = 0.
   sigeffr = 0.

   for ii in range(1,11):
      for kk in range(1,21):
         tcut = h.GetXaxis().GetBinCenter(ii)
         ETcut = h.GetYaxis().GetBinCenter(kk)

         print("Bin x = ",ii,", y = ",kk)
         print("tcut = ",tcut,"; ETcut = ",ETcut)

         nsb = double(t.GetEntries("t>0.&&pidtag==22&&Bmass>%g&&Bmass<%g&&pt/1000.>%g&&abs(t-true_timing-0.067)*1000/TRes<%g"%(mean-3.*sigma,mean+3.*sigma,ETcut,tcut)))
         ns = double(t.GetEntries("t>0.&&pidtag==22&&Bmass>%g&&Bmass<%g&&pt/1000>%g&&abs(t-true_timing-0.067)*1000/TRes<%g&&match==1"%(mean-3.*sigma,mean+3.*sigma,ETcut,tcut)))

         sigeff = ns/nsigtot
         sigeffr = sqrt(sigeff*(1-sigeff)/nsigtot)

         signif = ns/sqrt(nsb)/sqrt(ntot)
         signifr = signif*sqrt(1./ns+1./4./nsb)

         print("signif = ",signif," +- ",signifr)

         fw.write("%.1f %.1f %.4f %.4f %.4f %.4f\n"%(ETcut,tcut,signif,signifr,sigeff,sigeffr))



#gROOT.ProcessLine(".x ~/functions/lhcbStyle.C");
gStyle.SetMarkerSize(0.3)
#gStyle.SetLineScalePS(1);


draw("1.5e34")





