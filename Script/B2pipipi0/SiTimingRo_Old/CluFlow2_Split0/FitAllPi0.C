#include "RooRealVar.h"
#include "TStyle.h"
#include "RooAbsReal.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCrystalBall.h"
#include "RooExponential.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>
#include <streambuf>
#include "TFile.h"
#include "RooFitResult.h"
#include "RooArgList.h"
#include "TChain.h"
#include "TPaveText.h"
#include "/Users/fjl/lhcbStyle.C"

using namespace RooFit;
using namespace std;

std::map<int, int> Part_LayerNum = {{1, 2}, {2, 2}, {3, 2}, {4, 4}, {5, 4}};
std::map<int, int> Type_LayerNum = {
	{1, 2},
	{2, 2},
	{3, 2},
	{4, 4},
	{5, 4},
	{6, 4},
	{7, 4},
	{8, 4},
	{9, 4},
	{10, 4},
	{11, 4},
};
std::map<int, std::vector<int>> Part2Type = {{1, {1}}, {2, {2}}, {3, {3}}, {4, {4, 5, 6, 7}}, {5, {8, 9, 10, 11}}};
std::map<int, std::vector<int>> Type2Id = {
	{1, {1}},
	{2, {2}},
	{3, {3}},
	{4, {4, 6, 7}},
	{5, {5, 14, 15}},
	{6, {8, 9, 10}},
	{7, {11, 12, 13}},
	{8, {16, 18, 19}},
	{9, {25, 27, 17}},
	{10, {20, 21, 22}},
	{11, {23, 24, 26}},
};

inline bool exists_test1(const std::string &name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return 1;
	}
	else
	{
		return 0;
	}
}

std::string lumi = "PureSig";

void FitAllPi0(int part)
{
	lhcbStyle();
	// read data
	gStyle->SetOptFit();
	TChain *mytree = new TChain();
	mytree->Add(Form("/Users/fjl/workdir/ecal_reconstruction/Configuration/SiTimingRoOld/RecFile/Pi0_2_0.root/Pi0"));

	std::stringstream tmp_name;
	std::string regionCut;
	tmp_name.str("");
	tmp_name << "(";
	int TotRegionNum = 0;
	int Part = part;
	int Type = 0;
	std::vector<int> PartType;
	if (Type == 0)
	{
		if (Part == 45)
		{
			for (int subPart = 4; subPart <= 5; subPart++)
				for (auto type : Part2Type.at(subPart))
				{
					PartType.emplace_back(type);
				}
		}
		else
		{
			for (auto type : Part2Type.at(Part))
			{
				PartType.emplace_back(type);
			}
		}
	}
	if (Type == 0)
	{
		for (int i = 0; i < PartType.size(); i++)
		{
			TotRegionNum += Type2Id.at(PartType.at(i)).size();
			tmp_name << "HitRegionType==" << PartType.at(i);
			if (i == PartType.size() - 1)
			{
				tmp_name << ")";
			}
			else
			{
				tmp_name << "||";
			}
		}
	}
	else
	{
		tmp_name << "HitRegionType==" << Type << ")";
	}
	regionCut = tmp_name.str();

	TTree *newTree = mytree->CopyTree(regionCut.c_str());
	float TotalNum = newTree->GetEntries();

	// 创建一个RooRealVar代表Pi0_M，并指定范围
	RooRealVar Pi0_M("Pi0_M", "Pi0_M", 90, 180);

	// 将TTree中的数据转换为RooDataSet
	RooDataSet data("data", "data", newTree, RooArgSet(Pi0_M));

	// 创建信号和本底的数量变量
	RooRealVar nsig("nsig", "number of signal events", 100, 0, 10000);
	RooRealVar nbkg("nbkg", "number of background events", 200, 0, 10000);

	// 创建高斯信号模型
	RooRealVar mean("mean", "mean of Gaussian", 136.5, 135, 150);
	RooRealVar sigma("sigma", "width of Gaussian", 7, 6, 8);
	RooRealVar alphaL("alphaL", "alphaL of DoubleCB", 1.5, 0, 5);
	RooRealVar alphaR("alphaR", "alphaR of DoubleCB", 1.5, 0, 5);
	RooRealVar nL("nL", "nL of DoubleCB", 1, 0, 10);
	RooRealVar nR("nR", "nR of DoubleCB", 1, 0, 10);
	RooCrystalBall signal("signal", "signal PDF", Pi0_M, mean, sigma, alphaL, nL, alphaR, nR);

	// 创建指数本底模型
	RooRealVar tau = RooRealVar("tau", "tau", -0.01, -0.1, -0.);
	RooExponential background("background", "background PDF", Pi0_M, tau);

	// 创建信号加本底的总模型
	RooAddPdf model("model", "signal+background", RooArgList(signal, background), RooArgList(nsig, nbkg));

	// 进行拟合
	RooFitResult *result = model.fitTo(data, RooFit::Extended(true));

	// // 输出拟合结果
	// result->Print();

	// 获取信号和本底的数量
	double signalEvents = nsig.getVal();
	double backgroundEvents = nbkg.getVal();

	std::cout << "Signal events: " << signalEvents << std::endl;
	std::cout << "Background events: " << backgroundEvents << std::endl;

	// 画出拟合图
	RooPlot *frame = Pi0_M.frame();
	data.plotOn(frame);
	model.plotOn(frame);
	TCanvas *c1 = new TCanvas("c", "c", 800, 600);
	frame->Draw();
	c1->SaveAs(Form("Plots/sig_bk.png"));
}
