#include "RooRealVar.h"
#include "TStyle.h"
#include "RooAbsReal.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooCrystalBall.h"
#include "RooExponential.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>
#include <streambuf>
#include "TFile.h"
#include "RooFitResult.h"
#include "RooArgList.h"
#include "TChain.h"
#include "TPaveText.h"
#include "/Users/fjl/lhcbStyle.C"

using namespace RooFit;
using namespace std;

std::map<int, int> Part_LayerNum = {{1, 2}, {2, 2}, {3, 2}, {4, 4}, {5, 4}};
std::map<int, int> Type_LayerNum = {
	{1, 2},
	{2, 2},
	{3, 2},
	{4, 4},
	{5, 4},
	{6, 4},
	{7, 4},
	{8, 4},
	{9, 4},
	{10, 4},
	{11, 4},
};
std::map<int, std::vector<int>> Part2Type = {{1, {1}}, {2, {2}}, {3, {3}}, {4, {4, 5, 6, 7}}, {5, {8, 9, 10, 11}}};
std::map<int, std::vector<int>> Type2Id = {
	{1, {1}},
	{2, {2}},
	{3, {3}},
	{4, {4, 6, 7}},
	{5, {5, 14, 15}},
	{6, {8, 9, 10}},
	{7, {11, 12, 13}},
	{8, {16, 18, 19}},
	{9, {25, 27, 17}},
	{10, {20, 21, 22}},
	{11, {23, 24, 26}},
};

inline bool exists_test1(const std::string &name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return 1;
	}
	else
	{
		return 0;
	}
}

std::string lumi = "PureSig";

void CountAllPi0(int part)
{
	lhcbStyle();
	// read data
	gStyle->SetOptFit();
	TChain *mytree = new TChain();
	mytree->Add(Form("/Users/fjl/workdir/ecal_reconstruction/Configuration/SiTimingRoOld/RecFile/Pi0_3D.root/Pi0"));

	std::stringstream tmp_name;
	std::string regionCut;
	tmp_name.str("");
	tmp_name << "(";
	int TotRegionNum = 0;
	int Part = part;
	int Type = 0;
	std::vector<int> PartType;
	if (Type == 0)
	{
		if (Part == 45)
		{
			for (int subPart = 4; subPart <= 5; subPart++)
				for (auto type : Part2Type.at(subPart))
				{
					PartType.emplace_back(type);
				}
		}
		else
		{
			for (auto type : Part2Type.at(Part))
			{
				PartType.emplace_back(type);
			}
		}
	}
	if (Type == 0)
	{
		for (int i = 0; i < PartType.size(); i++)
		{
			TotRegionNum += Type2Id.at(PartType.at(i)).size();
			tmp_name << "HitRegionType==" << PartType.at(i);
			if (i == PartType.size() - 1)
			{
				tmp_name << ")";
			}
			else
			{
				tmp_name << "||";
			}
		}
	}
	else
	{
		tmp_name << "HitRegionType==" << Type << ")";
	}
	regionCut = tmp_name.str();

	TTree *newTree = mytree->CopyTree(regionCut.c_str());
	float TotalNum = newTree->GetEntries();
	std::vector<float> bins = {0, 20, 40, 70, 100, 200};
	std::vector<float> binsTruNum = {2170 / 2., 9968 / 2., 11662 / 2., 6632 / 2., 7842 / 2.};

	std::vector<float> bins_pt = {0, 2, 4, 8, 12, 20};
	std::vector<float> binsTruNum_pt = {15030 / 2., 15072 / 2., 8590 / 2., 1270 / 2., 398 / 2.};
	TGraphErrors *EffVsE = new TGraphErrors();
	TGraphErrors *EffVsPt = new TGraphErrors();
	for (int i = 0; i < bins.size() - 1; i++)
	{
		// 获取信号和本底的数量
		double signalEvents = newTree->GetEntries(Form("Pi0_M>110&&Pi0_M<150&&abs(Pi0_E-Pi0_TruE)/Pi0_TruE<0.1&&Pi0_TruE>%f*1000.&&Pi0_TruE<=%f*1000.", bins.at(i), bins.at(i + 1)));
		double signalEventsPt = newTree->GetEntries(Form("Pi0_M>110&&Pi0_M<150&&abs(Pi0_E-Pi0_TruE)/Pi0_TruE<0.1&&Pi0_TruPt>%f*1000.&&Pi0_TruPt<=%f*1000.", bins_pt.at(i), bins_pt.at(i + 1)));

		double Eff = signalEvents / binsTruNum.at(i);
		double delta_efficiency = sqrt(Eff * (1 + Eff) / binsTruNum.at(i));

		double Eff_Pt = signalEventsPt / binsTruNum_pt.at(i);
		double delta_efficiency_pt = sqrt(Eff_Pt * (1 + Eff_Pt) / binsTruNum_pt.at(i));
		// std::cout << Eff << std::endl;
		EffVsE->SetPoint(i, (bins.at(i) + bins.at(i + 1)) / 2., Eff);
		EffVsE->SetPointError(i, 0, delta_efficiency);

		EffVsPt->SetPoint(i, (bins_pt.at(i) + bins_pt.at(i + 1)) / 2., Eff_Pt);
		EffVsPt->SetPointError(i, 0, delta_efficiency_pt);
	}
	EffVsE->GetXaxis()->SetTitle("E(#pi^{0})/[GeV]");
	EffVsE->GetYaxis()->SetTitle("Reconstruction Efficiency(#pi^{0})");
	EffVsPt->GetXaxis()->SetTitle("Pt(#pi^{0})/[GeV]");
	EffVsPt->GetYaxis()->SetTitle("Reconstruction Efficiency(#pi^{0})");
	TCanvas *c1 = new TCanvas("c", "c", 800, 600);
	EffVsE->Draw();
	c1->SaveAs(Form("Plots/Run5不分层.pdf"));
	TCanvas *c2 = new TCanvas("c2", "c2", 800, 600);
	EffVsPt->Draw();
	c2->SaveAs(Form("Plots/Run5不分层_Pt.pdf"));
}
