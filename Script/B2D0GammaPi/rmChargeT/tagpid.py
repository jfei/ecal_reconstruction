import sys,os
import math
import root_numpy
import pandas
from ROOT import *
from array import array
import numpy as np


def domatch(lumi,nstart,nend):
   t = TChain()
   t.Add("../../Configuration/B2Kstgamma/%s/B.root/tree"%(lumi))
   nentries = t.GetEntries()
   if nend >= nentries:
      nend = nentries-1
   t.GetEntry(nstart)
   evtNstart = t.evtNumber
   t.GetEntry(nend)
   evtNend = t.evtNumber
   print("evtNumber from",evtNstart," to",evtNend)


   #if "downscoped" in lumi:
   lumi_charged = lumi.replace("_downscoped","")

   tflux = TChain()
   tflux.Add("./charged_sample/charged_sample_%s.root/tree"%(lumi_charged))

   variables = {
         "evtIndex":"evtIndex",
         "entry_x":"entry_x",
         "entry_y":"entry_y",
         "eTot":"eTot",
         "charge":"charge"
         }
   selection = ""
   fluxData = root_numpy.tree2array(tree=tflux,branches=variables.values(),selection=selection)
   fluxData = pandas.DataFrame(fluxData)
   fluxData = fluxData[(fluxData['evtIndex']>=evtNstart) & (fluxData['evtIndex']<=evtNend)]




   newfile = TFile("rootfiles/Reconstruction_tagT_%s_%i_to_%i.root"%(lumi,nstart,nend),"recreate")
   newtree = t.CloneTree(0)
   
   pidtag = array("i",[0])
   newtree.Branch("pidtag",pidtag,"pidtag/I")


   print("Processing events: ",nentries)

   for jentry in range(nstart,nend):
      t.GetEntry(jentry)
      pidtag[0] = 22

      #nn = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&(abs(pdgID)==11||abs(pdgID)==211||abs(pdgID)==321||abs(pdgID)==2212)&&{e}/1000./eTot>0.75&&{e}/1000./eTot<1.25".format(evtNumber=t.evtNumber,x=t.x,y=t.y,cellSize=t.cellSize,e=t.e))
      #nn = tflux.Filter("evtIndex=={evtNumber}&&sqrt(pow(entry_x-({x}),2)+pow(entry_y-({y}),2))<{cellSize}&&{e}/1000./eTot<1.25".format(evtNumber=t.evtNumber,x=t.x,y=t.y,cellSize=t.cellSize,e=t.e)).Count()

      nn = len(fluxData[(fluxData['evtIndex'] == t.evtNumber) & (np.sqrt(pow(fluxData['entry_x']-t.rec_entry_x,2)+pow(fluxData['entry_y']-t.rec_entry_y,2))<t.cellSize) & (t.e/1000./fluxData['eTot']<1.25) & fluxData['charge'] == -1])
      nz = len(fluxData[(fluxData['evtIndex'] == t.evtNumber) & (np.sqrt(pow(fluxData['entry_x']-t.rec_entry_x,2)+pow(fluxData['entry_y']-t.rec_entry_y,2))<t.cellSize) & (t.e/1000./fluxData['eTot']<1.25) & fluxData['charge'] == 1])
      #print("nn = ",nn)
      #print("nz = ",nz)

      if nn+nz > 0:
         if nn >= nz:
            pidtag[0] = 11
            print("nn>=nz = ",nn-nz)
         else :
            pidtag[0] = -11
            print("nz>nn = ",nz-nn)
         





      if jentry%100 == 0:
         print(jentry)
      newtree.Fill()



   newtree.Print()
   newtree.AutoSave()
   newfile.Close()





domatch("1.5e34",int(sys.argv[-2]),int(sys.argv[-1]))

