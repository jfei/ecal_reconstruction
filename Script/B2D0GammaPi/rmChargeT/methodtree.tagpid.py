import sys,os
import math
from ROOT import *
from array import array


def domatch(lumi,nstart,nend):
   tflux = TChain()
   tflux.Add("./charged_sample/charged_sample_%s.root/tree"%(lumi))

   t = TChain()
   t.Add("../1_match/Reconstruction_matched_%s.root/cluster"%(lumi))

   newfile = TFile("Reconstruction_tagT_%s_%i_to_%i.root"%(lumi,nstart,nend),"recreate")
   newtree = t.CloneTree(0)
   
   pidtag = array("i",[0])
   newtree.Branch("pidtag",pidtag,"pidtag/I")


   nentries = t.GetEntries()
   print "Processing events: ",nentries

   for jentry in range(nstart,nend):
      t.GetEntry(jentry)
      pidtag[0] = 22

      #nn = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&(abs(pdgID)==11||abs(pdgID)==211||abs(pdgID)==321||abs(pdgID)==2212)&&{e}/1000./eTot>0.75&&{e}/1000./eTot<1.25".format(evtNumber=t.evtNumber,x=t.x,y=t.y,cellSize=t.cellSize,e=t.e))
      nn = tflux.GetEntries("evtIndex=={evtNumber}&&sqrt(pow(entry_x-{x},2)+pow(entry_y-{y},2))<{cellSize}&&{e}/1000./eTot<1.25".format(evtNumber=t.evtNumber,x=t.x,y=t.y,cellSize=t.cellSize,e=t.e))
      print "nn =",nn

      if nn > 0:
         pidtag[0] = 111

      if jentry%100 == 0:
         print jentry
      newtree.Fill()



   newtree.Print()
   newtree.AutoSave()
   newfile.Close()





domatch("lumi0.6",int(sys.argv[-2]),int(sys.argv[-1]))

