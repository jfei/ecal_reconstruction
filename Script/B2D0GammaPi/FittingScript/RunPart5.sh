rm ./Run5RoOut/Part5*.txt
rm ./Run5RoFtOut/Part5*.txt
rm ./SiTimingRoOut/Part5*.txt
rm ./SiTimingRoFtOut/Part5*.txt
for((tCut=1; tCut<=8; tCut++))
do
for ((pt=100; pt<=600; pt+=10))
do
#root -l -q FitB2KstGamma_SiTimingRo.C\(3,$pt,4\)
root -l -q -b FitB2D0GammaPi_Run5Ro.C\($tCut,$pt,5\) 
root -l -q -b FitB2D0GammaPi_Run5Ro_ft.C\($tCut,$pt,5\) 
root -l -q -b FitB2D0GammaPi_SiTimingRo_ft.C\($tCut,$pt,5\) 
root -l -q -b FitB2D0GammaPi_SiTimingRo.C\($tCut,$pt,5\)
done
done