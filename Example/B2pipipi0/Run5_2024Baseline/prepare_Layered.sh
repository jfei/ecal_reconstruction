#!/bin/bash

#########################################
# Hybrid MC - Full ECAL simulations     #
#########################################
#### Small script to prepare full ECAL MC production 
#### This one is already prepared for Run5 configuration with flux
#### but can be adapted to any use case
####
#### This script prepares simulations for 3 different platforms
#### and 3 different simulation modalities. 
#### Possible platforms are:
#### 1) Locally on your PC  --> local
#### 2) On LXPLUS           --> lxplus
#### 3) On the grid         --> grid
#### Possible simulation modalities are
#### 1) Particle gun        --> pgun
#### 2) Flux                --> flux
#### So for example, if you want to run it locally a particle gun simulation, the sintax is
####
#### ./prepare.sh local pgun
####
#### Before running, you will need to modify some variables in this script to 
#### adapt to your configuration. In particular, read Section 1. to 
#### understand the meaning of the variables, then modify Sections 2. and 3.


#########################################
# ATTENTION!                            #
#########################################
# On lxplus, DO NOT run this from a EOS folder! copy the script to one of your AFS folders, modify and run.
#########################################


#########################################
# WHAT YOU NEED TO DO                   #
#########################################
#### In a nutshell, the MINIMAL things to do are:
#### 1) Define your platform and sim modality 
#### 2) Modify SIMPATH in section 2, to the full path where you downloaded the gitlab code
#### 2) Modify LISTCALIBRATIONS, and FLUX in Section 2. if needed (depending on your 
####    usecase, you might want to use the same files already written here)
#### 3) Modify BASEFOLDEROUT in Section 3 if needed. If you don't, the output will go in a subfolder 'out' of current
####    folder. While this would be ok for local runs and irrelevant for grid runs, it would be a problem for lxplus runs,
####    where you probably want to store the output files in some EOS folder and not in your AFS space 
#### 4) If the other flags are already adapted to your needs, save and run this script, as explained above
####    Otherwise modify also the other flags, as explained in Section 1
#### 5) Submit the jobs running the run script produced at the end of execution
#########################################


#########################################
# SOME OPERATIONS...                    #
#########################################
### Store cmd args
PLATFORM=$1
### Prepare some flags
PLATFORM_FLAG=''
### Some variable declaration
RECPATH=''
LISTCALIBRATIONS=''

### Check cmd args number
if [ "$#" -ne 1 ]; then
  echo "You need to provide 1 arguments:"
  echo "./prepare.sh [local|lxplus|grid]"
  exit 1
fi
### Check cmd args values 
## PLATFORM
if [ ${PLATFORM} != "local" ] && [ ${PLATFORM} != "lxplus" ] && [ ${PLATFORM} != "grid" ]&& [ ${PLATFORM} != "slurm" ]; then
  echo "Invalid argument ${PLATFORM} . Valid choices for argument 1 are [local|lxplus|grid]"
  exit 1
fi
### Set env variables, flags etc 
### for PLATFORM (MODALITY needs to be done later)
if [ ${PLATFORM} = "local" ]; then
  PLATFORM_FLAG='--local'
else 
  if [ ${PLATFORM} = "lxplus" ]; then
    PLATFORM_FLAG=''
    source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
  else 
    if [ ${PLATFORM} = "grid" ]; then
      PLATFORM_FLAG='--grid'
      source /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
    fi
    if [ ${PLATFORM} = "slurm" ]; then
      PLATFORM_FLAG='--slurm'
    fi
  fi
fi
#########################################


#########################################
# SECTION 1. VARIABLES EXPLAINED        #
#########################################
#
### Files and paths: 
#
# - SIMPATH                             Path to your spacal simulation git code folder
# - BASEFOLDEROUT                       Folder where the output will be saved
# - BASEFOLDERJOB                       Folder where the job scripts will be saved
# - CONFIG                              Main module/calorimeter config file
# - PULSECONFIG                         Pulse formation config file
# - BASEGPS                             Base GPS file for pgun simulations. Ignored for flux simulations
# - FLUX                                The flux file you want to use. Ignored in pgun simulations
#
### Lists. 
## Associate module types to their calibrations. These 2 lists need to be specified in the same order, 
## the values are just separated by a space (no commas), and the lists need to have the same number of entries
#
# - LISTTYPES                           List of module types in the simulation, with the same order as LISTCALIBRATIONS
# - LISTCALIBRATIONS                    List of optical calibration files - this is already set for you, although
#                                       clearly for the 'local' condition it will not make sense for your PC
#
### Input particles:
## Except EVENTS, which is a single number, the 3 lists below need to be specified in the same order, 
## the values are just separated by a space (no commas), and the lists need to have the same number of entries
#
# - LISTENERGY                          pgun: the list of energies in GeV (of the primary particle defined in BASEGPS)
#                                             that will be shot to the module/calorimeter. e.g. LISTENERGY='1 2 3 5 10 20 35 50 100'
#                                       flux: this is just the name of the output subfolder (subfolder of BASEFOLDEROUT) 
#                                             and IT HAS to be a number, but it could be any number, it won't matter (eg: LISTENERGY='5555')
# - EVENTS                              pgun: number of primaries shot, for each energy in the above list
#                                       flux: number of flux events taken from the flux file defined in FLUX. 
#                                             Leave -1 to take all flux events 
# - LISTEVENTS                          pgun: how many primaries (of each energy) are shot per job  
#                                       flux: Leave it equal to 1 for flux simulations
# - LISTQUEUE                           List job queues on lxplus, irrelevant in local and grid jobs
#                                       See LXPLUS/HTCondor documentation for a list of options
#
### Just to clarify the last 3 flags above, an example. For a pgun simulation, if you set:
#
# LISTENERGY='1 5 10'
# EVENTS='1000'
# LISTEVENTS='200 100 50'
# LISTQUEUE='longlunch tomorrow nextweek'
#
# The script will prepare (1000/200) = 5  jobs with primary particles shot at 1 GeV, on longlunch queue
#                         (1000/100) = 10 jobs with primary particles shot at 5 GeV, on tomorrow queue
#                         (1000/50)  = 20 jobs with primary particles shot at 10 GeV, on nextweek queue
#
### Option:
# - DISK                                Facultative: on LXPLUS, irrelevant in local and grid jobs
#                                       if you need some specific amount of disk 
#                                       memory during the simulation chain  (e.g. 50GB for the hybrid 
#                                       files with Upgrade II MB) you request it here. If the default HTCondor 
#                                       disk amount is enough (20 GB), you can remove it
#########################################


#########################################
# SECTION 2. SIM CODE, CALIB. AND FLUX  #
#########################################
### Modify what you need among these 3 cases, following guidelines above. Obviously you 
### Need to modify only flag relative to your current use case
if [ ${PLATFORM} = "local" ]; then
  RECPATH='/Users/fjl/workdir/ecal_reconstruction'
elif [ ${PLATFORM} = "lxplus" ]; then
  RECPATH='/afs/cern.ch/work/j/jfei/spacal-simulation/'
elif [ ${PLATFORM} = "grid" ]; then
  RECPATH='/home/fjl/ecal_reconstruction'
elif [ ${PLATFORM} = "slurm" ]; then
  RECPATH='/home/fjl/ecal_reconstruction'
fi
#########################################


#########################################
# SECTION 3. FILES, FOLDERS AND FLAGS   #
#########################################
BASEFOLDEROUT='./'
BASEFOLDERIN='./'
BASEFOLDERJOB='jobs'
LISTDIR='./'

LISTFILES='-1'
LISTQUEUE='testmatch' 
DISK='100GB'    
RecProgram='0' # 0 clustering 1 cell calibration 2 Clu correction 3. Transform format 4. Check resolution
ChargeSampleFilePrefix='ElectronSample_'
SimFileName='out' #If it's a full filename and not a prefix, you have to give an absolute path.
TrigFileName='OutTrigd_' #If it's a full filename and not a prefix, you have to give an absolute path.
FluxFileName='./FluxGammaFromB2pipipi0_SPACAL_6.root'
RecFileName=''
RecGeoFileName=$RECPATH'/Layout/Run5_2024baseline/SimGeo.root' #If it's a full filename and not a prefix, you have to give an absolute path.
TruthFileType='2' # Init Pv and Hit information 1 : From SimFile（out.root)  2:from flux file  3:from rec file (RecProgram==3)
GeoFilesType='2' # Init Geometry 1:from Sim（out.root) and TrigOut_ 2: from Rec Geometry File
DataFileType='1' # Init readout information 1:from TrigOut_ 2:3:from rec file (RecProgram==3)
InitTruthInfo='1' #Whether to initialise Truth information 
listTypes='1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16'
DoS1Cor='1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1'
listLayerNum='2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2'
Seed3DMethod='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0' # 0: 3*3 1: Cross 2: Fork
WindowShape='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0' # 0: 3*3 1: 2*2 2: cross 3: gradient
WindowShape4Layers='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0'
Seed2DCut='25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25 25' # 2*16
Seed3DCut='50' #pt/MeV
RatioECor='0'
LISTCALIBRATIONS=(
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type1_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type2_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type3_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type4_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type5_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type6_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type7_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type8_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type9_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type10_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type11_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type12_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type13_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type14_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type15_CaliFile.root"
    "$RECPATH/Layout/Run5_2024baseline/Layered/CorrectionFile2/Type16_CaliFile.root"
) 
CluFlow='2'
Candidates='Bd02PipPimPi0'
listSplitLayer='01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01'
tagCharge=1
#
RecFileName_FLAG=''
if [ "${RecFileName}" ]; then
RecFileName_FLAG="--RecFileName ${RecFileName}"
fi
SimFileName_FLAG=''
if [ -n "${SimFileName}" ]; then
  SimFileName_FLAG="--SimFileName ${SimFileName}"
fi
FluxFileName_FLAG=''
if [ -n "${FluxFileName}" ]; then
  FluxFileName_FLAG="--FluxFileName ${FluxFileName}"
fi
TrigFileName_FLAG=''
if [ -n "${TrigFileName}" ]; then
  TrigFileName_FLAG="--TrigFileName ${TrigFileName}"
fi
RecGeoFileName_FLAG=''
if [ -n "${RecGeoFileName}" ]; then
  RecGeoFileName_FLAG="--RecGeoFileName ${RecGeoFileName}"
fi

#########################################

#########################################
### Now we can set some flags...

if [ ${PLATFORM} = "lxplus" ]; then
  QUEUE_FLAG="--listQueue ${LISTQUEUE}"
  if [ "${DISK}" != '' ]; then
    DISK_FLAG="--requestDisk ${DISK}"
  fi
fi
#########################################

#########################################
# THE ACTUAL PREPARE COMMAND            #
#########################################

python3 ${RECPATH}/Script/PrepareWork/PrepareWorkFlow.py ${PLATFORM_FLAG} ${QUEUE_FLAG} ${DISK_FLAG} \
--listCalibrations ${LISTCALIBRATIONS[@]} \
--listFiles ${LISTFILES} \
--build ${RECPATH}/build \
--baseFolderOut ${BASEFOLDEROUT} \
--baseFolderIn ${BASEFOLDERIN} \
--listDir ${LISTDIR} \
--baseFolderJobs ${BASEFOLDERJOB} \
${RecFileName_FLAG} \
${SimFileName_FLAG} \
${FluxFileName_FLAG} \
${TrigFileName_FLAG} \
${RecGeoFileName_FLAG} \
--GeoFilesType ${GeoFilesType} \
--TruthFileType ${TruthFileType} \
--DataFileType ${DataFileType} \
--InitTruthInfo ${InitTruthInfo} \
--RecProgram ${RecProgram} \
--listTypes ${listTypes} \
--Seed3DCut ${Seed3DCut} \
--Seed2DCut ${Seed2DCut} \
--WindowShape ${WindowShape} \
--WindowShape4Layers ${WindowShape4Layers} \
--CluFlow ${CluFlow} \
--listSplitLayer ${listSplitLayer} \
--listLayerNum ${listLayerNum} \
--Candidates ${Candidates} \
--tagCharge ${tagCharge} \
--DoS1Cor ${DoS1Cor} \
--Seed3DMethod ${Seed3DMethod} \
--ChargeSampleFilePrefix ${ChargeSampleFilePrefix}


