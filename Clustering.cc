
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TRandom.h"
#include "TCanvas.h"
#include "TStyle.h"

#include <iostream>
#include <getopt.h>
#include "Mixture_Calo.hh"
#include "RecAlg.h"
#include <sstream>
#include "Mixture_Draw.hh"
#include "Parameters.hh"
#include "CreateTree.hh"
#include "FCell.hh"
#include "AnalysisAlg.hh"
#include "FLog.hh"
void usage()
{
    std::cout << "\t\t"
              << "[ [-c|--ConfigFile] <configuration file>                      ] " << std::endl
              << "\t\t"
              << "[ [-n|--InputFileNum] <input file num>     ] " << std::endl
              << "\t\t"
              << "[ [-v|--verbose] <1:info 2:debug 3:trace>     ] " << std::endl
              << "\t\t" << std::endl;
}

int main(int argc, char **argv)
{
    // check args
    if (argc < 2)
    {
        std::cout << argv[0] << std::endl;
        usage();
        return 1;
    }

    std::string configFileName;
    int inputFileNum = 0;
    int Verbose = 1;

    static struct option longOptions[] =
        {

            {"ConfigFile", required_argument, 0, 0},
            {"InputFileNum", required_argument, 0, 0},
            {"Verbose", required_argument, 0, 0},

            {NULL, 0, 0, 0}};

    while (1)
    {
        int optionIndex = 0;
        int c = getopt_long(argc, argv, "c:n:v:", longOptions, &optionIndex);
        if (c == -1)
        {
            break;
        }
        else if (c == 'c')
        {
            configFileName = (char *)optarg;
        }
        else if (c == 'n')
        {
            inputFileNum = atoi((char *)optarg);
        }
        else if (c == 'v')
        {
            Verbose = atoi((char *)optarg);
        }
        else if (c == 0 && optionIndex == 1)
        {
            configFileName = (char *)optarg;
        }
        else if (c == 0 && optionIndex == 2)
        {
            inputFileNum = atoi((char *)optarg);
        }
        else if (c == 0 && optionIndex == 3)
        {
            Verbose = atoi((char *)optarg);
        }
        else
        {
            std::cout << "Usage: " << argv[0] << std::endl;
            usage();
            return 1;
        }
    }

    // 设置日志格式，包括文件名和行号
    auto logger = spdlog::stdout_logger_mt(LOG_NAME);
    auto error_logger = spdlog::stderr_logger_mt(ERROR_NAME);
    auto traceAndDebug_logger = spdlog::stdout_logger_mt(DEBUG_TRACE_NAME);
    logger->set_level(spdlog::level::info);
    logger->set_pattern("[%H:%M:%S %D] [%^%l%$] %v");
    if (Verbose == 1)
    {
        traceAndDebug_logger->set_level(spdlog::level::info);
    }
    else if (Verbose == 2)
    {
        traceAndDebug_logger->set_level(spdlog::level::debug);
    }
    else if (Verbose > 2)
    {
        traceAndDebug_logger->set_level(spdlog::level::trace);
    }
    traceAndDebug_logger->set_pattern("[%H:%M:%S %D] [%^%l%$] [%@,%!] %v");
    error_logger->set_pattern("[%H:%M:%S %D] [%^%l%$] [%@,%!] %v");
    // error_logger->set_pattern("[%H:%M:%S %z] [%^%l%$] [file: %s] [line: %#] %v");

    if (configFileName == "")
    {
        error_logger->error("\n");
        error_logger->error("ERROR! You need to provide an configuration file name!");
        error_logger->error("See program usage below...");
        error_logger->error("\n");

        usage();
        return 1;
    }
    logger->info("Configuration file name : {}", configFileName);
    logger->info("Input file number : {}", inputFileNum);

    Parameters *parameters = new Parameters();
    Parameters::Instance()->ReadMainConfig(configFileName);

    if (Parameters::Instance()->module_config_list.size() > 0)
    {
        for (int iC = 0; iC < Parameters::Instance()->module_config_list.size(); iC++)
        {
            std::cout << "Module configuration file  '" << Parameters::Instance()->module_config_list[iC] << "'" << std::endl;
            Parameters::Instance()->ReadModuleConfig(Parameters::Instance()->module_config_list[iC],
                                                     Parameters::Instance()->ecal_type[iC]);
        }
    }

    TFile *Out_File = new TFile(".temp.root", "RECREATE");
    Mixture_Calo *my_ECAL = Mixture_Calo::get_instance();

    if (Parameters::Instance()->ProgramType == 1 || Parameters::Instance()->ProgramType == 2)
    {
        if (Parameters::Instance()->ProgramType == 1 && Parameters::Instance()->TruthFileType == 2)
        {
            spdlog::get(ERROR_NAME)->error("ProgramType = {}, and TruthFileType = {}, but flux files do not have EnDep info", Parameters::Instance()->ProgramType, Parameters::Instance()->TruthFileType);
        }

        Parameters::Instance()->InitTruthInfo = 1;
        logger->info("Save calibration information, so set TruthFileType to 1 ({}) and InitTruthInfo to 1 ({})", Parameters::Instance()->TruthFileType, Parameters::Instance()->InitTruthInfo);
    }

    logger->info("Set input and output file name");
    my_ECAL->InitFileName(inputFileNum);

    logger->info("Initializing Calo");
    my_ECAL->InitCalo();
    

    logger->info("Program : {}", my_ECAL->ProgramType);
    logger->info("CluFlow : {}", my_ECAL->CluFlow);
    if (my_ECAL->ProgramType == 0)
    {

        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");
        Parameters::Instance()->SaveCluster3D = 1; // save the para to the output file;
        Parameters::Instance()->SavePrimaryInfo = 1; // save the para to the output file;
        if (Parameters::Instance()->ConstructGamma == 1)
        {
            Parameters::Instance()->SavePrimaryInfo = 1;
            Parameters::Instance()->SaveGamma = 1;
        }
        if (Parameters::Instance()->ConstructPi0 == 1)
        {
            Parameters::Instance()->SavePrimaryInfo = 1;
            Parameters::Instance()->ConstructGamma = 1;
            Parameters::Instance()->SavePi0 = 1;
        }
        if (Parameters::Instance()->ConstructElectron == 1)
        {
            Parameters::Instance()->SavePrimaryInfo = 1;
            Parameters::Instance()->SaveElectron = 1;
        }
        if (Parameters::Instance()->Bd02PipPimPi0)
        {
            Parameters::Instance()->ConstructGamma = 1;
            Parameters::Instance()->ConstructPi0 = 1;
        }
        if (Parameters::Instance()->B2KstGamma || Parameters::Instance()->B2D0GammaPi)
        {
            Parameters::Instance()->ConstructGamma = 1;
        }
    }
    if (my_ECAL->ProgramType == 1)
    {
        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");
        Parameters::Instance()->SavePrimaryInfo = 1; // save the para to the output file;
        Parameters::Instance()->SaveRawInfo = 1;     // save the para to the output file;
    }
    else if (my_ECAL->ProgramType == 2)
    {
        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");

        Parameters::Instance()->SavePrimaryInfo = 1; // save the para to the output file;
        Parameters::Instance()->SaveCluster3D = 1;   // save the para to the output file;
        if (my_ECAL->CluFlow == 1 || my_ECAL->CluFlow == 2)
            Parameters::Instance()->SaveCluster2D = 1; // save the para to the output file;
        Parameters::Instance()->SaveRawInfo = 0;       // save the para to the output file;
    }
    else if (my_ECAL->ProgramType == 3)
    {
        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");

        Parameters::Instance()->SavePrimaryInfo = 1; // save the para to the output file;
        Parameters::Instance()->SaveCellInfo = 1;    // save the para to the output file;
        Parameters::Instance()->SaveGeometry = 1;    // save the para to the output file;
        Parameters::Instance()->SaveRawInfo = 1;     // save the para to the output file;
    }
    else if (my_ECAL->ProgramType == 4)
    {
        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");

        Parameters::Instance()->SavePrimaryInfo = 1; // save the para to the output file;
        Parameters::Instance()->SaveCluster3D = 1;   // save the para to the output file;
        if (my_ECAL->CluFlow == 1 || my_ECAL->CluFlow == 2)
            Parameters::Instance()->SaveCluster2D = 1; // save the para to the output file;
        Parameters::Instance()->SaveRawInfo = 0;       // save the para to the output file;
    }
    else
    {
        Out_File = new TFile(my_ECAL->OutFileName.c_str(), "RECREATE");
    }
    Out_File->cd();
    CreateTree *mytree = new CreateTree("tree");
    TFile *CandidateFile = new TFile(".tempCandi.root", "RECREATE");
    if (my_ECAL->ProgramType == 0)
    {
        logger->info("Clustering ................");
        my_ECAL->Clustering();
        //std::cout << "my_ECAL->tagCharge " << my_ECAL->tagCharge << std::endl;
        if (my_ECAL->tagCharge)
        {
            logger->info("Eval Min Dist 2 Charge...");
            my_ECAL->EvalDist2Charge();
        }
        if (Parameters::Instance()->ConstructGamma == 1)
        {
            logger->info("Reconstruct Gamma...");

            my_ECAL->RecGamma();
        }
        if (Parameters::Instance()->ConstructPi0 == 1)
        {
            logger->info("Reconstruct Resolved Pi0...");
            my_ECAL->RecResolvedPi0();
            logger->info("Reconstruct Merged Pi0...");
            my_ECAL->RecMergedPi0();
        }
        if (Parameters::Instance()->ConstructElectron == 1)
        {
            logger->info("Reconstruct Electron...");
            my_ECAL->RecElectron();
        }
        if (Parameters::Instance()->InitTruthInfo == 1)
        {
            logger->info("Matching signal");
            my_ECAL->Matching();
        }
        if (Parameters::Instance()->B2KstGamma == 1)
        {

            logger->info("Construct candidate for B2KstGamma");
            std::string Bd02PipPimPi0Name = Parameters::Instance()->OutputPath + "/B2KstGamma_" + std::to_string(my_ECAL->IntputNum) + ".root";
            CandidateFile = new TFile(Bd02PipPimPi0Name.c_str(), "recreate");
            ConstructB2KstGamma(my_ECAL, CandidateFile);

            // CandidateFile->Close();
        }
        if (Parameters::Instance()->B2D0GammaPi == 1)
        {
            logger->info("Construct candidate for B2D0GammaPi");
            std::string Bd02PipPimPi0Name = Parameters::Instance()->OutputPath + "/B2D0GammaPi_" + std::to_string(my_ECAL->IntputNum) + ".root";
            CandidateFile = new TFile(Bd02PipPimPi0Name.c_str(), "recreate");
            ConstructB2D0Gammapi(my_ECAL, CandidateFile);

            // CandidateFile->Close();
        }
        if (Parameters::Instance()->Bd02PipPimPi0 == 1)
        {

            logger->info("Construct candidate for Bd02PipPimPi0");
            std::string Bd02PipPimPi0Name = Parameters::Instance()->OutputPath + "/Bd02PipPimPi0_" + std::to_string(my_ECAL->IntputNum) + ".root";
            CandidateFile = new TFile(Bd02PipPimPi0Name.c_str(), "recreate");
            ConstructBd02PipPimPi0(my_ECAL, CandidateFile);

            // CandidateFile->Close();
        }

        my_ECAL->WriteTree();
    }
    else if (my_ECAL->ProgramType == 1)
    {
        logger->info("Doing calibration step1................");
        my_ECAL->Calibration();
    }
    else if (my_ECAL->ProgramType == 2)
    {
        logger->info("Doing calibration step2................");
        my_ECAL->Calibration2();
    }
    else if (my_ECAL->ProgramType == 3)
    {
        logger->info("Write All things to out tree");
        my_ECAL->WriteTree();
    }
    else if (my_ECAL->ProgramType == 4)
    {
        logger->info("Clustering and check pgun performance ................");
        my_ECAL->Clustering();
        my_ECAL->WriteTree();
    }
    else
    {
        logger->info("Write All things to out tree");
        my_ECAL->WriteTree();
    }

    logger->info("Write Tree to output file");
    CreateTree::Instance()->Write(Out_File);

    Out_File->Close();
    CandidateFile->Close();
    logger->info("Everything done, bye.");

    return 0;

    // gStyle->SetOptStat(kFALSE);
    // std::vector<float> v_granularity = {0.1, 0.15, 0.2, 0.5, 1, 5, 10};

    // my_ECAL->module_container[0]->WritePartcileGun2("/home/fjl/Downloads/gamma_10.0GeV/10.0GeV/out0.root");
    // my_ECAL->module_container[0]->SameDiff(v_granularity, (float)-3.25);
    // my_ECAL->module_container[0]->noSameDiff(v_granularity, (float)-3.25);

    //  Draw_PosRes(my_ECAL, v_granularity);
}

// g++ Clustering.cc ./src/Mixture_Draw.cc ./src/RecAlg.C ./src/Mixture_Calo.cc $(root-config --glibs --cflags --libs) -o app -I ./include

/////////////////////////////////////////////////////////////////////////////
//                     test code
// ./app -i /home/fjl/workdir/geant4/spacal-simulation/SiTimingGunOut/1.0GeV/out0.root -t /home/fjl/workdir/geant4/spacal-simulation/SiTimingGunOut/1.0GeV/OutTrigd_0.root