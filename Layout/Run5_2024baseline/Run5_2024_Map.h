#ifndef _RegionMap_h

#define _RegionMap_h
#include "map"

class Run5_2024_Config
{
public:
    static std::map<int, int> Part_LayerNum;
    static std::map<int, int> Type_LayerNum;
    static std::map<int, float> Part_CellSize;
    static std::map<int, std::vector<int>> Part2Type;
    static std::map<int, int> Type2Part;
    static std::map<int, std::vector<int>> Type2Id;

    static std::map<int, std::vector<int>> Part2Id;

    static std::map<int, int> Id2Type;

    static std::vector<float> PartModulePos;

    static std::map<int, std::vector<Double_t>> EBin; // ECor step1

    static std::map<int, std::vector<Double_t>> EBin2; // ECor step2

    static std::map<int, std::vector<Double_t>> EBinFront;

    static std::map<int, std::vector<Double_t>> LEBin;

    static std::map<int, std::vector<Double_t>> TEBin;

    static std::map<int, std::pair<float, float>> DeltaERange_Cor1;

    static std::map<int, std::pair<float, float>> DeltaERange_Cor2;

    static std::map<int, std::pair<float, float>> DeltaTRange;

    static std::map<int, std::pair<float, float>> ERatioRange;

    static std::pair<int, Double_t *>
    GetEbin(int Part)
    {
        int nbin = EBin.at(Part).size() - 1;
        Double_t *bins = new Double_t[EBin.at(Part).size()];
        for (int i = 0; i < EBin.at(Part).size(); i++)
        {
            bins[i] = EBin.at(Part).at(i);
        }
        return {nbin, bins};
    }

    static std::pair<int, Double_t *> GetEbin2(int Part)
    {
        int nbin = EBin2.at(Part).size() - 1;
        Double_t *bins = new Double_t[EBin2.at(Part).size()];
        for (int i = 0; i < EBin2.at(Part).size(); i++)
        {
            bins[i] = EBin2.at(Part).at(i);
        }
        return {nbin, bins};
    }

    static std::pair<int, Double_t *> GetEbinFront(int Part)
    {
        int nbin = EBinFront.at(Part).size() - 1;
        Double_t *bins = new Double_t[EBinFront.at(Part).size()];
        for (int i = 0; i < EBinFront.at(Part).size(); i++)
        {
            bins[i] = EBinFront.at(Part).at(i);
        }
        return {nbin, bins};
    }

    static std::pair<int, Double_t *> GetLEBin(int Type)
    {
        int nbin = LEBin.at(Type).size() - 1;
        Double_t *bins = new Double_t[LEBin.at(Type).size()];
        for (int i = 0; i < LEBin.at(Type).size(); i++)
        {
            bins[i] = LEBin.at(Type).at(i);
        }
        return {nbin, bins};
    }

    static std::pair<int, Double_t *> GetTEbin(int Part)
    {
        int nbin = TEBin.at(Part).size() - 1;
        Double_t *bins = new Double_t[TEBin.at(Part).size()];
        for (int i = 0; i < TEBin.at(Part).size(); i++)
        {
            bins[i] = TEBin.at(Part).at(i);
        }
        return {nbin, bins};
    }
    static float GetMinDeltaE_Cor1(int Part)
    {

        return DeltaERange_Cor1.at(Part).first;
    }

    static float GetMaxDeltaE_Cor1(int Part)
    {

        return DeltaERange_Cor1.at(Part).second;
    }

    static float GetMinDeltaE_Cor2(int Part)
    {

        return DeltaERange_Cor2.at(Part).first;
    }

    static float GetMaxDeltaE_Cor2(int Part)
    {

        return DeltaERange_Cor2.at(Part).second;
    }

    static float GetMinDeltaTRange(int Part)
    {

        return DeltaTRange.at(Part).first;
    }

    static float GetMaxDeltaTRange(int Part)
    {

        return DeltaTRange.at(Part).second;
    }

    static float GetModulePos(int Part)
    {
        return PartModulePos.at(Part - 1);
    }

    static float GetMinERatio(int Part)
    {

        return ERatioRange.at(Part).first;
    }

    static float GetMaxERatio(int Part)
    {

        return ERatioRange.at(Part).second;
    }
};

std::map<int, std::pair<float, float>> Run5_2024_Config::DeltaERange_Cor1 = {{1, {-0.15, 0.3}},
                                                                             {2, {-0.15, 0.4}},
                                                                             {3, {-0.1, 0.3}},
                                                                             {4, {-0.05, 0.3}},
                                                                             {5, {-0.05, 0.25}},
                                                                             {6, {-0.05, 0.25}},
                                                                             {7, {0., 0.3}}};

std::map<int, std::pair<float, float>> Run5_2024_Config::DeltaERange_Cor2 = {{1, {-0.15, 0.25}},
                                                                             {2, {-0.15, 0.25}},
                                                                             {3, {-0.15, 0.25}},
                                                                             {4, {-0.15, 0.25}},
                                                                             {5, {-0.15, 0.25}},
                                                                             {6, {-0.15, 0.25}},
                                                                             {7, {-0.15, 0.25}}};

std::map<int, std::pair<float, float>> Run5_2024_Config::DeltaTRange = {{1, {43.5, 45.5}},
                                                                        {2, {43, 45.5}},
                                                                        {3, {43.3, 45.5}},
                                                                        {4, {43, 45.2}},
                                                                        {5, {42, 43.8}},
                                                                        {6, {42.1, 43.5}},
                                                                        {7, {41.8, 42.8}}};

std::map<int, std::pair<float, float>> Run5_2024_Config::ERatioRange = {{1, {-0.5, 2}},
                                                                        {2, {-0.5, 2}},
                                                                        {3, {-0.5, 2}},
                                                                        {4, {-0.5, 2}},
                                                                        {5, {-0.5, 6}},
                                                                        {6, {-0.5, 6}},
                                                                        {7, {-0.5, 2.5}}};

std::map<int, std::vector<Double_t>> Run5_2024_Config::EBin = {
    {1, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {2, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {3, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {4, {0, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {5, {0, 10, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 210, 250}},
    {6, {0, 10, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 210, 250}},
    {7, {0, 15, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 210, 250, 300}}};
// EBin  1:Part1 2 3 4  2:Part5 6 3:Part7
std::map<int, std::vector<Double_t>> Run5_2024_Config::EBin2 = {
    {1, {0, 0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1, 1.5, 2, 3}},
    {2, {0, 0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1, 1.5, 2, 3}},
    {3, {0, 0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1, 1.5, 2, 3}},
    {4, {0, 0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1, 1.5, 2.5}},
    {5, {0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.6, 2, 2.5, 3, 4, 5, 8}},
    {6, {0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 2, 2.5, 3, 4, 6}},
    {7, {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 1.2, 1.4, 1.6, 2}}};

std::map<int, std::vector<Double_t>> Run5_2024_Config::EBinFront = {
    {1, {0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25, 30}},
    {2, {0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25, 30}},
    {3, {0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25, 30}},
    {4, {0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 25, 30}},
    {5, {5, 7.5, 10, 20, 25, 30, 40, 50, 60, 80, 120}},
    {6, {5, 7.5, 10, 20, 25, 30, 40, 50, 60, 80, 120}},
    {7, {10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 120}}};

std::map<int, std::vector<Double_t>> Run5_2024_Config::LEBin = {
    {1, {0, 10, 20, 30, 40, 60, 80, 100, 120}},
    {2, {0, 10, 20, 30, 40, 60, 80, 100, 120}},
    {3, {0, 10, 20, 30, 40, 60, 80, 100, 120}},
    {4, {0, 10, 20, 30, 40, 60, 80, 100, 120}},
    {5, {0, 10, 20, 30, 40, 50, 60, 80, 100, 140}},
    {6, {0, 10, 20, 30, 40, 50, 60, 80, 100, 140}},
    {7, {0, 10, 20, 30, 40, 50, 60, 80, 100, 140}},
    {8, {0, 10, 20, 30, 40, 50, 60, 80, 100, 140}},
    {9, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {10, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {11, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {12, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {13, {0, 20, 40, 60, 80, 100, 125, 150, 180, 210, 250, 300}},
    {14, {0, 20, 40, 60, 80, 100, 125, 150, 180, 210, 250, 300}},
    {15, {0, 20, 40, 60, 80, 100, 125, 150, 180, 210, 250, 300}},
    {16, {0, 20, 40, 60, 80, 100, 125, 150, 180, 210, 250, 300}},
}; // 1:1 2 3 4   2: 5 6 7 8  3:9 10 11 12  4:13 14 15 16

std::map<int, std::vector<Double_t>> Run5_2024_Config::TEBin = {
    {1, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {2, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {3, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {4, {0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100}},
    {5, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {6, {0, 20, 30, 40, 50, 60, 80, 100, 125, 150, 180, 250}},
    {7, {0, 20, 40, 60, 80, 100, 125, 150, 180, 210, 250, 300}},
};

std::vector<float> Run5_2024_Config::PartModulePos = {0, 0, 0, 0, -65.91220, -65.91220, -140.7070};

std::map<int, int> Run5_2024_Config::Part_LayerNum = {{1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {7, 2}};
std::map<int, int> Run5_2024_Config::Type_LayerNum = {
    {1, 2},
    {2, 2},
    {3, 2},
    {4, 2},
    {5, 2},
    {6, 2},
    {7, 2},
    {8, 2},
    {9, 2},
    {10, 2},
    {11, 2},
    {12, 2},
    {13, 2},
    {14, 2},
    {15, 2},
    {16, 2},
};
std::map<int, std::vector<int>> Run5_2024_Config::Part2Type = {{1, {1}}, {2, {2}}, {3, {3}}, {4, {4}}, {5, {5, 6, 7, 8}}, {6, {9, 10, 11, 12}}, {7, {13, 14, 15, 16}}};
std::map<int, int> Run5_2024_Config::Type2Part = {
    {1, 1},
    {2, 2},
    {3, 3},
    {4, 4},
    {5, 5},
    {6, 5},
    {7, 5},
    {8, 5},
    {9, 6},
    {10, 6},
    {11, 6},
    {12, 6},
    {13, 7},
    {14, 7},
    {15, 7},
    {16, 7},
};
std::map<int, std::vector<int>> Run5_2024_Config::Type2Id = {
    {1, {1}},
    {2, {2}},
    {3, {3}},
    {4, {4}},
    {5, {5, 7, 9}},
    {6, {6, 16, 15}},
    {7, {8, 10, 11}},
    {8, {12, 13, 14}},
    {9, {17, 19, 21}},
    {10, {18, 28, 27}},
    {11, {20, 22, 23}},
    {12, {24, 25, 26}},
    {13, {29, 31, 33}},
    {14, {30, 40, 39}},
    {15, {32, 34, 35}},
    {16, {36, 37, 38}},
};

std::map<int, std::vector<int>> Run5_2024_Config::Part2Id = {
    {1, {1}},
    {2, {2}},
    {3, {3}},
    {4, {4}},
    {5, {5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}},
    {6, {17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28}},
    {7, {29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40}},
};

std::map<int, int> Run5_2024_Config::Id2Type = {
    {1, 1},
    {2, 2},
    {3, 3},
    {4, 4},
    {5, 5},
    {6, 6},
    {7, 5},
    {8, 7},
    {9, 5},
    {10, 7},
    {11, 7},
    {12, 8},
    {13, 8},
    {14, 8},
    {15, 6},
    {16, 6},
    {17, 9},
    {18, 10},
    {19, 9},
    {20, 11},
    {21, 9},
    {22, 11},
    {23, 11},
    {24, 12},
    {25, 12},
    {37, 16},
    {38, 16},
    {39, 14},
    {40, 14},
};

std::map<int, float> Run5_2024_Config::Part_CellSize = {{1, 120}, {2, 60}, {3, 60}, {4, 40}, {5, 40}, {6, 30}, {7, 15}};

#endif